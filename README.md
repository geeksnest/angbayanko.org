# README #

This website if for Ang Bayan Ko Foundation being owned by Mr. Dick Yanson

### What is this repository for? ###

* This site is developed under Phalconphp framework by Efren Bautista Jr
* Beta Version 0.1
* https://ebautistajr@bitbucket.org/ebautistajr/abksitephalcon.git

### How do I get set up? ###

* Install Phalconphp framework on your server, Phalcon does not run on Shared Servers.
* Configuration is located at app/config/config.php
* Dependencies are phalconphp .dll

### Contribution guidelines ###

* Code review

* Add more element in the CMS such as users and others, and current time
* In the program page menu add sorting of pages in the left
* Put add program page on the side with the Page menu Sorting
* Add image uploader for each pages, display its link and add delete ajax on the left side.
* Add Page Slug in the Database
* Save Page Contents
* Preview Button for Page
* When Login add name of the person login on top
* Fix BreadCrumbs
* Fix Post Layout
* Finish Add Edit and Delete Tonight.
* Integrate Tommorow
* Prepare Forums Sunday and Deploy