CREATE TABLE `tblnewsletter` (
  `letterID` int(11) NOT NULL AUTO_INCREMENT,
  `letterSubject` varchar(255) NOT NULL,
  `letterBody` varchar(255) NOT NULL,
  `dateCreated` int(11) NOT NULL,
  PRIMARY KEY (`letterID`)
);