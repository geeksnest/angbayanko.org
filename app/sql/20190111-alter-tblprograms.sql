ALTER TABLE `tblprograms` 
ADD COLUMN `specialPage` INT(11) NULL DEFAULT '0' AFTER `programBanner`;

ALTER TABLE `dbabk`.`tblprograms`
ADD COLUMN `status` VARCHAR(45) NOT NULL DEFAULT '0' AFTER `specialPage`;

ALTER TABLE `dbabk`.`tblprograms` 
ADD COLUMN `programColor` VARCHAR(45) NOT NULL DEFAULT '#000000' AFTER `status`;

ALTER TABLE `dbabk`.`tblprograms` 
ADD COLUMN `programIcon` VARCHAR(200) NOT NULL DEFAULT 'linux.png' AFTER `programColor`;


