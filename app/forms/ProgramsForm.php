<?php

use Phalcon\Forms\Form,
Phalcon\Forms\Element\Text,
Phalcon\Forms\Element\Hidden,
Phalcon\Forms\Element\Password,
Phalcon\Forms\Element\Submit,
Phalcon\Forms\Element\Check,
Phalcon\Forms\Element\File,
Phalcon\Validation\Validator\PresenceOf,
Phalcon\Validation\Validator\Email,
Phalcon\Validation\Validator\Identical,
Phalcon\Validation\Validator\StringLength,
Phalcon\Validation\Validator\Confirmation;

class ProgramsForm extends Form
{
    public function initialize($entity = null, $options = null)
    {

        //Title
        $title = new Text('title', array('class' => 'form-control', 'placeholder' => 'Title', 'id' => 'program-title'));
        $title->setLabel('Title');
        $title->addFilter('trim');
        $title->addValidators(array(
            new PresenceOf(array(
                'message' => 'Title is required'
                ))
            ));
        $this->add($title);

        //Tool Tip
        $tooltip = new Text('tooltip', array('class' => 'form-control', 'placeholder' => 'Tool Tip'));
        $tooltip->setLabel('Tool Tip');
        $tooltip->addFilter('trim');
        $tooltip->addValidators(array(
            new PresenceOf(array(
                'message' => 'Tooltip is required.'
                ))
            ));
        $this->add($tooltip);     

        //Program Tag Line
        $tagline = new Text('tagline', array('class' => 'form-control', 'placeholder' => 'Tag Line'));
        $tagline->setLabel('Tag Line');
        $tagline->addFilter('trim');
        $tagline->addValidators(array(
            new PresenceOf(array(
                'message' => 'Tag Line is required.'
                ))
            ));
        $this->add($tagline);     

        //Program URL
        $programurl = new Text('programurl', array('class' => 'form-control program-url', 'placeholder' => 'Program URL','readonly'=>'true'));
        $programurl->setLabel('Program URL');
        $programurl->addFilter('trim');
        $programurl->addValidators(array(
            new PresenceOf(array(
                'message' => 'Program URL is required.'
                ))
            ));
        $this->add($programurl); 
        //image upload
        $imageupload = new file('files[]',array('id'=>'fileupload','multiple accept'=>'image/*'));
        $imageupload->setLabel('files[]');
        $imageupload->addFilter('trim');
        $imageupload->addValidators(array(
            new PresenceOf(array(
                'message' => 'Image is required.'
                ))
            ));
        $this->add($imageupload);
        // <input type='hidden' name='program_picture[]' value='" + file.name + "' > 
        // $hiddensimage = new hidden('program_picture[]',array('value'=> file.name));
        // $hiddensimage->setLabel('program_picture[]');
        // $hiddensimage->addFilter('trim');
        // $hiddensimage->addValidators(array(
        //     new PresenceOf(array(
        //         'message' => 'Image hidden is required.'
        //         ))
        //     ));
        // $this->add($hiddensimage);
        //CSRF
        $csrf = new Hidden('csrf');

      /*  $csrf->addValidator(new Identical(array(
            'value' => $this->security->getSessionToken(),
            'message' => 'CSRF validation failed'
            )));
        */
        $csrf->addValidator(new Identical(array(
            $this->security->checkToken() => 1,
            'message' => 'CSRF-token validation failed'
        )));
        $this->add($csrf);    

    }
    /**
     * Prints messages for a specific element
     */
    public function messages($name)
    {
        if ($this->hasMessagesFor($name)) {
            foreach ($this->getMessagesFor($name) as $message) {
                $this->flash->error($message);
            }
        }
    }    
}