<?php

use Phalcon\Forms\Form,
Phalcon\Forms\Element\TextArea,
Phalcon\Forms\Element\Text,
Phalcon\Forms\Element\Hidden,
Phalcon\Forms\Element\Password,
Phalcon\Forms\Element\Submit,
Phalcon\Forms\Element\Check,
Phalcon\Validation\Validator\PresenceOf,
Phalcon\Validation\Validator\Email,
Phalcon\Validation\Validator\Identical,
Phalcon\Validation\Validator\StringLength,
Phalcon\Validation\Validator\Confirmation;

class CreatepostForm extends Form
{
    public function initialize($entity = null, $options = null)
    {
        // In edition the id is hidden
        if (isset($options['edit']) && $options['edit']) {
            $hposttitle = new Hidden('hposttitle');
            $this->add($husername);
            $hemail = new Hidden('hpostid');
            $this->add($hpostid);            
        } 

        //Title
        $title = new Text('post_title', array('class' => 'form-control col-lg-8', 'placeholder' => 'Enter title'));
        $title->setLabel('title');
        $title->addFilter('trim');
        $title->addValidators(array(
            new PresenceOf(array(
                'message' => 'Title is required'
                ))
            ));
        $this->add($title);

//For Activity AbkRescue Val in Edit

        $title = new Text('page_title', array('class' => 'form-control col-lg-8', 'placeholder' => 'Enter title'));
        $title->setLabel('title');
        $title->addFilter('trim');
        $title->addValidators(array(
            new PresenceOf(array(
                'message' => 'Title is required'
                ))
            ));
        $this->add($title);

        $keyword = new Text('page_keyword', array('class' => 'form-control col-lg-8', 'placeholder' => 'Enter Keyword'));
        $keyword->setLabel('Keyword');
        $keyword->addFilter('trim');
        $keyword->addValidators(array(
            new PresenceOf(array(
                'message' => 'Keyword is required'
                ))
            ));
        $this->add($keyword);


        $description = new Text('page_description', array('class' => 'form-control col-lg-8', 'placeholder' => 'Enter Description of Activity'));
        $description->setLabel('description');
        $description->addFilter('trim');
        $description->addValidators(array(
            new PresenceOf(array(
                'message' => 'Activity description is required'
                ))
            ));
        $this->add($description);


$content = new TextArea('page_content', array('class' => 'form-control programPageText','id'=>'programPageText'));
        $content->setLabel('Content');
        $content->addFilter('trim');
        $content->addValidators(array(
            new PresenceOf(array(
                'message' => 'Content is required'
                ))
            ));
        $this->add($content);



    $hah= Tblprograms::find();

    foreach ($hah as $key) {
        
        $check=new Check('chkProg'.$key->programID,array('name'=>'chkProg'.$key->programID,'value' => $key->programID));
        $this->add($check);

    }




//End For Activity AbkRescue Val in Edit

        //Title
        $content = new TextArea('post_content', array('class' => 'form-control newsPostContent','id'=>'newsPostContent'));
        $content->setLabel('Content');
        $content->addFilter('trim');
        $content->addValidators(array(
            new PresenceOf(array(
                'message' => 'Content is required'
                ))
            ));
        $this->add($content);


  

        //Title
        $content = new TextArea('blog_content', array('class' => 'form-control newsPostContent','id'=>'newsPostContent'));
        $content->setLabel('Content');
        $content->addFilter('trim');
        $content->addValidators(array(
            new PresenceOf(array(
                'message' => 'Content is required'
                ))
            ));
        $this->add($content);


        //PageBanner
        $fi = new Text('pageBannerUrl', array('class' => 'form-control'));
        $fi->setLabel('Featured Image');
        $fi->addFilter('trim');
        $this->add($fi);

        
        //CSRF
        $csrf = new Hidden('csrf');

        // $csrf->addValidator(new Identical(array(
        //     'value' => $this->serialize(value)curity->getSessionToken(),
        //     'message' => 'CSRF validation failed'
        //     )));
        $csrf->addValidator(new Identical(array(
            $this->security->checkToken() => 1,
            'message' => 'CSRF-token validation failed'
        )));

        $this->add($csrf);        

    }
    /**
     * Prints messages for a specific element
     */
    public function messages($name)
    {
        if ($this->hasMessagesFor($name)) {
            foreach ($this->getMessagesFor($name) as $message) {
                $this->flash->error($message);
            }
        }
    }    
}