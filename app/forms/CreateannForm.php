<?php

use Phalcon\Forms\Form,
Phalcon\Forms\Element\TextArea,
Phalcon\Forms\Element\Text,
Phalcon\Forms\Element\Hidden,
Phalcon\Forms\Element\Password,
Phalcon\Forms\Element\Submit,
Phalcon\Forms\Element\Check,
Phalcon\Validation\Validator\PresenceOf,
Phalcon\Validation\Validator\Email,
Phalcon\Validation\Validator\Identical,
Phalcon\Validation\Validator\StringLength,
Phalcon\Validation\Validator\Confirmation;

class CreateannForm extends Form
{
    public function initialize($entity = null, $options = null)
    {
        // In edition the id is hidden
        if (isset($options['edit']) && $options['edit']) {
            $hposttitle = new Hidden('hposttitle');
            $this->add($husername);
            $hemail = new Hidden('hpostid');
            $this->add($hpostid);
        }

        //Title
        $title = new Text('ann_title', array('class' => 'form-control col-lg-8', 'placeholder' => 'Enter Title'));

        $title->setLabel('ann_title');
        $title->addFilter('trim');
        $title->addValidators(array(
            new PresenceOf(array(
                'message' => 'Title is required'
                )),
            ));

        $this->add($title);

        //Content
        $content = new TextArea('ann_content', array('class' => 'form-control newsPostContent'));
        $content->setLabel('Content');
        $content->addFilter('trim');
        $content->addValidators(array(
            new PresenceOf(array(
                'message' => 'Content is required'
                ))
            ));
        $this->add($content);
        //Content Dashboard
        $contentDash = new TextArea('ann_contentDash', array('class' => 'form-control' , 'placeholder' => 'Enter Content'));
        $contentDash->setLabel('Content');
        $contentDash->addFilter('trim');
        $contentDash->addValidators(array(
            new PresenceOf(array(
                'message' => 'Content is required'
                ))
            ));
        $this->add($contentDash);
        //annStart
        $annStart = new Text('ann_start', array('readonly'=>'readonly', 'class' => 'form-control form-control dtpicker', 'data-format'=>'yyyy-MM-dd'));
        $annStart->setLabel('Start date');
        $annStart->addValidators(array(
            new PresenceOf(array(
                'message' => 'Start date is required'
            ))
        ));
        $this->add($annStart);
        //annStartDash
        $annStartDash = new Text('ann_start_dash', array('readonly'=>'readonly', 'class' => 'form-control', 'data-format'=>'yyyy-MM-dd'));
        $annStartDash->setLabel('Start date');
        $annStartDash->addValidators(array(
            new PresenceOf(array(
                'message' => 'Start date is required'
            ))
        ));
        $this->add($annStartDash);
        //annEndDash
        $annEndDash = new Text('ann_end_dash', array('readonly'=>'readonly', 'class' => 'form-control', 'data-format'=>'yyyy-MM-dd'));
        $annEndDash->setLabel('End date');
        $annEndDash->addValidators(array(
            new PresenceOf(array(
                'message' => 'End date is required'
            ))
        ));
        $this->add($annEndDash);
        //annEnd
        $annEnd = new Text('ann_end', array('readonly'=>'readonly', 'class' => 'form-control form-control dtpicker', 'data-format'=>'yyyy-MM-dd'));
        $annEnd->setLabel('End date');
        $annEnd->addValidators(array(
            new PresenceOf(array(
                'message' => 'End date is required'
            ))
        ));
        $this->add($annEnd);

        //CSRF
        $csrf = new Hidden('csrf');

       /* $csrf->addValidator(new Identical(array(
            'value' => $this->security->getSessionToken(),
            'message' => 'CSRF validation failed'
            )));
*/
        $csrf->addValidator(new Identical(array(
            $this->security->checkToken() => 1,
            'message' => 'CSRF-token validation failed'
        )));
        $this->add($csrf);

    }
    /**
     * Prints messages for a specific element
     */
    public function messages($name)
    {
        if ($this->hasMessagesFor($name)) {
            foreach ($this->getMessagesFor($name) as $message) {
                $this->flash->error($message);
            }
        }
    }
}
