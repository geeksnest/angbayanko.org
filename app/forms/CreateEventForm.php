<?php

use Phalcon\Forms\Form,
Phalcon\Forms\Element\Text,
Phalcon\Forms\Element\TextArea,
Phalcon\Forms\Element\Date,
Phalcon\Forms\Element\Hidden,
Phalcon\Forms\Element\Password,
Phalcon\Forms\Element\Submit,
Phalcon\Forms\Element\Check,
Phalcon\Validation\Validator\PresenceOf,
Phalcon\Validation\Validator\Email,
Phalcon\Validation\Validator\Identical,
Phalcon\Validation\Validator\StringLength as StringLength,
Phalcon\Validation\Validator\Regex,
Phalcon\Validation\Validator\Confirmation;

class CreateEventForm extends Form
{
    public function initialize($entity = null, $options = null)
    {
        // In edition the id is hidden
        if (isset($options['edit']) && $options['edit']) {
            $husername = new Hidden('huserName');
            $this->add($husername);
            $hemail = new Hidden('huserEmail');
            $this->add($hemail);
        }

        //User Account
        $name = new Text('eventname', array('class' => 'form-control', 'placeholder' => 'Eventname'));
        $name->setLabel('Event name');
        $name->addValidators(array(
            new PresenceOf(array(

                'message' => 'Eventname is required '
                )),
            new StringLength(array(
                'min' => 2,
                'messageMinimum' => 'Eventname should have at least 2 minimum characters'
                 )),
            ));
        $this->add($name);

        //event slugs
        $eventlsugs = new Text('eventslugs', array('class' => 'form-control' , 'placeholder' => 'Event URL'));
        $eventlsugs->setLabel('Event URL');
        $eventlsugs->addFilter('trim');
        // $eventlsugs->addValidators(array(
        //     new PresenceOf(array(
        //         'message' => 'Event Slug is required'
        //         )),
        //     new StringLength(array(
        //       'min' => 2,
        //       'messageMinimum' => 'Eventname should have at least 2 minimum characters'
        //       )),
        //
        //     ));
        $this->add($eventlsugs);

        //location
        $location = new Text('location', array('class' => 'form-control' , 'placeholder' => 'Location'));
        $location->setLabel('Location');
        $location->addFilter('trim');
        $location->addValidators(array(
            new PresenceOf(array(
                'message' => 'hello aklsdhfsnasndfkjansdfkjnasdjfkn'
                )),
            new StringLength(array(
              'min' => 2,
              'messageMinimum' => 'Location should have at least 2 minimum characters'
              )),
            new Regex(array(
              'message'    => 'Location is invalid. Avoid spaces and symbols.',
              'pattern'    => '/^[a-zA-Z0-9._]+/',
              'allowEmpty' => false
            )),
            ));
        $this->add($location);

        //desc
        $desc = new TextArea('desc', array('class' => 'form-control' , 'placeholder' => 'Description'));
        $desc->setLabel('Short Description');
        $this->add($desc);



         //ldesc
        $ldesc = new TextArea('ldesc', array('class' => 'form-control programPageText' , 'placeholder' => 'Long Description'));
        $ldesc->setLabel('Long Description');
        $this->add($ldesc);

        //Contact
        $date = new Date('date', array('class' => 'form-control' , 'placeholder' => 'Date'));
        $date->setLabel('Event Date');
        $date->addValidators(array(
            new PresenceOf(array(
                'message' => 'Date is required'
                ))
            ));
        $this->add($date);

        //Address
        $address = new Text('address', array('class' => 'form-control' , 'placeholder' => 'Address'));
        $address->setLabel('Address');
        $this->add($address);

        //Company
        $company = new Text('company', array('class' => 'form-control' , 'placeholder' => 'Company'));
        $company->setLabel('Company');
        $this->add($company);

        //position
        $position = new Text('position', array('class' => 'form-control' , 'placeholder' => 'Position'));
        $position->setLabel('Position');
        $this->add($position);

        //CSRF
        $csrf = new Hidden('csrf');

        $csrf->addValidator(new Identical(array(
            'value' => $this->security->getSessionToken(),
            'message' => 'CSRF validation failed'
            )));
        // $csrf->addValidator(new Identical(array(
        //     $this->security->checkToken() => 1,
        //     'message' => 'CSRF-token validation failed'
        // )));

        $this->add($csrf);

    }
    /**
     * Prints messages for a specific element
     */
    public function messages($name)
    {
        if ($this->hasMessagesFor($name)) {
            foreach ($this->getMessagesFor($name) as $message) {
                $this->flash->error($message);
            }
        }
    }
}
