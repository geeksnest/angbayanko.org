<?php

use Phalcon\Forms\Form,
Phalcon\Forms\Element\Text,
Phalcon\Forms\Element\Hidden,
Phalcon\Forms\Element\Password,
Phalcon\Forms\Element\Submit,
Phalcon\Forms\Element\Check,
Phalcon\Forms\Element\Numeric,
Phalcon\Forms\Element\Select,
Phalcon\Validation\Validator\PresenceOf,
Phalcon\Validation\Validator\Email,
Phalcon\Validation\Validator\Identical,
Phalcon\Validation\Validator\StringLength,
Phalcon\Validation\Validator\Regex,
Phalcon\Validation\Validator\Confirmation;

class CreateuserForm extends Form
{
    public function initialize($entity = null, $options = null)
    {
        // In edition the id is hidden
        if (isset($options['edit']) && $options['edit']) {
            $husername = new Hidden('huserName');
            $this->add($husername);
            $hemail = new Hidden('huserEmail');
            $this->add($hemail);
        }

        //User Account
        $name = new Text('username', array('class' => 'form-control', 'placeholder' => 'Username' ));
        $name->setLabel('Username');
        $name->addValidators(array(
            new PresenceOf(array(
                'message' => 'The Username is required'
                )),
            new StringLength(array(
                'min' => 6,
                'messageMinimum' => 'Username should have at least 6 minimum characters'
                 )),
            new Regex(array(
                'message'    => 'Username is invalid. Avoid spaces and symbols.',
                'pattern'    => '/^[a-zA-Z0-9._]+/',
                'allowEmpty' => false
                )),
            ));
        $this->add($name);

        //Email
        $email = new Text('email', array('class' => 'form-control', 'placeholder' => 'Email Address'));
        $email->setLabel('Email Address');
        $email->addFilter('trim');
        $email->addValidators(array(
           new PresenceOf(array(
                    'message' => 'Email is required'
                    ))/*
                new Email(array(
                    'message' => 'The Email is required'
                    )),*/
             // new Regex(array(
             //        'message'    => 'Not a valid email format',
             //        'pattern'    => '/([a-z0-9_]+|[a-z0-9_]+\.[a-z0-9_]+)@(([a-z0-9]|[a-z0-9]+\.[a-z0-9]+)+\.([a-z]{2,4}))/i',
             //        'allowEmpty' => false
             //        ))
            ));
        $this->add($email);

        if (!isset($options['edit']) && !$options['edit']/* || !isset($options['editown']) && !$options['editown']*/) {
            //Password
            $password = new Password('password', array('class' => 'form-control' , 'placeholder' => 'Password'));
            $password->setLabel('Password');
            $password->addFilter('trim');
            $password->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Password is required'
                    )),
                new StringLength(array(
                  'max' => 20,
                  'min' => 8,
                  'messageMaximum' => 'Thats an exagerated password.',
                  'messageMinimum' => 'Password should be Minimum of 8 characters.'
                  )),
                new Regex(array(
                    'message'    => 'Avoid using spaces.',
                    'pattern'    => '/^\S+$/',
                    'allowEmpty' => false
                    ))

                ));
            $this->add($password);

            //Re Password
            $repassword = new Password('repassword', array('class' => 'form-control' , 'placeholder' => 'Re-Type Password'));
            $repassword->setLabel('Re-Type Password');
            $repassword->addFilter('trim');
            $repassword->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Retyping your password is required'
                    ))
                ));
            $this->add($repassword);
        }else{
            //Password
            /*$oldpassword = new Password('oldpassword', array('class' => 'form-control' , 'placeholder' => 'Old Password'));
            $oldpassword->setLabel('Old Password');
            $this->add($oldpassword); */

            //Password
            $password = new Password('password', array('class' => 'form-control' , 'placeholder' => 'New Password'));
            $password->setLabel('New Password');
            $this->add($password);

            //Re Password
            $repassword = new Password('repassword', array('class' => 'form-control' , 'placeholder' => 'Re-Type Password'));
            $repassword->setLabel('Re-Type Password');
            $this->add($repassword);
        }

        // if(!empty($options['editprofile']) && $options['editprofile']){
        //     //Password
        //     $oldpassword = new Password('oldpassword', array('class' => 'form-control' , 'placeholder' => 'Old Password'));
        //     $oldpassword->setLabel('Old Password');
        //     $oldpassword->addValidators(array(
        //         new PresenceOf(array(
        //             'message' => 'The old password is required'
        //             ))
        //         ));
        //     $this->add($oldpassword);
        // }

        //User Profile
        //firstname
        $firstname = new Text('firstname', array('class' => 'form-control' , 'placeholder' => 'First Name'));
        $firstname->setLabel('First Name');
        $firstname->addFilter('trim');
        $firstname->addValidators(array(
            new PresenceOf(array(
                'message' => 'First Name is required'
                )),
            new StringLength(array(
              'min' => 2,
              'messageMinimum' => 'First Name should have at least 2 minimum characters'
              )),

            ));
        $this->add($firstname);
        //lastname
        $lastname = new Text('lastname', array('class' => 'form-control' , 'placeholder' => 'Last Name'));
        $lastname->setLabel('Last Name');
        $lastname->addFilter('trim');
        $lastname->addValidators(array(
            new PresenceOf(array(
                'message' => 'Last name is required'
                )),
            new StringLength(array(
              'min' => 2,
              'messageMinimum' => 'Lastname should have at least 2 minimum characters'
              ))
            ));
        $this->add($lastname);

        //middlename
        $middlename = new Text('middlename', array('class' => 'form-control' , 'placeholder' => 'Middle Name'));
        $middlename->setLabel('Middle Name');
        $this->add($middlename);

        //Contact
        $contact = new Text('contact', array('class' => 'form-control' , 'placeholder' => '09xxxxxxxxx',
'onkeypress'=>'return isNumber(event)','maxlength'=> 11));
        $contact->setLabel('Contact Number');
        $contact->addValidators(array(
            new PresenceOf(array(
                'message' => 'Contact number is required'
                )),
             new Regex(array(
                    'message'    => 'Numbers Only.',
                    'pattern'    => '/^[0-9]+$/i',
                    'allowEmpty' => false
                    ))

            ));
        $this->add($contact);

        //Address
        $address = new Text('address', array('class' => 'form-control' , 'placeholder' => 'Address'));
        $address->setLabel('Address');
        $this->add($address);

        //Company
        $company = new Text('company', array('class' => 'form-control' , 'placeholder' => 'Company'));
        $company->setLabel('Company');
        $this->add($company);

        //position
        $position = new Text('position', array('class' => 'form-control' , 'placeholder' => 'Position'));
        $position->setLabel('Position');
        $this->add($position);


        //address
        $address = new Text('address', array('class' => 'form-control' , 'placeholder' => 'Address'));
        $address->setLabel('Address');
        $this->add($address);

        //city
        $city = new Text('city', array('class' => 'form-control' , 'placeholder' => 'City'));
        $city->setLabel('City');
        $this->add($city);
        //country
        $country = new Select('country',array("Country",
                "Afghanistan" => "Afghanistan",
                "Albania" => "Albania",
                "Algeria" => "Algeria",
                "American Samoa" => "American Samoa",
                "Andorra" => "Andorra",
                "Angola" => "Angola",
                "Anguilla" => "Anguilla",
                "Antarctica" => "Antarctica",
                "Antigua and Barbuda" => "Antigua and Barbuda",
                "Argentina" => "Argentina",
                "Armenia" => "Armenia",
                "Aruba" => "Aruba",
                "Australia" => "Australia",
                "Austria" => "Austria",
                "Azerbaijan" => "Azerbaijan",
                "Bahamas" => "Bahamas",
                "Bahrain" => "Bahrain",
                "Bangladesh" => "Bangladesh",
                "Barbados" => "Barbados",
                "Belarus" => "Belarus",
                "Belgium" => "Belgium",
                "Belize" => "Belize",
                "Benin" => "Benin",
                "Bermuda" => "Bermuda",
                "Bhutan" => "Bhutan",
                "Bolivia" => "Bolivia",
                "Bosnia and Herzegowina" => "Bosnia and Herzegowina",
                "Botswana" => "Botswana",
                "Bouvet Island" => "Bouvet Island",
                "Brazil" => "Brazil",
                "British Indian Ocean Territory" => "British Indian Ocean Territory",
                "Brunei Darussalam" => "Brunei Darussalam",
                "Bulgaria" => "Bulgaria",
                "Burkina Faso" => "Burkina Faso",
                "Burundi" => "Burundi",
                "Cambodia" => "Cambodia",
                "Cameroon" => "Cameroon",
                "Canada" => "Canada",
                "Cape Verde" => "Cape Verde",
                "Cayman Islands" => "Cayman Islands",
                "Central African Republic" => "Central African Republic",
                "Chad" => "Chad",
                "Chile" => "Chile",
                "China" => "China",
                "Christmas Island" => "Christmas Island",
                "Cocos (Keeling) Islands" => "Cocos (Keeling) Islands",
                "Colombia" => "Colombia",
                "Comoros" => "Comoros",
                "Congo" => "Congo",
                "Congo, the Democratic Republic of the" => "Congo, the Democratic Republic of the",
                "Cook Islands" => "Cook Islands",
                "Costa Rica" => "Costa Rica",
                "Cote d'Ivoire" => "Cote d'Ivoire",
                "Croatia (Hrvatska)" => "Croatia (Hrvatska)",
                "Cuba" => "Cuba",
                "Cyprus" => "Cyprus",
                "Czech Republic" => "Czech Republic",
                "Denmark" => "Denmark",
                "Djibouti" => "Djibouti",
                "Dominica" => "Dominica",
                "Dominican Republic" => "Dominican Republic",
                "East Timor" => "East Timor",
                "Ecuador" => "Ecuador",
                "Egypt" => "Egypt",
                "El Salvador" => "El Salvador",
                "Equatorial Guinea" => "Equatorial Guinea",
                "Eritrea" => "Eritrea",
                "Estonia" => "Estonia",
                "Ethiopia" => "Ethiopia",
                "Falkland Islands (Malvinas)" => "Falkland Islands (Malvinas)",
                "Faroe Islands" => "Faroe Islands",
                "Fiji" => "Fiji",
                "Finland" => "Finland",
                "France" => "France",
                "France Metropolitan" => "France Metropolitan",
                "French Guiana" => "French Guiana",
                "French Polynesia" => "French Polynesia",
                "French Southern Territories" => "French Southern Territories",
                "Gabon" => "Gabon",
                "Gambia" => "Gambia",
                "Georgia" => "Georgia",
                "Germany" => "Germany",
                "Ghana" => "Ghana",
                "Gibraltar" => "Gibraltar",
                "Greece" => "Greece",
                "Greenland" => "Greenland",
                "Grenada" => "Grenada",
                "Guadeloupe" => "Guadeloupe",
                "Guam" => "Guam",
                "Guatemala" => "Guatemala",
                "Guinea" => "Guinea",
                "Guinea-Bissau" => "Guinea-Bissau",
                "Guyana" => "Guyana",
                "Haiti" => "Haiti",
                "Heard and Mc Donald Islands" => "Heard and Mc Donald Islands",
                "Holy See (Vatican City State)" => "Holy See (Vatican City State",
                "Honduras" => "Honduras",
                "Hong Kong" => "Hong Kong",
                "Hungary" => "Hungary",
                "Iceland" => "Iceland",
                "India" => "India",
                "Indonesia" => "Indonesia",
                "Iran (Islamic Republic of)" => "Iran (Islamic Republic of)",
                "Iraq" => "Iraq",
                "Ireland" => "Ireland",
                "Israel" => "Israel",
                "Italy" => "Italy",
                "Jamaica" => "Jamaica",
                "Japan" => "Japan",
                "Jordan" => "Jordan",
                "Kazakhstan" => "Kazakhstan",
                "Kenya" => "Kenya",
                "Kiribati" => "Kiribati",
                "Korea, Democratic People's Republic of" => "Korea, Democratic People's Republic of",
                "Korea, Republic of" => "Korea, Republic of",
                "Kuwait" => "Kuwait",
                "Kyrgyzstan" => "Kyrgyzstan",
                "Lao, People's Democratic Republic" => "Lao, People's Democratic Republic",
                "Latvia" => "Latvia",
                "Lebanon" => "Lebanon",
                "Lesotho" => "Lesotho",
                "Liberia" => "Liberia",
                "Libyan Arab Jamahiriya" => "Libyan Arab Jamahiriya",
                "Liechtenstein" => "Liechtenstein",
                "Lithuania" => "Lithuania",
                "Luxembourg" => "Luxembourg",
                "Macau" => "Macau",
                "Macedonia, The Former Yugoslav Republic of" => "Macedonia, The Former Yugoslav Republic of",
                "Madagascar" => "Madagascar",
                "Malawi" => "Malawi",
                "Malaysia" => "Malaysia",
                "Maldives" => "Maldives",
                "Mali" => "Mali",
                "Malta" => "Malta",
                "Marshall Islands" => "Marshall Islands",
                "Martinique" => "Martinique",
                "Mauritania" => "Mauritania",
                "Mauritius" => "Mauritius",
                "Mayotte" => "Mayotte",
                "Mexico" => "Mexico",
                "Micronesia, Federated States of" => "Micronesia, Federated States of",
                "Moldova, Republic of" => "Moldova, Republic of",
                "Monaco" => "Monaco",
                "Mongolia" => "Mongolia",
                "Montserrat" => "Montserrat",
                "Morocco" => "Morocco",
                "Mozambique" => "Mozambique",
                "Myanmar" => "Myanmar",
                "Namibia" => "Namibia",
                "Nauru" => "Nauru",
                "Nepal" => "Nepal",
                "Netherlands" => "Netherlands",
                "Netherlands Antilles" => "Netherlands Antilles",
                "New Caledonia" => "New Caledonia",
                "New Zealand" => "New Zealand",
                "Nicaragua" => "Nicaragua",
                "Niger" => "Niger",
                "Nigeria" => "Nigeria",
                "Niue" => "Niue",
                "Norfolk Island" => "Norfolk Island",
                "Northern Mariana Islands" => "Northern Mariana Islands",
                "Norway" => "Norway",
                "Oman" => "Oman",
                "Pakistan" => "Pakistan",
                "Palau" => "Palau",
                "Panama" => "Panama",
                "Papua New Guinea" => "Papua New Guinea",
                "Paraguay" => "Paraguay",
                "Peru" => "Peru",
                "Philippines" => "Philippines",
                "Pitcairn" => "Pitcairn",
                "Poland" => "Poland",
                "Portugal" => "Portugal",
                "Puerto Rico" => "Puerto Rico",
                "Qatar" => "Qatar",
                "Reunion" => "Reunion",
                "Romania" => "Romania",
                "Russian Federation" => "Russian Federation",
                "Rwanda" => "Rwanda",
                "Saint Kitts and Nevis" => "Saint Kitts and Nevis",
                "Saint Lucia" => "Saint Lucia",
                "Saint Vincent and the Grenadines" => "Saint Vincent and the Grenadines",
                "Samoa" => "Samoa",
                "San Marino" => "San Marino",
                "Sao Tome and Principe" => "Sao Tome and Principe",
                "Saudi Arabia" => "Saudi Arabia",
                "Senegal" => "Senegal",
                "Seychelles" => "Seychelles",
                "Sierra Leone" => "Sierra Leone",
                "Singapore" => "Singapore",
                "Slovakia (Slovak Republic)" => "Slovakia (Slovak Republic)",
                "Slovenia" => "Slovenia",
                "Solomon Islands" => "Solomon Islands",
                "Somalia" => "Somalia",
                "South Africa" => "South Africa",
                "South Georgia and the South Sandwich Islands" => "South Georgia and the South Sandwich Islands",
                "Spain" => "Spain",
                "Sri Lanka" => "Sri Lanka",
                "St. Helena" => "St. Helena",
                "St. Pierre and Miquelon" => "St. Pierre and Miquelon",
                "Sudan" => "Sudan",
                "Suriname" => "Suriname",
                "Svalbard and Jan Mayen Islands" => "Svalbard and Jan Mayen Islands",
                "Swaziland" => "Swaziland",
                "Sweden" => "Sweden",
                "Switzerland" => "Switzerland",
                "Syrian Arab Republic" => "Syrian Arab Republic",
                "Taiwan, Province of China" => "Taiwan, Province of China",
                "Tajikistan" => "Tajikistan",
                "Tanzania, United Republic of" => "Tanzania, United Republic of",
                "Thailand" => "Thailand",
                "Togo" => "Togo",
                "Tokelau" => "Tokelau",
                "Tonga" => "Tonga",
                "Trinidad and Tobago" => "Trinidad and Tobago",
                "Tunisia" => "Tunisia",
                "Turkey" => "Turkey",
                "Turkmenistan" => "Turkmenistan",
                "Turks and Caicos Islands" => "Turks and Caicos Islands",
                "Tuvalu" => "Tuvalu",
                "Uganda" => "Uganda",
                "Ukraine" => "Ukraine",
                "United Arab Emirates" => "United Arab Emirates",
                "United Kingdom" => "United Kingdom",
                "United States" => "United States",
                "United States Minor Outlying Islands" => "United States Minor Outlying Islands",
                "Uruguay" => "Uruguay",
                "Uzbekistan" => "Uzbekistan",
                "Vanuatu" => "Vanuatu",
                "Venezuela" => "Venezuela",
                "Vietnam" => "Vietnam",
                "Virgin Islands (British)" => "Virgin Islands (British)",
                "Virgin Islands (U.S.)" => "Virgin Islands (U.S.)",
                "Wallis and Futuna Islands" => "Wallis and Futuna Islands",
                "Western Sahara" => "Western Sahara",
                "Yemen" => "Yemen",
                "Yugoslavia" => "Yugoslavia",
                "Zambia" => "Zambia",
                "Zimbabwe" => "Zimbabwe"
                                  ));
            $country->setLabel('Country');
            $this->add($country);

            //state
            $state = new Text('state', array('class' => 'form-control' , 'placeholder' => 'State'));
            $state->setLabel('State');
            $this->add($state);
            //zip
            $zip = new Numeric('zip', array('class' => 'form-control' , 'placeholder' => 'Zip', 'maxlength'  => '5'));
            $zip->setLabel('Zip');
            $this->add($zip);
            //zip
            $contactnomask = new Numeric('contactnomask', array('class' => 'form-control' , 'placeholder' => 'Contact Number', 'maxlength'  => '13'));
            $contactnomask->setLabel('Contact Number');
            $this->add($contactnomask);
            //contact number
            $contact = new Numeric('contact', array('maxlength'=> '11' ,'class' => 'form-control' , 'placeholder' => 'Contact Number'));
            $contact->setLabel('Contact no:');
            $contact->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Contact number is required'
                    ))
                ));
            $this->add($contactnomask);

        //===============abkrescue update detaisl
        $global_mem = new Numeric('global_mem', array('class' => 'form-control' , 'placeholder' => 'Global Members', 'maxlength'  => '50000'));
             $zip->setLabel('global_mem');
            $this->add($global_mem);

        $accomp_miss = new Numeric('accomp_miss', array('class' => 'form-control' , 'placeholder' => 'Accomplish Missions', 'maxlength'  => '50000'));
            $zip->setLabel('accomp_miss');
            $this->add($accomp_miss);

        $worth_donat = new Numeric('worth_donat', array('class' => 'form-control' , 'placeholder' => 'Worth of Donation', 'maxlength'  => '9999999'));
            $zip->setLabel('worth_donat');
            $this->add($worth_donat);
//
//            new check_field(role.roleCode, 'value':role.roleCode)
        // $rolecheck = new check_field('role'.'roleCode','value'=>'role'.'roleCode');
        // $this->add($rolecheck);
//
        //CSRF

        $csrf = new Hidden('csrf');

        // $csrf->addValidator(new Identical(array(
        //     'value' => $this->security->getSessionToken(),
        //     'message' => 'CSRF validation failed'
        //     )));
        $csrf->addValidator(new Identical(array(
            $this->security->checkToken() => 1,
            'message' => 'CSRF-token validation failed'
        )));

        $this->add($csrf);
    }

    /**
     * Prints messages for a specific element
     */
    public function messages($name)
    {
        if ($this->hasMessagesFor($name)) {
            foreach ($this->getMessagesFor($name) as $message) {
                $this->flash->error($message);
            }
        }
    }
}
