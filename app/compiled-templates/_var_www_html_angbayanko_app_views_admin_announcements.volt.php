

             <!-- Modal View-->
            <div id="modalView" class="modal fade modalView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header-color">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color: white;font-size: 17px">×</button>
                     <span  style="color: white;font-size: 17px">View Announcement</span>
                  </div>
                  <div class="modal-body" style="word-break: break-all;">
                    <p id="modaltxt" class="modal-message"></p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><span class="icon glyphicon glyphicon-remove"></span> Close</button>
                  </div>
                </div>
              </div>
            </div>
<!-- Modal Prompt-->

<div id="modalPrompt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div id="headerColor" >
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color: white;font-size: 17px">×</button>

                <span  style="color: white;font-size: 17px" id="changewarning"></span>
            </div>
            <div class="modal-body"  style="word-break: break-all;">
                <p class="modal-message"></p>
                <span class="modal-list-names"></span>
            </div>
            <div class="modal-footer">
                <a href="#" type="button" class="btn btn-primary modal-btn-yes" data-form='userform'><span class="icon glyphicon glyphicon-ok"></span> Yes</a>
                <a type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><span class="icon glyphicon glyphicon-remove"></span> No</a>
            </div>
        </div>
    </div>
</div>
<!-- Page heading -->
<div class="page-head">
    <!-- Page heading -->
    <h2 class="pull-left">
        <!-- page meta -->
        <span class="page-meta">List of Announcements</span>
    </h2>


    <!-- Breadcrumb -->
    <div class="bread-crumb pull-right">
        <a href="/admin"><i class="icon-home"></i> Home</a>
        <!-- Divider -->
        <span class="divider">/</span>
        <a href="/admin/announcements" class="bread-current">Announcements</a>
        <!-- Divider -->
    </div>

    <div class="clearfix"></div>

</div>
<!-- Page heading ends -->
<div class="matter">
    <div class="container">

        <?= $this->tag->form(['admin/announcements', 'id' => 'main-table-form']) ?>
        <?= $this->tag->hiddenField(['csrf', 'id' => 'csrfToken', 'value' => '']) ?>

        <div class="row">
          <div class="col-md-2">
            <label>Search by Text</label>
          </div>
          <div class="col-md-6 form-group">
            <div class="col-md-12">
              <?= $this->tag->textField(['search_text', 'class' => 'form-control', 'value' => '']) ?>
            </div>
          </div>
          <div class="col-md-3 form-group">
            <!-- <?= $this->tag->submitButton(['Search', 'class' => 'btn btn-default']) ?> -->
            <button type="submit" name="Search" class='btn btn-success'><span class="icon-search"></span>&nbsp;Search</button>
            <button type="submit" name="clear_search" class="btn btn-danger" value="Clear Search"><span class="icon-refresh"></span>&nbsp;Refresh</button>
          </div>
        </div>

        <!-- Table -->
        <div class="row">
          <div class="col-md-2">
            <label>Filter by Date</label>
          </div>
          <div class="col-md-4 form-group" id="fromDatepicker" class="input-append">
            <label class="col-md-2 control-label">From</label>
            <div class="col-md-10">
              <?= $this->tag->textField(['fromDate', 'readonly' => 'true', 'data-format' => 'yyyy-MM-dd', 'class' => 'form-control dtpicker', 'data-time-icon' => 'icon-time']) ?>
              <span class="add-on">
                <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="btn btn-info btn-lg icon-calendar"></i>
              </span>
            </div>
          </div>
          <div class="col-md-3 form-group" id="toDatepicker" class="input-append" style="margin-left: -8.5%;">
            <label class="col-md-2 control-label">To</label>
            <div class="col-md-12">
              <?= $this->tag->textField(['toDate', 'readonly' => 'true', 'data-format' => 'yyyy-MM-dd', 'class' => 'form-control dtpicker', 'data-time-icon' => 'icon-time']) ?>
              <span class="add-on">
                <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="btn btn-info btn-lg icon-calendar"></i>
              </span>
            </div>
          </div>
          <div class="col-md-2">
            <?= $this->tag->submitButton(['Filter', 'class' => 'btn btn-primary']) ?>
          </div>
        </div>

        <!-- Table -->

        <div class="row">

            <div class="col-md-12">
                <form name="form1" class="" method="post" action="" id='main-table-form' class="form-horizontal">
                    <?= $this->getContent() ?>
                    <!-- <div class="form-group">
                        <label class="col-lg-1 control-label">Search</label>
                        <div class="col-lg-4">
                            
                        </div>
                        <div class="col-lg-7">
                            
                            <button type="submit" name="clear_search" class="btn btn-default" value="Clear Search"><span class="icon-refresh"></span></button>

                        </div>
                    </div> -->
                    <div class="clearfix"></div>
                    <div class="widget">

                        <div class="widget-head">
                            <div class="pull-left">All Announcements</div>
                            <div class="widget-icons pull-right">
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="widget-content">

                            <?= $this->tag->hiddenField(['csrf', 'value' => $this->security->getToken()]) ?>
                            <input type="hidden" class="tbl-action" name="action" value=""/>
                            <input type="hidden" class="tbl-recordID" name="recordID" value=""/>
                            <input type="hidden" class="tbl-edit-url" name="editurl" value="editannouncement/"/>
                            <!-- <input type="text" name="enddate" value="Back End Management Modules">  -->
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th width="2%"><?= $this->tag->checkField(['select_all[]', 'class' => 'tbl_select_all']) ?></th>
                                    <th width="35%"><a href="?sort=<?= $titleHref ?>">Title <i class="<?= ($titleIndicator ? $titleIndicator : '') ?>"></i></a></th>
                                    <th><a href="?sort=<?= $statusHref ?>">Status <i class="<?= ($statusIndicator ? $statusIndicator : '') ?>"></i></a></th>
                                    <th><a href="?sort=<?= $dateHref ?>">Date Created <i class="<?= ($dateIndicator ? $dateIndicator : '') ?>"></i></a></th>
                                    <th><a href="?sort=<?= $startHref ?>">Date Started <i class="<?= ($startIndicator ? $startIndicator : '') ?>"></i></a></th>
                                    <th><a href="?sort=<?= $endHref ?>">Date Ended <i class="<?= ($endIndicator ? $endIndicator : '') ?>"></i></a></th>
                                    <th width="100px">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if ($page->total_pages == 0) { ?>
                                    <tr>
                                        <td colspan="7" align="center">No Records Found</td>
                                    </tr>
                                <?php } else { ?>
                                    <?php foreach ($page->items as $post) { ?>
                                        <tr>
                                            <td><input type="checkbox" name="tbl_id[]" class="tbl_select_row" value="<?= $post->annID ?>"> </td>
                                            <td class="name"><?= $post->annTitle ?></td>
                                            <td>
                                             <?php if ($post['annStatus'] == 'activated') { ?>
                                             <span class="label label-success">Active</span>
                                             <a href="#modalPrompt" class="btn btn-xs btn-default tbl_delete_row modal-control-button" data-toggle="modal" data-action="deactivate" data-recorID="<?= $post->annID ?>"><i class="icon-ban-circle"></i> </a>
                                              <?php } else { ?>
                                              <span class="label label-default">Inactive</span>
                                              <a href="#modalPrompt" class="btn btn-xs btn-default tbl_delete_row modal-control-button"  data-toggle="modal" data-action="activate" data-recorID="<?= $post->annID ?>"><i class="icon-ban-circle"></i> </a>
                                             <?php } ?>
                                           </td>
                                            <td><?= date('F j, Y', $post->annDate) ?></td>
                                            <td><?= date('F j, Y', $post->annStart) ?></td>
                                            <td><?= date('F j, Y', $post->annEnd) ?>
                                                <!-- <input type="text" name="samdate" value="<?= date($post->annEnd) ?>"> -->
                                            </td>
                                            <td>
                                              <center>
                                                <a style="font-size:30px" href="#modalView" data-toggle="modal" class="btn btn-xs btn-success modal-record-view" data-href="ajaxUserView/<?= $post->annID ?>/<?= $post->annTitle ?>/<?= $post->annDate ?>" ><i class="icon-ok"></i></a>
                <!--                                  <a  href="viewannouncement/<?= $post->annID ?>" class="btn btn-xs btn-success modal-record-view" data-href="ajaxUserView/<?= $user->userID ?>" ><i class="icon-ok"></i>View </a> -->
                                                <a href="#modalPrompt" class="btn btn-xs btn-warning tbl_edit_row modal-control-button" data-toggle="modal" data-action="edit" data-recorID="<?= $post->annID ?>"><i class="icon-pencil"></i></a>

                                                <a href="#modalPrompt" class="btn btn-xs btn-danger tbl_delete_row modal-control-button" data-toggle="modal" data-action="delete" data-recorID="<?= $post->annID ?>"><i class="fa fa-trash"></i></a>
                                           
                                              </center>


                                           </td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>

                                </tbody>
                            </table>
                            <div class="tblbottomcontrol" style="display:none">
                                <a href="#modalPrompt" class="tbl_delete_all" data-toggle="modal" data-action="delete_selected" > Delete all Selected </a> |
                                <a href="#" class="tbl_unselect_all"> Unselect </a>
                            </div>

                            <div class="widget-foot">
                              <?php $limit = 10; ?>
                              <?php $start = ($limit * ($page->current - 1)) + 1; ?>
                              <?php $end = ($limit * ($page->current - 1)) + $limit; ?>

                              <?php if ($end > $page->total_items) { ?>
                              <?php $end = $page->total_items; ?>
                              <?php } ?>
                              <?php if ($limit) { ?>
                              <div style="margin-left: 85.5%">
                                <span>&nbsp;Showing <?= $start ?> - <?= $end ?> of <?= $page->total_items ?></span>
                              </div>
                              <?php } ?>
                              <?php if ($page->items && $page->total_pages > 0) { ?>
                                <ul class="pagination pull-right">
                                    <!---->
                                <?php if ($page->current == 1 && $page->total_pages >= 5 && $page->total_pages > 0) { ?>
                                 <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                                  <?php foreach (range(1, 5) as $index) { ?>
                                    <?php if ($page->current == $index) { ?>
                                      <li><?= $this->tag->linkTo(['admin/announcements?page=' . $index, $index, 'style' => 'background-color:#eee']) ?></li>
                                      <?php } else { ?>
                                      <li><?= $this->tag->linkTo(['admin/announcements?page=' . $index, $index]) ?></li>
                                    <?php } ?>
                                  <?php } ?>
                                <?php } elseif ($page->current == 1 && $page->total_pages < 5 && $page->total_pages > 0) { ?>
                                <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                                  <?php foreach (range(1, $page->total_pages) as $index) { ?>
                                    <?php if ($page->current == $index) { ?>
                                      <li><?= $this->tag->linkTo(['admin/announcements?page=' . $index, $index, 'style' => 'background-color:#eee']) ?></li>
                                      <?php } else { ?>
                                      <li><?= $this->tag->linkTo(['admin/announcements?page=' . $index, $index]) ?></li>
                                    <?php } ?>
                                  <?php } ?>
                                <?php } ?>
                                <!---->
                                <!---->
                                <?php if ($page->current != 1 && $page->total_pages < 5 && $page->total_pages > 0) { ?>
                                <li><?= $this->tag->linkTo(['admin/announcements', 'First']) ?></li>
                                <li><?= $this->tag->linkTo(['admin/announcements?page=' . $page->before, 'Prev']) ?></li>
                                  <?php foreach (range(1, $page->total_pages) as $index) { ?>
                                    <?php if ($page->current == $index) { ?>
                                      <li><?= $this->tag->linkTo(['admin/announcements?page=' . $index, $index, 'style' => 'background-color:#eee']) ?></li>
                                      <?php } else { ?>
                                      <li><?= $this->tag->linkTo(['admin/announcements?page=' . $index, $index]) ?></li>
                                    <?php } ?>
                                  <?php } ?>
                                <?php } elseif ($page->current != 1 && $page->current < 4 && $page->total_pages >= 5) { ?>
                                <li><?= $this->tag->linkTo(['admin/announcements', 'First']) ?></li>
                                <li><?= $this->tag->linkTo(['admin/announcements?page=' . $page->before, 'Prev']) ?></li>
                                  <?php foreach (range(1, 5) as $index) { ?>
                                    <?php if ($page->current == $index) { ?>
                                      <li><?= $this->tag->linkTo(['admin/announcements?page=' . $index, $index, 'style' => 'background-color:#eee']) ?></li>
                                      <?php } else { ?>
                                      <li><?= $this->tag->linkTo(['admin/announcements?page=' . $index, $index]) ?></li>
                                    <?php } ?>
                                  <?php } ?>
                                <?php } elseif ($page->current >= 4 && $page->current + 2 < $page->total_pages) { ?>
                                <li><?= $this->tag->linkTo(['admin/announcements', 'First']) ?></li>
                                <li><?= $this->tag->linkTo(['admin/announcements?page=' . $page->before, 'Prev']) ?></li>
                                  <?php foreach (range($page->current - 2, $page->current + 2) as $index) { ?>
                                    <?php if ($page->current == $index) { ?>
                                      <li><?= $this->tag->linkTo(['admin/announcements?page=' . $index, $index, 'style' => 'background-color:#eee']) ?></li>
                                      <?php } else { ?>
                                      <li><?= $this->tag->linkTo(['admin/announcements?page=' . $index, $index]) ?></li>
                                    <?php } ?>
                                  <?php } ?>
                                <?php } elseif ($page->current >= 4 && ($page->current + 2 == $page->total_pages || $page->current + 2 > $page->total_pages)) { ?>
                                <li><?= $this->tag->linkTo(['admin/announcements', 'First']) ?></li>
                                <li><?= $this->tag->linkTo(['admin/announcements?page=' . $page->before, 'Prev']) ?></li>
                                  <?php foreach (range($page->total_pages - 4, $page->total_pages) as $index) { ?>
                                    <?php if ($page->current == $index) { ?>
                                      <li><?= $this->tag->linkTo(['admin/announcements?page=' . $index, $index, 'style' => 'background-color:#eee']) ?></li>
                                      <?php } else { ?>
                                      <li><?= $this->tag->linkTo(['admin/announcements?page=' . $index, $index]) ?></li>
                                    <?php } ?>
                                  <?php } ?>
                                <?php } ?>
                                <!---->
                                <!---->
                                <?php if ($page->current != $page->last) { ?>
                                <li><?= $this->tag->linkTo(['admin/announcements?page=' . $page->next, 'Next']) ?></li>
                                <li><?= $this->tag->linkTo(['admin/announcements?page=' . $page->last, 'Last']) ?></li>
                                </ul>
                                <?php } else { ?>
                                <li><?= $this->tag->linkTo(['admin/announcements?page=' . $page->next, 'Next', 'onclick' => 'return false', 'style' => 'cursor:default']) ?></li>
                                <li><?= $this->tag->linkTo(['admin/announcements?page=' . $page->last, 'Last', 'onclick' => 'return false', 'style' => 'cursor:default']) ?></li>
                                </ul>
                                <?php } ?>
                                <!---->
                              <?php } elseif ($page->total_pages == 0) { ?>
                              <ul class="pagination pull-right">
                              <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                              <li><?= $this->tag->linkTo(['admin/announcements?page=' . $page->next, 'Next', 'onclick' => 'return false', 'style' => 'cursor:default']) ?></li>
                                <li><?= $this->tag->linkTo(['admin/announcements?page=' . $page->last, 'Last', 'onclick' => 'return false', 'style' => 'cursor:default']) ?></li>
                                </ul>
                 <?php } ?>

                              <div class="clearfix"></div>

                            </div>

                        </div>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>
