          <?php $token = $this->security->getToken() ?>

          <!-- Modal Prompt-->
          <div id="modalPrompt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
               <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title"></h4>
                  </div>
                <div id="headerColor">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color: white;font-size: 17px">×</button>
                    <span  style="color: white;font-size: 17px" id="changewarning"></span>
                  </div>
                <div class="modal-body">
                  <p class="modal-message"></p>
                  <span class="modal-list-names"></span>
                </div>
                <div class="modal-footer">
                  <button class="btn btn-primary modal-btn-yes"><a href="#" type="button"  data-form='userform' style="text-decoration: none; color: white;"><span class="glyphicon glyphicon-ok"></span> Yes</a></button>
                  <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove"></span> No</button>
                </div>
              </div>
            </div>
          </div>
          <!-- Page heading -->
          <div class="page-head">
            <h2 class="pull-left">Towns</h2>

            <!-- Breadcrumb -->
            <div class="bread-crumb pull-right">
              <a href="/admin"><i class="icon-home"></i> Home</a>
              <!-- Divider -->
              <span class="divider">/</span>
              <span>Towns</span>
            </div>

            <div class="clearfix"></div>

          </div>
          <!-- Page heading ends -->

          <!-- Matter -->

          <div class="matter">
            <?= $this->tag->form(['id' => 'main-table-form']) ?>
            <div class="container">
              <!-- Table -->

              <!-- <div class="row">
                <div class="form-group">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="control-label col-lg-3">Search</label>
                    <div class="col-lg-9">
                      
                    </div>
                  </div>
                </div>

                <div class="col-lg-6">
                  
                   <button type="submit" name="clear_search" class="btn btn-default" value="Clear Search"><span class="icon-refresh"></span></button>
                </div>
              </div> -->

              <div class="row">
                  <div class="col-md-2">
                    <label>Search by Text</label>
                  </div>
                  <div class="col-md-6 form-group">
                    <div class="col-md-12">
                      <?= $this->tag->textField(['search_text', 'class' => 'form-control', 'name' => 'search_text']) ?>
                    </div>
                  </div>
                  <div class="col-md-3 form-group">
                    
                    <button type="submit" name="search" class="btn btn-success"><span class="glyphicon glyphicon-search"></span> Search</button>
                    <button type="reset" name="clear_search" class="btn btn-danger" value="Clear Search" onclick="window.location.reload()"><span class="icon-refresh"></span> Refresh</button>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-2">
                    <label>Filter by Date</label>
                  </div>
                  <div class="col-md-4 form-group" id="fromDatepicker" class="input-append">
                    <label class="col-md-2 control-label">From</label>
                    <div class="col-md-10">
                      <?= $this->tag->textField(['fromDate', 'readonly' => 'true', 'data-format' => 'yyyy-MM-dd', 'class' => 'form-control dtpicker', 'data-time-icon' => 'icon-time']) ?>
                      <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="btn btn-info btn-lg icon-calendar"></i>
                      </span>
                    </div>
                  </div>
                  <div class="col-md-3 form-group" id="toDatepicker" class="input-append" style="margin-left: -8.5%;">
                    <label class="col-md-2 control-label">To</label>
                    <div class="col-md-12">
                      <?= $this->tag->textField(['toDate', 'readonly' => 'true', 'data-format' => 'yyyy-MM-dd', 'class' => 'form-control dtpicker', 'data-time-icon' => 'icon-time']) ?>
                      <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="btn btn-info btn-lg icon-calendar"></i>
                      </span>
                    </div>
                  </div>
                  <div class="col-md-2">
                    
                    <?= $this->tag->submitButton(['Filter', 'class' => 'btn btn-primary', 'id' => 'filter-btn', 'name' => 'submits']) ?>
                     <!-- <button type="submit" class="btn btn-primary"> Filter</button> -->
                  </div>
                </div>
                <?= $delete ?>
                <?= $flash ?>
                <?= $this->getContent() ?>

                <div class="">

                  <ul class="nav nav-tabs">
                    <li <?php if ($viewlist) { ?>class="active"<?php } ?>><a href="<?= $this->url->get('admin/towns?sort=dateAdded-desc') ?>">List View</a></li>
                    <li <?php if (!$viewlist) { ?>class="active"<?php } ?>><a href="<?= $this->url->get('admin/towns/map') ?>">Map View</a></li>
                  </ul>

                  <div class="widget">

                    <div class="widget-head">
                      <div class="pull-left"><?php if ($viewlist) { ?>Towns<?php } else { ?>Town Map<?php } ?></div>
                      <div class="widget-icons pull-right">
                      </div>
                      <div class="clearfix"></div>
                    </div>


                    <div class="widget-content">
                      <input type="hidden" name="csrf" value="<?php echo $token ?>">
                      <?php if ($viewlist) { ?>

                      <input type="hidden" class="tbl-action" name="action" value=""/>
                      <input type="hidden" class="tbl-recordID" name="recordID" value=""/>
                      <input type="hidden" class="tbl-edit-url" name="editurl" value="pintown/"/>
                      <table class="table table-striped table-bordered table-hover tblusers">
                        <thead>
                          <tr>
                            <th width="10"><?= $this->tag->checkField(['select_all[]', 'class' => 'tbl_select_all']) ?></th>
                            <th><a href="?sort=<?= $townnameHref ?>">Town Name <i class=" <?= ($townnameIndicator ? $townnameIndicator : '') ?>"></i></a></th>
                            <th><a href="?sort=<?= $latitudeHref ?>">Latitude <i class=" <?= ($latitudeIndicator ? $latitudeIndicator : '') ?>"></i></a></th>
                            <th><a href="?sort=<?= $longitudeHref ?>">Longitude <i class=" <?= ($longitudeIndicator ? $longitudeIndicator : '') ?>"></i></a></th>
                            <th><a href="?sort=<?= $dateaddedHref ?>">Date Added <i class=" <?= ($dateaddedIndicator ? $dateaddedIndicator : '') ?>"></i></a></th>
                            <th width="70px">Action</th>
                          </tr>
                        </thead>
                        <tbody>

                          <?php if ($page->total_pages == 0) { ?>
                              <tr>
                                  <td colspan="7" align="center">No Records Found</td>
                              </tr>
                          <?php } else { ?>
                              <?php foreach ($page->items as $post) { ?>
                                  <tr>
                                      <td><input type="checkbox" name="tbl_id[]" class="tbl_select_row" value="<?= $post->townID ?>"> </td>
                                      <td class="name"><strong><?= $this->tag->linkTo(['admin/viewtown/' . $post->townID, $post->townName]) ?></strong></td>
                                      <td><?= $post->townLat ?></td>
                                      <td><?= $post->townLong ?></td>
                                      <td><?= date('F j, Y', $post->dateAdded) ?></td>
                                      <td>
                                        <center>
                                            <a href="#modalPrompt" class="btn btn-xs btn-warning tbl_edit_row modal-control-button" data-toggle="modal" data-action="edit" data-recorID="<?= $post->townID ?>"><i class="icon-pencil"></i></a>
                                            <a href="#modalPrompt" class="btn btn-xs btn-danger tbl_delete_row modal-control-button" data-toggle="modal" data-action="delete" data-recorID="<?= $post->townID ?>"><i class="fa fa-trash"></i></a>
                                        
                                        </center>
                                         </td>
                                  </tr>
                              <?php } ?>
                          <?php } ?>
                        </tbody>
                      </table>

                      <div class="tblbottomcontrol" style="display:none">
                        <a href="#modalPrompt" class="tbl_delete_all" data-toggle="modal" data-action="delete_selected" > Delete all Selected </a> |
                        <a href="#" class="tbl_unselect_all"> Unselect </a>
                      </div>

                      <div class="widget-foot">
                              <?php $limit = 10; ?>
                              <?php $start = ($limit * ($page->current - 1)) + 1; ?>
                              <?php $end = ($limit * ($page->current - 1)) + $limit; ?>

                              <?php if ($end > $page->total_items) { ?>
                              <?php $end = $page->total_items; ?>
                              <?php } ?>
                              <?php if ($limit) { ?>
                              <div style="margin-left: 85.5%">
                                <span>&nbsp;Showing <?= $start ?> - <?= $end ?> of <?= $page->total_items ?></span>
                              </div>
                              <?php } ?>
                              <?php if ($page->items && $page->total_pages > 0) { ?>
                                <ul class="pagination pull-right">
                                    <!---->
                                <?php if ($page->current == 1 && $page->total_pages >= 5 && $page->total_pages > 0) { ?>
                                 <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                                  <?php foreach (range(1, 5) as $index) { ?>
                                    <?php if ($page->current == $index) { ?>
                                      <li><?= $this->tag->linkTo(['admin/towns?page=' . $index, $index, 'style' => 'background-color:#eee']) ?></li>
                                      <?php } else { ?>
                                      <li><?= $this->tag->linkTo(['admin/towns?page=' . $index, $index]) ?></li>
                                    <?php } ?>
                                  <?php } ?>
                                <?php } elseif ($page->current == 1 && $page->total_pages < 5 && $page->total_pages > 0) { ?>
                                <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                                  <?php foreach (range(1, $page->total_pages) as $index) { ?>
                                    <?php if ($page->current == $index) { ?>
                                      <li><?= $this->tag->linkTo(['admin/towns?page=' . $index, $index, 'style' => 'background-color:#eee']) ?></li>
                                      <?php } else { ?>
                                      <li><?= $this->tag->linkTo(['admin/towns?page=' . $index, $index]) ?></li>
                                    <?php } ?>
                                  <?php } ?>
                                <?php } ?>
                                <!---->
                                <!---->
                                <?php if ($page->current != 1 && $page->total_pages < 5 && $page->total_pages > 0) { ?>
                                <li><?= $this->tag->linkTo(['admin/towns?sort=dateAdded-desc', 'First']) ?></li>
                                <li><?= $this->tag->linkTo(['admin/towns?page=' . $page->before, 'Prev']) ?></li>
                                  <?php foreach (range(1, $page->total_pages) as $index) { ?>
                                    <?php if ($page->current == $index) { ?>
                                      <li><?= $this->tag->linkTo(['admin/towns?page=' . $index, $index, 'style' => 'background-color:#eee']) ?></li>
                                      <?php } else { ?>
                                      <li><?= $this->tag->linkTo(['admin/towns?page=' . $index, $index]) ?></li>
                                    <?php } ?>
                                  <?php } ?>
                                <?php } elseif ($page->current != 1 && $page->current < 4 && $page->total_pages >= 5) { ?>
                                <li><?= $this->tag->linkTo(['admin/towns?sort=dateAdded-desc', 'First']) ?></li>
                                <li><?= $this->tag->linkTo(['admin/towns?page=' . $page->before, 'Prev']) ?></li>
                                  <?php foreach (range(1, 5) as $index) { ?>
                                    <?php if ($page->current == $index) { ?>
                                      <li><?= $this->tag->linkTo(['admin/towns?page=' . $index, $index, 'style' => 'background-color:#eee']) ?></li>
                                      <?php } else { ?>
                                      <li><?= $this->tag->linkTo(['admin/towns?page=' . $index, $index]) ?></li>
                                    <?php } ?>
                                  <?php } ?>
                                <?php } elseif ($page->current >= 4 && $page->current + 2 < $page->total_pages) { ?>
                                <li><?= $this->tag->linkTo(['admin/towns?sort=dateAdded-desc', 'First']) ?></li>
                                <li><?= $this->tag->linkTo(['admin/towns?page=' . $page->before, 'Prev']) ?></li>
                                  <?php foreach (range($page->current - 2, $page->current + 2) as $index) { ?>
                                    <?php if ($page->current == $index) { ?>
                                      <li><?= $this->tag->linkTo(['admin/towns?page=' . $index, $index, 'style' => 'background-color:#eee']) ?></li>
                                      <?php } else { ?>
                                      <li><?= $this->tag->linkTo(['admin/towns?page=' . $index, $index]) ?></li>
                                    <?php } ?>
                                  <?php } ?>
                                <?php } elseif ($page->current >= 4 && ($page->current + 2 == $page->total_pages || $page->current + 2 > $page->total_pages)) { ?>
                                <li><?= $this->tag->linkTo(['admin/towns?sort=dateAdded-desc', 'First']) ?></li>
                                <li><?= $this->tag->linkTo(['admin/towns?page=' . $page->before, 'Prev']) ?></li>
                                  <?php foreach (range($page->total_pages - 4, $page->total_pages) as $index) { ?>
                                    <?php if ($page->current == $index) { ?>
                                      <li><?= $this->tag->linkTo(['admin/towns?page=' . $index, $index, 'style' => 'background-color:#eee']) ?></li>
                                      <?php } else { ?>
                                      <li><?= $this->tag->linkTo(['admin/towns?page=' . $index, $index]) ?></li>
                                    <?php } ?>
                                  <?php } ?>
                                <?php } ?>
                                <!---->
                                <!---->
                                <?php if ($page->current != $page->last) { ?>
                                <li><?= $this->tag->linkTo(['admin/towns?page=' . $page->next, 'Next']) ?></li>
                                <li><?= $this->tag->linkTo(['admin/towns?page=' . $page->last, 'Last']) ?></li>
                                </ul>
                                <?php } else { ?>
                                <li><?= $this->tag->linkTo(['admin/towns?page=' . $page->next, 'Next', 'onclick' => 'return false', 'style' => 'cursor:default']) ?></li>
                                <li><?= $this->tag->linkTo(['admin/towns?page=' . $page->last, 'Last', 'onclick' => 'return false', 'style' => 'cursor:default']) ?></li>
                                </ul>
                                <?php } ?>
                                <!---->
                              <?php } elseif ($page->total_pages == 0) { ?>
                              <ul class="pagination pull-right">
                              <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                              <li><?= $this->tag->linkTo(['admin/towns?page=' . $page->next, 'Next', 'onclick' => 'return false', 'style' => 'cursor:default']) ?></li>
                                <li><?= $this->tag->linkTo(['admin/towns?page=' . $page->last, 'Last', 'onclick' => 'return false', 'style' => 'cursor:default']) ?></li>
                                </ul>
                 <?php } ?>

                              <div class="clearfix"></div>

                            </div>
                      <?php } else { ?>
                      <!-- <div id="map_wrapper">
                          <div id="map_canvas" class="mapping"></div>
                      </div> -->

                      <div id="map" style="width: 100%; height: 1500px;"></div>
                      <?php } ?>
                    </div>

                  </div>


                </div>

              </div>


            </div>
          </form>
        </div>


<!-- Matter ends -->
<script>
if ( window.history.replaceState ) {
window.history.replaceState( null, null, window.location.href );
}
</script>
