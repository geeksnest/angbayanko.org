	    <!-- Modal Prompt-->
      <div id="modalPrompt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
           <!--                   <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title"></h4>
              </div> -->
            <div id="headerColor">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color: white;font-size: 17px">×</button>
                <span  style="color: white;font-size: 17px" id="changewarning"></span>
              </div>
            <div class="modal-body">
              <p class="modal-message"></p>
              <span class="modal-list-names"></span>
            </div>
            <div class="modal-footer">
              <button class="btn btn-primary modal-btn-yes"><a href="#" type="button"  data-form='userform' style="text-decoration: none; color: white;"><span class="glyphicon glyphicon-ok"></span> Yes</a></button>
              <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove"></span> No</button>
            </div>
          </div>
        </div>
      </div>
      <!-- Page heading -->
	    <div class="page-head">
        <!-- Page heading -->
	      <h2 class="pull-left"> 
          <!-- page meta -->
          <span class="page-meta">Extras</span>
        </h2>


        <!-- Breadcrumb -->
        <div class="bread-crumb pull-right">
          <a href="/admin"><i class="icon-home"></i> Home</a> 
          <!-- Divider -->
          <span class="divider">/</span> 
          <a href="#" class="bread-current">Extras</a>        
        </div>

        <div class="clearfix"></div>

	    </div>
	    <!-- Page heading ends -->
      <!-- Matter -->

        <div class="matter">
          <div class="container">
            <?= $this->getContent() ?>
            <div class="row">

            <div class="col-md-6">              
              <div class="widget">
                <div class="widget-head">
                  <div class="pull-left"><?= ($edit ? 'Edit' : 'Create') ?> Slider</div>
                  <div class="widget-icons pull-right">
                  </div>  
                  <div class="clearfix"></div>
                </div>
                <div class="widget-content">
                  <div class="padd">
                    <!-- Form starts.  -->
                      <?php if ($edit) { ?>
                        <form action="<?= $this->url->get('admin/settings/' . $eslide->slideID) ?>" method="post" class="form-horizontal">
                      <?php } else { ?>
                        <?= $this->tag->form(['admin/settings', 'class' => 'form-horizontal']) ?>
                      <?php } ?>
                      <div class="form-group" id="imageBanner">
                        <div class="col-lg-3">
                          <label class="">Preview</label>
                        </div>
                        <div class="col-lg-9" id="imagePreview">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-lg-3">
                          <label class=""><?= $form->label('imageUrl') ?></label>
                          <span class="asterisk">*</span>
                        </div>
                        <div class="col-lg-9">
                        <?php if ($edit) { ?>
                          <?php echo $form->render('imageUrl', array( 'value' => $eslide->slideImgUrl )); ?>
                        <?php } else { ?>
                          <?= $form->render('imageUrl') ?>
                        <?php } ?>
                          <?= $form->messages('imageUrl') ?><?= $imgUrlError ?>
                        </div>
                      </div>
                      <div class="form-group" id="embedVideo">
                        <div class="col-lg-3">
                          <label class="">Preview</label>
                        </div>
                        <div class="col-lg-9" id="videoPreview" class="embed-responsive embed-responsive-16by9">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-lg-3">
                        <label class=""><?= $form->label('videoUrl') ?></label>
                        </div>
                        <div class="col-lg-9">
                          <?php if ($edit) { ?>
                            <?php echo $form->render('videoUrl', array( 'value' => $eslide->slideVideoUrl )); ?>
                          <?php } else { ?>
                            <?= $form->render('videoUrl') ?>
                          <?php } ?>
                          <?= $form->messages('videoUrl') ?><?= $vidUrlError ?>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-lg-3">
                        <label>Video Alignment</label>
                         </div>
                        <div class="col-lg-9">
                          <div class="radio">
                            <label>
                            <?php if ($edit) { ?>
                              <?php if ($eslide->slideVideoAlignment == 'left') { ?>
                                <?= $this->tag->radioField(['alignment', 'value' => 'left', 'checked' => 'checked']) ?>
                              <?php } else { ?>
                                <?= $this->tag->radioField(['alignment', 'value' => 'left']) ?>
                              <?php } ?>
                            <?php } else { ?>
                              <?= $this->tag->radioField(['alignment', 'value' => 'left', 'checked' => 'checked']) ?>
                            <?php } ?>
                              Left
                            </label>
                          </div>
                          <div class="radio">
                            <label>
                              <?php if ($edit) { ?>
                                <?php if ($eslide->slideVideoAlignment == 'center') { ?>
                                  <?= $this->tag->radioField(['alignment', 'value' => 'center', 'checked' => 'checked']) ?>
                                <?php } else { ?>
                                  <?= $this->tag->radioField(['alignment', 'value' => 'center']) ?>
                                <?php } ?>
                              <?php } else { ?>
                                <?= $this->tag->radioField(['alignment', 'value' => 'center', 'checked' => 'checked']) ?>
                              <?php } ?>
                              Center
                            </label>
                          </div>
                          <div class="radio">
                            <label>
                              <?php if ($edit) { ?>
                                <?php if ($eslide->slideVideoAlignment == 'right') { ?>
                                  <?= $this->tag->radioField(['alignment', 'value' => 'right', 'checked' => 'checked']) ?>
                                <?php } else { ?>
                                  <?= $this->tag->radioField(['alignment', 'value' => 'right']) ?>
                                <?php } ?>
                              <?php } else { ?>
                                <?= $this->tag->radioField(['alignment', 'value' => 'right', 'checked' => 'checked']) ?>
                              <?php } ?>
                              Right
                            </label>
                          </div>
                        </div>
                      </div>

                      <hr />
                                                          

              <div style="text-align: center;">

                          <?php if ($edit) { ?>
                            <?= $this->tag->submitButton(['Save Changes', 'id' => 'btnSubmit', 'class' => 'btn btn-primary']) ?>
                          <?php } else { ?>
                            <?= $this->tag->submitButton(['Create Slider', 'id' => 'btnSubmit', 'class' => 'btn btn-primary']) ?>
                          <?php } ?>
                          <?php if ($edit) { ?>
                            <a href="/admin/settings" class="btn btn-default">Cancel</a>
                          <?php } else { ?>
                            <button class="btn btn-danger" type="reset" onclick="javascript:removePreview()">Reset</button>
                          <?php } ?>

                  </div>

                     </form>
                  </div>
                </div>
                <div class="widget-foot">
                  <!-- Footer goes here -->
                </div>
              </div>
            </div>

            <div class="col-md-6">              
              <div class="widget">
                <div class="widget-head">
                  <div class="pull-left">Manage Sliders</div>
                  <div class="widget-icons pull-right">
                  </div>  
                  <div class="clearfix"></div>
                </div>
                <div class="widget-content">
                  <div class="padd">
                    <?= $this->tag->form(['admin/settings', 'class' => 'form-horizontal', 'id' => 'main-table-form']) ?>
                      
                      <!-- Widget content -->
                      <input type="hidden" class="tbl-action" name="action" value=""/>
                      <input type="hidden" class="tbl-recordID" name="recordID" value=""/>
                      <input type="hidden" class="tbl-edit-url" name="editurl" value=""/>
                      <table class="table table-striped table-bordered table-hover">
                        <tbody>
                          <?php if($slides == true){ ?>
                          <?php foreach ($slides as $s) { ?>
                          <tr>
                            <td class="name">
                              <label>Banner URL:</label> 
                                <?php if ($this->length($s->slideImgUrl) > 50) { ?>
                                  <span><?php echo substr($s->slideImgUrl, 0, 49) ?>...</span>
                                <?php } else { ?>
                                  <span><?= $s->slideImgUrl ?></span>
                                <?php } ?>
                              <br/>
                              <label>Video URL:</label> <span><?= ($s->slideVideoUrl ? $s->slideVideoUrl : 'None') ?></span>
                              <?php
                                if( !is_null($s->slideVideoUrl)) {
                                  echo "<br><label>Video Alignment:</label> <span>".ucwords($s->slideVideoAlignment)."</span>";
                                }
                              ?>
                            </td>
                            <td>
                            <center>
                                <a href="#modalPrompt" class="btn btn-xs btn-warning tbl_edit_row modal-control-button" data-toggle="modal" data-action="edit-slide" data-recorID="<?= $s->slideID ?>"><i class="icon-pencil"></i></a>
                                <!-- <a href="#modalPrompt" class="btn btn-xs btn-warning tbl_edit_row modal-control-button" data-toggle="modal" data-action="edit-slide" data-recorID="<?= $s->slideID ?>"><i class="icon-pencil"></i></a> -->
                              <a href="#modalPrompt" class="btn btn-xs btn-danger tbl_delete_row modal-control-button" data-toggle="modal" data-toggle="modal" data-action="delete" data-recorID="<?= $s->slideID ?>"><i class="fa fa-trash"></i></a>
                            </center>
                            </td>
                          </tr> 

                          <?php } ?>      
                          <?php } else {
                                  echo "<tr><td>No Slides Yet.</td></tr>";
                                } ?>
                        </tbody></table>

                        <!--   <input id="digitalAssets" type="file" name="files[]" multiple accept="image/*"> -->
                      </form>
                  </div>
                </div>

              </div>
            </div>

          </div> <!-- row -->
        </div> <!-- container -->
      </div> <!-- matter -->

<form>
         <div class="col-lg-12">
                    <div class="widget">
                      <div class="widget-head">
                      <div class="pull-left">Digital Assets (<?php echo count($digital_assets).' files'?>)</div>
                        <div class="clearfix"></div>
                      </div>
                      <div class="widget-content">
                        <div class="padd">
                            <span class="btn btn-success fileinput-button">
                              <i class="glyphicon glyphicon-plus"></i>
                              <span>Add files...</span>
                              <!-- The file input field used as target for the file upload widget -->
                    
                              <input id="digitalAssets" type="file" name="files[]" multiple accept="image/*">
                            </span>
                            <!-- The global progress bar -->
                            <div id="progress" class="progress">
                              <div class="progress-bar progress-bar-success"></div>
                            </div>
                            <div class="gallery digital-assets-gallery">
                              <?php foreach ($digital_assets as $img) { ?>
                                <div class="program-digital-assets-library pull-left" style="position: relative;">
                                  <a href='<?= $this->url->get('server/php/files/' . $img . '') ?>' class='prettyPhoto[pp_gal]'>
                                  <img src='<?= $this->url->get('server/php/files/' . $img . '') ?>' alt=""></a>
                                  <input type="text" onclick="this.focus();this.select()" name="picturename" class="form-control" value='<?= $this->url->get('server/php/files/' . $img . '') ?>'>
                                  <button type="button" class="btn btn-xs btn-danger digital-assets-delete" data-filename="<?= $img ?>" style="position: absolute; top: 0px; left:0px; z-index:999999"><i class="icon-remove"></i> </button>
                                </div>
                              <?php } ?>
                              <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="widget-foot">
                        </div>
                      </div>
                    </div>
                    </div>  
</form>
                     <div class="widget-foot">
                  <!-- Footer goes here -->
                </div>  

