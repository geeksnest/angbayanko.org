<div class="inner-programs pull-left">
	<div class="tabs inner-tabs">
	        <?= $this->getContent() ?>

	        <p>
	        	<?= $donatecontent->content ?>
	        </p>

	        <p>
	        	<?= $donateother->content ?>
	        </p>

	        <br />
	    <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top" class="form-horizontal">
	    	<input type="hidden" name="cmd" value="_donations">
			<input type="hidden" name="business" value="jplacsinto-facilitator@gmail.com">
			<input type="hidden" name="lc" value="US">
			<input type="hidden" name="item_name" value="Ang Bayan Ko">
			<input type="hidden" name="no_note" value="0">
			<input type="hidden" name="currency_code" value="PHP">
			<input type="hidden" name="return" value="http://angbayanko.org/donate/success">
			<input type="hidden" name="cancel_return" value="http://angbayanko.org/donate/canceled">			
			<input type="hidden" name="notify_url" value="http://angbayanko.org/donate/paypalipn">
			<input type="hidden" name="bn" value="PP-DonationsBF:btn_donateCC_LG.gif:NonHostedGuest">
			<input type="hidden" name="amount" value="">

			<button type="submit" name="submit" class="btn btn-primary btn-lg">Donate With <i><strong>Paypal</strong></i></button> 
			<br /><br />
			<p>You will be redirected to the PayPal website to complete your payment.</p>
			
			<!-- <div class="text-center">
				<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt=
				"PayPal - The safer, easier way to pay online!">
				<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
			</div> -->
		</form>


    </div>


</div>
