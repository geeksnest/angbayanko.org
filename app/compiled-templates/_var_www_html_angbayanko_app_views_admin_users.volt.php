             <!-- Modal View-->


             </style>
            <div id="modalView" class="modal fade modalView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header-color">
                    <button type="button" style="color: white;font-size: 17px" class="close" data-dismiss="modal" aria-hidden="true" >×</button>
                    <span class="modal-title" style="color: white;font-size: 17px">View Record</span>
                  </div>
                  <div class="modal-body">
                    <p class="modal-message"></p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><span class="icon glyphicon glyphicon-remove"></span>&nbsp;Close</button>
                  </div>
                </div>
              </div>
            </div>
          <!-- Modal Prompt-->
            <div id="modalPrompt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
<!--                   <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title"></h4>
                  </div> -->
                 <div id="headerColor">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color: white;font-size: 17px">×</button>
                    <span  style="color: white;font-size: 17px" id="changewarning"></span>
                  </div>
                  <div class="modal-body">
                    <p class="modal-message"></p>
                    <span class="modal-list-names"></span>
                  </div>
                  <div class="modal-footer">
                    <a href="#" type="button" class="btn btn-primary modal-btn-yes" data-form='userform'><span class="icon glyphicon glyphicon-ok"></span> Yes</a>
                    <a type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><span class="icon glyphicon glyphicon-remove"></span> No</a>
                  </div>
                </div>
              </div>
            </div>
            <!-- Page heading -->
            <div class="page-head">
              <h2 class="pull-left"><i class="icon-table"></i> User List</h2>

              <!-- Breadcrumb -->
              <div class="bread-crumb pull-right">
                <a href="/admin"><i class="icon-home"></i> Home</a>
                <!-- Divider -->
                <span class="divider">/</span>
                <a href="" class="bread-current">Users</a>
              </div>

              <div class="clearfix"></div>

            </div>
            <!-- Page heading ends -->

            <!-- Matter -->

            <div class="matter">

              <div class="container">

                <?= $this->tag->form(['admin/users', 'id' => 'main-table-form']) ?>
                <?= $this->tag->hiddenField(['csrf', 'id' => 'csrfToken', 'value' => '']) ?>

                <div class="row">
                  <div class="col-md-2">
                    <label>Search by Text</label>
                  </div>
                  <div class="col-md-6 form-group">
                    <div class="col-md-12">
                      <?= $this->tag->textField(['search_text', 'class' => 'form-control', 'id' => 'GG']) ?>
                    </div>
                  </div>
                  <div class="col-md-3 form-group">
                   <!--  <?= $this->tag->submitButton(['Search', 'class' => 'btn btn-success']) ?> -->
                   <button type="submit"   name="Search" class='btn btn-success'><span class="icon-search"></span> Search</button>
                    <button type="submit" name="clear_search" class="btn btn-danger" value="Clear Search"><span class="icon-refresh"></span> Refresh</button>
                  </div>
                </div>

                <!-- Table -->
                <div class="row">

                  <div class="col-md-2">
                    <label>Filter by Date</label>
                  </div>
                  <div class="col-md-4 form-group start" id="fromDatepicker" class="input-append">
                    <label class="col-md-2 control-label">From</label>
                    <div class="col-md-10">
                      <?= $this->tag->textField(['fromDate', 'readonly' => 'true', 'data-format' => 'yyyy-MM-dd', 'class' => 'form-control dtpicker', 'data-time-icon' => 'icon-time']) ?>
                      <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="btn btn-info btn-lg icon-calendar " ></i>
                      </span>
                    </div>
                  </div>
                  <div class="col-md-3 form-group end" id="toDatepicker" class="input-append" style="margin-left: -8.5%;">
                    <label class="col-md-2 control-label">To</label>
                    <div class="col-md-12">
                      <?= $this->tag->textField(['toDate', 'readonly' => 'true', 'data-format' => 'yyyy-MM-dd', 'class' => 'form-control dtpicker', 'data-time-icon' => 'icon-time']) ?>
                      <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="btn btn-info btn-lg icon-calendar"></i>
                      </span>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <?= $this->tag->submitButton(['Filter', 'class' => 'btn btn-primary']) ?>
                  </div>
                </div>


                <!-- Table -->

                <div class="row">

                  <div class="col-md-12">
                    <?= $this->tag->form(['admin/users', 'class' => 'form-horizontal', 'id' => 'main-table-form']) ?>
                  <?= $this->getContent() ?>
                    <!-- <div class="form-group">
                      <label class="col-lg-1 control-label">Search</label>
                      <div class="col-lg-4">
                            
                        </div>
                      <div class="col-lg-6">
                        
                        <button type="submit" name="clear_search" class="btn btn-default" value="Clear Search"><span class="icon-refresh"></span></button>
                      </div>

                    </div> -->

                    <div class="widget">

                      <div class="widget-head">
                        <div class="pull-left">Users</div>
                        <div class="widget-icons pull-right">
                        </div>
                        <div class="clearfix"></div>
                      </div>

                      <?= $this->tag->hiddenField(['csrf', 'value' => $this->security->getToken()]) ?>
                      <div class="widget-content">
                        <?php $v39827031523751059871iterated = false; ?><?php $v39827031523751059871iterator = $page->items; $v39827031523751059871incr = 0; $v39827031523751059871loop = new stdClass(); $v39827031523751059871loop->self = &$v39827031523751059871loop; $v39827031523751059871loop->length = count($v39827031523751059871iterator); $v39827031523751059871loop->index = 1; $v39827031523751059871loop->index0 = 1; $v39827031523751059871loop->revindex = $v39827031523751059871loop->length; $v39827031523751059871loop->revindex0 = $v39827031523751059871loop->length - 1; ?><?php foreach ($v39827031523751059871iterator as $user) { ?><?php $v39827031523751059871loop->first = ($v39827031523751059871incr == 0); $v39827031523751059871loop->index = $v39827031523751059871incr + 1; $v39827031523751059871loop->index0 = $v39827031523751059871incr; $v39827031523751059871loop->revindex = $v39827031523751059871loop->length - $v39827031523751059871incr; $v39827031523751059871loop->revindex0 = $v39827031523751059871loop->length - ($v39827031523751059871incr + 1); $v39827031523751059871loop->last = ($v39827031523751059871incr == ($v39827031523751059871loop->length - 1)); ?><?php $v39827031523751059871iterated = true; ?>
                        <?php if ($v39827031523751059871loop->first) { ?>

                        <input type="hidden" class="tbl-action" name="action" value=""/>
                        <input type="hidden" class="tbl-recordID" name="recordID" value=""/>
                        <input type="hidden" class="tbl-edit-url" name="editurl" value="edituser/"/>
                        <table class="table table-striped table-bordered table-hover tblusers">
                          <thead>
                            <tr>
                              <th><?= $this->tag->checkField(['select_all[]', 'class' => 'tbl_select_all']) ?></th>
                              <!-- <th>UserId</th> -->
                              <th><a href="?sort=<?= $usernameHref ?>">Username <i class="<?= ($usernameIndicator ? $usernameIndicator : '') ?>"></i></a></th>
                              <th><a href="?sort=<?= $nameHref ?>">Name <i class="<?= ($nameIndicator ? $nameIndicator : '') ?>"></i></a></th>
                              <th><a href="?sort=<?= $emailHref ?>">Email <i class="<?= ($emailIndicator ? $emailIndicator : '') ?>"></i></a></th>
                              <th><a href="?sort=<?= $datecreatedHref ?>">Date Created <i class="<?= ($datecreatedIndicator ? $datecreatedIndicator : '') ?>"></i></a></th>
                              <th><a href="?sort=<?= $positionHref ?>">Position <i class="<?= ($positionIndicator ? $positionIndicator : '') ?>"></i></a></th>
                              <th><a href="?sort=<?= $statusHref ?>">Status <i class="<?= ($statusIndicator ? $statusIndicator : '') ?>"></i></a></th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <?php } ?>
                          <tbody>
                            <tr>
                              <td>
                                <input type="checkbox" name="tbl_id[]" class="tbl_select_row" value="<?= $user['userID'] ?>">
                                <!-- <?= $this->tag->checkField(['tbl_id', 'class' => 'tbl_select_row', 'value' => $user['userID']]) ?> -->
                              </td>
                              <td class="name"><?= $user['username'] ?></td>
                              <td><?= $user['name'] ?></td>
                              <td><?= $user['email'] ?></td>
                              <td style="width: 100px"><?= date('F d, Y', $user['dateCreated']) ?></td>
                              <td><?= $user['position'] ?></td>
                              <td style="width: 100px">
                                <?php if ($user['status'] == 'active') { ?>
                                <span class="label label-success">Active</span>

                                <a href="#modalPrompt" class="btn btn-xs btn-default tbl_delete_row modal-control-button" data-toggle="modal" data-action="deactivate" data-recorID="<?= $user['userID'] ?>"><i class="icon-ban-circle"></i></a>
                                <?php } else { ?>
                                <span class="label label-default">Inactive</span>

                                <a href="#modalPrompt" class="btn btn-xs btn-success tbl_delete_row modal-control-button" data-toggle="modal" data-action="activate" data-recorID="<?= $user['userID'] ?>"><i class="icon-ok"></i></a>
                                <?php } ?>
                              </td>
                              <td style="width: 100px">
                                <center>
                                    <a  href="#modalView" data-toggle="modal" class="btn btn-xs btn-success modal-record-view" data-href="ajaxUserView/<?= $user['userID'] ?>" ><i class="icon-ok"></i></a>
                                    <a href="#modalPrompt" class="btn btn-xs btn-warning tbl_edit_row modal-control-button" data-toggle="modal" data-action="edit" data-recorID="<?= $user['userID'] ?>" id="coloryellow"><i class="icon-pencil"></i></a>
                                    <a href="#modalPrompt" class="btn btn-xs btn-danger tbl_delete_row modal-control-button" data-toggle="modal" data-action="delete" data-recorID="<?= $user['userID'] ?>" id="colorblue"><i class="fa fa-trash"></i></a>    
                                </center>
                              </td>
                            </tr>

                          </tbody>
                          <?php if ($v39827031523751059871loop->last) { ?>
                          <tbody>
                            <tr>
                              <td colspan="10" align="right">
                                <div class="btn-group">

                                </div>
                              </td>
                            </tr>
                            <tbody>
                            </table>
                            <div class="tblbottomcontrol" style="display:none; padding: 10px 10px 10px 10px;">
                              <a href="#modalPrompt" class="tbl_delete_all btn btn-danger btn-xs" data-toggle="modal" data-action="delete_selected" ><i class="icon-remove"></i>  Delete all Selected </a> |
                              <a href="#" class="tbl_unselect_all btn btn-info btn-xs"> Unselect </a>
                            </div>
                            <?php } ?>
                            <?php $v39827031523751059871incr++; } if (!$v39827031523751059871iterated) { ?>
                           <table class="table table-striped table-bordered table-hover tblusers">
                          <thead>
                            <tr>
                              <th><?= $this->tag->checkField(['select_all[]', 'class' => 'tbl_select_all']) ?></th>
                              <!-- <th>UserId</th> -->
                              <th><a href="?sort=<?= $usernameHref ?>">Username <i class="<?= ($usernameIndicator ? $usernameIndicator : '') ?>"></i></a></th>
                              <th><a href="?sort=<?= $nameHref ?>">Name <i class="<?= ($nameIndicator ? $nameIndicator : '') ?>"></i></a></th>
                              <th><a href="?sort=<?= $emailHref ?>">Email <i class="<?= ($emailIndicator ? $emailIndicator : '') ?>"></i></a></th>
                              <th><a href="?sort=<?= $datecreatedHref ?>">Date Created <i class="<?= ($datecreatedIndicator ? $datecreatedIndicator : '') ?>"></i></a></th>
                              <th><a href="?sort=<?= $positionHref ?>">Position <i class="<?= ($positionIndicator ? $positionIndicator : '') ?>"></i></a></th>
                              <th><a href="?sort=<?= $statusHref ?>">Status <i class="<?= ($statusIndicator ? $statusIndicator : '') ?>"></i></a></th>
                              <th>Action</th>
                            </tr>
                          </thead>
                            </table>
                            <div class="padd">
                            <center>No Record found.</center>
                            </div>

                            <?php } ?>

                            <div class="widget-foot">
                              <?php $limit = 10; ?>
                              <?php $start = ($limit * ($page->current - 1)) + 1; ?>
                              <?php $end = ($limit * ($page->current - 1)) + $limit; ?>
                              <?php if ($end > $page->total_items) { ?>
                              <?php $end = $page->total_items; ?>
                              <?php } ?>
                              <?php if ($limit) { ?>
                              <div style="margin-left: 85.5%">
                                <span>&nbsp;Showing <?= $start ?> - <?= $end ?> of <?= $page->total_items ?></span>
                              </div>
                              <?php } ?>
                              <?php if ($page->items && $page->total_pages > 0) { ?>
                                <ul class="pagination pull-right">
                                    <!---->
                                <?php if ($page->current == 1 && $page->total_pages >= 5 && $page->total_pages > 0) { ?>
                                 <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                                  <?php $v39827031523751059871iterator = range(1, 5); $v39827031523751059871incr = 0; $v39827031523751059871loop = new stdClass(); $v39827031523751059871loop->self = &$v39827031523751059871loop; $v39827031523751059871loop->length = count($v39827031523751059871iterator); $v39827031523751059871loop->index = 1; $v39827031523751059871loop->index0 = 1; $v39827031523751059871loop->revindex = $v39827031523751059871loop->length; $v39827031523751059871loop->revindex0 = $v39827031523751059871loop->length - 1; ?><?php foreach ($v39827031523751059871iterator as $index) { ?><?php $v39827031523751059871loop->first = ($v39827031523751059871incr == 0); $v39827031523751059871loop->index = $v39827031523751059871incr + 1; $v39827031523751059871loop->index0 = $v39827031523751059871incr; $v39827031523751059871loop->revindex = $v39827031523751059871loop->length - $v39827031523751059871incr; $v39827031523751059871loop->revindex0 = $v39827031523751059871loop->length - ($v39827031523751059871incr + 1); $v39827031523751059871loop->last = ($v39827031523751059871incr == ($v39827031523751059871loop->length - 1)); ?>
                                    <?php if ($page->current == $index) { ?>
                                      <li><?= $this->tag->linkTo(['admin/users?page=' . $index, $index, 'style' => 'background-color:#eee']) ?></li>
                                      <?php } else { ?>
                                      <li><?= $this->tag->linkTo(['admin/users?page=' . $index, $index]) ?></li>
                                    <?php } ?>
                                  <?php $v39827031523751059871incr++; } ?>
                                <?php } elseif ($page->current == 1 && $page->total_pages < 5 && $page->total_pages > 0) { ?>
                                <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                                  <?php $v39827031523751059871iterator = range(1, $page->total_pages); $v39827031523751059871incr = 0; $v39827031523751059871loop = new stdClass(); $v39827031523751059871loop->self = &$v39827031523751059871loop; $v39827031523751059871loop->length = count($v39827031523751059871iterator); $v39827031523751059871loop->index = 1; $v39827031523751059871loop->index0 = 1; $v39827031523751059871loop->revindex = $v39827031523751059871loop->length; $v39827031523751059871loop->revindex0 = $v39827031523751059871loop->length - 1; ?><?php foreach ($v39827031523751059871iterator as $index) { ?><?php $v39827031523751059871loop->first = ($v39827031523751059871incr == 0); $v39827031523751059871loop->index = $v39827031523751059871incr + 1; $v39827031523751059871loop->index0 = $v39827031523751059871incr; $v39827031523751059871loop->revindex = $v39827031523751059871loop->length - $v39827031523751059871incr; $v39827031523751059871loop->revindex0 = $v39827031523751059871loop->length - ($v39827031523751059871incr + 1); $v39827031523751059871loop->last = ($v39827031523751059871incr == ($v39827031523751059871loop->length - 1)); ?>
                                    <?php if ($page->current == $index) { ?>
                                      <li><?= $this->tag->linkTo(['admin/users?page=' . $index, $index, 'style' => 'background-color:#eee']) ?></li>
                                      <?php } else { ?>
                                      <li><?= $this->tag->linkTo(['admin/users?page=' . $index, $index]) ?></li>
                                    <?php } ?>
                                  <?php $v39827031523751059871incr++; } ?>
                                <?php } ?>
                                <!---->
                                <!---->
                                <?php if ($page->current != 1 && $page->total_pages < 5 && $page->total_pages > 0) { ?>
                                <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                                  <?php $v39827031523751059871iterator = range(1, $page->total_pages); $v39827031523751059871incr = 0; $v39827031523751059871loop = new stdClass(); $v39827031523751059871loop->self = &$v39827031523751059871loop; $v39827031523751059871loop->length = count($v39827031523751059871iterator); $v39827031523751059871loop->index = 1; $v39827031523751059871loop->index0 = 1; $v39827031523751059871loop->revindex = $v39827031523751059871loop->length; $v39827031523751059871loop->revindex0 = $v39827031523751059871loop->length - 1; ?><?php foreach ($v39827031523751059871iterator as $index) { ?><?php $v39827031523751059871loop->first = ($v39827031523751059871incr == 0); $v39827031523751059871loop->index = $v39827031523751059871incr + 1; $v39827031523751059871loop->index0 = $v39827031523751059871incr; $v39827031523751059871loop->revindex = $v39827031523751059871loop->length - $v39827031523751059871incr; $v39827031523751059871loop->revindex0 = $v39827031523751059871loop->length - ($v39827031523751059871incr + 1); $v39827031523751059871loop->last = ($v39827031523751059871incr == ($v39827031523751059871loop->length - 1)); ?>
                                    <?php if ($page->current == $index) { ?>
                                      <li><?= $this->tag->linkTo(['admin/users?page=' . $index, $index, 'style' => 'background-color:#eee']) ?></li>
                                      <?php } else { ?>
                                      <li><?= $this->tag->linkTo(['admin/users?page=' . $index, $index]) ?></li>
                                    <?php } ?>
                                  <?php $v39827031523751059871incr++; } ?>
                                <?php } elseif ($page->current != 1 && $page->current < 4 && $page->total_pages >= 5) { ?>
                                <li><?= $this->tag->linkTo(['admin/users', 'First']) ?></li>
                                <li><?= $this->tag->linkTo(['admin/users?page=' . $page->before, 'Prev']) ?></li>
                                  <?php $v39827031523751059871iterator = range(1, 5); $v39827031523751059871incr = 0; $v39827031523751059871loop = new stdClass(); $v39827031523751059871loop->self = &$v39827031523751059871loop; $v39827031523751059871loop->length = count($v39827031523751059871iterator); $v39827031523751059871loop->index = 1; $v39827031523751059871loop->index0 = 1; $v39827031523751059871loop->revindex = $v39827031523751059871loop->length; $v39827031523751059871loop->revindex0 = $v39827031523751059871loop->length - 1; ?><?php foreach ($v39827031523751059871iterator as $index) { ?><?php $v39827031523751059871loop->first = ($v39827031523751059871incr == 0); $v39827031523751059871loop->index = $v39827031523751059871incr + 1; $v39827031523751059871loop->index0 = $v39827031523751059871incr; $v39827031523751059871loop->revindex = $v39827031523751059871loop->length - $v39827031523751059871incr; $v39827031523751059871loop->revindex0 = $v39827031523751059871loop->length - ($v39827031523751059871incr + 1); $v39827031523751059871loop->last = ($v39827031523751059871incr == ($v39827031523751059871loop->length - 1)); ?>
                                    <?php if ($page->current == $index) { ?>
                                      <li><?= $this->tag->linkTo(['admin/users?page=' . $index, $index, 'style' => 'background-color:#eee']) ?></li>
                                      <?php } else { ?>
                                      <li><?= $this->tag->linkTo(['admin/users?page=' . $index, $index]) ?></li>
                                    <?php } ?>
                                  <?php $v39827031523751059871incr++; } ?>
                                <?php } elseif ($page->current >= 4 && $page->current + 2 < $page->total_pages) { ?>
                                <li><?= $this->tag->linkTo(['admin/users', 'First']) ?></li>
                                <li><?= $this->tag->linkTo(['admin/users?page=' . $page->before, 'Prev']) ?></li>
                                  <?php $v39827031523751059871iterator = range($page->current - 2, $page->current + 2); $v39827031523751059871incr = 0; $v39827031523751059871loop = new stdClass(); $v39827031523751059871loop->self = &$v39827031523751059871loop; $v39827031523751059871loop->length = count($v39827031523751059871iterator); $v39827031523751059871loop->index = 1; $v39827031523751059871loop->index0 = 1; $v39827031523751059871loop->revindex = $v39827031523751059871loop->length; $v39827031523751059871loop->revindex0 = $v39827031523751059871loop->length - 1; ?><?php foreach ($v39827031523751059871iterator as $index) { ?><?php $v39827031523751059871loop->first = ($v39827031523751059871incr == 0); $v39827031523751059871loop->index = $v39827031523751059871incr + 1; $v39827031523751059871loop->index0 = $v39827031523751059871incr; $v39827031523751059871loop->revindex = $v39827031523751059871loop->length - $v39827031523751059871incr; $v39827031523751059871loop->revindex0 = $v39827031523751059871loop->length - ($v39827031523751059871incr + 1); $v39827031523751059871loop->last = ($v39827031523751059871incr == ($v39827031523751059871loop->length - 1)); ?>
                                    <?php if ($page->current == $index) { ?>
                                      <li><?= $this->tag->linkTo(['admin/users?page=' . $index, $index, 'style' => 'background-color:#eee']) ?></li>
                                      <?php } else { ?>
                                      <li><?= $this->tag->linkTo(['admin/users?page=' . $index, $index]) ?></li>
                                    <?php } ?>
                                  <?php $v39827031523751059871incr++; } ?>
                                <?php } elseif ($page->current >= 4 && ($page->current + 2 == $page->total_pages || $page->current + 2 > $page->total_pages)) { ?>
                                <li><?= $this->tag->linkTo(['admin/users', 'First']) ?></li>
                                <li><?= $this->tag->linkTo(['admin/users?page=' . $page->before, 'Prev']) ?></li>
                                  <?php $v39827031523751059871iterator = range($page->total_pages - 4, $page->total_pages); $v39827031523751059871incr = 0; $v39827031523751059871loop = new stdClass(); $v39827031523751059871loop->self = &$v39827031523751059871loop; $v39827031523751059871loop->length = count($v39827031523751059871iterator); $v39827031523751059871loop->index = 1; $v39827031523751059871loop->index0 = 1; $v39827031523751059871loop->revindex = $v39827031523751059871loop->length; $v39827031523751059871loop->revindex0 = $v39827031523751059871loop->length - 1; ?><?php foreach ($v39827031523751059871iterator as $index) { ?><?php $v39827031523751059871loop->first = ($v39827031523751059871incr == 0); $v39827031523751059871loop->index = $v39827031523751059871incr + 1; $v39827031523751059871loop->index0 = $v39827031523751059871incr; $v39827031523751059871loop->revindex = $v39827031523751059871loop->length - $v39827031523751059871incr; $v39827031523751059871loop->revindex0 = $v39827031523751059871loop->length - ($v39827031523751059871incr + 1); $v39827031523751059871loop->last = ($v39827031523751059871incr == ($v39827031523751059871loop->length - 1)); ?>
                                    <?php if ($page->current == $index) { ?>
                                      <li><?= $this->tag->linkTo(['admin/users?page=' . $index, $index, 'style' => 'background-color:#eee']) ?></li>
                                      <?php } else { ?>
                                      <li><?= $this->tag->linkTo(['admin/users?page=' . $index, $index]) ?></li>
                                    <?php } ?>
                                  <?php $v39827031523751059871incr++; } ?>
                                <?php } ?>
                                <!---->
                                <!---->
                                <?php if ($page->current != $page->last) { ?>
                                <li><?= $this->tag->linkTo(['admin/users?page=' . $page->next, 'Next']) ?></li>
                                <li><?= $this->tag->linkTo(['admin/users?page=' . $page->last, 'Last']) ?></li>
                                </ul>
                                <?php } else { ?>
                                <li><?= $this->tag->linkTo(['admin/users?page=' . $page->next, 'Next', 'onclick' => 'return false', 'style' => 'cursor:default']) ?></li>
                                <li><?= $this->tag->linkTo(['admin/users?page=' . $page->last, 'Last', 'onclick' => 'return false', 'style' => 'cursor:default']) ?></li>
                                </ul>
                                <?php } ?>
                                <!---->
                              <?php } elseif ($page->total_pages == 0) { ?>
                              <ul class="pagination pull-right">
                              <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                              <li><?= $this->tag->linkTo(['admin/users?page=' . $page->next, 'Next', 'onclick' => 'return false', 'style' => 'cursor:default']) ?></li>
                                <li><?= $this->tag->linkTo(['admin/users?page=' . $page->last, 'Last', 'onclick' => 'return false', 'style' => 'cursor:default']) ?></li>
                                </ul>
                 <?php } ?>


                              <div class="clearfix"></div>

                            </div>

                          </div>

                        </div>
                      </form>

                    </div>

                  </div>


                </div>

              </div>

<!-- Matter ends -->
<!-- <script type="text/javascript">
  $().ready(function(){
                            $("#GG").keyup(removeextra).blur(removeextra);
                          });
                          function removeextra() {
                            var initVal = $(this).val();
                            outputVal = initVal.replace(/[^a-zA-Z .]/g,"");
                            if (initVal != outputVal) {
                              $(this).val(outputVal);
                            }
                          };
</script> -->
