
            <!-- Page heading -->
            <div class="page-head">
              <h2 class="pull-left"><i class="icon-table"></i> Donations</h2>

              <!-- Breadcrumb -->
              <div class="bread-crumb pull-right">
                <a href="<?= $this->url->get('admin') ?>"><i class="icon-home"></i> Home</a> 
                <!-- Divider -->
                <span class="divider">/</span> 
                <span>Donations</span>
              </div>

              <div class="clearfix"></div>

            </div>
            <!-- Page heading ends -->

            <!-- Matter -->

            <div class="matter">


<?= $this->tag->form(['admin/donations', 'class' => 'form-horizontal', 'id' => 'main-table-form']) ?>
  <div class="container">

<!-- Modal Prompt-->
            <div id="modalPrompt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                <!--   <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Alert!</h4>
                  </div> -->
                  <div id="headerColor">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color: white;font-size: 17px">×</button>
                    <span  style="color: white;font-size: 17px" id="changewarning"></span>
                  </div>
                  <div class="modal-body">
                    <p class="modal-message"></p>
                    <span class="modal-list-names"></span>
                  </div>
                   <div class="modal-footer">
                    <a href="#" type="button" class="btn btn-primary modal-btn-yes" data-form='userform'><span class="icon glyphicon glyphicon-ok"></span> Yes</a>
                    <a type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><span class="icon glyphicon glyphicon-remove"></span> No</a>
                  </div>
                </div>
              </div>
            </div>

      <div class="form-group">
        <label class="col-lg-1 control-label">Search</label>
        <div class="col-lg-4">
              <?= $this->tag->textField(['search_text', 'class' => 'form-control']) ?>
          </div>
        <div class="col-lg-6">
          <!-- <?= $this->tag->submitButton(['Search', 'class' => 'btn btn-default']) ?> -->
          <button type="submit" value="send" class="btn btn-success"><span class="glyphicon glyphicon-search"></span> Search</button>
           <button type="submit" name="clear_search" class="btn btn-danger" value="Clear Search"><span class="icon-refresh"></span> Refresh</button>
        </div>

      </div>  
      
      <div class="col-md-12">
        <?= $this->getContent() ?>
        
        
        <div class="widget">

          <div class="widget-head">
            <div class="pull-left">Donations List</div>
            <div class="clearfix"></div>
          </div>


          <div class="widget-content">
            <?= $this->tag->hiddenField(['csrf', 'value' => $this->security->getToken()]) ?>
            <input type="hidden" class="tbl-action" name="action" value=""/>
            <input type="hidden" class="tbl-recordID" name="recordID" value=""/>
            <input type="hidden" class="tbl-edit-url" name="editurl" value="editpage/"/>

            

            <table class="table table-striped table-bordered table-hover tblusers">
              <thead>
                <tr>
                  <th width="10"><?= $this->tag->checkField(['select_all[]', 'class' => 'tbl_select_all']) ?></th>
                  <th width="100">Transaction ID</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone</th>
                  <th width="100">Amount</th>
                  <th width="200">Date</th>
                  <th width="10"></th>
                </tr>
              </thead>
              <tbody>

              <?php if ($page->total_pages == 0) { ?>  
                                    <tr>
                                        <td colspan="7" align="center">No Records Found</td>
                                    </tr>
                                <?php } else { ?>
                                    <?php foreach ($page->items as $post) { ?>

                    <tr>
                      <td><input type="checkbox" name="tbl_id[]" class="tbl_select_row" value="<?php echo $value['txn_id'] ?>"></td>
                        <td class="name">
                          <?php echo $value['txn_id'] ?>
                        </td>
                        <td><?php echo $value['fname'].' '.$value['lname'] ?></td>
                        <td><?php echo $value['email'] ?></td>
                        <td><?php echo $value['phone'] ?></td>
                        <td><?php echo $value['amount'] ?></td>
                        <td><?php echo date("F j, Y", $value['date']) ?></td>
                        <td>
                            <a href="#modalPrompt" class="btn btn-xs btn-danger tbl_delete_row modal-control-button" data-toggle="modal" data-action="delete" data-recorID="<?php echo $value['txn_id'] ?>"><i class="icon-remove"></i> Delete</a>
                        </td>
                    </tr>

                <?php } ?>
                                <?php } ?>
              </tbody>
              </table>

              <div class="tblbottomcontrol" style="display:none">
                <a href="#modalPrompt" class="tbl_delete_all" data-toggle="modal" data-action="delete_selected" > Delete all Selected </a> | 
                <a href="#" class="tbl_unselect_all"> Unselect </a>
              </div>

                <div class="widget-foot">
                              <?php $limit = 10; ?>
                              <?php $start = ($limit * ($page->current - 1)) + 1; ?>
                              <?php $end = ($limit * ($page->current - 1)) + $limit; ?>

                              <?php if ($end > $page->total_items) { ?>
                              <?php $end = $page->total_items; ?>
                              <?php } ?>
                              <?php if ($limit) { ?>
                              <div style="margin-left: 85.5%">
                                <span>&nbsp;Showing <?= $start ?> - <?= $end ?> of <?= $page->total_items ?></span>
                              </div>
                              <?php } ?>
                              <?php if ($page->items && $page->total_pages > 0) { ?>
                                <ul class="pagination pull-right">
                                    <!---->
                                <?php if ($page->current == 1 && $page->total_pages >= 5 && $page->total_pages > 0) { ?>                                
                                 <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                                  <?php foreach (range(1, 5) as $index) { ?>
                                    <?php if ($page->current == $index) { ?>
                                      <li><?= $this->tag->linkTo(['admin/donations?page=' . $index, $index, 'style' => 'background-color:#eee']) ?></li>
                                      <?php } else { ?>
                                      <li><?= $this->tag->linkTo(['admin/donations?page=' . $index, $index]) ?></li>
                                    <?php } ?>
                                  <?php } ?>
                                <?php } elseif ($page->current == 1 && $page->total_pages < 5 && $page->total_pages > 0) { ?>
                                <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                                  <?php foreach (range(1, $page->total_pages) as $index) { ?>
                                    <?php if ($page->current == $index) { ?>
                                      <li><?= $this->tag->linkTo(['admin/donations?page=' . $index, $index, 'style' => 'background-color:#eee']) ?></li>
                                      <?php } else { ?>
                                      <li><?= $this->tag->linkTo(['admin/donations?page=' . $index, $index]) ?></li>
                                    <?php } ?>
                                  <?php } ?>
                                <?php } ?>
                                <!---->
                                <!---->
                                <?php if ($page->current != 1 && $page->total_pages < 5 && $page->total_pages > 0) { ?>
                                <li><?= $this->tag->linkTo(['admin/donations', 'First']) ?></li>
                                <li><?= $this->tag->linkTo(['admin/donations?page=' . $page->before, 'Prev']) ?></li>
                                  <?php foreach (range(1, $page->total_pages) as $index) { ?>
                                    <?php if ($page->current == $index) { ?>
                                      <li><?= $this->tag->linkTo(['admin/donations?page=' . $index, $index, 'style' => 'background-color:#eee']) ?></li>
                                      <?php } else { ?>
                                      <li><?= $this->tag->linkTo(['admin/donations?page=' . $index, $index]) ?></li>
                                    <?php } ?>
                                  <?php } ?>
                                <?php } elseif ($page->current != 1 && $page->current < 4 && $page->total_pages >= 5) { ?>
                                <li><?= $this->tag->linkTo(['admin/donations', 'First']) ?></li>
                                <li><?= $this->tag->linkTo(['admin/donations?page=' . $page->before, 'Prev']) ?></li>
                                  <?php foreach (range(1, 5) as $index) { ?>
                                    <?php if ($page->current == $index) { ?>
                                      <li><?= $this->tag->linkTo(['admin/donations?page=' . $index, $index, 'style' => 'background-color:#eee']) ?></li>
                                      <?php } else { ?>
                                      <li><?= $this->tag->linkTo(['admin/donations?page=' . $index, $index]) ?></li>
                                    <?php } ?>
                                  <?php } ?>
                                <?php } elseif ($page->current >= 4 && $page->current + 2 < $page->total_pages) { ?>
                                <li><?= $this->tag->linkTo(['admin/donations', 'First']) ?></li>
                                <li><?= $this->tag->linkTo(['admin/donations?page=' . $page->before, 'Prev']) ?></li>
                                  <?php foreach (range($page->current - 2, $page->current + 2) as $index) { ?>
                                    <?php if ($page->current == $index) { ?>
                                      <li><?= $this->tag->linkTo(['admin/donations?page=' . $index, $index, 'style' => 'background-color:#eee']) ?></li>
                                      <?php } else { ?>
                                      <li><?= $this->tag->linkTo(['admin/donations?page=' . $index, $index]) ?></li>
                                    <?php } ?>
                                  <?php } ?>
                                <?php } elseif ($page->current >= 4 && ($page->current + 2 == $page->total_pages || $page->current + 2 > $page->total_pages)) { ?>
                                <li><?= $this->tag->linkTo(['admin/donations', 'First']) ?></li>
                                <li><?= $this->tag->linkTo(['admin/donations?page=' . $page->before, 'Prev']) ?></li>
                                  <?php foreach (range($page->total_pages - 4, $page->total_pages) as $index) { ?>
                                    <?php if ($page->current == $index) { ?>
                                      <li><?= $this->tag->linkTo(['admin/donations?page=' . $index, $index, 'style' => 'background-color:#eee']) ?></li>
                                      <?php } else { ?>
                                      <li><?= $this->tag->linkTo(['admin/donations?page=' . $index, $index]) ?></li>
                                    <?php } ?>
                                  <?php } ?>
                                <?php } ?>
                                <!---->
                                <!---->
                                <?php if ($page->current != $page->last) { ?>                 
                                <li><?= $this->tag->linkTo(['admin/donations?page=' . $page->next, 'Next']) ?></li>
                                <li><?= $this->tag->linkTo(['admin/donations?page=' . $page->last, 'Last']) ?></li>
                                </ul>
                                <?php } else { ?>
                                <li><?= $this->tag->linkTo(['admin/donations?page=' . $page->next, 'Next', 'onclick' => 'return false', 'style' => 'cursor:default']) ?></li>
                                <li><?= $this->tag->linkTo(['admin/donations?page=' . $page->last, 'Last', 'onclick' => 'return false', 'style' => 'cursor:default']) ?></li>
                                </ul>
                                <?php } ?>
                                <!---->
                              <?php } elseif ($page->total_pages == 0) { ?>
                              <ul class="pagination pull-right">
                              <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                              <li><?= $this->tag->linkTo(['admin/donations?page=' . $page->next, 'Next', 'onclick' => 'return false', 'style' => 'cursor:default']) ?></li>
                                <li><?= $this->tag->linkTo(['admin/donations?page=' . $page->last, 'Last', 'onclick' => 'return false', 'style' => 'cursor:default']) ?></li>
                                </ul>
                 <?php } ?>

                              <div class="clearfix"></div> 

                            </div>

              </div>

            </div>

          </div>

        </div>



              </div>
    </form>

</div>

<!-- Matter ends -->