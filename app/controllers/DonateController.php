<?php
use Phalcon\Validation\Validator\PresenceOf,
    Phalcon\Validation\Validator\Email,
    Phalcon\Validation\Validator\StringLength as StringLength;
class DonateController extends ControllerBase
{

    protected $breadCrumbs = "<a href='/'>Home</a> > ";
    protected $_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';

    public function initialize()
    {
        parent::initialize();
        date_default_timezone_set('Asia/Manila');
        $this->view->bread_crumbs = $this->breadCrumbs;
        $this->validateLoginVolunteer();
    }

    public function indexAction(){
        $this->view->bread_crumbs = $this->breadCrumbs .= 'Donate';
         $contact= Tblcontact::find();
         $this->view->contacts=$contact;

         $donatecontent=Tblother::findfirst("title='Donate'");
         $this->view->donatecontent=$donatecontent;

         $donateother=Tblother::findfirst("title='Donate Other Info'");
         $this->view->donateother=$donateother;
        //  'txn_id' =>  $txn_id,
        //  'fname' =>  $fname,
        //  'lname' =>  $lname,
        //  'phone' =>  $phone,
        //  'payer_email' => $payer_email,
        //  'payment_amount' => $payment_amount,
        //  'donation_date' => time()
        $this->view->donation = $donation = Tbldonations::findFirst();
        $donationArray = array();
        $donationArray[] = array(
            'txn_id' =>  $txn_id,
            'fname' =>  $fname,
            'lname' =>  $lname,
            'phone' =>  $phone,
            'payer_email' => $payer_email,
            'payment_amount' => $payment_amount,
            'donation_date' => time()
        );
    }

    public function paypalipnAction(){
        $postFields = 'cmd=_notify-validate';
        //$postFieldsTxt = '';

        foreach($_POST as $key => $value)
        {
            $postFields .= "&$key=".urlencode($value);
            //$postFieldsTxt .= "$key=".$value."\n";
        }

        $ch = curl_init();

        curl_setopt_array($ch, array(
            CURLOPT_URL => $this->_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postFields
        ));

        $result = curl_exec($ch);
        curl_close($ch);

        /*$fh = fopen('result.txt', 'w');
        fwrite($fh, $result . ' -- ' . $postFieldsTxt);*/
        fclose($fh);

        if($result == 'VERIFIED'){
            $payment_amount   = $_POST['mc_gross'];
            $txn_id           = $_POST['txn_id'];
            $fname            = !empty($_POST['first_name'])?$_POST['first_name']:null;
            $lname            = !empty($_POST['last_name'])?$_POST['last_name']:null;
            $phone            = !empty($_POST['contact_phone'])?$_POST['contact_phone']:null;
            $payer_email      = $_POST['payer_email'];

            $donate = new Tbldonations();
            $donate->assign(array(
                    'txn_id' =>  $txn_id,
                    'fname' =>  $fname,
                    'lname' =>  $lname,
                    'phone' =>  $phone,
                    'payer_email' => $payer_email,
                    'payment_amount' => $payment_amount,
                    'donation_date' => time()
                ));
            $donate->save();
        }
        //echo $result;
    }

    public function canceledAction(){

    }

    public function successAction(){

    }

}
