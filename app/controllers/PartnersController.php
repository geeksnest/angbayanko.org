<?php

class PartnersController extends ControllerBase
{

    protected $breadCrumbs = "<a href='/'>Home</a> > Partners";
    public function initialize()
    {
        parent::initialize();
        date_default_timezone_set('Asia/Manila');
        $this->view->bread_crumbs = $this->breadCrumbs;
        $this->validateLoginVolunteer();
    }

    public function indexAction()
    {
       $contact= Tblcontact::find();
       $this->view->contacts=$contact;

        $numberPage = $this->request->getQuery("page", "int");
        $numberPage = !empty($numberPage)?$numberPage:1;

        $phql = 'SELECT
                Tblpartners.partnerID,
                Tblpartners.partnerName,
                Tblpartners.partnerInfo
                FROM Tblpartners
                LEFT JOIN Tblusers
                ON Tblpartners.userID = Tblusers.userID
                WHERE userLevel = 2 AND userStatus ="active" ORDER BY dateCreated DESC';
        $result = $this->modelsManager->executeQuery($phql);

        $dataArray = array();
        foreach ($result as $key => $value) {
            $dataArray[] = array(
                'partnerID'=>$value->partnerID,
                'partnerName'=>$value->partnerName,
                'partnerInfo'=>$this->_truncateHtml($value->partnerInfo),
                );
        }

        $paginator = new Phalcon\Paginator\Adapter\NativeArray(array(
            "data" => $dataArray,
            "limit"=> 10,
            "page" => $numberPage
            ));
        $this->view->page = $paginator->getPaginate();
    }

    private function _getPartnerInfo($id){
        $phql = 'SELECT * FROM Tblpartners WHERE partnerID ='. $id;
        $result = $this->modelsManager->executeQuery($phql);
        if(!empty($result)){
            $this->view->partner = $result[0];
            return $result[0];
        }
        return null;
    }

    public function viewAction($id){
        $this->_getPartnerInfo($id);
        $this->view->tabs = $this->_tabs(null, $id);

       $about=Tblother::findfirst("title='Main Tagline'");
       $this->view->about=$about;
       $contact= Tblcontact::find();
       $this->view->contacts=$contact;
    }

    private function _tabs($active=null, $partnerID){
        $html = '<li ';
        $html .= (is_null($active))?'class="active">':'>';
        $html .= '<a href="/partners/view/'.$partnerID.'">Program Information</a></li>';
        $html .= '<li ';
        $html .= ($active == 'currentevents')?'class="active">':'>';
        $html .= '<a href="/partners/currentevents/'.$partnerID.'">Current Events</a></li>';
        $html .= '<li ';
        $html .= ($active == 'pictures')?'class="active">':'>';
        $html .= '<a href="/partners/pictures/'.$partnerID.'">Pictures</a></li>';
        $html .= '
            <li><a href="">Partner\'s Forum</a></li>
            <li><a href="">Please Donate</a></li>
        ';
        return $html;
    }

    public function currenteventsAction($partnerID){
        $this->view->partnerID = $partnerID;
        $auth = $this->session->get('auth');
        $this->view->userlevel = $auth['abk_userlevel'];
        $numberPage = $this->request->getQuery("page", "int");
        $numberPage = !empty($numberPage)?$numberPage:1;

        $phql = 'SELECT * FROM Tblpartnerevents WHERE partnerID = '.$partnerID.' ORDER BY eventDate DESC';
        $result = $this->modelsManager->executeQuery($phql);

        $dataArray = array();
        foreach ($result as $key => $value) {
            $dataArray[] = array(
                'eventID'=>$value->eventID,
                'eventTitle'=>$value->eventTitle,
                'eventDate'=>date("m-d-Y", $value->eventDate),
                'eventDetails'=>$this->_truncateHtml($value->eventDetails),
                );
        }

        $paginator = new Phalcon\Paginator\Adapter\NativeArray(array(
            "data" => $dataArray,
            "limit"=> 10,
            "page" => $numberPage
            ));
        $this->view->page = $paginator->getPaginate();

        $this->_getPartnerInfo($partnerID);
        $this->view->tabs = $this->_tabs('currentevents', $partnerID);
    }

    public function vieweventAction($eventID){

        $this->view->event = $event = Tblpartnerevents::findFirst('eventID='.$eventID);

        $this->_getPartnerInfo($event->partnerID);
        $this->view->tabs = $this->_tabs('currentevents', $event->partnerID);
    }

    public function picturesAction($partnerID){
        $this->view->partnerID = $partnerID;
        $numberPage = $this->request->getQuery("page", "int");
        $numberPage = !empty($numberPage)?$numberPage:1;

        $phql = 'SELECT Tblpartnersalbums.albumID, Tblpartnersalbums.albumName, Tblpartnersalbums.dateCreated, Tblpartnerspictures.pictureFilename FROM Tblpartnersalbums
        LEFT JOIN Tblpartnerspictures ON Tblpartnerspictures.pictureID = Tblpartnersalbums.coverID
        WHERE Tblpartnersalbums.partnerID = '.$partnerID.' ORDER BY Tblpartnersalbums.dateCreated DESC';
        $result = $this->modelsManager->executeQuery($phql);

        $dataArray = array();
        foreach ($result as $key => $value) {
            $cover = null;
            if(empty($value->pictureFilename)){
                $lastPic = $this->modelsManager->executeQuery("SELECT pictureFilename FROM Tblpartnerspictures WHERE albumID = ".$value->albumID." ORDER BY dateUploaded DESC LIMIT 1");
                $cover = !empty($lastPic[0])?$lastPic[0]->pictureFilename:null;
                //$cover = null;
            }else{
                $cover = $value->pictureFilename;
            }

            if(!is_null($cover)){
                $dataArray[] = array(
                    'albumID'=>$value->albumID,
                    'albumName'=>$value->albumName,
                    'dateCreated'=>date("m-d-Y", $value->dateCreated),
                    'coverFileName'=>$this->url->get().'img/partnerspictures/'.$partnerID.'/'.$value->albumName.'/'.$cover
                );
            }
        }

        $paginator = new Phalcon\Paginator\Adapter\NativeArray(array(
            "data" => $dataArray,
            "limit"=> 15,
            "page" => $numberPage
            ));
        $this->view->page = $paginator->getPaginate();

        $this->_getPartnerInfo($partnerID);
        $this->view->tabs = $this->_tabs('pictures', $partnerID);
    }

    public function showpicturesAction($albumID){
        $this->view->albumID = $albumID;
        $numberPage = $this->request->getQuery("page", "int");
        $numberPage = !empty($numberPage)?$numberPage:1;

        $phql = 'SELECT Tblpartnersalbums.albumName,
        Tblpartnerspictures.pictureID,
        Tblpartnerspictures.partnerID,
        Tblpartnerspictures.albumID,
        Tblpartnerspictures.pictureFilename,
        Tblpartnerspictures.pictureCaption,
        Tblpartnerspictures.pictureSize,
        Tblpartnerspictures.dateUploaded,
        Tblpartnersalbums.albumName
        FROM Tblpartnerspictures
        LEFT JOIN Tblpartnersalbums ON Tblpartnersalbums.albumID = Tblpartnerspictures.albumID
        WHERE Tblpartnerspictures.albumID = '.$albumID;
        $result = $this->modelsManager->executeQuery($phql);

        $dataArray = array();
        foreach ($result as $key => $value) {
            $dataArray[] = array(
                'thumbPath'=>$this->url->get().'img/partnerspictures/'.$value->partnerID.'/'.$value->albumName.'/thumbnail/'.$value->pictureFilename,
                'picturePath'=>$this->url->get().'img/partnerspictures/'.$value->partnerID.'/'.$value->albumName.'/'.$value->pictureFilename,
                'pictureCaption'=>$value->pictureCaption
                );
        }

        $paginator = new Phalcon\Paginator\Adapter\NativeArray(array(
            "data" => $dataArray,
            "limit"=> 15,
            "page" => $numberPage
            ));
        $this->view->page = $paginator->getPaginate();

        $this->_getPartnerInfo($result[0]->partnerID);
        $this->view->partnerID = $result[0]->partnerID;
        $this->view->albumName = $result[0]->albumName;
        $this->view->tabs = $this->_tabs('pictures', $result[0]->partnerID);
    }
}
