<?php

class PostController extends ControllerBase
{
    // protected $breadCrumbs = "Home > News";
    protected $breadCrumbs = "<a href='/'>Home</a> > News";
    public function initialize()
    {
        parent::initialize();
        date_default_timezone_set('Asia/Manila');
        $this->view->announcements = $this->_getSideBarAnnouncements();
        $this->view->moreInfoLinks = $this->_getPagesLinks();
        $this->view->programLinks = $this->_getProgramsLinks();
        $this->view->specialPagesLinks = $this->_getPagesLinks(1);

    	$progs = Tblprograms::find();
    	foreach ($progs as $p => $v) {
    		$this->view->setVar("prog_menu".$v->programID,
    			array(
    				'id' => $v->programID,
    				'title' => $v->programName,
    				'tagline' => $v->programTagline,
    				'url' => $v->programPage,
    				'banner' => $v->programBanner
    				));
    	}
        $this->validateLoginVolunteer();
    }
    public function indexAction($slug)
    {

    }
    public function newsAction($slug){


    	$this->view->bread_crumbs = $this->breadCrumbs;
    	$post = Tblpost::findFirst('postSlug="'.$slug.'"');
    	$this->view->postNone = false;
    	if($post){
    		$this->view->post = $post;
    		$this->view->postNone = true;
    	}else{

			$this->flash->error('News not found!');
    	}
    }
}
