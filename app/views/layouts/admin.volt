<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <!-- Title and other stuffs -->
  {{ get_title() }}
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">

  <!-- Stylesheets -->
  <script type="text/javascript" src="https://www.google.com/jsapi"></script>
  <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
  <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  {{ javascript_include('js/sparklines.js') }}
  {{ stylesheet_link('css/bootstrap.css') }}
  {{ stylesheet_link('css/font-awesome.css') }}
  {#{{ stylesheet_link('css/bootstrap-responsive.css') }}#}
  {{ stylesheet_link('css/bootstrap-select.css') }}
  {{ stylesheet_link('css/jquery-ui.css') }}
  {{ stylesheet_link('css/fullcalendar.css') }}
  {{ stylesheet_link('css/prettyPhoto.css') }}
  {{ stylesheet_link('css/rateit.css') }}
  {{ stylesheet_link('css/fullcalendar.css') }}
  {{ stylesheet_link('css/bootstrap-datetimepicker.min.css') }}
  {{ stylesheet_link('css/bootstrap-timepicker.min.css') }}
  {{ stylesheet_link('css/bootstrap-switch.css') }}
  {{ stylesheet_link('css/style/widgets.css') }}
  {{ stylesheet_link('css/jquery.fileupload.css') }}
  {{ stylesheet_link('css/abkadmin/style.css') }}
  {{ stylesheet_link('css/chosen.css') }}
  {{ stylesheet_link('css/font.css') }}
  <!-- HTML5 Support for IE -->
  <!--[if lt IE 9]>
  <script src="js/html5shim.js"></script>
  <![endif]-->

  <!-- Favicon -->
  <link rel="shortcut icon" href="{{url.getBaseUri()}}img/favicon/favicon.ico">
  <script>
  function displayDate() {
    var months = new Array(12);
    months[0] = "January";
    months[1] = "February";
    months[2] = "March";
    months[3] = "April";
    months[4] = "May";
    months[5] = "June";
    months[6] = "July";
    months[7] = "August";
    months[8] = "September";
    months[9] = "October";
    months[10] = "November";
    months[11] = "December";

    // var current_date = new Date();
    // month_value = current_date.getMonth();
    // day_value = current_date.getDate();
    // year_value = current_date.getFullYear();
    // var today = months[month_value]+' '+day_value+', '+year_value;
    // document.getElementById("today").value = today;
    
    
  }

  // keep selected option
  $(function() {
    if (localStorage.getItem('form_frame')) {
      $("#form_frame option").eq(localStorage.getItem('form_frame')).prop('selected', true);
    }

    $("#form_frame").on('change', function() {
      localStorage.setItem('form_frame', $('option:selected', this).index());
    });
  });

  $("#reset").on("click", function () {
    $('#form_frame option').prop('selected', function() {
      return this.defaultSelected;
    });
  });
  </script>


</head>

<body>

  <div class="navbar navbar-fixed-top bs-docs-nav" role="banner">

    <div class="conjtainer">
      <!-- Menu button for smallar screens -->
      <div class="navbar-header">
        <button class="navbar-toggle btn-navbar" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
          <span>Menu</span>
        </button>
        <!-- Site name for smallar screens -->
        <a href="index.html" class="navbar-brand hidden-lg">ABK CMS</a>
      </div>

      <!-- Navigation starts -->
      <nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
        <div class="col-md-3 header-logo">
          {{ image("img/admintoppart.png") }}
        </div>
        <!-- Links -->
        <ul class="nav navbar-nav pull-right">
          <li class="dropdown pull-right">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
              <i class="icon-user"></i> {{ abk_user }} <b class="caret"></b>
            </a>

            <!-- Dropdown menu -->
            <ul class="dropdown-menu">
              <li><a href="{{ url('admin/editprofile') }}"><i class="icon-user"></i> Profile</a></li>
              <!-- <li class="<?php if($abk_userlevel != 1) echo "hide"; ?>"><a href="/admin/settings"><i class="icon-cogs"></i> Settings</a></li> -->
              <!-- <li><a href="/manual/ABKusermanual.pdf" download="ABK users manual"><i class="icon-question-sign"></i> Help</a></li> -->
              <li><a href="/manual/ABKusermanual.pdf" data="/manual/ABKusermanual.pdf" type="application/pdf"><i class="icon-question-sign"></i> Help</a></li>
              <li><a href="{{ url('admin/logout') }}"><i class="icon-off"></i> Logout</a></li>
            </ul>
          </li>

        </ul>
      </nav>

    </div>

  </div>


  <!-- Header starts -->
  <header>
    <div class="container">
      <div class="row">

        <!-- Button section -->
        <div class="col-md-5">

          <!-- Buttons -->
          <ul class="nav nav-pills">

            <!-- Comment button with number of latest comments count -->
            <!-- set data-toggle to "dropdown" to show menu -->
            <li class="dropdown dropdown-big">
              <a class="dropdown-toggle" style="color: #FFF; background: #428bca;" href="https://www.facebook.com/Ang-Bayan-Ko-Foundation-1664040513867744/" data-toggle="">
                <i class="icon-facebook-sign"></i> Facebook <span style="color: #428bca; background: #FFF; margin-left: 8px;" class="label">{{ fbLikes }}</span>
              </a>

              <ul class="dropdown-menu">
                <li>
                  <!-- Heading - h5 -->
                  <h5><i class="icon-facebook-sign"></i> Timeline</h5>
                  <!-- Use hr tag to add border -->
                  <hr />
                </li>
                <li>
                  <!-- List item heading h6 -->
                  <h6><a href="#">Future Integration for ABK</a> <span class="label label-warning pull-right">10:42</span></h6>
                  <div class="clearfix"></div>
                  <hr />
                </li>
                <li>
                  <h6><a href="#">Future Integration for ABK</a> <span class="label label-warning pull-right">20:42</span></h6>
                  <div class="clearfix"></div>
                  <hr />
                </li>
                <li>Future Integration for ABK</a> <span class="label label-warning pull-right">14:42</span></h6>
                  <div class="clearfix"></div>
                  <hr />
                </li>
                <li>
                  <div class="drop-foot">
                    <a href="#">View All</a>
                  </div>
                </li>
              </ul>
            </li>

            <!-- Message button with number of latest messages count-->
            <li class="dropdown dropdown-big">
              <a class="dropdown-toggle" style="color: #FFF; background: #5bc0de;" href="https://twitter.com/abk_foundation" data-toggle="">
                <i class="icon-twitter-sign"></i> Twitter <span style="margin-left: 8px; color: #5bc0de; background: #FFF;" class="label">{{ followerCount }}</span>
              </a>

              <ul class="dropdown-menu">
                <li>
                  <!-- Heading - h5 -->
                  <h5><i class="icon-twitter-sign"></i> Tweets</h5>
                  <!-- Use hr tag to add border -->
                  <hr />
                </li>
                <li>
                  <!-- List item heading h6 -->
                  <h6><a href="#">Future Integration for ABK?</a></h6>
                  <!-- List item para -->
                  <p>Quisque eu consectetur erat eget  semper...</p>
                  <hr />
                </li>
                <li>
                  <h6><a href="#">Future Integration for ABK?</a></h6>
                  <p>Quisque eu consectetur erat eget  semper...</p>
                  <hr />
                </li>
                <li>
                  <div class="drop-foot">
                    <a href="#">View All</a>
                  </div>
                </li>
              </ul>
            </li>

            <!-- Members button with number of latest members count -->
            <li class="dropdown dropdown-big">
              <a class="dropdown-toggle" style="color: #FFF; background: #ff5d5e;" href="https://plus.google.com/u/0/101853314610354158387" data-toggle="">
                <i class="icon-google-plus-sign"></i> Google Plus <span style="margin-left: 8px; color: #ff5d5e; background: #FFF;" class="label">{{ plusonersCount }}</span>
              </a>

              <ul class="dropdown-menu">
                <li>
                  <!-- Heading - h5 -->
                  <h5><i class="icon-google-plus-sign"></i> Timeline</h5>
                  <!-- Use hr tag to add border -->
                  <hr />
                </li>
                <li>
                  <!-- List item heading h6-->
                  <h6><a href="#">Future Integration for ABK</a> <span class="label label-warning pull-right">Free</span></h6>
                  <div class="clearfix"></div>
                  <hr />
                </li>
                <li>
                  <h6><a href="#">Future Integration for ABK</a> <span class="label label-important pull-right">Premium</span></h6>
                  <div class="clearfix"></div>
                  <hr />
                </li>
                <li>
                  <h6><a href="#">Future Integration for ABK</a> <span class="label label-warning pull-right">Free</span></h6>
                  <div class="clearfix"></div>
                  <hr />
                </li>
                <li>
                  <div class="drop-foot">
                    <a href="#">View All</a>
                  </div>
                </li>
              </ul>
            </li>

          </ul>

        </div>


        <!-- Logo section -->
        <div class="col-md-3 pull-right text-right">
          <!-- Logo. -->
          <div class="logo">
            <h1><a href="#"><span id="currentDate" class="bold">Admin</span></a></h1>
            <p class="meta" id="currentDateDay">something goes in meta area</p>
          </div>
          <!-- Logo ends -->
        </div>

        <!-- Counts section -->

        <!-- <div class="col-md-5 pull-right">
        <div class="header-data"> -->

        <!-- Members data -->
        <!-- <div class="hdata">
        <div class="mcol-left"> -->
        <!-- Icon with blue background -->
        <!-- <i class="icon-user bblue"></i>
      </div>
      <div class="mcol-right"> -->
      <!-- Number of visitors -->
      <!-- <p><a href="#">{# usersCount #}</a> <em>Users</em></p>
    </div>
    <div class="clearfix"></div>
  </div> -->

  <!-- Traffic data -->
  <!-- <div class="hdata">
  <div class="mcol-left"> -->
  <!-- Icon with red background -->
  <!-- <i class="icon-suitcase byellow"></i>
</div>
<div class="mcol-right"> -->
<!-- Number of visitors -->
<!-- <p><a href="#">{# partnerCount #}</a> <em>Partners</em></p>
</div>
<div class="clearfix"></div>
</div> -->

<!-- Traffic data -->
<!-- <div class="hdata">
<div class="mcol-left"> -->
<!-- Icon with red background -->
<!-- <i class="icon-male borange"></i>
</div>
<div class="mcol-right"> -->
<!-- Number of visitors -->
<!-- <p><a href="#">{# volCount #}</a> <em>Volunteers</em></p>
</div>
<div class="clearfix"></div>
</div> -->

<!-- revenue data -->
<!-- <div class="hdata">
<div class="mcol-left"> -->
<!-- Icon with green background -->
<!-- <i class="icon-money bgreen"></i>
</div>
<div class="mcol-right"> -->
<!-- Number of visitors -->
<!-- <p><a href="#">{# donateCount #}</a><em>Donations</em></p>
</div>
<div class="clearfix"></div>
</div>  -->

<!-- Traffic data -->
<!-- <div class="hdata">
<div class="mcol-left"> -->
<!-- Icon with red background -->
<!-- <i class="icon-envelope bred"></i>
</div>
<div class="mcol-right"> -->
<!-- Number of visitors -->
<!-- <p><a href="#">{# subsCount #}</a> <em>Subscribers</em></p>
</div>
<div class="clearfix"></div>
</div>

</div>
</div> -->



</div>
</div>
</header>

<!-- Header ends -->

<!-- Main content starts -->

<div class="content">

  <!-- Sidebar -->
  <div class="sidebar">
    <div class="sidebar-dropdown"><a href="#">Navigation</a></div>

    <!--- Sidebar navigation -->
    <!-- If the main navigation has sub navigation, then add the class "has_sub" to "li" of main navigation. -->
    <ul id="nav">
      <!-- Main menu with font awesome icon -->
      <li><a href="{{ url('admin') }}" {% if menu['dashboard'] %} class="open" {% endif %}><i class="icon-home"></i> Dashboard</a></li>
      <?php
      if (in_array('umrole', $userpages)){
        ?>
        <li class="has_sub"><a href="#" {% if menu['umrole'] %} class="open" {% endif %}><i class="icon-user-md"></i> Users  <span class="pull-right"><i class="icon-chevron-right"></i></span></a>
          <ul>
            <li><a href="{{ url("admin/createuser") }}">Create User</a></li>
            <li><a href="{{ url("admin/users") }}">All Users <span class="badge">{{ usersCount }}</span></a></li>
          </ul>
        </li>
        <?php
      }
      if (in_array('pmrole', $userpages)){
        ?>
        <li class="has_sub"><a href="#" {% if menu['pmrole'] %} class="open" {% endif %}><i class="icon-tasks"></i> Programs  <span class="pull-right"><i class="icon-chevron-right"></i></span></a>
          <ul>
            <li class="open"><a class="li-drop" href="{{ url("admin/programs/0") }}">Create Program</a></li>
            <li class="open"><a class="li-drop" href="{{ url("admin/programlist") }}">List of Programs  <span class="badge">{{ progCount }}</span></a></li>
            <li><a class="li-drop" href="{{ url('admin/suggestedprograms')}}">Suggested Programs <span class="badge">{{ suggestedCount }}</span></a></li>

          </ul>
        </li>
        <?php
      }
      if (in_array('postrole', $userpages)){
        ?>
        <li class="has_sub"><a href="#" {% if menu['postrole'] %} class="open" {% endif %}><i class="icon-quote-left"></i> News Post  <span class="pull-right"><i class="icon-chevron-right"></i></span></a>
          <ul>
            <li><a href="{{ url("admin/createpost") }}">Create News Post</a></li>
            <li><a href="{{ url("admin/allnewspost") }}">All News Post <span class="badge">{{ allnewspost }}</span></a></li>
          </ul>
        </li>

        <?php
      }
      if (in_array('annrole', $userpages)){
        ?>
        <li class="has_sub"><a href="#" {% if menu['annrole'] %} class="open" {% endif %}><i class="icon-bullhorn"></i> Announcements <span class="pull-right"><i class="icon-chevron-right"></i></span></a>
          <ul>
            <li><a href="{{ url("admin/createannocements") }}">Create Announcement</a></li>
            <li><a href="{{ url("admin/announcements") }}">All Announcements <span class="badge">{{ annCount }}</span></a></li>
          </ul>
        </li>

        <?php
      }
      if (in_array('partrole', $userpages)){
        ?>
        <li class="has_sub"><a href="#" {% if menu['partrole'] %} class="open" {% endif %}><i class="icon-suitcase"></i> Partners <span class="pull-right"><i class="icon-chevron-right"></i></span></a>
          <ul>
            {{ partnerSubMenu }}
          </ul>
        </li>
        <?php
      }
      if (in_array('townrole', $userpages)){
        ?>
        <li class="has_sub"><a href="{{ url("admin/towns?sort=dateAdded-desc") }}" {% if menu['townrole'] %} class="open" {% endif %}><i class="icon-map-marker"></i> Towns <span class="pull-right"><i class="icon-chevron-right"></i></span></a>
          <ul>
            <li><a href="{{ url("admin/pintown") }}">Pin Town</a></li>
            <li><a href="{{ url("admin/towns?sort=dateAdded-desc") }}">All Towns <span class="badge">{{ townscount }}</span></a></li>
          </ul>
        </li>
        <?php
      }
      if (in_array('conrole', $userpages)){
        ?>
        <li><a class="{% if menu['conrole'] %}open{% endif %}" href="{{ url('admin/inquiries') }}"><i class="icon-phone "></i> Contacts <span class="badge">{{ contactscount }}</span></a></li>
        <?php
      }
      if (in_array('vmrole', $userpages)){
        ?>
        <li><a class="{% if menu['vmrole'] %}open{% endif %}" href="{{ url("admin/volunteers") }}"><i class="icon-male"></i> Volunteers  <span class="badge">{{ volunteerscount }}</span></a></li>
        <?php
      }
      if (in_array('enmrole', $userpages)){
        ?>
        <li class="has_sub"><a class="{% if menu['enmrole'] %}open{% endif %}" href="{{ url("admin/newsletter") }}"><i class="icon-envelope"></i> Newsletter <span class="pull-right"><i class="icon-chevron-right"></i></span></a>
          <ul>
            <li><a href="{{ url("admin/createnewsletter") }}">Create Newsletter</a></li>
            <li><a href="{{ url("admin/managenewsletter") }}">Manage Newsletters <span class="badge">{{ letterCount }}</span></a></li>
            <li><a href="{{ url("admin/newsletter") }}">Subscribers <span class="badge">{{ subsCount }}</span></a></li>
          </ul>
        </li>
        <?php
      }
      if (in_array('pagerole', $userpages)){
        ?>
        <li class="has_sub"><a href="{{ url("admin/pages") }}" {% if menu['pagerole'] %} class="open" {% endif %}><i class="icon-bookmark-empty"></i> Pages <span class="pull-right"><i class="icon-chevron-right"></i></span></a>
          <ul>
            <li><a href="{{ url("admin/createpage") }}">Create Page</a></li>
            <li><a href="{{ url("admin/pages") }}">All Pages <span class="badge">{{ pagescount }}</span></a></li>
            <li><a href="{{ url("admin/other") }}">Other</a></li>
          </ul>
        </li>
        <?php
      }

      ?>

      <?php

      if (in_array('donrole', $userpages)){
        ?>
        <li><a href="{{ url("admin/donations") }}" {% if menu['donrole'] %} class="open" {% endif %}><i class="icon-money"></i> Donations</a>
        </li>
        <li class="has_sub"><a href="{{ url("admin/pages") }}" {% if menu['rescuerole'] %} class="open" {% endif %}><i class="icon-globe"></i> ABK RESCUE <span class="pull-right"><i class="icon-chevron-right"></i></span></a>
          <ul>
            <li>
              <span><a class="open"> Activities
                <span class="v-down"><i class="icon-chevron-down"></i></span>
              </a>
            </span>
            <ul id="dropme">
              <li><a href='{{ url("admin/abkrescreateactivity") }}'>&nbsp;&nbsp;&nbsp;&nbsp;
                - <strong> Create Activity </strong></a></li>
                <li><a href='{{ url("admin/abkresactivities") }}'>&nbsp;&nbsp;&nbsp;&nbsp;
                  - <strong> All Activities </strong><span class="badge">{{ activitiesCount }}</span></a></li>
                </ul>
              </li>
              <li><a href='{{ url("admin/abkresaboutus") }}'>About Us</a></li>
              <li><a href='{{ url("admin/abkresdonate") }}'>Donation</a></li>
              <li><a href='{{ url("admin/abkresexpert") }}'>Experties</a></li>
              <li>
                <span><a class="open"> Blog&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <span class="v-down"><i class="icon-chevron-down"></i></span>
                </a>
              </span>
              <ul>
                <li><a href='{{ url("admin/abkrescreateblog") }}'>&nbsp;&nbsp;&nbsp;&nbsp;
                  - <strong> Create Blog </strong></a></li>
                  <li><a href='{{ url("admin/allblogspost") }}'>&nbsp;&nbsp;&nbsp;&nbsp;
                    - <strong> Blog List </strong></a></li>
                  </ul>
                </li>




              <li><a href='{{ url("admin/abkrescueteam") }}'>Volunteer</a></li>
              <li>
                <span><a class="open"> Events&nbsp;&nbsp;&nbsp;
                  <span class="v-down"><i class="icon-chevron-down"></i></span>
                </a>
              </span>
              <ul>
                <li><a href='{{ url("admin/abkrescreateevents") }}'>&nbsp;&nbsp;&nbsp;&nbsp;
                  - <strong> Create Event </strong></a></li>
                  <li><a href='{{ url("admin/abkresevents") }}'>&nbsp;&nbsp;&nbsp;&nbsp;
                    - <strong> Event List </strong></a></li>
                  </ul>
                </li>


                <li><a href='{{ url("admin/abkrescuesettings") }}'>Settings</a></li>
              </ul>
            </li>


            <?php
          }
          if (in_array('extrole', $userpages)){
            ?>
            <li><a href="/admin/settings"><i class="icon-list"></i> Extras</a></li>
            <?php
          }
          ?>
        </ul>
      </div>

      <!-- Sidebar ends -->
<style type="text/css">

li{
  cursor: pointer;
}

</style>



      <!-- Main bar -->
      <div class="mainbar">
        {{ content() }}
      </div>

      <!-- Mainbar ends -->
      <div class="clearfix"></div>

    </div>
    <!-- Content ends -->

    <!-- Footer starts -->
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-md-12" style="margin-left: 20%">
            <!-- Copyright info -->
            <p class="copy">Copyright &copy; 2014 - Present | <a href="http://www.angbayanko.org">ABK Foundation</a> </p>
          </div>
        </div>
      </div>
    </footer>

    <!-- Footer ends -->

    <!-- Scroll to top -->
    <a href="#" class="totop" title="Back to top"><i class="glyphicon glyphicon-chevron-up" style="font-size: 20px;color: white;"></i></a>
    <script>
    var BASE_URL = "{{ url() }}";
    </script>

    <!-- JS -->
    {{ javascript_include('js/jquery.js') }}
    {{ javascript_include('js/bootstrap.js') }}
    {{ javascript_include('js/jquery-ui-1.9.2.custom.min.js') }}
    {{ javascript_include('js/fullcalendar.min.js') }}
    {{ javascript_include('js/jquery.rateit.min.js') }}
    {{ javascript_include('js/jquery.prettyPhoto.js') }}

    <!-- jquery fileuploader -->
    {{ javascript_include('js/vendor/jquery.ui.widget.js') }}
    {{ javascript_include('js/load-image.min.js') }}
    {{ javascript_include('js/canvas-to-blob.min.js') }}
    {{ javascript_include('js/jquery.iframe-transport.js') }}
    {{ javascript_include('js/jquery.fileupload.js') }}
    {{ javascript_include('js/jquery.fileupload-process.js') }}
    {{ javascript_include('js/jquery.fileupload-image.js') }}
    {{ javascript_include('js/jquery.fileupload-audio.js') }}
    {{ javascript_include('js/jquery.fileupload-video.js') }}
    {{ javascript_include('js/jquery.fileupload-validate.js') }}

    <!-- jquery Flot -->
    {{ javascript_include('js/excanvas.min.js') }}
    {{ javascript_include('js/jquery.flot.js') }}
    {{ javascript_include('js/jquery.flot.resize.js') }}
    {{ javascript_include('js/jquery.flot.pie.js') }}
    {{ javascript_include('js/jquery.flot.stack.js') }}

    <!-- Other JS-->
    {{ javascript_include('js/sparklines.js') }}
    {{ javascript_include('js/ckeditor/ckeditor.js') }}
    {{ javascript_include('js/ckeditor/adapters/jquery.js') }}
    {{ javascript_include('js/bootstrap-datetimepicker.min.js') }}
    {{ javascript_include('js/bootstrap-timepicker.min.js') }}
    {{ javascript_include('js/bootstrap-switch.min.js') }}
    {{ javascript_include('js/filter.js') }}
    {{ javascript_include('js/charts.js') }}
    {{ javascript_include('js/jquery.maskedinput.min.js') }}
    {{ javascript_include('js/admin/custom.js') }}
    {{ javascript_include('js/bootstrap-select.js') }}


    <?php if($menu['townrole'] && !empty($pintown) && $pintown){ ?>
      {{ javascript_include('https://maps.googleapis.com/maps/api/js?v=3.exp') }}
      <script>
      // In the following example, markers appear when the user clicks on the map.
      // The markers are stored in an array.
      // The user can then click an option to hide, show or delete the markers.
      var map;
      var markers = [];

      function initialize() {
        var haightAshbury = new google.maps.LatLng(14.594461297930993, 120.99428721289064);
        var mapOptions = {
          zoom: 8,
          center: haightAshbury,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById('map-canvas'),
        mapOptions);

        // This event listener will call addMarker() when the map is clicked.
        google.maps.event.addListener(map, 'click', function(event) {
          addMarker(event.latLng);
        });



        // Adds a marker at the center of the map.
        //addMarker(haightAshbury);
      }

      // Add a marker to the map and push to the array.
      function addMarker(location) {
        var marker = new google.maps.Marker({
          position: location,
          map: map,
          draggable:true,
          animation: google.maps.Animation.DROP
        });
        deleteMarkers();
        markers[0]=marker;

        /*document.getElementById("us2-lat").value = location.k;
        document.getElementById("us2-lon").value = location.D;*/
        document.getElementById("us2-lat").value = marker.getPosition().lat();
        document.getElementById("us2-lon").value = marker.getPosition().lng();

        google.maps.event.addListener(marker, 'dragend', function (event) {
          document.getElementById("us2-lat").value = this.getPosition().lat();
          document.getElementById("us2-lon").value = this.getPosition().lng();
        });
      }

      // Sets the map on all markers in the array.
      function setAllMap(map) {
        for (var i = 0; i < markers.length; i++) {
          markers[i].setMap(map);
        }
      }
      // Removes the markers from the map, but keeps them in the array.
      function clearMarkers() {
        setAllMap(null);
      }
      clearText = function(){
        document.getElementById('theText').value = "";
      }
      // Shows any markers currently in the array.
      function showMarkers() {
        setAllMap(map);
        document.getElementById('theText').value = "";
      }

      // Deletes all markers in the array by removing references to them.
      function deleteMarkers() {
        clearMarkers();
        markers = [];
        document.getElementById("us2-lat").value = "";
        document.getElementById("us2-lon").value = "";
      }

      function addDefaultMarker(){
        addMarker(map.getCenter());
      }

      google.maps.event.addDomListener(window, 'load', initialize);

      </script>
      <?php }elseif($menu['townrole'] && !empty($viewTown) && $viewTown){ ?>
        {{ javascript_include('https://maps.googleapis.com/maps/api/js?v=3.exp') }}
        <script type="text/javascript">
        function initialize() {
          var myLatlng = new google.maps.LatLng(<?php echo $townMarkers['townLat'].', '.$townMarkers['townLong'] ?>);
          var mapOptions = {
            zoom: 12,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            center: myLatlng
          }
          var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

          var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            title: 'Hello World!'
          });
        }

        google.maps.event.addDomListener(window, 'load', initialize);
        </script>

        <?php }elseif($menu['townrole'] && !empty($viewTownsMap) && $viewTownsMap){ ?>
          {{ javascript_include('https://maps.googleapis.com/maps/api/js?v=3.exp') }}
          <script type="text/javascript">
          var locations = [
            <?php
            if(!empty($townMarkers)){
              foreach ($townMarkers as $key => $value) {
                echo "['<a href=\"/admin/viewtown/".$value['townID']."\"><h2>".$value['townName']."</h2></a>".preg_replace('/[\n\r]/',"<br/>",$value['townInfo']) ." <a href=\"/admin/viewtown/".$value['townID']."\">Read More</a>', ".$value['townLat'].", ".$value['townLong']."],";
              }
            }
            ?>
          ];

          var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 7,
            center: new google.maps.LatLng(11.71233062544614, 122.83037730078127),
            mapTypeId: google.maps.MapTypeId.ROADMAP
          });

          var infowindow = new google.maps.InfoWindow({
            maxWidth: 350
          });

          var marker, i;

          for (i = 0; i < locations.length; i++) {
            marker = new google.maps.Marker({
              position: new google.maps.LatLng(locations[i][1], locations[i][2]),
              map: map
            });

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
              return function() {
                infowindow.setContent(locations[i][0]);
                infowindow.open(map, marker);
                window[e.type].innerHTML = e.containerPoint.toString() + ', ' + e.latlng.toString();
              }
            })(marker, i));
          }
          </script>
          <?php }elseif($menu['townrole'] && !empty($townPartners) && $townPartners){ ?>
            {{ javascript_include('js/chosen.jquery.min.js') }}
            <?php } ?>

          </body>
          <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-71209033-4', 'auto');
          ga('send', 'pageview');

          </script>
          <script type = "text/javascript">
          // from textBoxes.html
          function editcontact(){
            var textcontid = document.getElementById("contid");
            var txtOutput = document.getElementById("txtOutput");
            var name = textcontid.value;
            window.location="/admin/editcontact/"+name;
          } // end sayHi
          </script>

          <script type="text/javascript">
          var allSpan = document.getElementsByTagName('SPAN');
          for(var i = 0; i < allSpan.length; i++){
            allSpan[i].onclick=function(){
              if(this.parentNode){
                var childList = this.parentNode.getElementsByTagName('UL');
                for(var j = 0; j< childList.length;j++){
                  var currentState = childList[j].style.display;
                  if(currentState=="none" || currentState==""){
                    childList[j].style.display="block";
                  }else{
                    childList[j].style.display="none";
                  }
                }
              }
            }
          }
          </script>

          <script type="text/javascript">
          $(document).ready(function () {

            (function ($) {

              $('#filter').keyup(function () {

                var rex = new RegExp($(this).val(), 'i');
                $('.searchable tr').hide();
                $('.searchable tr').filter(function () {
                  return rex.test($(this).text());
                }).show();

              })

            }(jQuery));

          });
          </script>
          </html>
