 <div id="modalPrompt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div id="headerColor">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color: white;font-size: 17px">×</button>
        <span  style="color: white;font-size: 17px" id="changewarning"></span>
      </div>
      <div class="modal-body">
        <p class="modal-message"></p>
        <span class="modal-list-names"></span>
      </div>
      <div class="modal-footer">
<!--                     <a href="#" type="button" class="btn btn-primary modal-btn-yes" data-form='userform'>Yes</a>
        <a type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancel</a> -->
    <a href="#" type="button" class="btn btn-primary modal-btn-yes" data-form='userform'><span class="icon glyphicon glyphicon-ok"></span> Yes</a>
    <a type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><span class="icon glyphicon glyphicon-remove"></span> No</a>
      </div>
    </div>
  </div>
</div>

 <div id="modalPromptFeatureYes" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div id="headerColor">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color: white;font-size: 17px">×</button>
        <span  style="color: white;font-size: 17px" id="changewarning"></span>
      </div>
      <div class="modal-body">
        <p class="modal-message">Are you sure you want to deactivate this program?</p>
        <span class="modal-list-names"></span>
      </div>
      <div class="modal-footer">
<!--                     <a href="#" type="button" class="btn btn-primary modal-btn-yes" data-form='userform'>Yes</a>
        <a type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancel</a> -->
    <a href="" type="button" class="btn btn-primary modal-btn-yes" data-form='userform'><span class="icon glyphicon glyphicon-ok"></span> Yes</a>
    <a type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><span class="icon glyphicon glyphicon-remove"></span> No</a>
      </div>
    </div>
  </div>
</div>

 <div id="modalPromptFeatureNo" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div id="headerColor">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color: white;font-size: 17px">×</button>
        <span  style="color: white;font-size: 17px" id="changewarning"></span>
      </div>
      <div class="modal-body">
        <p class="modal-message"> Are you sure you want to active this program?</p>
        <span class="modal-list-names"></span>
      </div>
      <div class="modal-footer">
<!--                     <a href="#" type="button" class="btn btn-primary modal-btn-yes" data-form='userform'>Yes</a>
        <a type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancel</a> -->
    <a href="" type="button" class="btn btn-primary modal-btn-yes" data-form='userform'><span class="icon glyphicon glyphicon-ok"></span> Yes</a>
    <a type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><span class="icon glyphicon glyphicon-remove"></span> No</a>
      </div>
    </div>
  </div>
</div>

 <div id="modalPromptFeatureFull" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div id="headerColor">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color: white;font-size: 17px">×</button>
        <span  style="color: white;font-size: 17px" id="changewarning"></span>
      </div>
      <div class="modal-body">
        <p class="modal-message"> Featured Programs have reach the limit. Please uncheck atleast 1 feature to add.</p>
        <span class="modal-list-names"></span>
      </div>
      <div class="modal-footer">
         <!--            <a href="#" type="button" class="btn btn-primary modal-btn-yes" data-form='userform'>Yes</a> -->
        <a type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Okay</a>
   <!--  <a type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><span class="icon glyphicon glyphicon-remove"></span> Ok</a> -->
      </div>
    </div>
  </div>
</div>


            <div class="matter">

              <div class="container">

                {{ form('admin/programlist', 'id':'main-table-form') }}
                {{ hidden_field('csrf', 'id':'csrfToken', 'value': "")}}

                <div class="row">
                  <div class="col-md-2">
                    <label>Search by Text</label>
                  </div>
                  <div class="col-md-6 form-group">
                    <div class="col-md-12">
                      {{ text_field('search_text' , 'class':'form-control', 'id':'GG') }}
                    </div>
                  </div>
                  <div class="col-md-3 form-group">
                   <!--  {{ submit_button('Search', 'class':'btn btn-success') }} -->
                   <button type="submit" name="Search" class='btn btn-success'><span class="icon-search"></span> Search</button>
                  <a href="/admin/programlist"> <button type="button" name="clear_search" class="btn btn-danger" value="Clear Search"><span class="icon-refresh">
                   </span> Refresh</button></a>
                  </div>
                </div>

                <!-- Table -->
                <div class="row">

                  <div class="col-md-2">
                    <label>Filter by Date</label>
                  </div>
                  <div class="col-md-4 form-group start" id="fromDatepicker" class="input-append">
                    <label class="col-md-2 control-label">From</label>
                    <div class="col-md-10">
                      {{ text_field('fromDate', 'readonly': 'true', 'data-format':'yyyy-MM-dd','class':'form-control dtpicker', 'data-time-icon':'icon-time', 'id':'a') }}
                      <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="btn btn-info btn-lg icon-calendar " ></i>
                      </span>
                    </div>
                  </div>
                  <div class="col-md-3 form-group end" id="toDatepicker" class="input-append" style="margin-left: -8.5%;">
                    <label class="col-md-2 control-label">To</label>
                    <div class="col-md-12">
                      {{ text_field('toDate', 'readonly': 'true', 'data-format':'yyyy-MM-dd','class':'form-control dtpicker', 'data-time-icon':'icon-time','id':'b')}}
                      <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="btn btn-info btn-lg icon-calendar"></i>
                      </span>
                    </div>
                  </div>
                  <div class="col-md-2">
                   <!--  {{ submit_button('Filter', 'class':'btn btn-primary', 'name':'filter') }} -->
                   <!-- <button type="submit" class="btn btn-primary" name="btnFilter">Filter</button> -->
                    <button type="submit" name="filterSearch" class='btn btn-primary'>Filter</button>
                  </div>
                </div>

              </div>
            </div>


<div class="col-md-12">
  <label style="width: 100%; background-color: #ff9494; padding: 10px; margin-top: 10px; margin-bottom: -10px; display: none;" id="fullStatus">Featured Programs have reach the limit. Please uncheck atleast 1 feature to add.</label>
      <div class="widget">
        <div class="widget-head">
          <div class="pull-left">List of Programs</div>
            <div class="widget-icons pull-right">
            </div>  
          <div class="clearfix"></div>
        </div>
          <div class="widget-content referrer">
            {{ form('admin/programlist', 'class': 'form-horizontal', 'id':'main-table-form', 'method':'POST') }}
            <input type="hidden" class="tbl-action" name="action" value=""/>
            <input type="hidden" class="tbl-recordID" name="recordID" value=""/>
            <input type="hidden" class="tbl-edit-url" name="editurl" value=""/>
              <table class="table table-striped table-bordered table-hover">
                <thead>
                  <th>Title</th>
                  <th>SLUGS</th>
                  <th>Date Created</th>

                  <th style="width: 150px">Featured</th>
                  <th style="width: 200px;">Action</th>

                </thead>
                <tbody>   
                <?php if($prog == true){ ?>
                {% for user in page.items %}
                <tr>
                <td class="name">{{ user['programName'] }}</td>
                <td class="name">{{ user['programPage']}}</td>
                <!-- actually uses last update date in database -->
                <td class="name">{{ user['dateCreated']}}</td>
                <td class="name">
                <?php if($user['status'] == '1') { ?>
                      <span class='label label-success'>Active</span>
<!--                       <a href='#modalPromptFeatureYes'class='btn btn-xs btn-default modal-control-button' data-toggle='modal'
                      data-recorID='{{ p.programID }}'><i class='icon-ban-circle'></i></a> -->
                 <button type='submit' name='btnStatus' class='btn btn-xs btn-default' data-form='userform' value ="{{ user['programID'] }}">
                  <i class='icon-ban-circle'></i></button>

                <?php } else{ ?>

                      <span class='label label-default'>Inactive</span>

                     <!--  <button type='submit' name='btnStatus' class='btn btn-xs btn-default' data-form='userform' value ='{{ p.programID }}'>
                      <i class='icon-star'></i></button> -->

                      <!-- <button type='button' name='btnStatus' class='btn btn-xs btn-default' data-form='userform' value ='{{ p.programID }}'>
                      <i class='icon-star'></i></button> -->

              <?php  
                 if ($z >= 6) {
                  // echo "<a href='#modalPromptFeatureFull'";
                  echo "<button type='button' onclick='myFunction()'";

                }else{
                  echo "<button type='submit' ";
                }
                ?>

                name='btnStatus' class='btn btn-xs btn-success' data-form='userform' value ="{{ user['programID'] }}">
                <i class='icon-star'></i></button>
                   
                <?php  }
                ?>

                </td>
                <td>
               
               <!--  <a href="#modalPrompt" class="btn btn-xs btn-view modal-control-button" data-toggle="modal" data-action="manage" data-recorID="{{ p.programID }}"><i class="icon-star"></i></a>
               -->

              
                <center>
                  <a href="#modalPrompt" title="Manage" class="btn btn-xs btn-primary tbl_manage_row modal-control-button" data-toggle="modal" data-action="manage" data-recorID="{{ user['programID'] }}"><i class="glyphicon glyphicon-th-list"></i>  </a>
                  <a href="#modalPrompt" title="Edit" class="btn btn-xs btn-warning tbl_edit_row modal-control-button" data-toggle="modal" data-action="edit" data-recorID="programs/{{ user['programID'] }}"><i class="icon-pencil"></i>  </a>
                  <a href="#modalPrompt" title="Delete" class="btn btn-xs btn-danger tbl_delete_row modal-control-button" data-toggle="modal" data-action="delete" data-recorID="programs/{{ user['programID'] }}"><i class="fa fa-trash"></i> </a>
                </center>

                </td>
                </tr> 

                {% endfor %}      
                <?php } else {
                echo "<tr><td>No Programs Yet.</td></tr>";
                } ?>
                </tbody>
              </table>

              <div class="widget-foot">
                              {% set limit = 10 %}
                              {% set start = (limit * (page.current - 1)) + 1 %}
                              {% set end = (limit * (page.current-1)) + limit %}
                              {% if end > page.total_items %}
                              {% set end = page.total_items %}
                              {% endif %}
                              {% if limit %}
                              <div style="margin-left: 85.5%">
                                <span>&nbsp;Showing {{ start }} - {{ end  }} of {{ page.total_items }}</span>
                              </div>
                              {% endif %}
                              {% if page.items and page.total_pages > 0 %}
                                <ul class="pagination pull-right">
                                    <!---->

                                {% if page.current == 1 and page.total_pages >= 5 and page.total_pages > 0 %}
                                 <li><a href="" onclick="return false" style="cursor:default;">First</a></li>
                                 <li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                                  {% for index in 1..5 %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/programlist?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/programlist?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}


                                {% elseif page.current == 1 and page.total_pages < 5 and page.total_pages > 0 %}
                                <li><a href="" onclick="return false" style="cursor:default;">First</a></li>
                                <li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                                  {% for index in 1..page.total_pages %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/programlist?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/programlist?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% endif %}
                                <!---->
                                <!---->

                                {% if page.current != 1 and page.total_pages < 5 and page.total_pages > 0 %}
                        <!--         <li><a href="" onclick="return false" style="cursor:default;">First</a></li>
                                <li><a href="" onclick="return false" style="cursor:default;">Prev</a></li> -->
                                <li>{{ link_to("admin/programlist", 'First') }}</li>
                                <li>{{ link_to("admin/programlist?page=" ~ page.before, 'Prev') }}</li>
                                  {% for index in 1..page.total_pages %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/programlist?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/programlist?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}


                                {% elseif page.current != 1 and page.current < 4 and page.total_pages >= 5 %}
                                <li>{{ link_to("admin/programlist", 'First') }}</li>
                                <li>{{ link_to("admin/programlist?page=" ~ page.before, 'Prev') }}</li>
                                  {% for index in 1..5 %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/programlist?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/programlist?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}


                                {% elseif page.current >=4 and page.current+2 < page.total_pages%}
                                <li>{{ link_to("admin/programlist", 'First') }}</li>
                                <li>{{ link_to("admin/programlist?page=" ~ page.before, 'Prev') }}</li>
                                  {% for index in page.current-2..page.current+2 %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/programlist?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/programlist?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}


                                {% elseif page.current >=4 and (page.current+2 == page.total_pages or page.current+2 > page.total_pages) %}
                                <li>{{ link_to("admin/programlist", 'First') }}</li>
                                <li>{{ link_to("admin/programlist?page=" ~ page.before, 'Prev') }}</li>
                                  {% for index in page.total_pages-4..page.total_pages %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/programlist?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/programlist?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% endif %}
                                <!---->
                                <!---->
                                {% if page.current != page.last %}
                                <li>{{ link_to("admin/programlist?page=" ~ page.next, 'Next') }}</li>
                                <li>{{ link_to("admin/programlist?page=" ~ page.last, 'Last') }}</li>
                                </ul>
                                {% else %}
                                <li>{{ link_to("admin/programlist?page=" ~ page.next, 'Next','onclick':'return false','style':'cursor:default') }}</li>
                                <li>{{ link_to("admin/programlist?page=" ~ page.last, 'Last','onclick':'return false','style':'cursor:default') }}</li>
                                </ul>
                                {% endif %}
                                <!---->
                              {% elseif page.total_pages == 0 %}
                              <ul class="pagination pull-right">
                              <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                              <li>{{ link_to("admin/programlist?page=" ~ page.next, 'Next','onclick':'return false','style':'cursor:default') }}</li>
                                <li>{{ link_to("admin/programlist?page=" ~ page.last, 'Last','onclick':'return false','style':'cursor:default') }}</li>
                                </ul>
                              {% endif %}

                                <div class="clearfix"></div>

<script>
function myFunction() {
  // document.getElementById("fullStatus").show();
  document.getElementById('fullStatus').style.display = 'block';
}
</script>
            </form>
          </div>
      </div>
    </div>

