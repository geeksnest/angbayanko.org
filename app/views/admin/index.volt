                 <style type="text/css">
              #draftBtn{
                  background: none;
                  border: none;
                  outline: none;
                  color: white;
                  text-decoration:none;
                  cursor: pointer;

              }
              #draftBtn:hover {
              background-color:none;

                }

              </style>
      <!-- Page heading -->
      <div class="page-head">
        <h2 class="pull-left"><i class="icon-home"></i> Dashboard</h2>

        <!-- Breadcrumb -->
        <div class="bread-crumb pull-right">
          <a href="index.html"><i class="icon-home"></i> Home</a>
          <!-- Divider -->
          <span class="divider">/</span>
          <a href="#" class="bread-current">Dashboard</a>
        </div>

        <div class="clearfix"></div>

      </div>
      <!-- Page heading ends -->
      <!-- Matter -->

      <div class="matter">
<style>
  .counter{
    border-width: 2px;
    height: 60px;
    width: 32.5%;
    text-align: center;
    color: rgb(255, 255, 255);
    float: left;
    margin-bottom: 8px;
    border-radius:3px;
  }
  .cfirst{
    margin-left: 50px;
  }
  .counter i{
    color: whitesmoke;
    font-size: 40px;
    float: right;
    bottom: 0px;
    margin-right: 3px;
    margin-top: 20px;
    position: relative;
  }
  .countertext{
    margin-top: 15px;
    margin-left: 30px;
  }
  .counternum{
    font-size: 25px;
    font-weight: bold;
    color: white;
  }
  .counternum a{
    color: white;
    text-decoration: none;
  }
  .countertitle{
    font-size: 12px;
    margin-top: -5px;
  }
  .cusers{
    background-color:#5ca0d3;
  }
  .cdonations{
    background-color:#facf5a;
  }
  .cvolunteers{
    background-color: #257aa6;
  }
  .cpartners{
    background-color:#f09c67;
  }
  .ctowns{
    background-color:#621e81;
  }
  .cvisitors{
    background-color:#fd5f00;
  }
</style>
        <!-- counters -->
          <!-- users -->
            <div class="counter cusers" style="margin-left: 4px; margin-right:4px;">
                <i class="fa fa-user"></i>
                <div class="countertext">
                    <p class="counternum">{{ usersCount }}</p>
                    <p class="countertitle">users</p>
                </div>
            </div>
          
          <!-- donations -->
          <div class="counter cdonations" style="margin-left: 4px; margin-right:4px;">
              <i class="icon-money"></i>
              <div class="countertext">
                  <p class="counternum">{{ donationcount }}</p>
                  <p class="countertitle">donations</p>
              </div>
          </div>

          <!-- volunteers -->
          <div class="counter cvolunteers" style="margin-left: 4px; margin-right:4px;">
              <i class="fa fa-handshake-o"></i>
              <div class="countertext">
                  <p class="counternum">{{ volCount }}</p>
                  <p class="countertitle">volunteers</p>
              </div>
          </div>

          <!-- partners -->
          <div class="counter cpartners" style="margin-left: 4px; margin-right:4px;">
              <i class="icon-suitcase"></i>
              <div class="countertext">
                  <p class="counternum">{{ partnerCount }}</p>
                  <p class="countertitle">partners</p>
              </div>
          </div>

          <!-- towns -->
          <div class="counter ctowns" style="margin-left: 4px; margin-right:4px;">
              <i class="icon-map-marker"></i>
              <div class="countertext">
                  <p class="counternum">{{ townscount }}</p>
                  <p class="countertitle">towns</p>
              </div>
          </div>

          <!-- visitors -->
          <div class="counter cvisitors" style="margin-left: 4px; margin-right:4px;">
              <i class="fa fa-globe"></i>
              <div class="countertext">
                  <p class="counternum"><script language="JavaScript">var fhsh = document.createElement('script');var fhs_id_h = "3333362";
                      fhsh.src = "//freehostedscripts.net/ocount.php?site="+fhs_id_h+"&name=&a=1";
                      document.head.appendChild(fhsh);document.write("<span id='h_"+fhs_id_h+"' ></span>");
                      </script>                    
                  <p class="countertitle">visitors</p>
              </div>
          </div>

        <!-- <div class="container">
          <div class="row">
            <div class="col-md-8">
              <div class="widget">
                <div class="widget-head">
                  <div class="pull-left">Session Status</div>
                  <div class="widget-icons pull-right"> -->
                    <!-- <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>  -->
                    <!-- <a href="#" class="wclose"><i class="icon-remove"></i></a> -->
                  <!-- </div>
                  <div class="clearfix"></div>
                </div>
                <div class="widget-content">
                  <div class="padd">
                    <div id="auth-button"></div>
                    <div style="display:none;" id="view-selector"></div>
                    <div id="timeline"></div>
                  </div>
                </div>
              </div>
            </div> -->

              <!-- <div class="col-md-4">

              <div class="widget">

                <div class="widget-head">
                  <div class="pull-left">Today Status</div>
                  <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
                    <a href="#" class="wclose"><i class="icon-remove"></i></a>
                  </div>
                  <div class="clearfix"></div>
                </div> -->

                <!-- <div class="widget-content">
                  <div class="padd">
                    Visitors, pageview, bounce rate, etc., Sparklines plugin used
                    <ul class="current-status">
                       <li>
                        <span id="status1"></span> <span class="bold">Users : <span id="usersCount">N/A</span></span>
                      </li>
                      <li>
                        <span id="status2"></span> <span class="bold">New Sessions : <span id="newSessionsCount">N/A</span></span>
                      </li>
                      <li>
                        <span id="status3"></span> <span class="bold">Sessions : <span id="sessionsCount">N/A</span></span>
                      </li>
                      <li>
                        <span id="status4"></span> <span class="bold">Bounce Rate : <span id="bounceRateCount">N/A</span></span>
                      </li>
                      <li>
                        <span id="status5"></span> <span class="bold">Avg. Session Duration : <span id="AvgSessionDurationCount">N/A</span></span>
                      </li>
                      <li>
                        <span id="status6"></span> <span class="bold">Page Views : <span id="pageviewsCount">N/A</span></span>
                      </li>
                      <li>
                        <span id="status7"></span> <span class="bold">Pages / Session : <span id="pagesPerSectionCount">N/A</span></span>
                      </li>
                    </ul>

                  </div>
                </div> -->

              <!-- </div>

            </div>
          </div>
          </div>
          <div class="container"> -->

          <!-- Dashboard Graph starts -->
<style>
  .col-md-6 {
    width: 40%;
    margin-left: 30px;
  }
  #latestannouncements{
    width: 55%;
    margin-left: 0px;
  }
</style>
          <div class="row">
            <div class="col-md-6">
              <div class="widget">
                <div class="widget-head">
                  <div class="pull-left">Quick Announcement Post</div>
                  <div class="widget-icons pull-right">
                    <!-- <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>  -->
                  </div>
                  <div class="clearfix"></div>
                </div>
                <div class="widget-content">
                  <div class="padd">
                    <p><?php $this->flashSession->output(); ?></p>
                     <span id="conthide">{{ content() }}</span>
                     <script type="text/javascript">

                        $("#conthide").show();
                        setTimeout(function() {
                       $("#conthide").hide(); }, 3000);

                     </script>

                      <div class="form quick-post">
                                      <!-- Edit profile form (not working)-->
                                      <form class="form-horizontal" method="post">

                                          <!-- Title -->
                                          <div class="form-group">
                                            <div class="col-lg-3">
                                              <label for="title">Title</label>
                                               <span class="asterisk">*</span>
                                            </div>
                                            <div class="col-lg-9">
                                              {{ form.render('ann_title') }}
                                              <!-- <input class="form-control" id="title" type="text" name="title"> -->
                                              <span id="hidetitle">{{ TitleErr }}</span>
                                            </div>
                                          </div>
                                          <!-- Content -->
                                          <div class="form-group">
                                            <div class="col-lg-3">
                                              <label for="content">Content</label>
                                               <span class="asterisk">*</span>
                                            </div>
                                            <div class="col-lg-9">
                                              {{ form.render('ann_contentDash') }}

                                             <!--  <textarea class="form-control" id="content" name="content"></textarea> -->
                                             <span id="hidecontent">{{ ContentErr }} </span>
                                            </div>
                                          </div>
                                          <div class="form-group">
                                            <div class="col-lg-9">
                                              <label>Announcement Duration <span class="asterisk">*</span></label>
                                            </div>
                                          </div>

                                            <div class="form-group">
                                             <div class="col-lg-3">
                                              <label>From</label>
                                             </div>

                                            <div class="col-lg-9 pull-right">

                                              <span id="dateStart" class="start">
                                                <div class="input-group">

                                                {{ form.render('ann_start_dash') }}

                                              <span class="add-on input-group-btn" id="valdate">
                                                  <i class="btn btn-info btn-lg icon-calendar"></i>
                                              </span>

                                                </div>

                                              </span>
                                             <span id="valdate1"> {{ StartDateErr }} </span>
                                            </div>

                                          </div>

                                          <div class="form-group">
                                          <div class="col-lg-3">
                                               <label> To </label><!-- &nbsp;&nbsp;&nbsp;&nbsp; -->
                                          </div>
                                              <div class="col-lg-9 pull-right">

                                                <span id="dateEnd" class="end">

                                                  <div class="input-group">

                                                 {{ form.render('ann_end_dash') }}

                                                <span class="add-on input-group-btn" id="ed">

                                                  <i class="btn btn-info btn-lg icon-calendar"></i>

                                                 </span>

                                                  </div>

                                               </span>
                                              <span id="valend"> {{ EndDateErr }} </span>
                                             </div>
                                          </div>

                                              <!-- -->

                                          <!-- -->
                                          <!-- Cateogry -->
                                          <!-- <div class="form-group">
                                            <div class="col-lg-3">
                                              <label for="category">Category</label>
                                               <span class="asterisk">*</span>
                                            </div>
                                            <div class="col-lg-9">
                                                <select class="form-control" name="ann_cat">
                                                  <option value="">- Choose Cateogry -</option>
                                                  <option value="1">General</option>
                                                  <option value="2">News</option>
                                                  <option value="3">Media</option>
                                                  <option value="4">Funny</option>
                                                </select>
                                            </div>
                                          </div>               -->
                                          <!-- Tags -->
                                          <!-- <div class="form-group">
                                            <div class="col-lg-3">
                                              <label for="tags">Tags</label>
                                               <span class="asterisk">*</span>
                                            </div>
                                            <div class="col-lg-9">
                                              <input class="form-control" id="tags" type="text" name="ann_tag">
                                            </div>
                                          </div> -->

                                          <!-- Buttons -->
                                          <div class="form-group">
                                             <!-- Buttons -->
                       <div class="col-lg-offset-2 col-lg-9 pull-right">


                            <button type="submit" id="saveBtn"  class="btn btn-primary"><span class="glyphicon glyphicon-floppy-disk"></span> Publish </button>

                          <button class="btn btn-success">
                          <span class=" glyphicon glyphicon-floppy-disk"></span>
                          <input type="submit" name="ann_drafts"  value="Save Draft" id="draftBtn"   />
                          </button>
                          <button class="btn btn-danger" id="resetBtn" type="button">
                          <span class=" glyphicon glyphicon-refresh"></span> Reset
                        </button>

                      </div>
                </form>
                                    </div>



                  </div>
                  <!-- <div class="widget-foot"> -->
                    <!-- Footer goes here -->
                  <!-- </div> -->
                </div>
              </div>
            </div>

            <div class="">

              <div class="widget">

                <div class="widget-head">
                  <div class="pull-left">Visitor Status</div>
                  <div class="widget-icons pull-right">
                    <!-- <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>  -->
                  </div>
                  <div class="clearfix"></div>
                </div>

                <div class="widget-content">
                  <div class="padd">
                  <div id="piechart"></div>
                    <!-- Visitors, pageview, bounce rate, etc., Sparklines plugin used -->
                    <!-- <ul class="current-status">
                      <li>
                        <span id="status1"></span> <span class="bold">Visits : 2000</span>
                      </li>
                      <li>
                        <span id="status2"></span> <span class="bold">Unique Visitors : 1,345</span>
                      </li>
                      <li>
                        <span id="status3"></span> <span class="bold">Pageviews : <span id="pageviewsCount">N/A</span></span>
                      </li>
                      <li>
                        <span id="status4"></span> <span class="bold">Pages / Visit : 2000</span>
                      </li>
                      <li>
                        <span id="status5"></span> <span class="bold">Avg. Visit Duration : 2000</span>
                      </li>
                      <li>
                        <span id="status6"></span> <span class="bold">Bounce Rate : 2000</span>
                      </li>
                      <li>
                        <span id="status7"></span> <span class="bold">% New Visits : 2000</span>
                      </li>
                    </ul> -->

                  </div>
                </div>

              </div>

            </div>
          <!-- Dashboard graph ends -->
        </div>

        <div class="row">
            <div id="latestannouncements" class="col-md-6">
              <div class="widget">
                <div class="widget-head">
                  <div class="pull-left">Latest Announcements</div>
                  <div class="widget-icons pull-right">
                    <!-- <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>  -->
                  </div>
                  <div class="clearfix"></div>
                </div>
                <div class="widget-content">
                  <div class="padd">
                      <table class="table table-striped table-bordered table-hover tblusers">
                          <thead style="border-top: 1px solid #ddd">
                            <tr>
                              <!-- <th width="10">{{ check_field('select_all[]', 'class':'tbl_select_all') }}</th> -->
                              <th width="20%" style="border-left: 1px solid #ddd">Title</th>
                              <th width="40%">Description</th>
                              <th width="10%">Start</th>
                              <th width="10%">End</th>
                              <!-- <th width="5%">Status</th> -->
                              <!-- <th width="100">Amount</th>
                              <th width="200">Date</th> -->
                              <!-- <th width="10">Action</th> -->
                            </tr>
                          </thead>
                          <tbody>
                              <?php
                              if($page->items){
                                foreach ($page->items as $key => $value) {?>
          
                                  <tr>
                                    <!-- <td><input type="checkbox" name="tbl_id[]" class="tbl_select_row" value="<?php echo $value['volunteerID'] ?>"></td> -->
                                      <!-- <td class="name"><?php echo $value['txn_id'] ?>
                                      </td> -->
                                      <td style="border-left: 1px solid #ddd"><?php echo $value['annTitle'] ?></td>
                                      <td><?php echo $value['annDesc'] ?></td>
                                      <td><?php echo date("F j, Y", $value['annStart']) ?></td>
                                      <td><?php echo date("F j, Y", $value['annEnd']) ?></td>
                                      <!-- <td><?php echo $value['annStatus'] ?></td> -->
                                      <!-- <td>
                                        <center>
                                            <a title="Delete" href="#modalPrompt" class="btn btn-xs btn-danger tbl_delete_row modal-control-button" data-toggle="modal" data-action="delete" data-recorID="<?php echo $value['txn_id'] ?>"><i class="fa fa-trash"></i></a>
                                        </center>
                                      </td> -->
                                  </tr>
            
                              <?php
                                }
                              }else{
                                echo '
                                  <tr>
                                      <td  id="tdmess" colspan="6" align="center">NO ANNOUNCEMENTS FOUND</td>
                                  </tr>
                                ';
                              }
                              ?>
                          </tbody>
                          </table>
                          <br>  
                          <a class="btn btn-primary" href="/admin/announcements" style="float: right"><i class="fa fa-eye"></i>  View All Announcements</a>
                      <!-- <table class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <th width="2%">{{ check_field('select_all[]', 'class':'tbl_select_all') }}</th>
                              <th width="35%"><a href="?sort={{ titleHref }}">Title <i class="{{ titleIndicator ? titleIndicator : "" }}"></i></a></th>
                              <th><a href="?sort={{ statusHref }}">Status <i class="{{ statusIndicator ? statusIndicator : "" }}"></i></a></th>
                              <th><a href="?sort={{ dateHref }}">Date Created <i class="{{ dateIndicator ? dateIndicator : "" }}"></i></a></th>
                              <th><a href="?sort={{ startHref }}">Date Started <i class="{{ startIndicator ? startIndicator : "" }}"></i></a></th>
                              <th><a href="?sort={{ endHref }}">Date Ended <i class="{{ endIndicator ? endIndicator : "" }}"></i></a></th>
                              <th width="100px">Action</th>
                          </tr>
                          </thead>
                          <tbody>
                          {% if page.total_pages == 0 %}
                              <tr>
                                  <td colspan="7" align="center">No Records Found</td>
                              </tr>
                          {% else %}
                              {% for post in page.items %}
                                  <tr>
                                      <td><input type="checkbox" name="tbl_id[]" class="tbl_select_row" value="{{ post.annID}}"> </td>
                                      <td class="name">{{ post.annTitle }}</td>
                                      <td>
                                       {% if post['annStatus'] == "activated" %}
                                       <span class="label label-success">Active</span>
                                       <a href="#modalPrompt" class="btn btn-xs btn-default tbl_delete_row modal-control-button" data-toggle="modal" data-action="deactivate" data-recorID="{{ post.annID }}"><i class="icon-ban-circle"></i> </a>
                                        {% else %}
                                        <span class="label label-default">Inactive</span>
                                        <a href="#modalPrompt" class="btn btn-xs btn-default tbl_delete_row modal-control-button"  data-toggle="modal" data-action="activate" data-recorID="{{ post.annID }}"><i class="icon-ban-circle"></i> </a>
                                       {% endif %}
                                     </td>
                                      <td>{{ date("F j, Y", post.annDate) }}</td>
                                      <td>{{ date("F j, Y", post.annStart) }}</td>
                                      <td>{{ date("F j, Y", post.annEnd) }}
                                          <input type="text" name="samdate" value="{{ date(post.annEnd) }}">
                                      </td>
                                      <td>
                                        <center>
                                          <a title="View" style="font-size:30px" href="#modalView" data-toggle="modal" class="btn btn-xs btn-success modal-record-view" data-href="ajaxUserView/{{ post.annID }}/{{ post.annTitle }}/{{ post.annDate }}" ><i class="icon-ok"></i></a>
                                           <a  href="viewannouncement/{{ post.annID }}" class="btn btn-xs btn-success modal-record-view" data-href="ajaxUserView/{{ user.userID }}" ><i class="icon-ok"></i>View </a>
                                          <a title="Edit" href="#modalPrompt" class="btn btn-xs btn-warning tbl_edit_row modal-control-button" data-toggle="modal" data-action="edit" data-recorID="{{ post.annID }}"><i class="icon-pencil"></i></a>

                                          <a title="Delete" href="#modalPrompt" class="btn btn-xs btn-danger tbl_delete_row modal-control-button" data-toggle="modal" data-action="delete" data-recorID="{{ post.annID }}"><i class="fa fa-trash"></i></a>
                                     
                                        </center>


                                     </td>
                                  </tr>
                              {% endfor %}
                          {% endif %}

                          </tbody>
                      </table> -->
                <!-- <div class="widget-foot"> -->


                  <!-- {% set limit = 5 %}
                  {% set start = (limit * (page.current - 1)) + 1 %}
                  {% set end = (limit * (page.current-1)) + limit %}

                  {% if end > page.total_items %}
                  {% set end = page.total_items %}
                  {% endif %} -->
                  <!-- {% if limit %}
                  <div style="margin-left: 85.5%">
                    <span>&nbsp;Showing {{ start }} - {{ end  }} of {{ page.total_items }}</span>
                  </div>
                  {% endif %} -->
                  <!-- {% if page.items and page.total_pages > 0 %}
                    <ul class="pagination pull-right">
                        
                    {% if page.current == 1 and page.total_pages >= 5 and page.total_pages > 0 %}
                     <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                      {% for index in 1..5 %}
                        {% if page.current == index %}
                          <li>{{ link_to("admin/announcements?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                          {% else %}
                          <li>{{ link_to("admin/announcements?page=" ~ index, index) }}</li>
                        {% endif %}
                      {% endfor %}
                    {% elseif page.current == 1 and page.total_pages < 5 and page.total_pages > 0 %}
                    <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                      {% for index in 1..page.total_pages %}
                        {% if page.current == index %}
                          <li>{{ link_to("admin/announcements?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                          {% else %}
                          <li>{{ link_to("admin/announcements?page=" ~ index, index) }}</li>
                        {% endif %}
                      {% endfor %}
                    {% endif %} -->
                    
                    
                    <!-- {% if page.current != 1 and page.total_pages < 5 and page.total_pages > 0 %}
                    <li>{{ link_to("admin/announcements", 'First') }}</li>
                    <li>{{ link_to("admin/announcements?page=" ~ page.before, 'Prev') }}</li>
                      {% for index in 1..page.total_pages %}
                        {% if page.current == index %}
                          <li>{{ link_to("admin/announcements?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                          {% else %}
                          <li>{{ link_to("admin/announcements?page=" ~ index, index) }}</li>
                        {% endif %}
                      {% endfor %}
                    {% elseif page.current != 1 and page.current < 4 and page.total_pages >= 5 %}
                    <li>{{ link_to("admin/announcements", 'First') }}</li>
                    <li>{{ link_to("admin/announcements?page=" ~ page.before, 'Prev') }}</li>
                      {% for index in 1..5 %}
                        {% if page.current == index %}
                          <li>{{ link_to("admin/announcements?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                          {% else %}
                          <li>{{ link_to("admin/announcements?page=" ~ index, index) }}</li>
                        {% endif %}
                      {% endfor %}
                    {% elseif page.current >=4 and page.current+2 < page.total_pages%}
                    <li>{{ link_to("admin/announcements", 'First') }}</li>
                    <li>{{ link_to("admin/announcements?page=" ~ page.before, 'Prev') }}</li>
                      {% for index in page.current-2..page.current+2 %}
                        {% if page.current == index %}
                          <li>{{ link_to("admin/announcements?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                          {% else %}
                          <li>{{ link_to("admin/announcements?page=" ~ index, index) }}</li>
                        {% endif %}
                      {% endfor %}
                    {% elseif page.current >=4 and (page.current+2 == page.total_pages or page.current+2 > page.total_pages) %}
                    <li>{{ link_to("admin/announcements", 'First') }}</li>
                    <li>{{ link_to("admin/announcements?page=" ~ page.before, 'Prev') }}</li>
                      {% for index in page.total_pages-4..page.total_pages %}
                        {% if page.current == index %}
                          <li>{{ link_to("admin/announcements?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                          {% else %}
                          <li>{{ link_to("admin/announcements?page=" ~ index, index) }}</li>
                        {% endif %}
                      {% endfor %}
                    {% endif %}
                    
                    
                    {% if page.current != page.last %}
                    <li>{{ link_to("admin/announcements?page=" ~ page.next, 'Next') }}</li>
                    <li>{{ link_to("admin/announcements?page=" ~ page.last, 'Last') }}</li>
                    </ul>
                    {% else %}
                    <li>{{ link_to("admin/announcements?page=" ~ page.next, 'Next','onclick':'return false','style':'cursor:default') }}</li>
                    <li>{{ link_to("admin/announcements?page=" ~ page.last, 'Last','onclick':'return false','style':'cursor:default') }}</li>
                    </ul>
                    {% endif %}
                    
                  {% elseif page.total_pages == 0 %}
                  <ul class="pagination pull-right">
                  <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                  <li>{{ link_to("admin/announcements?page=" ~ page.next, 'Next','onclick':'return false','style':'cursor:default') }}</li>
                    <li>{{ link_to("admin/announcements?page=" ~ page.last, 'Last','onclick':'return false','style':'cursor:default') }}</li>
                    </ul>
     {% endif %} -->

                  <div class="clearfix"></div>

                </div>
                </div>
              </div>
            </div>
          </div>
        </div>
          <!-- Dashboard graph ends -->
        </div>






      </div>
        </div>


    <!-- Matter ends -->

<script>
(function(w,d,s,g,js,fjs){
  g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(cb){this.q.push(cb)}};
  js=d.createElement(s);fjs=d.getElementsByTagName(s)[0];
  js.src='https://apis.google.com/js/platform.js';
  fjs.parentNode.insertBefore(js,fjs);js.onload=function(){g.load('analytics')};
}(window,document,'script'));
</script>

<script>
google.load("visualization", "1", {packages:["corechart"]});
gapi.analytics.ready(function() {

  // Step 3: Authorize the user.

  var CLIENT_ID = '400725802740-2uvmhtb39kj53d4oc10b8e0oshibk4oo.apps.googleusercontent.com';

  var client = gapi.analytics.auth.authorize({
    container: 'auth-button',
    clientid: CLIENT_ID,
  });

  // Step 4: Create the view selector.

  var viewSelector = new gapi.analytics.ViewSelector({
    container: 'view-selector'
  });

  // Step 5: Create the timeline chart.

  var timeline = new gapi.analytics.googleCharts.DataChart({
    reportType: 'ga',
    query: {
      'dimensions': 'ga:date',
      'metrics': 'ga:sessions',
      'start-date': '30daysAgo',
      'end-date': 'yesterday',
    },
    chart: {
      type: 'LINE',
      container: 'timeline',
      options: {
        backgroundColor: 'transparent',
        width: '100%',
      }
    }
  });

  // Step 6: Hook up the components to work together.

  gapi.analytics.auth.on('success', function(response) {
    viewSelector.set().execute();
    function getData(response) {
      var jsonData = $.ajax({
        url: 'https://www.googleapis.com/analytics/v3/data/ga?ids=ga%3A113820133&start-date=3750daysAgo&end-date=yesterday&metrics=ga%3Ausers%2Cga%3Apageviews&access_token='+response.access_token,
        dataType: 'json',
        async: false
      }).responseText;
      var obj = $.parseJSON(jsonData);
      console.log(obj.totalsForAllResults['ga:pageviews']);
      // document.getElementById('pageviewsCount').innerHTML = obj.totalsForAllResults['ga:pageviews'];
    }
    google.setOnLoadCallback(drawChart(response.access_token));
    function drawChart(token) {
      var jsonData = function(){
        return $.ajax({
          url: 'https://www.googleapis.com/analytics/v3/data/ga?ids=ga%3A113820133&start-date=30daysAgo&end-date=today&metrics=ga%3Ausers&dimensions=ga%3AuserType&access_token='+token,
          dataType: 'json',
          async: false
        }).responseText;
      }
      var jsonData2 = function(){
        return $.ajax({
          url: 'https://www.googleapis.com/analytics/v3/data/ga?ids=ga%3A113820133&start-date=9daysAgo&end-date=today&metrics=ga%3Ausers%2Cga%3ApercentNewSessions%2Cga%3Asessions%2Cga%3AbounceRate%2Cga%3AavgSessionDuration%2Cga%3Apageviews%2Cga%3ApageviewsPerSession&dimensions=ga%3Adate&access_token='+token,
          dataType: 'json',
          async: false
        }).responseText;
      }
      var jsonData3 = function(){
        return $.ajax({
          url: 'https://www.googleapis.com/analytics/v3/data/ga?ids=ga%3A113820133&start-date=30daysAgo&end-date=today&metrics=ga%3Ausers%2Cga%3ApercentNewSessions%2Cga%3Asessions%2Cga%3AbounceRate%2Cga%3AavgSessionDuration%2Cga%3Apageviews%2Cga%3ApageviewsPerSession&dimensions=ga%3Adate&access_token='+token,
          dataType: 'json',
          async: false
        }).responseText;
      }
      var obj = $.parseJSON(jsonData());
      var obj2 = $.parseJSON(jsonData2());
      var obj3 = $.parseJSON(jsonData3());
      var nv = parseInt(obj.rows[0][1]);
      var rv = parseInt(obj.rows[1][1]);
      var data = google.visualization.arrayToDataTable([
        ['Stats', 'Counts'],
        ['New Visitor', nv],
        ['Returning Visitor', rv]
      ]);

      var usersC = [];
      var newSessionsC = [];
      var sessionsC = [];
      var bounceRateC = [];
      var AvgSessionDurationC = [];
      var pageviewsC = [];
      var pagesPerSectionC = [];
      document.getElementById('usersCount').innerHTML = obj3.totalsForAllResults['ga:users'];
      document.getElementById('newSessionsCount').innerHTML = Number(obj3.totalsForAllResults['ga:percentNewSessions']).toFixed(2);
      document.getElementById('sessionsCount').innerHTML = obj3.totalsForAllResults['ga:sessions'];
      document.getElementById('bounceRateCount').innerHTML = Number(obj3.totalsForAllResults['ga:bounceRate']).toFixed(2);
      document.getElementById('AvgSessionDurationCount').innerHTML = Number(obj3.totalsForAllResults['ga:avgSessionDuration']).toFixed(2);
      document.getElementById('pageviewsCount').innerHTML = obj3.totalsForAllResults['ga:pageviews'];
      document.getElementById('pagesPerSectionCount').innerHTML = Number(obj3.totalsForAllResults['ga:pageviewsPerSession']).toFixed(2);

      for(var i = 0; i < 10; i++) {
        usersC.push(parseInt(obj2.rows[i][1]));
        newSessionsC.push(Number(obj2.rows[i][2]).toFixed(2));
        sessionsC.push(parseInt(obj2.rows[i][3]));
        bounceRateC.push(Number(obj2.rows[i][4]).toFixed(2));
        AvgSessionDurationC.push(Number(obj2.rows[i][5]).toFixed(2));
        pageviewsC.push(parseInt(obj2.rows[i][6]));
        pagesPerSectionC.push(Number(obj2.rows[i][7]).toFixed(2));
      }

      $("#status1").sparkline(usersC, {
      type: 'line',
      width: '80',
      height: '20',
      lineColor: '#0ca5e7',
      fillColor: '#e5f3f9'});
      $("#status2").sparkline(newSessionsC, {
      type: 'line',
      width: '80',
      height: '20',
      lineColor: '#0ca5e7',
      fillColor: '#e5f3f9'});
      $("#status3").sparkline(sessionsC, {
      type: 'line',
      width: '80',
      height: '20',
      lineColor: '#0ca5e7',
      fillColor: '#e5f3f9'});
      $("#status4").sparkline(bounceRateC, {
      type: 'line',
      width: '80',
      height: '20',
      lineColor: '#0ca5e7',
      fillColor: '#e5f3f9'});
      $("#status5").sparkline(AvgSessionDurationC, {
      type: 'line',
      width: '80',
      height: '20',
      lineColor: '#0ca5e7',
      fillColor: '#e5f3f9'});
      $("#status6").sparkline(pageviewsC, {
      type: 'line',
      width: '80',
      height: '20',
      lineColor: '#0ca5e7',
      fillColor: '#e5f3f9'});
      $("#status7").sparkline(pagesPerSectionC, {
      type: 'line',
      width: '80',
      height: '20',
      lineColor: '#0ca5e7',
      fillColor: '#e5f3f9'});

      var options = {
        chartArea : { height: '100%' },
        backgroundColor: 'transparent',
        legend : { 'position': 'right', 'alignment' : 'center' }
      };

      var chart = new google.visualization.PieChart(document.getElementById('piechart'));

      chart.draw(data, options);
    }
  });

  viewSelector.on('change', function(ids) {
    var newIds = {
      query: {
        ids: 'ga:113820133', // View IDs eintragen
        metrics: 'ga:sessions',
        dimensions: 'ga:date',
        'start-date': '30daysAgo',
        'end-date': 'yesterday'
      }
    }
    timeline.set(newIds).execute();
  });
});



$(document).ready(function(){
  $('#resetBtn').on('click', function(){
    $('#ann_contentDash').val('');
    $('#ann_title').val('');
    $('#ann_start_dash').val('');
    $('#ann_end_dash').val('');
  });
});

</script>
