          <?php $token = $this->security->getToken() ?>

          <!-- Page heading -->
          <div class="page-head">
            <h2 class="pull-left">Towns</h2>

            <!-- Breadcrumb -->
            <div class="bread-crumb pull-right">
              <a href="/admin"><i class="icon-home"></i> Home</a>
              <span class="divider">/</span>
              <a href="/admin/towns?sort=dateAdded-desc"> Towns </a>
              <!-- Divider -->
              <span class="divider">/</span>
              <a href="" class="bread-current">View</a>
            </div>

            <div class="clearfix"></div>

          </div>
          <!-- Page heading ends -->

          <!-- Matter -->

          <div class="matter">
            {{ form('admin/viewtown/'~town.townID, 'class': 'form-horizontal', 'id':'main-table-form') }}
            <div class="container">

              <!-- Table -->

              <div class="row">



                <div class="col-md-12">

                  <div class="padd"><h2>{{ town.townName }}</h2></div>

                  {{ content() }}

                  <div class="widget">

                    <div class="widget-head">
                      <div class="pull-left">Town info</div>
                      <div class="widget-icons pull-right">
                      </div>
                      <div class="clearfix"></div>
                    </div>

                    <div class="padd">
                    {{ townTab }}
                    </div>

                    <div id="map-canvas" style="height:500px"></div>

                    <div class="widget-content">

                      <div class="padd">
                        <span class="pull-right">Map Coordinates - Latitude : {{ town.townLat }}, Longitutde : {{ town.townLong }}</span>

                        <div class="clearfix"></div>
                        <div>
                          {{ town.townInfo }}
                        </div>
                      </div>

                      <div class="widget-foot">
                       <!-- <a href="/admin/towns" class="btn btn-primary">Back to Town List</a> -->
                        <button class="btn btn-primary"> <a href="/admin/towns?sort=dateAdded-desc" style="text-decoration: none;color: white;"><span class="glyphicon glyphicon-circle-arrow-left"></span> Back to Town List</a></button>
                        <div class="clearfix"></div>
                      </div>

                    </div>

                  </div>


                </div>

              </div>


            </div>
          </form>
        </div>

<!-- Matter ends -->
