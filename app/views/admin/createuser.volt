	         <style type="text/css">
              #savepassBtn{
                  background: none;
                  border: none;
                  outline: none;
                  color: white;
                  text-decoration:none;
                  cursor: pointer;

              }
              #savepassBtn:hover {
              background-color:none;

                }


          </style>
					<script>
				if ( window.history.replaceState ) {
				window.history.replaceState( null, null, window.location.href );
				}
				</script>

      <!-- Page heading -->
	    <div class="page-head">
        <!-- Page heading -->
	      <h2 class="pull-left">
          <!-- page meta -->
          <span class="page-meta"><span class="glyphicon glyphicon-user
"></span>Users</span>
        </h2>


        <!-- Breadcrumb -->
        <div class="bread-crumb pull-right">
          <a href="/admin"><i class="icon-home"></i> Home</a>
          <!-- Divider -->
          <span class="divider">/</span>
          <a href="/admin/users">Users</a>
          <!-- Divider -->
          <span class="divider">/</span>
          <a href="" class="bread-current">Create</a>
        </div>

        <div class="clearfix"></div>

	    </div>
	    <!-- Page heading ends -->

	    <!-- Matter -->

	    <div class="matter">
        <div class="container">

          <div class="row">

            <div class="col-md-12">
              {{ content() }}
              {{ notif }}
              <div class="widget wgreen">

                <div class="widget-head">
                  <div class="pull-left">Create User</div>
                  <div class="widget-icons pull-right">
                  </div>
                  <div class="clearfix"></div>
                </div>
                <div class="widget-content">
                  <div class="padd">
                    <h6>User Account</h6>
                    <hr />
                    <!-- Form starts.  -->
                     {{ form('admin/createuser', 'class': 'form-horizontal') }}

                                <div class="form-group" id="uname">
                                  <div class="col-lg-2">
                                  <label class="">{{ form.label('username') }}</label>
                                  <span class="asterisk">*</span>
                                </div>
                                  <div class="col-lg-8">
                                    {{ form.render('username') }}
                                    <!-- {{ form.messages('username') }} -->
                                  <span id="userhide"> {{ usernameErr }} </span>
                                   <!--  <div class="label label-danger" id="valuser0"></div>
                                    <div class="label label-danger" id="valuser1"></div>
                                    <div class="label label-danger" id="valuser3"></div> -->
                                  </div>
                                 </div>

                               <div class="form-group">
                                  <div class="col-lg-2">
                                  <label class="">{{ form.label('email') }}</label>
                                  <span class="asterisk">*</span>
                                </div>
                                  <div class="col-lg-8">
                                    {{ form.render('email') }}
                               <!-- {{ form.messages('email') }} -->
                                  <span id="emailhide">  {{ emailErr }} </span>
                                  <div class="label label-danger" id="valemail"></div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label class="">{{ form.label('password') }}</label>
                                  <span class="asterisk">*</span>
                                </div>
                                  <div class="col-lg-8">
                                    {{ form.render('password') }}
                                   <span id="passhide"> {{ passErr }} </span>

                                 <!--    {{ form.messages('password') }} -->
                                    <!-- <div class="label label-danger" id="passerror"></div>
                                    <div class="label label-danger" id="spaceerror"></div> -->
                                  </div>
                                </div>

                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label class="">{{ form.label('repassword') }}</label>
                                  <span class="asterisk">*</span>
                                </div>
                                  <div class="col-lg-8">
                                    {{ form.render('repassword') }}
                                   <span id="repasshide"> {{ repassErr }} </span>

                                <!--     {{ form.messages('repassword') }} -->
                                    <!-- <div class="label label-danger" id="repasserror"></div> -->

                                  </div>
                                </div>
                                
                    <h6>User Profile</h6>
                    <hr />
                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label>{{ form.label('firstname') }}</label>
                                  <span class="asterisk">*</span>
                                </div>
                                  <div class="col-lg-8">
                                    {{ form.render('firstname') }}
                                    <!-- {{ form.messages('firstname') }} -->
                                  <span id="fnamehide">  {{ fnameErr }} </span>
                                  </div>
                                </div>

                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label>{{ form.label('lastname') }}</label>
                                  <span class="asterisk">*</span>
                                   </div>
                                  <div class="col-lg-8">
                                    {{ form.render('lastname') }}
                                  <!--   {{ form.messages('lastname') }} -->
                                  <span id="lnamehide">  {{ lnameErr }} </span>
                                  </div>
                                </div>

                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label>{{ form.label('middlename') }}</label>
                                </div>

                                  <div class="col-lg-8">
                                    {{ form.render('middlename') }}
                                    {{ form.messages('middlename') }}
                                  </div>
                                </div>

                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label>{{ form.label('address') }}</label>
                                 </div>
                                  <div class="col-lg-8">
                                    {{ form.render('address') }}
                                    {{ form.messages('address') }}
                                  </div>
                                </div>

                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label>{{ form.label('company') }}</label>
                                  </div>
                                  <div class="col-lg-8">
                                    {{ form.render('company') }}
                                    {{ form.messages('company') }}
                                  </div>
                                </div>

                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label>{{ form.label('contact') }}</label>
                                  <span class="asterisk">*</span>
                                  </div>
                                  <div class="col-lg-8">

                                    {{ form.render('contact') }}
                                    <script type="text/javascript">

                                    </script>
                                    <!--{{ form.messages('contact') }} -->
                                  <span id="contacthide">  {{ contactErr }} </span>
                                  <div class="label label-danger" id="valcontact"></div>
                                  </div>
                                </div>

                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label>{{ form.label('position') }}</label>
                                  </div>
                                  <div class="col-lg-8">
                                    {{ form.render('position') }}
                                    {{ form.messages('position') }}
                                  </div>
                                </div>

                    <h6>User Role</h6>
                    <hr />
                                 <div class="form-group">
                                  <div class="col-lg-2">
                                  <label>Roles</label>
                                  <span class="asterisk">*</span>
                                  </div>
                                  <div class="col-lg-8">
                                  {%  for role in tblroles %}
                                    <label class="checkbox-inline">
                                      {{ check_field(role.roleCode, 'value':role.roleCode,'id':'boxesChecked','onclick':'rolecheck(this)','class':'checkbox') }}
                                      {{ role.roleDescription }}
                                    </label> <br/>
                                  {% endfor  %}
                                <span id="rolehide">{{ roleError }}</span>
                                  </div>
                                </div>

                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label>Status <span class="asterisk">*</span></label>
                                   </div>
                                  <div class="col-lg-8">
                                    <div class="radio">
                                      <label>
                                      {{ radio_field('status', 'value':'active', 'checked':'checked') }}
                                        Active
                                      </label>
                                    </div>
                                    <div class="radio">
                                      <label>
                                        {{ radio_field('status', 'value':'deactivate') }}
                                        Deactivate
                                      </label>
                                    </div>
                                  </div>
                                </div>
                                    <hr/>
                                  {#{ form.render('csrf', ['value': security.getToken()]) }#}
                                  {#{ form.messages('csrf') }#}
                                <div class="form-group">
                                  <div class="col-lg-offset-1 col-lg-9">
                                  <!--   {{ submit_button('Save' ,'name':'submitbtn', 'id':'btnSubmit', 'class':'btn btn-primary','id':'savepassBtn') }} -->


															      <button name="createusr" type="submit" id="submitbtn" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-disk"></span> Save </button>

                                  <button type="reset" onclick="javascript:resetTextarea();" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Reset</button>
                                  </div>
                                </div>
                              </form>

                  </div>
                </div>
                  <!-- <div class="widget-foot"> -->
                    <!-- Footer goes here -->
                  <!-- </div> -->
              </div>

            </div>

          </div>

        </div>
		  </div>
