 <style type="text/css">
        
 /*a:hover,a:visited,a:link,a:active {

    text-decoration:none;
    background-color:none;
}*/
.nocolor:hover{
  color:#2a6496;
  text-decoration:none;
  background-color: none;
}

      </style> 
            <!-- Modal View-->
           <div id="modalView" class="modal fade modalView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" style="color: white;font-size: 17px" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" style="color: white;font-size: 17px">View Record</h4>
                  </div>
                  <div class="modal-body">
                    <p class="modal-message"></p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Close</button>
                  </div>
                </div>
              </div>
            </div>
            <!-- Modal Prompt-->
            <div id="modalPrompt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                <!--   <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Alert!</h4>
                  </div> -->
                  <div id="headerColor">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color: white;font-size: 17px">×</button>
                    <span  style="color: white;font-size: 17px" id="changewarning"></span>
                  </div>
                  <div class="modal-body">
                    <p class="modal-message"></p>
                    <span class="modal-list-names"></span>
                  </div>
                   <div class="modal-footer">
                    <a href="#" type="button" class="btn btn-primary modal-btn-yes" data-form='userform'><span class="icon glyphicon glyphicon-ok"></span> Yes</a>
                    <a type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><span class="icon glyphicon glyphicon-remove"></span> No</a>
                  </div>
                </div>
              </div>
            </div>
            <!-- Modal Prompt-->
            <div id="modalPrompt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Alert!</h4>
                  </div>
                  <div class="modal-body">
                    <p class="modal-message"></p>
                    <span class="modal-list-names"></span>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
                    <a href="#" type="button" class="btn btn-primary modal-btn-yes" data-form='userform'>Yes</a>
                  </div>
                </div>
              </div>
            </div>

            <!-- Page heading -->
            <div class="page-head">
              <h2 class="pull-left"><i class="icon-table"></i> Abk Rescue Team </h2>

              <!-- Breadcrumb -->
              <div class="bread-crumb pull-right">
                <a href="/admin"><i class="icon-home"></i> Home</a> 
                <!-- Divider -->
                <span class="divider">/</span> 
                <span>Activities</span>
              </div>

              <div class="clearfix"></div>

            </div>
            <!-- Page heading ends -->

            <!-- Matter -->

            <div class="matter">
              {{ form('admin/abkrescueteam', 'id':'main-table-form') }}
              <div class="container">

                <div class="row">
                <div class="col-sm-12">
                  <div class="col-md-2">
                    <label>Search by Text</label>
                  </div>
                  <div class="col-md-6 form-group">
                    <div class="col-md-12">
                      {{ text_field('search_text' , 'class':'form-control') }}
                    </div>
                  </div>
                  <div class="col-md-3 form-group">
                    <!-- {{ submit_button('Search', 'class':'btn btn-default') }} -->
                    <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-search"></span> Search</button>
                    <button type="submit" name="clear_search" class="btn btn-danger" value="Clear Search"><span class="icon-refresh"></span> Refresh</button>
                  </div>
                </div>
                  
                </div>

                <!-- Table -->
                <div class="row">
                <div class="col-sm-12">
                  <div class="col-sm-2">
                    <label>Filter by Date</label>
                  </div>
                  <div class="col-sm-4 form-group" id="fromDatepicker" class="input-append">
                    <label class="col-sm-2 control-label">From</label>
                    <div class="col-sm-12">
                      {{ text_field('fromDate', 'readonly': 'true', 'data-format':'yyyy-MM-dd','class':'form-control dtpicker', 'data-time-icon':'icon-time') }}
                      <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="btn btn-info btn-lg icon-calendar"></i>
                      </span>
                    </div>
                  </div>
                  <div class="col-sm-3 form-group" id="toDatepicker" class="input-append" style="margin-left: -8.5%;">
                    <label class="col-sm-2 control-label">To</label>
                    <div class="col-sm-12">
                      {{ text_field('toDate', 'readonly': 'true', 'data-format':'yyyy-MM-dd','class':'form-control dtpicker', 'data-time-icon':'icon-time', 'style':'display:inline-block') }}
                      <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="btn btn-info btn-lg icon-calendar"></i>
                      </span>
                    </div>
                  </div>
                  <div class="col-sm-2">
                    {{ submit_button('Filter', 'class':'btn btn-primary') }}
                  </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-12">
                    <div class="col-sm-12">
                    
                      <!-- {{ link_to('/admin/createvolunteer', '+ Add Volunteer', 'class':'btn btn-info') }} -->
                      <button class="btn btn-info" style="margin-left: 87%;"><a href="/admin/createvolunteer" style="text-decoration: none;color: white;"><span class="glyphicon glyphicon-plus"></span> Add Volunteer</a></button>
                    </div>
                  </div>
                </div>
            
                <!-- Table -->
                  {{ content() }}
                <div class="row">

                  <!-- <div class="form-group">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="control-label col-lg-3">Search</label>
                        <div class="col-lg-9"> 
                          {#{ text_field('search_text' , 'class':'form-control') }#}
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      {#{ submit_button('Search', 'class':'btn btn-default') }#}
                     <button type="submit" name="clear_search" class="btn btn-default" value="Clear Search"><span class="icon-refresh"></span></button>
                    </div>
                  </div>  -->

                  <div class="col-md-12">
                    {{ msg }}
                    <div class="widget">

                      <div class="widget-head">
                        <div class="pull-left">Volunteers</div>
                        <div class="clearfix"></div>
                      </div>


                      <div class="widget-content">
                        {{ hidden_field('csrf', 'value': security.getToken())}}
                        <input type="hidden" class="tbl-action" name="action" value=""/>
                        <input type="hidden" class="tbl-recordID" name="recordID" value=""/>
                        <input type="hidden" class="tbl-edit-url" name="editurl" value="editvolunteer/"/>
                        <table class="table table-striped table-bordered table-hover tblusers">
                          <thead>
                            <tr>
                              <!-- <th>{{ check_field('select_all[]', 'class':'tbl_select_all') }}</th> -->
                              <th><a href="?sort={{ titleHref }}" class="nocolor">Name <i class="{{ titleIndicator ? titleIndicator : "" }}"></i></a></th>
                              <th><a href="?sort={{ slugHref }}" class="nocolor">Email <i class="{{ slugIndicator ? slugIndicator : "" }}"></i></a></th>
                              <th><a href="?sort={{ orderHref }}" class="nocolor"> Contact <i class="{{ orderIndicator ? orderIndicator : "" }}"></i></a></th>
                              <th><a href="?sort={{ lastupdateHref }}" class="nocolor">Date Registered <i class="{{ lastupdateIndicator ? lastupdateIndicator : "" }}"></i></a></th>
                              <th width="6%">Action</th>
                            </tr>
                          </thead>
                          <tbody>
                        
                          <?php foreach ($page->items as $rescue) {
                            $deleteLink = '<a href="#modalPrompt" class="btn btn-xs btn-danger tbl_delete_row modal-control-button" data-toggle="modal" data-action="delete" data-recorID="'.$events['id'].'"><i class="icon-ok"></i> </a>';
                           ?> 
                          <tr>
                              <!-- <td>
                                '.$pageCheck.'
                              </td> -->
                              <td class="name"> <?php echo $rescue['fname'].' '.$rescue['lname']; ?> </td>
                              <td><?php echo $rescue['email']; ?></td>
                              <td><?php echo $rescue['contact']; ?></td>
                              <td><?php echo $rescue['date_created']; ?></td>
                              <td>
                               <center><a href="#modalPrompt" class="btn btn-xs btn-warning tbl_edit_row modal-control-button" data-toggle="modal" data-action="edit" data-recorID="<?php echo $rescue['id']; ?>"><i class="icon-pencil"></i></a>
                                <a href="#modalPrompt" class="btn btn-xs btn-danger tbl_delete_row modal-control-button" data-toggle="modal" data-action="delete" data-recorID="<?php echo $rescue['id']; ?>"><i class="icon-remove"></i></a></center>
                              </td>
                            </tr>
                          <?php } 
                          if(empty($rescue)){
                            echo '<tr><td colspan="6">No records found</td></tr>';
                          }
                          ?>
                          </tbody>
                          </table>

                          <div class="tblbottomcontrol" style="display:none">
                            <a href="#modalPrompt" class="tbl_delete_all" data-toggle="modal" data-action="delete_selected" > Delete all Selected </a> | 
                            <a href="#" class="tbl_unselect_all"> Unselect </a>
                          </div>

                            <div class="widget-foot">
                              {% set limit = 10 %}
                              {% set start = (limit * (page.current - 1)) + 1 %}
                              {% set end = (limit * (page.current-1)) + limit %}

                              {% if end > page.total_items %}
                              {% set end = page.total_items %}
                              {% endif %}
                              {% if limit %}
                              <div style="margin-left: 85.5%">
                                <span>&nbsp;Showing {{ start }} - {{ end  }} of {{ page.total_items }}</span>
                              </div>
                              {% endif %}
                              {% if page.items and page.total_pages > 0 %}
                                <ul class="pagination pull-right">
                                    <!---->
                                {% if page.current == 1 and page.total_pages >= 5 and page.total_pages > 0 %}                                
                                 <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                                  {% for index in 1..5 %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/abkrescueteam?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/abkrescueteam?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% elseif page.current == 1 and page.total_pages < 5 and page.total_pages > 0 %}
                                <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                                  {% for index in 1..page.total_pages %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/abkrescueteam?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/abkrescueteam?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% endif %}
                                <!---->
                                <!---->
                                {% if page.current != 1 and page.total_pages < 5 and page.total_pages > 0 %}
                                <li>{{ link_to("admin/abkrescueteam", 'First') }}</li>
                                <li>{{ link_to("admin/abkrescueteam?page=" ~ page.before, 'Prev') }}</li>
                                  {% for index in 1..page.total_pages %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/abkrescueteam?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/abkrescueteam?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% elseif page.current != 1 and page.current < 4 and page.total_pages >= 5 %}
                                <li>{{ link_to("admin/abkrescueteam", 'First') }}</li>
                                <li>{{ link_to("admin/abkrescueteam?page=" ~ page.before, 'Prev') }}</li>
                                  {% for index in 1..5 %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/abkrescueteam?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/abkrescueteam?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% elseif page.current >=4 and page.current+2 < page.total_pages%}
                                <li>{{ link_to("admin/abkrescueteam", 'First') }}</li>
                                <li>{{ link_to("admin/abkrescueteam?page=" ~ page.before, 'Prev') }}</li>
                                  {% for index in page.current-2..page.current+2 %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/abkrescueteam?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/abkrescueteam?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% elseif page.current >=4 and (page.current+2 == page.total_pages or page.current+2 > page.total_pages) %}
                                <li>{{ link_to("admin/abkrescueteam", 'First') }}</li>
                                <li>{{ link_to("admin/abkrescueteam?page=" ~ page.before, 'Prev') }}</li>
                                  {% for index in page.total_pages-4..page.total_pages %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/abkrescueteam?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/abkrescueteam?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% endif %}
                                <!---->
                                <!---->
                                {% if page.current != page.last %}                 
                                <li>{{ link_to("admin/abkrescueteam?page=" ~ page.next, 'Next') }}</li>
                                <li>{{ link_to("admin/abkrescueteam?page=" ~ page.last, 'Last') }}</li>
                                </ul>
                                {% else %}
                                <li>{{ link_to("admin/abkrescueteam?page=" ~ page.next, 'Next','onclick':'return false','style':'cursor:default') }}</li>
                                <li>{{ link_to("admin/abkrescueteam?page=" ~ page.last, 'Last','onclick':'return false','style':'cursor:default') }}</li>
                                </ul>
                                {% endif %}
                                <!---->
                              {% elseif page.total_pages == 0 %}
                              <ul class="pagination pull-right">
                              <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                              <li>{{ link_to("admin/abkrescueteam?page=" ~ page.next, 'Next','onclick':'return false','style':'cursor:default') }}</li>
                                <li>{{ link_to("admin/abkrescueteam?page=" ~ page.last, 'Last','onclick':'return false','style':'cursor:default') }}</li>
                                </ul>
                 {% endif %}

                              <div class="clearfix"></div> 

                            </div>

                          </div>

                        </div>

                        {{ link_to('/admin/abkresjointeam', 'Edit Join Team Text', 'class':'btn btn-warning') }}
                        
                      </div>

                    </div>


                  </div>

                </form>
              </div>
<script type="text/javascript">
  function loadModalActivities(name, email,address, city, country, state, zip, contact){
    console.log(name);
    $("#name").html(name);
    $("#email").html(email);
    $('#address').html(address);
    $('#city').html(city);
    $('#country').html(country);
    $('#state').html(state);
    $('#zip').html(zip);
    $('#contact').html(contact);
    //$('#contact').modal('show');
  }
</script>
<!-- Matter ends -->