
<!-- Modal Prompt-->
<div id="modalPrompt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">Alert</h4>
			</div> -->
			<div id="headerColor">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color: white;font-size: 17px">×</button>
                    <span  style="color: white;font-size: 17px" id="changewarning"></span>
                  </div>
			<div class="modal-body">
				<p class="modal-message"></p>
				{% if viewlist==false %} <strong>{{ inquiry.inqSubject }}</strong>{% endif %}
				<span class="modal-list-names"></span>
			</div>
			<div class="modal-footer">
				<button type="button" id="btnGo" class="btn btn-primary modal-btn-del" data-form='userform'><span class="glyphicon glyphicon-ok"></span> Yes</button>
				<button type="button" id="btncancel" class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove"></span> No</button>

			</div>
		</div>
	</div>
</div>
<!-- Page heading -->
<div class="page-head">
	<h2 class="pull-left"><i class="icon-table"></i> Contacts</h2>

	<!-- Breadcrumb -->
	<div class="bread-crumb pull-right">
		<a href="{{ url('admin') }}"><i class="icon-home"></i> Home</a>
		<!-- Divider -->
		<span class="divider">/</span>
		<a href="" class="bread-current">Contacts</a>
	</div>

	<div class="clearfix"></div>

</div>
<!-- Page heading ends -->

<!-- Matter -->
<div class="matter">
	{% if viewlist == true %}
		<input type="text" name="statusA" id="statusA" value="true" style="display:none;">
		{{ form('admin/inquiries', 'id':'main-table-form') }}
			<div class="container"><!--Container-->
				<!--Row 1-->
				<div class="row">
					<div class="col-md-1">
						<label>Search</label>
					</div>
					<div class="col-md-3 form-group">
			            <div class="col-md-12">
			              {{ text_field('search_text' , 'class':'form-control') }}
			            </div>
			        </div>
			            <div class="col-md-1">
			            	<label>Filter by Status</label>
			          	</div>
			          	<div class="col-md-3 form-group">
			          		<div class="col-md-12">
			          			<select id="form_frame" class="form-control" name="searchstat">
					            	<option value="status" selected="selected">-Status-</option>
					            	<option value='0'>New</option>
					            	<option value='1'>Reviewed</option>
					            </select>
			          		</div>
			          	</div>

		          <!--DIVIDER-->
		          <div class="col-md-3 form-group">
		            <!-- {{ submit_button('Search', 'class':'btn btn-default') }} -->
		            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-search"></span> Search</button>
		            <button type="button" name="clear_search" class="btn btn-danger" value="Clear Search" onclick="history.go(0)"><span class="icon-refresh"></span> Refresh</button>
	              </div>
				</div>
				<!--Row 1-->
				<!--Row 2-->
				<div class="row">
		          <div class="col-md-1">
		            <label>Filter by Date</label>
		          </div>
		          <div class="col-md-4 form-group start" id="fromDatepicker" class="input-append">
		            <label class="col-md-2 control-label">From</label>
		            <div class="col-md-10">
		              {{ text_field('fromDate', 'readonly': 'true', 'data-format':'yyyy-MM-dd','class':'form-control dtpicker', 'data-time-icon':'icon-time', 'id':'a') }}
		              <span class="add-on">
		                <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="btn btn-info btn-lg icon-calendar"></i>
		              </span>
		            </div>
		          </div>
		          <div class="col-md-3 form-group end" id="toDatepicker" class="input-append" style="margin-left: -8.5%;">
		            <label class="col-md-2 control-label">To</label>
		            <div class="col-md-12">
		              {{ text_field('toDate', 'readonly': 'true', 'data-format':'yyyy-MM-dd','class':'form-control dtpicker', 'data-time-icon':'icon-time', 'id':'b') }}
		              <span class="add-on">
		                <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="btn btn-info btn-lg icon-calendar"></i>
		              </span>
		            </div>
		          </div>

		          <div class="col-md-2">
		           {{ submit_button('Filter', 'name':'filter-date', 'class':'btn btn-danger', 'id':'filter-btn') }}
		            <!--  <button type="submit" class="btn btn-primary" name="filter-date">Filter</button> -->
		          </div>
		        </div>
		       {{ searchResultStat1 }} {{ searchResultStat }}
		        <!-- {{ deleteinq }} -->
				<div class="alert alert-success" id="deleteinq" style="display: none;"></div>
	            <!--Row 2-->
	            <!--Row 3-->
	            <div class="row">
					<div class="col-md-12">
						{% if msg %}
                        	<div class='alert alert-success'>
                          		{{ msg }}
                        	</div>
                      	{% endif %}
						<div class="widget">

							<div class="widget-head">
								<div class="pull-left">Inquiries</div>
								<div class="widget-icons pull-right">
								</div>
								<div class="clearfix"></div>
							</div>


							<div class="widget-content">
								{{ hidden_field('csrf', 'value': security.getToken())}}
								<input type="hidden" class="tbl-action" name="action" value=""/>
								<input type="hidden" class="tbl-recordID" name="recordID" value=""/>
								<input type="hidden" class="tbl-edit-url" name="editurl" value="editpage/"/>
								<table class="table table-striped table-bordered table-hover tblusers">
									<thead>
										<tr>
											<th>{{ check_field('select_all[]', 'class':'tbl_select_all') }}</th>
											<th><a href="?sort={{ statusHref }}">Status <i class="{{ statusIndicator ? statusIndicator : "" }}"></i></a></th>
											<th><a href="?sort={{ subjectHref }}">Subject <i class="{{ subjectIndicator ? subjectIndicator : "" }}"></i></a></th>
											<th><a href="?sort={{ senderHref }}">Sender Name <i class="{{ senderIndicator ? senderIndicator : "" }}"></i></a></th>
											<th><a href="?sort={{ emailHref }}">Email <i class="{{ emailIndicator ? emailIndicator : "" }}"></i></a></th>
											<th><a href="?sort={{ dateHref }}">Date Received <i class="{{ dateIndicator ? dateIndicator : "" }}"></i></a></th>
											<th><a href="?sort={{ replydateHref }}">Date Replied <i class="{{ replydateIndicator ? replydateIndicator : "" }}"></i></a></th>
											<th width="5%">Action</th>
										</tr>
									</thead>
									<tbody>

										{% if page.total_pages == 0 %}
										<tr>
											<td colspan="7" align="center">NO RECORDS FOUND</td>
										</tr>
										{% else %}
										{% for post in page.items %}
										<tr>
											<td><input type="checkbox" name="tbl_id[]" class="tbl_select_row" value="{{ post.inqID}}"> </td>
											<td>
												{% if post.inqStatus == 0 %}
												<span class="label label-success">New</span>
												{% else %}
												<span class="label label-warning">Reviewed</span>
												{% endif %}
											</td>
											<td class="name">

												{{ link_to('admin/inquiries/'~post.inqID, post.inqSubject) }}

											</td>
											<td>{{ post.inqSender }}</td>
											<td>{{ post.inqEmail }}</td>
											<td>{{ date("F j, Y h:ia", post.inqDate) }}</td>
											<td>
												{{ post.inqLatestReplyDate ? date("F j, Y h:ia", post.inqLatestReplyDate) : "Not Yet Replied" }}
												</td>
											<td>
												<center>												<button  title="Delete" data-target="#modalPrompt" class="btn btn-xs btn-danger tbl_delete_row modal-control-button" data-toggle="modal" data-action="delete" id="deletebtn" data-recorID="{{ post.inqID }}"><i class="fa fa-trash"></i></button>
												</center>
											</td>
										</tr>
										{% endfor %}
										{% endif %}
									</tbody>
								</table>

								<div class="tblbottomcontrol" style="display:none">
									<a href="#modalPrompt" class="tbl_delete_all" data-toggle="modal" data-action="delete_selected" > Delete all Selected </a> |
									<a href="#" class="tbl_unselect_all"> Unselect </a>
								</div>

								<div class="widget-foot">
                              {% set limit = 10 %}
                              {% set start = (limit * (page.current - 1)) + 1 %}
                              {% set end = (limit * (page.current-1)) + limit %}

                              {% if end > page.total_items %}
                              {% set end = page.total_items %}
                              {% endif %}
                              {% if limit %}
                              <div style="margin-left: 85.5%">
                                <span>&nbsp;Showing {{ start }} - {{ end  }} of {{ page.total_items }}</span>
                              </div>
                              {% endif %}
                              {% if page.items and page.total_pages > 0 %}
                                <ul class="pagination pull-right">
                                    <!---->
                                {% if page.current == 1 and page.total_pages >= 5 and page.total_pages > 0 %}
                                 <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                                  {% for index in 1..5 %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/inquiries?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/inquiries?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% elseif page.current == 1 and page.total_pages < 5 and page.total_pages > 0 %}
                                <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                                  {% for index in 1..page.total_pages %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/inquiries?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/inquiries?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% endif %}
                                <!---->
                                <!---->
                                {% if page.current != 1 and page.total_pages < 5 and page.total_pages > 0 %}
                                <li>{{ link_to("admin/inquiries", 'First') }}</li>
                                <li>{{ link_to("admin/inquiries?page=" ~ page.before, 'Prev') }}</li>
                                  {% for index in 1..page.total_pages %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/inquiries?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/inquiries?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% elseif page.current != 1 and page.current < 4 and page.total_pages >= 5 %}
                                <li>{{ link_to("admin/inquiries", 'First') }}</li>
                                <li>{{ link_to("admin/inquiries?page=" ~ page.before, 'Prev') }}</li>
                                  {% for index in 1..5 %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/inquiries?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/inquiries?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% elseif page.current >=4 and page.current+2 < page.total_pages%}
                                <li>{{ link_to("admin/inquiries", 'First') }}</li>
                                <li>{{ link_to("admin/inquiries?page=" ~ page.before, 'Prev') }}</li>
                                  {% for index in page.current-2..page.current+2 %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/inquiries?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/inquiries?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% elseif page.current >=4 and (page.current+2 == page.total_pages or page.current+2 > page.total_pages) %}
                                <li>{{ link_to("admin/inquiries", 'First') }}</li>
                                <li>{{ link_to("admin/inquiries?page=" ~ page.before, 'Prev') }}</li>
                                  {% for index in page.total_pages-4..page.total_pages %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/inquiries?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/inquiries?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% endif %}
                                <!---->
                                <!---->
                                {% if page.current != page.last %}
                                <li>{{ link_to("admin/inquiries?page=" ~ page.next, 'Next') }}</li>
                                <li>{{ link_to("admin/inquiries?page=" ~ page.last, 'Last') }}</li>
                                </ul>
                                {% else %}
                                <li>{{ link_to("admin/inquiries?page=" ~ page.next, 'Next','onclick':'return false','style':'cursor:default') }}</li>
                                <li>{{ link_to("admin/inquiries?page=" ~ page.last, 'Last','onclick':'return false','style':'cursor:default') }}</li>
                                </ul>
                                {% endif %}
                                <!---->
                              {% elseif page.total_pages == 0 %}
                              <ul class="pagination pull-right">
                              <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                              <li>{{ link_to("admin/inquiries?page=" ~ page.next, 'Next','onclick':'return false','style':'cursor:default') }}</li>
                                <li>{{ link_to("admin/inquiries?page=" ~ page.last, 'Last','onclick':'return false','style':'cursor:default') }}</li>
                                </ul>
                 {% endif %}

                              <div class="clearfix"></div>

                            </div>

							</div>

						</div>


					</div>

				</div>
	            <!--Row 3-->
			</div>
			<!--Container-->
		</form>
	{% else %}
<!--Container-->
	<input type="text" name="statusA" id="statusA" value="False" style="display:none;">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div id="messagesuc">{{ content() }}</div>
				<div class="widget">
					{{ form('admin/inquiries', 'class': 'form-horizontal', 'id':'main-table-form') }}
						<div class="widget-head"><!--1-->
							<div class="pull-left">Inquiry</div>
							<div class="widget-icons pull-right"></div>
							<div class="clearfix"></div>
						</div><!--1-->

						<div class="widget-content"><!--2-->
							<div class="padd">
								{{ errSend }}
								{% if(inquiry == false) %}
								<div class="alert alert-danger">Inquiry not found</div>
								{% else %}
								<span class="pull-right recent-meta">{{ date("F j, Y", inquiry.inqDate) }}</span>
								<h3 class="name">{{ inquiry.inqSubject }}</h3>
								<span class="recent-meta">From: {{ inquiry.inqSender }}</span><span class="recent-meta"> < {{ inquiry.inqEmail }} ></span>
								<div class="clearfix"></div>
								<br />
								<p>
									{{ inquiry.inqMessage|nl2br }}
								</p>
								{% endif %}

								<br />
								<hr>
								<br />
								<?php
								$count = count($inquiryReplies);
								foreach ($inquiryReplies as $key => $value) {
									$count--;
									$hr = $count > -1? '<hr />':'';

									echo '
										<div id="reps" class="text-right">
											<p>'.date('F j, Y H:i:s', $value->dateReply).'</p>
											<p>Reply from: '.$value->sender.'</p>
											<br />
											<p>'.nl2br($value->message).'</p>
											<div class="clearfix"></div>
										</div>
									'.$hr;
								}

								?>
								<?php
						    if (! function_exists('imap_open')) {
						        echo "IMAP is not configured.";
						        exit();
						    } else {

						        ?>
						    <div id="listData" class="list-form-container">

										<?php
						        /* Connecting Gmail server with IMAP */
						        $connection = imap_open('{imap.gmail.com:993/imap/ssl/novalidate-cert}INBOX', 'ramonluzano@gmail.com', 'Momo201311623') or die('Cannot connect to Gmail: ' . imap_last_error());

						        /* Search Emails having the specified keyword in the email subject */
						        $search = 'SINCE "' . date("j F Y", strtotime("-2 days")) . '"';

						        $emailData = imap_search($connection, 'SUBJECT "'.$inquiry->inqSubject.'" FROM "'.$inquiry->inqEmail.'"  ');
									?>

								<?php

						        if (! empty($emailData)) {
						            ?>

						            <?php
						            foreach ($emailData as $emailIdent) {

						                $overview = imap_fetch_overview($connection, $emailIdent, 0);
						                $message = imap_fetchbody($connection, $emailIdent, '2');
						                $messageExcerpt = substr($message, 147, 1300);
						                $partialMessage = trim(quoted_printable_decode($message));
						                $date = date("F d, Y H:i:s", strtotime($overview[0]->date));
														$trim_body = substr($message, 0, strpos($message, 'On'));

						                ?>

														<div id="reps" class="text-left">
															<p><?php echo $date;?>  </p>
															<p>Reply From: <?php echo $inquiry->inqSender; ?>< <?php echo $inquiry->inqEmail ?> ></p>
															<p>Subject: <?php echo $overview[0]->subject; ?></p>
															<br />
															<p><?php echo $trim_body;?></p>

															<div class="clearfix"><hr></div>
														</div>

						                <?php
						            } // End foreach
						            ?>

						            <?php
						        } // end if

						        imap_close($connection);
						    }
						    ?>
						    </div>
								<br />
								{{ text_area('replyMessage', 'name':'replyMessage', 'class':'form-control', 'rows':'5', 'placeholder':'Send a reply') }}

							</div>
						</div><!--2-->
						<div class="widget-foot"><!--3-->
							{{ hidden_field('csrf', 'value': security.getToken())}}
							<input type="hidden" class="tbl-action" name="action" value=""/>
							<input type="hidden" class="tbl-recordID" name="recordID" value=""/>
							<input type="hidden" class="tbl-edit-url" name="editurl" value="editpage/"/>
							<input type="hidden" name="sendToID" value="{{ inquiry.inqID }}"/>
							<!-- {{ link_to('admin/inquiries', 'Back to inquiry list', 'class':'btn btn-default') }} -->
							 <button class="btn btn-primary" style="margin-left: 38%;"> <a href="/admin/inquiries" style="text-decoration: none;color: white;"><span class="glyphicon glyphicon-circle-arrow-left"></span> Back to Inquiry List</a></button>
							<!-- <a href="#modalPrompt" class="btn btn-warning tbl_delete_row modal-control-button" data-toggle="modal" data-action="delete" data-recorID="{{ inquiry.inqID }}">Delete </a> -->
							<!-- {{ submit_button("sendReply", 'class':'btn btn-primary pull-right', 'value':'Send', 'name':'sendReply', 'id':'sendReply' ) }} -->
							<button name="sendReply"  value="send" class="btn btn-primary" style="margin-right: 40%;"> <span class="glyphicon glyphicon-send"></span> Send</button>
						</div><!--3-->
					</form>
				</div>
			</div>

		</div>
	</div>
<!--Container-->
	{% endif %}
</div>
<script type="text/javascript">
  $(document).ready(function(){
      $('.modal-btn-del').on('click', function(){
            $('#main-table-form').submit();
          });

      var x = $('#statusA').val();
      if(x == 'False'){
		$(document).ready(function() {

		    $('html,body').animate({scrollTop: 10000}, 800);
		    $('#replyMessage').val('');
		    setTimeout(function() {
	 			$("#messagesuc").hide(); }, 5000);
			});

      }
  });

if ( window.history.replaceState ) {
window.history.replaceState( null, null, window.location.href );

}

</script>
<!-- Matter ends -->
