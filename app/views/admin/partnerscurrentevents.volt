<div id="deleteEventsConf" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header-now-now" style="background-color: #d9534f;height: 35px;">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color: white; margin-right: 10px;margin-top: 5px;">×</button>
        <label style="margin-left: 10px;margin-top: 5px;"><h4 class="modal-title" style="color: white; font-size: 17px;">Delete Warning!</h4></label>
      </div>
      <div class="modal-body">
        <span id="deleteMessage"></span>
      </div>
      <div class="modal-footer" id="modal-footer">
        <button type="button" id="deleteEventBtn" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Yes</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove"> </span> No</button>
      </div>
    </div>
  </div>
</div>

<!-- Page heading -->
<div class="page-head">
    <h2 class="pull-left"><i class="icon-suitcase"></i> {{ partner.partnerName }}</h2>

    <!-- Breadcrumb -->
    <div class="bread-crumb pull-right">
        <a href="{{ url('admin') }}"><i class="icon-home"></i> Home</a>
        <!-- Divider -->
        <span class="divider">/</span>
        <a href="{{ url('admin/partners') }}">Partners</a>
        <span class="divider">/</span>
        <a href="{{ url('admin/partnersinfo/') }}{{ partnerID }}">View</a>
        <span class="divider">/</span>
        <span>Current Events</span>
    </div>

    <div class="clearfix"></div>

</div>
<!-- Page heading ends -->

<!-- Matter -->

<div class="matter">
    {{ form('class': 'form-horizontal', 'id':'eventsForm') }}
    <div class="container">

        <!-- Table -->

        <div class="row">



            <div class="col-md-12">

                <div class="widget">

                    <div class="widget-head">
                        <div class="pull-left">ABK Partner Details</div>
                        <div class="widget-icons pull-right">
                            <!-- <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> -->
                            <!-- <a href="#" class="wclose"><i class="icon-remove"></i></a> -->
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="widget-content">
                        <div class="padd">
                            <?php if($userlevel!=2){ ?>
                            {{ tab }}
                            <?php } ?>
                            <br>
                            {{ content() }}
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="control-label col-lg-3">Search</label>
                                        <div class="col-lg-9"> 
                                            {{ text_field('search_text' , 'class':'form-control') }}
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">

                                    <!-- {{ submit_button('Search', 'name':'searchBtn', 'class':'btn btn-default') }} -->
                                     <button type="submit" name="searchBtn" class="btn btn-success" ><span class="icon-search"> </span>Search</button>
                                    <button type="submit" name="clear_search" class="btn btn-danger " value="Clear Search"><span class="icon-refresh"> </span>Refresh</button>
                                    <a href="{{ url('admin/createpartnerevent/') }}{{ partnerID }}" type="button" class="btn btn-primary pull-right">+ Create New Event</a>
                                </div>
                            </div> 
                            <hr>


                            {% for event in page.items %}
                            {% if loop.first %}
                            <!--head of table-->
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                  <tr>
                                    <th width="10">
                                      <span class="uni">
                                        <input type="checkbox" name="select_all[]" class="tbl_select_all">
                                    </span>
                                </th>
                                <th><a href="?sort={{ dateHref }}">Date <i class="{{ dateIndicator ? dateIndicator : "" }}"></i></a></th>
                                <th><a href="?sort={{ titleHref }}">Event Title <i class="{{ titleIndicator ? titleIndicator : "" }}"></th>
                                <th><a href="?sort={{ venueHref }}">Venue <i class="{{ venueIndicator ? venueIndicator : "" }}"></th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        {% endif %}
                        <!--body of table-->
                        <tbody>
                            <tr>
                                <td>                            
                                  <span class="uni">
                                    <input type="checkbox" name="tbl_id[]" class="tbl_select_row" value="{{ event.eventID }}">
                                  </span>
                                </td>
                                <td width="150">
                                    {{ date("F j, Y", event.eventDate) }}
                                </td>
                                <td>
                                    {{ event.eventTitle }}
                                </td>
                                <td>
                                    {{ event.eventVenue }}
                                </td>
                                <td width="100">
                                  <a href="/admin/editEvent/{{ event.eventID }}?viewonly" class="btn btn-xs btn-success" data-toggle="modal" data-placement="top" title="View"><i class="icon-ok"></i></a>
                                  <a href="/admin/editEvent/{{ event.eventID }}" class="btn btn-xs btn-warning" data-toggle="modal" title="Edit"><i class="icon-pencil"></i></a>
                                  <a data-event-id="{{ event.eventID }}" data-eventname = "{{ event.eventTitle }}" href="#deleteEventsConf" class="deleteEventLink btn btn-xs btn-danger" data-toggle="modal" title="Delete"><i class="icon-trash"></i></a>
                                </td>
                            </tr>
                        </tbody>
                        {% if loop.last %}
                    </table>
                    {% endif %}
                    
                    {% else %}
                    <div class="alert" align="center">NO RECORDS FOUND</div>
                    {% endfor %}
                    <div class="tblbottomcontrol" style="display: none;">
                        <a href="#deleteEventsConf" id="deleteAllSelectedEventLink" data-toggle="modal"> Delete all Selected </a> |
                        <a href="#" class="tbl_unselect_all"> Unselect </a>
                    </div>
                    <input type="hidden" name="delEventSingle" id="delEventSingle">
                </div>
            </div>


            <div class="widget-foot">
                {% if showBackToList %} 
                <!-- {{ link_to("admin/partners", "Back to Partner List", 'class':'btn btn-primary') }}  -->
                <button class="btn btn-primary"> <a href="/admin/partners" style="text-decoration: none;color: white;float: left;"><span class="glyphicon glyphicon-circle-arrow-left"></span> Back to Partner List</a></button>
                {% endif %}

                 {% set limit = 10 %}
                 {% set start = (limit * (page.current - 1)) + 1 %}
                 {% set end = (limit * (page.current-1)) + limit %}
                 {% if end > page.total_items %}
                 {% set end = page.total_items %}
                 {% endif %}
                 {% if limit %}
                 <div style="margin-left: 85.5%">
                 <span>&nbsp;Showing {{ start }} - {{ end  }} of {{ page.total_items }}</span>
                 </div>
                 {% endif %}

                 {% if page.items and page.total_pages > 0 %}
                            <ul class="pagination pull-right">
                                <!---->
                                {% if page.current == 1 and page.total_pages >= 5 and page.total_pages > 0 %}                                
                                 <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                                  {% for index in 1..5 %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/partnersinfo/"~ partnerID ~"/events?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/partnersinfo/"~ partnerID ~"/events?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% elseif page.current == 1 and page.total_pages < 5 and page.total_pages > 0 %}
                                <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                                  {% for index in 1..page.total_pages %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/partnersinfo/"~ partnerID ~"/events?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/partnersinfo/"~ partnerID ~"/events?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% endif %}
                                <!---->
                                <!---->
                                {% if page.current != 1 and page.total_pages < 5 and page.total_pages > 0 %}
                                <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                                  {% for index in 1..page.total_pages %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/partnersinfo/"~ partnerID ~"/events?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/partnersinfo/"~ partnerID ~"/events?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% elseif page.current != 1 and page.current < 4 and page.total_pages >= 5 %}
                                <li>{{ link_to("admin/partnersinfo/"~ partnerID ~"/events", 'First') }}</li>
                                <li>{{ link_to("admin/partnersinfo/"~ partnerID ~"/events?page=" ~ page.before, 'Prev') }}</li>
                                  {% for index in 1..5 %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/partnersinfo/"~ partnerID ~"/events?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/partnersinfo/"~ partnerID ~"/events?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% elseif page.current >=4 and page.current+2 < page.total_pages%}
                                <li>{{ link_to("admin/partnersinfo/"~ partnerID ~"/events", 'First') }}</li>
                                <li>{{ link_to("admin/partnersinfo/"~ partnerID ~"/events?page=" ~ page.before, 'Prev') }}</li>
                                  {% for index in page.current-2..page.current+2 %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/partnersinfo/"~ partnerID ~"/events?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/partnersinfo/"~ partnerID ~"/events?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% elseif page.current >=4 and (page.current+2 == page.total_pages or page.current+2 > page.total_pages) %}
                                <li>{{ link_to("admin/partnersinfo/"~ partnerID ~"/events", 'First') }}</li>
                                <li>{{ link_to("admin/partnersinfo/"~ partnerID ~"/events?page=" ~ page.before, 'Prev') }}</li>
                                  {% for index in page.total_pages-4..page.total_pages %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/partnersinfo/"~ partnerID ~"/events?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/partnersinfo/"~ partnerID ~"/events?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% endif %}
                                <!---->
                                <!---->
                                {% if page.current != page.last %}                 
                                <li>{{ link_to("admin/partnersinfo/"~ partnerID ~"/events?page=" ~ page.next, 'Next') }}</li>
                                <li>{{ link_to("admin/partnersinfo/"~ partnerID ~"/events?page=" ~ page.last, 'Last') }}</li>
                                </ul>
                                {% else %}
                                <li>{{ link_to("admin/partnersinfo/"~ partnerID ~"/events?page=" ~ page.next, 'Next','onclick':'return false','style':'cursor:default') }}</li>
                                <li>{{ link_to("admin/partnersinfo/"~ partnerID ~"/events?page=" ~ page.last, 'Last','onclick':'return false','style':'cursor:default') }}</li>
                                </ul>
                                {% endif %}
                                <!---->
                              {% elseif page.total_pages == 0 %}
                              <ul class="pagination pull-right">
                              <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                              <li>{{ link_to("admin/partnersinfo/"~ partnerID ~"/events?page=" ~ page.next, 'Next','onclick':'return false','style':'cursor:default') }}</li>
                                <li>{{ link_to("admin/partnersinfo/"~ partnerID ~"/events?page=" ~ page.last, 'Last','onclick':'return false','style':'cursor:default') }}</li>
                                </ul>
                 {% endif %}
                 
            </div>

        </div>


    </div>

</div>


</div>
</form>
</div>

<!-- Matter ends -->
