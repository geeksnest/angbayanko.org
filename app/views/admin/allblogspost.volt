      <style type="text/css">
        
 /*a:hover,a:visited,a:link,a:active {

    text-decoration:none;
    background-color:none;
}*/
.nocolor:hover{
  color:#2a6496;
  text-decoration:none;
  background-color: none;
}

      </style> 
            <!-- Modal View-->
           <div id="modalView" class="modal fade modalView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" style="color: white;font-size: 17px" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" style="color: white;font-size: 17px">View Record</h4>
                  </div>
                  <div class="modal-body">
                    <p class="modal-message"></p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Close</button>
                  </div>
                </div>
              </div>
            </div>
            <!-- Modal Prompt-->
            <div id="modalPrompt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                <!--   <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Alert!</h4>
                  </div> -->
                  <div id="headerColor">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color: white;font-size: 17px">×</button>
                    <span  style="color: white;font-size: 17px" id="changewarning"></span>
                  </div>
                  <div class="modal-body">
                    <p class="modal-message"></p>
                    <span class="modal-list-names"></span>
                  </div>
                   <div class="modal-footer">
                    <a href="#" type="button" class="btn btn-primary modal-btn-yes" data-form='userform'><span class="icon glyphicon glyphicon-ok"></span> Yes</a>
                    <a type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><span class="icon glyphicon glyphicon-remove"></span> No</a>
                  </div>
                </div>
              </div>
            </div>
      <!-- Page heading -->
      <div class="page-head">
        <!-- Page heading -->
        <h2 class="pull-left">
          <!-- page meta -->
          <span class="page-meta">List of Blogs</span>
        </h2>


        <!-- Breadcrumb -->
        <div class="bread-crumb pull-right">
          <a href="/admin"><i class="icon-home"></i> Home</a>
          <!-- Divider -->
          <span class="divider">/</span>
          <a href="/admin/allblogspost" class="bread-current">Blogs Post</a>
        </div>

        <div class="clearfix"></div>

      </div>
      <!-- Page heading ends -->
<div class="matter">
  <div class="container">

    <!-- Table -->

    {{ form('admin/allblogspost', 'id':'main-table-form') }}
    {{ hidden_field('csrf', 'id':'csrfToken', 'value': "")}}

    <div class="row">
      <div class="col-md-2">
        <label>Search by Text</label>
      </div>
      <div class="col-md-6 form-group">
        <div class="col-md-12">
          {{ text_field('search_text' , 'class':'form-control') }}
        </div>
      </div>
      <div class="col-md-3 form-group">
        <!-- {{ submit_button('Search', 'class':'btn btn-default') }} -->
        <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-search"> </span> Search</button>
        <button type="submit" name="clear_search" class="btn btn-danger" value="Clear Search"><span class="icon-refresh"></span> Refresh</button>
      </div>
    </div>

    <!-- Table -->
    <div class="row">
      <div class="col-md-2">
        <label>Filter by Date</label>
      </div>
      <div class="col-md-4 form-group" id="fromDatepicker" class="input-append">
        <label class="col-md-2 control-label">From</label>
        <div class="col-md-10">
          {{ text_field('fromDate', 'readonly': 'true', 'data-format':'yyyy-MM-dd','class':'form-control dtpicker', 'data-time-icon':'icon-time') }}
          <span class="add-on">
            <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="btn btn-info btn-lg icon-calendar"></i>
          </span>
        </div>
      </div>
      <div class="col-md-3 form-group" id="toDatepicker" class="input-append" style="margin-left: -8.5%;">
        <label class="col-md-2 control-label">To</label>
        <div class="col-md-12">
          {{ text_field('toDate', 'readonly': 'true', 'data-format':'yyyy-MM-dd','class':'form-control dtpicker', 'data-time-icon':'icon-time') }}
          <span class="add-on">
            <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="btn btn-info btn-lg icon-calendar"></i>
          </span>
        </div>
      </div>
      <div class="col-md-2">
        {{ submit_button('Filter', 'class':'btn btn-primary') }}
      </div>
    </div>

    <div class="row">

      <div class="col-md-12">
        {% if (not(msg is empty)) %}
          <div style="color:green;padding:15px;background-color:#D5FFCD;">
            {{ msg }}
          </div>
        {% endif %}
        <form name="form1" class="" method="post" action"" id='main-table-form' class="form-horizontal">
        {{ content() }}
                                  <!-- <div class="form-group">
                                  <label class="col-lg-1">Search</label>
                                  <div class="col-lg-4">
                                      {#{ text_field('search_text' , 'class':'form-control') }#}
                                  </div>
                                  <div class="col-lg-7">
                                      {#{ submit_button('Search', 'class':'btn btn-default') }#}
                                      <button type="submit" name="clear_search" class="btn btn-default" value="Clear Search"><span class="icon-refresh"></span></button>
                                  </div>
                                </div> -->
                                <div class="clearfix"></div>
        <div class="widget">

          <div class="widget-head">
            <div class="pull-left">All Posts</div>
            <div class="widget-icons pull-right">
            </div>
            <div class="clearfix"></div>
          </div>

          <div class="widget-content">

                        {{ hidden_field('csrf', 'value': security.getToken())}}
                        <input type="hidden" class="tbl-action" name="action" value=""/>
                        <input type="hidden" class="tbl-recordID" name="recordID" value=""/>
                        <input type="hidden" class="tbl-edit-url" name="editurl" value="abkreseditblog/"/>
            <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                  <th>{{ check_field('select_all[]', 'class':'tbl_select_all') }}</th>
                  <th><a href="?sort={{ blogtitleHref }}" class="nocolor">Title  <i class="{{ blogttitleIndicator ? blogttitleIndicator : "" }}"></i></a> </th>
                  <th><a href="?sort={{ blogauthorHref }}" class="nocolor"> Author <i class="{{ blogauthorIndicator ? blogauthorIndicator : "" }}"></i></a> </th>
                  <th><a href="?sort={{ blogdateHref }}" class="nocolor">Date Created<i class="{{ blogdateIndicator ? blogdateIndicator : "" }}"></i></a> </th>
                  <th><a href="?sort={{ publishdateHref }}" class="nocolor">Date Published <i class="{{ publishdateIndicator ? publishdateIndicator : "" }}"></i></a>  </th>
                  <th width="7%">Action</th>
                </tr>
              </thead>


              <tbody>
              {% for blog in page.items %}
                <tr>
                  <td><input type="checkbox" name="tbl_id[]" class="tbl_select_row" value="{{ blog.blogID}}"> </td>
                  <td class="name">{{ blog.blogTitle }}</td>
                  <td >{{ blog.blogAuthor }}</td>
                  <td>{{ date("F j, Y", blog.blogDate) }}</td>
                  <td>
                      {% if blog.blogStatus == 'publish' %}
                        <span class="label label-success">Published</span>
                      {% else %}
                        <span class="label label-warning">Draft</span>
                      {% endif %}
                      <br/>
                      {{ date("F j, Y", blog.blogPublishDate) }}
                  </td>
                  <td>
                    <center>                            <!--    <a  href="#modalView" data-toggle="modal" class="btn btn-xs btn-success modal-record-view" data-href="ajaxUserView/{{ blog.blogID }}" ><i class="icon-ok"></i> </a> -->
                      <a href="#modalPrompt" class="btn btn-xs btn-warning tbl_edit_row modal-control-button" data-toggle="modal" data-action="edit" data-recorID="{{ blog.blogID }}"><i class="icon-pencil"></i></a>
                      <a href="#modalPrompt" class="btn btn-xs btn-danger tbl_delete_row modal-control-button" data-toggle="modal" data-action="delete" data-recorID="{{ blog.blogID }}"><i class="icon-remove"></i></a>
</center>
                  </td>
                </tr>
              {% endfor %}
              {% if blog is empty %}
               <tr><td colspan="6"><center>NO RECORDS FOUND</center></td></tr>
               {% endif %}
              </tbody>
            </table>
                          <div class="tblbottomcontrol" style="display:none">
                            <a href="#modalPrompt" class="tbl_delete_all" data-toggle="modal" data-action="delete_selected" > Delete all Selected </a> |
                            <a href="#" class="tbl_unselect_all"> Unselect </a>
                          </div>


                       <div class="widget-foot">
                              {% set limit = 10 %}
                              {% set start = (limit * (page.current - 1)) + 1 %}
                              {% set end = (limit * (page.current-1)) + limit %}

                              {% if end > page.total_items %}
                              {% set end = page.total_items %}
                              {% endif %}
                              {% if limit %}
                              <div style="margin-left: 85.5%">
                                <span>&nbsp;Showing {{ start }} - {{ end  }} of {{ page.total_items }}</span>
                              </div>
                              {% endif %}
                              {% if page.items and page.total_pages > 0 %}
                                <ul class="pagination pull-right">
                                    <!---->
                                {% if page.current == 1 and page.total_pages >= 5 and page.total_pages > 0 %}                                
                                 <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                                  {% for index in 1..5 %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/allblogspost?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/allblogspost?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% elseif page.current == 1 and page.total_pages < 5 and page.total_pages > 0 %}
                                <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                                  {% for index in 1..page.total_pages %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/allblogspost?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/allblogspost?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% endif %}
                                <!---->
                                <!---->
                                {% if page.current != 1 and page.total_pages < 5 and page.total_pages > 0 %}
                                <li>{{ link_to("admin/allblogspost", 'First') }}</li>
                                <li>{{ link_to("admin/allblogspost?page=" ~ page.before, 'Prev') }}</li>
                                  {% for index in 1..page.total_pages %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/allblogspost?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/allblogspost?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% elseif page.current != 1 and page.current < 4 and page.total_pages >= 5 %}
                                <li>{{ link_to("admin/allblogspost", 'First') }}</li>
                                <li>{{ link_to("admin/allblogspost?page=" ~ page.before, 'Prev') }}</li>
                                  {% for index in 1..5 %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/allblogspost?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/allblogspost?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% elseif page.current >=4 and page.current+2 < page.total_pages%}
                                <li>{{ link_to("admin/allblogspost", 'First') }}</li>
                                <li>{{ link_to("admin/allblogspost?page=" ~ page.before, 'Prev') }}</li>
                                  {% for index in page.current-2..page.current+2 %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/allblogspost?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/allblogspost?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% elseif page.current >=4 and (page.current+2 == page.total_pages or page.current+2 > page.total_pages) %}
                                <li>{{ link_to("admin/allblogspost", 'First') }}</li>
                                <li>{{ link_to("admin/allblogspost?page=" ~ page.before, 'Prev') }}</li>
                                  {% for index in page.total_pages-4..page.total_pages %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/allblogspost?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/allblogspost?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% endif %}
                                <!---->
                                <!---->
                                {% if page.current != page.last %}                 
                                <li>{{ link_to("admin/allblogspost?page=" ~ page.next, 'Next') }}</li>
                                <li>{{ link_to("admin/allblogspost?page=" ~ page.last, 'Last') }}</li>
                                </ul>
                                {% else %}
                                <li>{{ link_to("admin/allblogspost?page=" ~ page.next, 'Next','onclick':'return false','style':'cursor:default') }}</li>
                                <li>{{ link_to("admin/allblogspost?page=" ~ page.last, 'Last','onclick':'return false','style':'cursor:default') }}</li>
                                </ul>
                                {% endif %}
                                <!---->
                              {% elseif page.total_pages == 0 %}
                              <ul class="pagination pull-right">
                              <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                              <li>{{ link_to("admin/allblogspost?page=" ~ page.next, 'Next','onclick':'return false','style':'cursor:default') }}</li>
                                <li>{{ link_to("admin/allblogspost?page=" ~ page.last, 'Last','onclick':'return false','style':'cursor:default') }}</li>
                                </ul>
                 {% endif %}

                              <div class="clearfix"></div> 

                            </div>
          </div>
        </div>

        </form>
      </div>

    </div>
  </div>
</div>
