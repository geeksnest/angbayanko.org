       <style type="text/css">
              #saveBtn{
                  background: none;
                  border: none;
                  outline: none;
                  color: white;
                  text-decoration:none;
                  cursor: pointer;

              }
              #saveBtn:hover {
              background-color:none;

                }                                             
          </style>
      <!-- Page heading -->
     
      <div class="page-head">
        <!-- Page heading -->
        <h2 class="pull-left"> 
          <!-- page meta -->
          <span class="page-meta">Users</span>
        </h2>


        <!-- Breadcrumb -->
        <div class="bread-crumb pull-right">
          <a href="/admin"><i class="icon-home"></i> Home</a> 
          <!-- Divider -->
          <span class="divider">/</span> 
          <a href="/admin/users" class="bread-current">Users</a>
          <!-- Divider -->
          <span class="divider">/</span> 
          <a href="" class="bread-current">Edit</a>          
        </div>

        <div class="clearfix"></div>

      </div>
      <!-- Page heading ends -->



      <!-- Matter -->

      <div class="matter">
        <div class="container">

          <div class="row">

            <div class="col-md-12">
              {{ content() }}

              <div class="widget wgreen">
                
                <div class="widget-head">
                  <div class="pull-left">Edit Account Information</div>
                  <div class="widget-icons pull-right">
                    <!-- <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>  -->
                    <!-- <a href="#" class="wclose"><i class="icon-remove"></i></a> -->
                  </div>
                  <div class="clearfix"></div>
                </div>

                <div class="widget-content">
                  <div class="padd">

                    <h6>User Account</h6>
                    <hr />
                    <!-- Form starts.  -->
                     {{ form('admin/edituser/' ~ user.userID, 'class': 'form-horizontal') }}
                                <?php echo $form->render('huserName', array( 'value' => $user->userName)); ?>
                                <?php echo $form->render('huserEmail', array( 'value' => $user->userEmail)); ?>
                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label>{{ form.label('username') }}</label>
                                   <span class="asterisk">*</span>
                                 </div>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('username', array( 'value' => $user->userName)); ?>
                                    <!-- {{ form.messages('username') }} -->
                                    <span id="userhide"> {{ usernameErr }} </span>
<!--                                     <div class="label label-danger" id="valuser0"></div>
                                    <div class="label label-danger" id="valuser1"></div>
                                    <div class="label label-danger" id="valuser3"></div> -->
                                  </div>
                                </div>
                              
                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label>{{ form.label('email') }}</label>
                                  <span class="asterisk">*</span>
                                </div>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('email', array( 'value' => $user->userEmail)); ?>
                                    <!-- {{ form.messages('email') }} -->
                                  <span id="emailhide">  {{ emailErr }} </span>
<!--                                   <div class="label label-danger" id="valemail"></div> -->
                                  </div>
                                </div>

                    <h6>User Profile</h6>
                    <hr />
                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label>{{ form.label('firstname') }}</label>
                                  <span class="asterisk">*</span>
                                </div>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('firstname', array( 'value' => $user->userFirstname)); ?>
                                   <!--  {{ form.messages('firstname') }} -->
                                    <span id="fnamehide">  {{ fnameErr }} </span>
                                  </div>
                                </div>  

                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label>{{ form.label('lastname') }}</label>
                                  <span class="asterisk">*</span>
                                </div>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('lastname', array( 'value' => $user->userLastname)); ?>
                                    <span id="lnamehide">  {{ lnameErr }} </span>
                                  </div>
                                </div>  

                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label>{{ form.label('middlename') }}</label>
                                </div>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('middlename', array( 'value' => $user->userMiddlename)); ?>
                                    {{ form.messages('middlename') }}
                                  </div>
                                </div>  

                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label>{{ form.label('address') }}</label>
                                </div>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('address', array( 'value' => $user->userAddress)); ?>
                                    {{ form.messages('address') }}
                                  </div>
                                </div> 

                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label>{{ form.label('company') }}</label>
                                </div>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('company', array( 'value' => $user->userCompany)); ?>
                                    {{ form.messages('company') }}
                                  </div>
                                </div> 

                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label>{{ form.label('contact') }}</label>
                                  <span class="asterisk">*</span>
                                </div>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('contact', array( 'value' => $user->userContact)); ?>
                                    <!-- {{ form.messages('contact') }} -->
                                  <span id="contacthide">  {{ contactErr }} </span>
<!--                                   <div class="label label-danger" id="valcontact"></div> -->
                                  </div>
                                </div> 

                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label>{{ form.label('position') }}</label>
                                </div>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('position', array( 'value' => $user->userPosition)); ?>
                                    {{ form.messages('position') }}
                                  </div>
                                </div>  

                    <h6>User Role</h6>
                    <hr />                                   
                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label>Roles</label>
                                  <span class="asterisk">*</span>
                                </div>
                                  <div class="col-lg-8">
                                  {% for role in tblroles %}

                                    <label class="checkbox-inline">
                                    
                                      <!-- <input type='checkbox' value='{{role.roleCode}}' name='{{role.roleCode}}' > -->
                                      {{ check_field(role.roleCode, 'value':role.roleCode) }}
                                      {{role.roleDescription}}
                                      
                                    </label><br/>
                                  {% endfor  %}
                                    <!-- {{ roleError }}  -->
                                     <span id="rolehide">{{ roleError }}</span>                                
                                  </div>
                                </div>

                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label>Status</label>
                                  <span class="asterisk">*</span>
                                </div>
                                  <div class="col-lg-8">
                                    <div class="radio">
                                      <label>
                                      {{ radio_field('status', 'value':'active') }}
                                        Active
                                      </label>
                                    </div>
                                    <div class="radio">
                                      <label>
                                        {{ radio_field('status', 'value':'deactivate') }}
                                        Deactivate
                                      </label>
                                    </div>
                                  </div>
                                </div>
                                    <hr />
                                  {# form.render('csrf', ['value': security.getToken()]) #}
                                  {#{ form.render('csrf', ['name': this.security.getTokenKey(), 'value': this.security.getToken()]) }#}
                                <div class="form-group">
                                  <div class="col-lg-offset-1 col-lg-9">
                                    <!-- {{ submit_button('Save Changes' , 'class':'btn btn-primary', 'name':'update_button') }} -->
                                    <button class="btn btn-primary" id="submitBtn">
                                   <span class=" glyphicon glyphicon-floppy-disk"></span><input type="submit" name="update_button"  value="Save Changes" id="saveBtn"/>
                                  </button> 
                                    <!-- <button type="button" class="btn btn-danger">Reset</button> -->
                                    <!-- <button type="button" class="btn btn-danger" onclick="window.location='/admin/users'"><span class="glyphicon glyphicon-remove"></span> Cancel</button> -->
                                    <button class="btn btn-danger"><a href="/admin/users" style="color: white"><span class=" glyphicon glyphicon-remove"></span> Cancel</a></button>
                                  </div>
                                </div>
                              </form>
                  </div>
                </div>
                 <!--  <div class="widget-foot"> -->
                    <!-- Footer goes here -->
                 <!--  </div> -->
              </div>  

              <div class="widget wgreen">
                <div class="widget-head">
                  <div class="pull-left">Change Password</div>
                  <div class="widget-icons pull-right">
                    <!-- <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>  -->
                    <!-- <a href="#" class="wclose"><i class="icon-remove"></i></a> -->
                  </div>
                  <div class="clearfix"></div>
                </div>
                <div class="widget-content">
                  <div class="padd">
                    <h6>Change Password</h6>
                    <hr />
                    {{ form('admin/edituser/' ~ user.userID, 'class': 'form-horizontal', 'id': 'updateform') }}
                   
                    <input type="text" name="currpass" id="currpass" value="<?php echo $user->userPassword;?>" style="display: none;">
                    <input type="text" name="currpass2" id="currpass2" style="display: none;">
                     <div class="form-group">
                      <label class="col-lg-4 control-label">Old Password</label>
                      <div class="col-lg-8">
                        <input type="password" name="oldpass" id="oldpass" class="form-control" placeholder="Old Password" value="<?php echo $_POST['oldpass'];?>">
                          <label id="valid" class="label label-info" style="display: none;">Validating . . .</label>
                         <div id="oldpasssuccess" class="label label-success" style="display: none;"><span class="glyphicon glyphicon-ok"> OK</span></div>
                         <div id="oldpasserror" class="label label-danger" style="display: none;"><span class="glyphicon glyphicon-remove"> INVALID</span></div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-lg-4 control-label">{{ form.label('password') }}</label>
                      <div class="col-lg-8">
                        {{ form.render('password') }}
                        {{ passError }}
                        <div class="label label-danger" id="passerror"></div>
                        <div class="label label-danger" id="spaceerror"></div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-lg-4 control-label">{{ form.label('repassword') }}</label>
                      <div class="col-lg-8">
                        {{ form.render('repassword') }}
                        {{ repassError }}
                        <div class="label label-danger" id="repasserror"></div>
                      </div>
                    </div>
                    <div class="form-group">
                      {{ form.render('csrf', ['name': this.security.getTokenKey(), 'value': this.security.getToken()]) }}
                      <div class="col-lg-offset-1 col-lg-9">
                        <!-- {{ submit_button('Update Password' , 'class':'btn btn-primary', 'name':'update_password','id':'savepassBtn') }} -->
                        <button class="btn btn-primary">
                          <span class="glyphicon glyphicon-lock"></span>
                          <input type="submit" name="update_password"  value="Save Password" id="saveBtn"/>
                        </button> 
                      </div>
                    </div>
                    </form>
                  </div>
                </div>
<!--                 <div class="widget-foot"> -->
                  <!-- Footer goes here
                </div> -->
              </div>

            </div>

          </div>

        </div>
      </div>

<script type="text/javascript">
  var typingTimer;                //timer identifier
  var doneTypingInterval = 5000;
$(document).ready(function(){
  $("#password").prop('disabled', true);
  $("#repassword").prop('disabled', true);
  

  
$('#oldpass').on('keyup', function () {
    $("#password").prop('disabled', true);
    $("#repassword").prop('disabled', true);
    $('#oldpasssuccess').hide();
    $('#oldpasserror').hide();
    $('#valid').show();
  clearTimeout(typingTimer);
  typingTimer = setTimeout(doneTyping, doneTypingInterval);
  });
  $("#password").prop('disabled', true);
  $("#repassword").prop('disabled', true);

  $('#oldpass').on('keydown', function () {
  $('#oldpasssuccess').hide();
  $('#oldpaserror').hide();
  $('#valid').show();
  clearTimeout(typingTimer);
  });

  $(window).load( function(){
    if($('#oldpass').val() != ''){
    doneTyping();
    }else{

    }  
  });

  $('#oldpass').on('focusout', function () {
    $('#valid').hide();
  });

  function doneTyping(){
    var data1=$('#userID').val();
    var data2=$("#oldpass").val();
    var curpass= $('#currpass').val();
    // var dataTosend='user='+data1+'&pass='+data2;
    $.ajax({
      url: '../edituserpass/'+data1+'/'+data2+'/'+curpass,
      type: 'GET',
      // data:dataTosend,
      async: true,

      success: function (res) {
        $('#currpass2').val(res.replace(/['"]+/g, ''));
      //var cur = ''+$('#currpass').val()+'';
        $('#valid').show();

        var str1 = $('#currpass2').val();
        var str2 = $('#currpass').val();
        if(str1 != str2){
          // console.log(res+'=='+x);
          $('#valid').hide();
          $("#password").prop('disabled', true);
          $("#repassword").prop('disabled', true);
          $('#oldpasssuccess').hide();
          $('#oldpasserror').show();

          console.log("not equal");
          console.log(str1.replace(/['"]+/g, ''));
        }else{
          // console.log(typeof(res) + ">>"+ typeof(x));
          $('#valid').hide();
          $("#password").prop('disabled', false);
          $("#repassword").prop('disabled', false);
          $('#oldpasssuccess').show();
          $('#oldpaserror').hide();
          console.log("equal");
        }
      },
    });
  }
});
  
</script>
<!--SHA1-->


    <!-- Matter ends -->