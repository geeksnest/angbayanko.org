<!-- Modal Prompt-->

<div id="modalPrompt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Alert!</h4>
            </div>
            <div class="modal-body">
                <p class="modal-message"></p>
                <span class="modal-list-names"></span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancel</button>
                <a href="#" type="button" class="btn btn-primary modal-btn-yes" data-form='userform'>Yes</a>
            </div>
        </div>
    </div>
</div>
<!-- Page heading -->
<div class="page-head">
    <!-- Page heading -->
    <h2 class="pull-left">
        <!-- page meta -->
        <span class="page-meta">List of Announcements</span>
    </h2>


    <!-- Breadcrumb -->
    <div class="bread-crumb pull-right">
        <a href="/admin"><i class="icon-home"></i> Home</a>
        <!-- Divider -->
        <span class="divider">/</span>
        <a href="/admin/announcements" class="bread-current">Announcements</a>
        <!-- Divider -->
    </div>

    <div class="clearfix"></div>

</div>
<!-- Page heading ends -->
<div class="matter">
    <div class="container">

        {{ form('admin/announcements', 'id':'main-table-form') }}
        {{ hidden_field('csrf', 'id':'csrfToken', 'value': "")}}

        <div class="row">
          <div class="col-md-2">
            <label>Search by Text</label>
          </div>
          <div class="col-md-6 form-group">
            <div class="col-md-12">
              {{ text_field('search_text' , 'class':'form-control') }}
            </div>
          </div>
          <div class="col-md-2 form-group">
            {{ submit_button('Search', 'class':'btn btn-default') }}
            <button type="submit" name="clear_search" class="btn btn-default" value="Clear Search"><span class="icon-refresh"></span></button>
          </div>
        </div>

        <!-- Table -->
        <div class="row">
          <div class="col-md-2">
            <label>Filter by Date</label>
          </div>
          <div class="col-md-3 form-group" id="fromDatepicker" class="input-append">
            <label class="col-md-2 control-label">From</label>
            <div class="col-md-10">
              {{ text_field('fromDate', 'readonly': 'true', 'data-format':'yyyy-MM-dd','class':'form-control dtpicker', 'data-time-icon':'icon-time') }}
              <span class="add-on">
                <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="btn btn-info btn-lg icon-calendar"></i>
              </span>
            </div>
          </div>
          <div class="col-md-3 form-group" id="toDatepicker" class="input-append">
            <label class="col-md-2 control-label">To</label>
            <div class="col-md-10">
              {{ text_field('toDate', 'readonly': 'true', 'data-format':'yyyy-MM-dd','class':'form-control dtpicker', 'data-time-icon':'icon-time') }}
              <span class="add-on">
                <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="btn btn-info btn-lg icon-calendar"></i>
              </span>
            </div>
          </div>
          <div class="col-md-2">
            {{ submit_button('Filter', 'class':'btn btn-default') }}
          </div>
        </div>

        <!-- Table -->

        <div class="row">

            <div class="col-md-12">
                <form name="form1" class="" method="post" action="" id='main-table-form' class="form-horizontal">
                    {{ content() }}
                    <!-- <div class="form-group">
                        <label class="col-lg-1 control-label">Search</label>
                        <div class="col-lg-4">
                            {#{ text_field('search_text' , 'class':'form-control') }#}
                        </div>
                        <div class="col-lg-7">
                            {#{ submit_button('Search', 'class':'btn btn-default') }#}
                            <button type="submit" name="clear_search" class="btn btn-default" value="Clear Search"><span class="icon-refresh"></span></button>

                        </div>
                    </div> -->
                    <div class="clearfix"></div>
                    <div class="widget">

                        <div class="widget-head">
                            <div class="pull-left">All Announcements</div>
                            <div class="widget-icons pull-right">
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="widget-content">

                            {{ hidden_field('csrf', 'value': security.getToken())}}
                            <input type="hidden" class="tbl-action" name="action" value=""/>
                            <input type="hidden" class="tbl-recordID" name="recordID" value=""/>
                            <input type="hidden" class="tbl-edit-url" name="editurl" value="editannouncement/"/>
                            <!-- <input type="text" name="enddate" value="Back End Management Modules">  -->
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th width="2%">{{ check_field('select_all[]', 'class':'tbl_select_all') }}</th>
                                    <th width="35%"><a href="?sort={{ titleHref }}">Title <i class="{{ titleIndicator ? titleIndicator : "" }}"></i></a></th>
                                    <th><a href="?sort={{ statusHref }}">Status <i class="{{ statusIndicator ? statusIndicator : "" }}"></i></a></th>
                                    <th><a href="?sort={{ dateHref }}">Date Created <i class="{{ dateIndicator ? dateIndicator : "" }}"></i></a></th>
                                    <th><a href="?sort={{ startHref }}">Date Started <i class="{{ startIndicator ? startIndicator : "" }}"></i></a></th>
                                    <th><a href="?sort={{ endHref }}">Date Ended <i class="{{ endIndicator ? endIndicator : "" }}"></i></a></th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                {% if page.total_pages == 0 %}
                                    <tr>
                                        <td colspan="7" align="center">No Records Found</td>
                                    </tr>
                                {% else %}
                                    {% for post in page.items %}
                                        <tr>
                                            <td><input type="checkbox" name="tbl_id[]" class="tbl_select_row" value="{{ post.annID}}"> </td>
                                            <td class="name">{{ post.annTitle }}</td>
                                            <td>
                                             {% if post['annEnd'] >= day %}
                                             <span class="label label-success">Active</span>
                                              {% else  %}
                                              <span class="label label-default">Inactive</span>
                                             {% endif %}
                                           </td>
                                            <td>{{ date("F j, Y", post.annDate) }}</td>
                                            <td>{{ date("F j, Y", post.annStart) }}</td>
                                            <td>{{ date("F j, Y", post.annEnd) }}
                                                <!-- <input type="text" name="samdate" value="{{ date(post.annEnd) }}"> -->
                                            </td>
                                            <td>
                                                 <a  href="{{ post.annID }}" class="btn btn-xs btn-success modal-record-view" data-href="ajaxUserView/{{ user.userID }}" ><i class="icon-ok"></i>View </a>
                                                <a href="#modalPrompt" class="btn btn-xs btn-warning tbl_edit_row modal-control-button" data-toggle="modal" data-action="edit" data-recorID="{{ post.annID }}"><i class="icon-pencil"></i> Edit</a>
                                                <a href="#modalPrompt" class="btn btn-xs btn-danger tbl_delete_row modal-control-button" data-toggle="modal" data-action="delete" data-recorID="{{ post.annID }}"><i class="icon-remove"></i> Delete</a>
                                            </td>
                                        </tr>
                                    {% endfor %}
                                {% endif %}

                                </tbody>
                            </table>
                            <div class="tblbottomcontrol" style="display:none">
                                <a href="#modalPrompt" class="tbl_delete_all" data-toggle="modal" data-action="delete_selected" > Delete all Selected </a> |
                                <a href="#" class="tbl_unselect_all"> Unselect </a>
                            </div>

                            <div class="widget-foot">



                                    {% if page.total_pages != 0 %}
                                    <ul class="pagination pull-right">

                                        {% if page.current != 1 %}
                                            <li>{{ link_to("admin/announcements?page=" ~ page.before, 'Prev') }}</li>
                                        {% endif %}

                                        {% for index in 1..page.total_pages %}
                                            {% if page.current == index %}
                                                <li>{{ link_to("admin/announcements?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                            {% else %}
                                                <li>{{ link_to("admin/announcements?page=" ~ index, index) }}</li>
                                            {% endif %}
                                        {% endfor %}

                                        {% if page.current != page.total_pages %}
                                            <li>{{ link_to("admin/announcements?page=" ~ page.next, 'Next') }}</li>
                                        {% endif %}
                                    </ul>

                                    <div class="clearfix"></div>
                                    {% endif %}

                            </div>

                        </div>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>


<script type="text/javascript">
    $(window).load(function () {
    $('#myModal').modal('show');
    });
</script>

<!--++++++++++++++++++++++++++++++++++++++++++Modal++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<div id="myModal" style="display: block;" class="modal fade modalView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
                              <div class="modal-dialog">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title">View Announcement</h4>
                                  </div>
                                  <div class="modal-body">
                                    <div class="padd">
                                        <span class="pull-right muted">{{ date("F j, Y", ann.annDate) }}</span>
                                        <div class="form-group">
                                            <label>Event Title</label>
                                            <div>
                                                {{ ann.annTitle }}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Content</label>
                                            <div>
                                                {{ ann.annDesc }}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Date Duration:</label>
                                            <div>
                                                {{ date("F j, Y", ann.annStart) }}
                                                <span> to </span>
                                                {{ date("F j, Y", ann.annEnd) }}
                                            </div>
                                        </div>

                                    </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" id="modalClose" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Close</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <script type="text/javascript">
                              $("#modalClose").on("click", function(){
                                   //code here
                                   window.location.assign("/admin/announcements");
                            });
                            </script>
<!--++++++++++++++++++++++++++++++++++++++++++Modal++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->

<!-- Matter ends -->
