          <?php $token = $this->security->getToken() ?>
          <input type="hidden" id="townID" value="{{ townID }}">
          <div id="editCaptionModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Edit Picture</h4>
              </div>
              <form method="post">
                <input type="hidden" name="town_csrf" value="<?php echo $token ?>">
                <input type="hidden" name="hTownPic" id="hTownPic">
                <div class="modal-body">
                  {{ albumOptions }}
                  <br />
                  <img src="" id="edit-pic-thumbnail">
                  <br />
                  <br />
                  <label>Caption</label>
                  <textarea name="pictureCaption" class="form-control" id="pictureCaption"></textarea>
                  <br />
                  <input type="hidden" id="albumCoverID" value="{{ albumCoverID }}">
                  <label><input type="checkbox" name="albumCover" id="albumCover" value="1"> Make album cover</label>
                </div>
                <div class="modal-footer" id="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
                  {{ submit_button('Save' , 'class':'btn btn-primary', 'name':'save_town_pic') }}
                </div>

              </form>
            </div>
          </div>
        </div>


          <div id="uploadTownPicture" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog" style="width: 900px;">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 class="modal-title" id="townAlbumModalTitle">Upload picture to Album: <strong>{{ albumName }}</strong></h4>
                </div>
                <div class="modal-body" style=" height: 400px; overflow:scroll;">
                    <div id="imageError"></div>
                    <input type="hidden" name="albumID" id="albumID" value="{{ albumID }}">
                    <span class="btn btn-success fileinput-button">
                      <i class="glyphicon glyphicon-plus"></i>
                      <span>Add files...</span>
                      <!-- The file input field used as target for the file upload widget -->
                      <input id="townPictures" type="file" name="files[]" multiple accept="image/*">
                    </span>
                    <!-- The global progress bar -->
                    <div id="progress" class="progress">
                      <div class="progress-bar progress-bar-success"></div>
                    </div>
                    <div class="gallery digital-assets-gallery">
                      <span id="selectImg">Please select files to upload</span>
                    </div>
                    <div class="clearfix"></div>
                    
                  </div>
                <div class="modal-footer" id="modal-footer">
                  <span id="createAlbumClose">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
                  </span>
                </div>
              </div>
            </div>
          </div>

          <!-- Modal Prompt-->
          <div id="modalPrompt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 class="modal-title">Alert!</h4>
                </div>
                <div class="modal-body">
                  <p class="modal-message"></p>
                  <span class="modal-list-names"></span>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
                  <a href="#" type="button" class="btn btn-primary modal-btn-yes" data-form='userform'>Yes</a>
                </div>
              </div>
            </div>
          </div>
          <!-- Page heading -->
          <div class="page-head">
            <h2 class="pull-left"><i class="icon-table"></i> Town Gallery</h2>

            <!-- Breadcrumb -->
            <div class="bread-crumb pull-right">
              <a href="index.html"><i class="icon-home"></i> Home</a> 
              <!-- Divider -->
              <span class="divider">/</span> 
              {{ link_to('admin/towns', 'Towns') }}
              <span class="divider">/</span> 
              {{ link_to('admin/townsgallery', 'Gallery') }}
              <span class="divider">/</span>
              <span>{{ albumName }}</span>
            </div>

            <div class="clearfix"></div>

          </div>
          <!-- Page heading ends -->

          <!-- Matter -->

          <div class="matter">
            {{ form('admin/townAlbum/'~townID~'/'~albumID, 'class': 'form-horizontal', 'id':'main-table-form') }}
            <div class="container">

              <!-- Table -->

              <div class="row">

                <div class="col-md-12">
                <h2>{{ town.townName }}</h2>

                  {{ content() }}


                  <div class="widget">

                    <div class="widget-head">
                      <div class="pull-left">Pictures</div>
                      <div class="widget-icons pull-right">
                      </div>  
                      <div class="clearfix"></div>
                    </div>


                    <div class="widget-content">
                      <div class="padd">
                        {{ townTab }}
                      </div>
                      
                      <input type="hidden" name="csrf" value="<?php echo $token ?>">
                      <input type="hidden" class="tbl-action" name="action" value=""/>
                      <input type="hidden" class="tbl-recordID" name="recordID" value=""/>
                      <input type="hidden" class="tbl-edit-url" name="editurl" value="editpage/"/>
                      <table class="table table-striped table-bordered table-hover tblusers">
                        <thead>
                          <tr>
                            <th colspan="6">

                            <div class="col-lg-6">
                              <div class="form-group">
                                <label class="control-label col-lg-3">Search</label>
                                <div class="col-lg-9"> 
                                  {{ text_field('search_text' , 'class':'form-control') }}
                                </div>
                              </div>
                            </div>

                            <div class="col-lg-6">
                                <button type="submit" name="searchBtn" class="btn btn-default" value="Clear Search">Search</button>
                                <button type="submit" name="clear_search" class="btn btn-default" value="Clear Search"><span class="icon-refresh"></span></button>
                              <a href="#uploadTownPicture" style="margin-right:25px" class="btn btn-primary pull-right" data-toggle="modal">+ Add Pictures</a>
                            </div>

                              {{ albumOptions2 }}
                            </th>
                          </tr>
                          <tr>
                            <th width="10">{{ check_field('select_all[]', 'class':'tbl_select_all') }}</th>
                            <th>Picture</th>
                            <th>Caption</th>
                            <th>Date</th>
                            <th>Size</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php foreach ($page->items as $pic) {
                          ?>
                          <tr id="picture<?php echo $pic['pictureID'] ?>">
                            <td>                            
                              <span class="uni">
                                <input type="checkbox" name="tbl_id[]" class="tbl_select_row" value="<?php echo $pic['pictureID'] ?>">
                              </span>
                            </td>
                            <td><a class="prettyPhoto[pp_gal]" href="<?php echo $pic['path'] ?>"><img src="<?php echo $pic['thumbPath'] ?>" alt="<?php echo $pic['pictureCaption'] ?>" title="<?php echo $pic['pictureCaption'] ?>"></a></td>
                            <td class="name"><p id="caption<?php echo $pic['pictureID'] ?>"><?php echo $pic['pictureCaption'] ?></p></td>
                            <td><?php echo date("m-d-Y", $pic['dateUploaded']) ?></td>
                            <td><?php echo $pic['pictureSize'] ?></td>
                            <td>

                              <a data-picture-id="<?php echo $pic['pictureID']?>" href="#editCaptionModal" class="editTownPicLink btn btn-xs btn-warning" data-picture-src="<?php echo $pic['thumbPath'] ?>" data-caption="<?php echo $pic['pictureCaption'] ?>" data-toggle="modal"><i class="icon-pencil"></i></a>

                            
                              <a href="#modalPrompt" class="btn btn-xs btn-danger tbl_delete_row modal-control-button" data-toggle="modal" data-action="delete" data-recorID="<?php echo $pic['pictureID'] ?>"><i class="icon-remove"></i> </a>

                            </td>
                          </tr>
                          <?php
                        } ?>

                        <?php if(empty($page->items)){ ?>
                              <tr><td colspan="6">No pictures found</td></tr>
                            <?php } ?>
                        </tbody>
                      </table>

                      <div class="tblbottomcontrol" style="display:none">
                        <a href="#modalPrompt" class="tbl_delete_all" data-toggle="modal" data-action="delete_selected" > Delete all Selected </a> | 
                        <a href="#" class="tbl_unselect_all"> Unselect </a>
                      </div>

                      <div class="widget-foot">
                      {{ link_to("admin/townsgallery/"~townID, "Back to Album List", 'class':'btn btn-primary') }}
                        {% if page.total_pages > 1 %}

                        <ul class="pagination pull-right">

                          {% if page.current != 1 %}
                          <li>{{ link_to("admin/townAlbum/"~townID~"/"~albumID~"?page=" ~ page.before, 'Prev') }}</li>
                          {% endif %}

                          {% for index in 1..page.total_pages %}
                          {% if page.current == index %}
                          <li>{{ link_to("admin/townAlbum/"~townID~"/"~albumID~"?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                          {% else %}
                          <li>{{ link_to("admin/townAlbum/"~townID~"/"~albumID~"?page=" ~ index, index) }}</li>
                          {% endif %}
                          {% endfor %}         

                          {% if page.current != page.total_pages %}                 
                          <li>{{ link_to("admin/townAlbum/"~townID~"/"~albumID~"?page=" ~ page.next, 'Next') }}</li>
                          {% endif %}
                        </ul>
                        {% endif %}

                        <div class="clearfix"></div> 

                      </div>

                    </div>

                  </div>


                </div>

              </div>


            </div>
          </form>
        </div>

<!-- Matter ends -->