<!-- Page heading -->
<div class="page-head">
    <!-- Page heading -->
    <h2 class="pull-left">
        <!-- page meta -->
        <span class="page-meta">Edit Announcement</span>
    </h2>

    <!-- Breadcrumb -->
    <div class="bread-crumb pull-right">
        <a href="/admin"><i class="icon-home"></i> Home</a>
        <!-- Divider -->
        <span class="divider">/</span>
        <a href="/admin/announcements" class="bread-current">Announcements</a>
        <!-- Divider -->
        <span class="divider">/</span>
        <span>Edit</span>
    </div>

    <div class="clearfix"></div>

</div>
<!-- Page heading ends -->
<!-- Matter -->

<div class="matter">
    <div class="container">
        <form name="postform" method="post" action="">
            <div class="row">

                <div class="col-md-12">
                    {{ content() }}
                    <div style="color:green;padding:10px;">                   
                        {{ success }}
                    </div>
                    <div class="widget">
                        <div class="widget-head">
                            <div class="pull-left">Edit Announcement</div>                           
                            <div class="clearfix"></div>
                        </div>
                        <div class="widget-content">
                            <div class="padd">
                                <div class="form-group">
                                    <label>Enter Title</label>
                                    <div>
                                        <input type="hidden" name="orig_title" value="<?php echo $ann->annTitle ?>">
                                        <?php echo $form->render('ann_title', array( 'value' => $ann->annTitle)); ?>
       <!--                                  {{ form.messages('ann_title') }} -->
                                           <span id="hidetitle"> {{ titleErr }} </span>
                                    </div>
                                </div>

                                <div class="text-area">
                                    <label>Content</label>
                                    <?php echo $form->render('ann_content', array( 'value' => $ann->annDesc)); ?>
                                </div>
                                <!-- {{ form.messages('ann_content') }} -->
                                <span id="errdetails"> {{ contentErr }} </span>
                                <br />
                                <div class="form-group">
                                    <label>Date Duration</label>

                                    <div class="input-append">
                                        <span id="dateStart" class="start">
                                            <?php echo $form->render('ann_start', array( 'value' => date('Y-m-d', $ann->annStart))); ?>
                                            <span class="add-on"  id="valdate">
                                                <i class="btn btn-info btn-lg icon-calendar"></i>
                                            </span>
                                        </span>
                                        <span>To</span>
                                        <span id="dateEnd" class="end">
                                            <?php echo $form->render('ann_end', array( 'value' => date('Y-m-d', $ann->annEnd))); ?>
                                            <span class="add-on" id="ed">
                                                <i class="btn btn-info btn-lg icon-calendar"></i>
                                            </span>
                                        </span>
                                    </div>
                                    <!-- -->
                                    <div   style="margin-left: 3.5%">
                                       <table style="width: 365px">
                                        <tr>
                                            <th style="margin-right: 100%;width: 100px">
                                        <label id="valdate1">{{startErr}}</label>
                                            </th>
                                            <th  style="margin-left: 115%;width: 100px">
                                        <label id="valend">{{ endErr }}</label>
                                            </th>
                                        </tr>
                                       </table>
                                
                                    </div>

                                    <!-- -->
                                </div>
                            </div>
                            <div class="widget-foot">
                            
                                <div class="pull-right">
                               <!--  {{ submit_button('Save Changes' , 'name':'ann_update', 'class':'btn btn-primary') }} -->
                                <button type="submit" name="ann_update" value="Save Changes" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-disk"></span> Save Changes
                            </button>

                                <button type="button" class="btn btn-danger" onclick="window.location='/admin/announcements'"><span class="glyphicon glyphicon-remove"></span>  Cancel</button>
                            </div>
                                <input type="hidden" name="csrf" value="<?php echo $this->security->getToken() ?>"/>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- Matter ends