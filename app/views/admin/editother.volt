  <!-- Page heading -->
            <div class="page-head">
              <h2 class="pull-left"><i class="icon-table"></i> Other</h2>

              <!-- Breadcrumb -->
              <div class="bread-crumb pull-right">
                <a href="/admin"><i class="icon-home"></i> Home</a> 
                <!-- Divider -->
                <span class="divider">/</span> 
                <a href="/admin/other">Other</a> 
                <!-- Divider -->
                <span class="divider">/</span> 
                <span>Edit</span>
              </div>

              <div class="clearfix"></div>

            </div>
            <!-- Page heading ends -->

            <div class="matter">
            <div class="container">

            <div class="row">

             <div class="col-md-12">           
             <div class="widget"> 
          
          <!-- container block  -->
            <div class="widget-head">
            <div class="pull-left">Edit</div>
            <div class="clearfix"></div>
            </div>

            <div class="widget-content">
              <div class="padd">
                 {{ content() }}
              <form class="form-horizontal" method="post">
              <label>Content</label>
              {{ required }}
              {{ text_area('page_content', 'name':'page_content' , 'placeholder':'Enter content', 'class':'form-control programPageText','value':about.content) }}

            </div>
          </div>

             <!-- footer start -->
                 <div class="widget-foot">
                  <div class="clearfix"></div> 
                  <input type="Submit" name="save_page" id="inputEmail" Value="Save Changes" class="btn btn-info">
                 <button type="button" class="btn btn-default" onclick="window.location='/admin/other'">Cancel</button>

               </div>
              <!-- footer end -->
            </form>
          <!-- container end  -->
            </div>
            </div>

            </div>

            </div>
          </div>