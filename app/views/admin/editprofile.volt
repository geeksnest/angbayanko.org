	    <!-- Page heading -->
	    <div class="page-head">
        <!-- Page heading -->
	      <h2 class="pull-left"> 
          <!-- page meta -->
          <span class="page-meta">Account</span>
        </h2>


        <!-- Breadcrumb -->
        <div class="bread-crumb pull-right">
          <a href="/admin"><i class="icon-home"></i> Home</a> 
          <!-- Divider -->
          <span class="divider">/</span> 
          <a href="#" class="bread-current">Edit Profile</a>        
        </div>

        <div class="clearfix"></div>

	    </div>
	    <!-- Page heading ends -->



	    <!-- Matter -->

	    <div class="matter">
        <div class="container">

          <div class="row">

            <div class="col-md-12">
              {{ content() }}

              <div class="widget wgreen">
                
                <div class="widget-head">
                  <div class="pull-left">Edit User</div>
                  <div class="widget-icons pull-right">
                    <!-- <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>  -->
                    <!-- <a href="#" class="wclose"><i class="icon-remove"></i></a> -->
                  </div>
                  <div class="clearfix"></div>
                </div>

                <div class="widget-content">
                  <div class="padd">

                    <h6>User Account</h6>
                    <hr />
                    <!-- Form starts.  -->
                     {{ form('admin/editprofile', 'class': 'form-horizontal') }}
                                <?php echo $form->render('huserName', array( 'value' => $user->userName)); ?>
                                <?php echo $form->render('huserEmail', array( 'value' => $user->userEmail)); ?>
                                <div class="form-group">
                                  <label class="col-lg-4 control-label">{{ form.label('username') }}</label>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('username', array( 'value' => $user->userName )); ?>
                                    {{ form.messages('username') }}
                                  </div>
                                </div>
                              
                                <div class="form-group">
                                  <label class="col-lg-4 control-label">{{ form.label('email') }}</label>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('email', array( 'value' => $user->userEmail )); ?>
                                    {{ form.messages('email') }}
                                  </div>
                                </div>
                    
                    <h6>User Profile</h6>
                    <hr />
                                <div class="form-group">
                                  <label class="col-lg-4 control-label">{{ form.label('firstname') }}</label>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('firstname', array( 'value' => $user->userFirstname)); ?>
                                    {{ form.messages('firstname') }}
                                  </div>
                                </div>  

                                <div class="form-group">
                                  <label class="col-lg-4 control-label">{{ form.label('lastname') }}</label>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('lastname', array( 'value' => $user->userLastname)); ?>
                                    {{ form.messages('lastname') }}
                                  </div>
                                </div>  

                                <div class="form-group">
                                  <label class="col-lg-4 control-label">{{ form.label('middlename') }}</label>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('middlename', array( 'value' => $user->userMiddlename)); ?>
                                    {{ form.messages('middlename') }}
                                  </div>
                                </div>  

                                <div class="form-group">
                                  <label class="col-lg-4 control-label">{{ form.label('address') }}</label>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('address', array( 'value' => $user->userAddress)); ?>
                                    {{ form.messages('address') }}
                                  </div>
                                </div> 

                                <div class="form-group">
                                  <label class="col-lg-4 control-label">{{ form.label('company') }}</label>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('company', array( 'value' => $user->userCompany)); ?>
                                    {{ form.messages('company') }}
                                  </div>
                                </div> 

                                <div class="form-group">
                                  <label class="col-lg-4 control-label">{{ form.label('contact') }}</label>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('contact', array( 'value' => $user->userContact)); ?>
                                    {{ form.messages('contact') }}
                                  </div>
                                </div> 

                                <div class="form-group">
                                  <label class="col-lg-4 control-label">{{ form.label('position') }}</label>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('position', array( 'value' => $user->userPosition)); ?>
                                    {{ form.messages('position') }}
                                  </div>
                                </div>  
                                  {# form.render('csrf', ['value': security.getToken()]) #}
                                   {#{ form.render('csrf', ['name': this.security.getTokenKey(), 'value': this.security.getToken()]) }#}
                                <div class="form-group">
                                  <div class="col-lg-offset-1 col-lg-9">
                                    {{ submit_button('Update' , 'class':'btn btn-primary', 'name':'update_button') }}
                                    <button type="reset" class="btn btn-danger">Reset</button>
                                  </div>
                                </div>
                              </form>
                  </div>
                </div>
                  <div class="widget-foot">
                    <!-- Footer goes here -->
                  </div>
                </div>  
              <div class="widget wgreen">
                <div class="widget-head">
                  <div class="pull-left">Change Password</div>
                  <div class="widget-icons pull-right">
                  </div>
                  <div class="clearfix"></div>
                </div>
                <div class="widget-content">
                  <div class="padd">
                  <h6>Change Password</h6>
                  <hr />
                  {{ form('admin/editprofile', 'class': 'form-horizontal') }}
                 <!--  <div class="form-group">
                    <label class="col-lg-4 control-label">{#{ form.label('oldpassword') }}</label>
                    <div class="col-lg-8">
                      {{ form.render('oldpassword') }#}
                      {{ oldpassError }}
                    </div>
                  </div>
 -->
                  <div class="form-group">
                    <label class="col-lg-4 control-label">{{ form.label('password') }}</label>
                    <div class="col-lg-8">
                      {{ form.render('password') }}
                      {{ passError }}
                      <div class="label label-danger" id="passerror"></div>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-lg-4 control-label">{{ form.label('repassword') }}</label>
                    <div class="col-lg-8">
                      {{ form.render('repassword') }}
                      {{ repassError }}
                       <div class="label label-danger" id="repasserror"></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-lg-offset-1 col-lg-9">
                      {{ submit_button('Update Password' , 'class':'btn btn-primary', 'name':'update_password','id':'savepassBtn') }}
                    </div>
                    </div>
                    </form>
                  </div>
                </div>
                <div class="widget-foot">
                  <!-- Footer goes here -->
                </div>
              </div>
          </div>

        </div>
		  </div>

		<!-- Matter ends -->