            <!-- Modal Prompt-->
            <div id="modalPrompt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Alert!</h4>
                  </div>
                  <div class="modal-body">
                    <p class="modal-message"></p>
                    <span class="modal-list-names"></span>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
                    <a href="#" type="button" class="btn btn-primary modal-btn-yes" data-form='userform'>Yes</a>
                  </div>
                </div>
              </div>
            </div>
            <!-- Page heading -->
            <div class="page-head">
              <h2 class="pull-left"><i class="icon-table"></i> Newsletters</h2>

              <!-- Breadcrumb -->
              <div class="bread-crumb pull-right">
                <a href="/admin"><i class="icon-home"></i> Home</a>
                <!-- Divider -->
                <span class="divider">/</span>
                <span>Create Newsletter</span>
              </div>

              <div class="clearfix"></div>

            </div>
            <!-- Page heading ends -->

            <!-- Matter -->
            {{ script }}
            
            <div class="matter">
              {{ form('admin/createnewsletter', 'class': 'form-horizontal', 'id':'main-table-form') }}
              <div class="container">
                {% if (not(msg is empty)) %}
                <div style="color:green;padding:15px;background-color:#D5FFCD;">
                  {{ msg }}
                </div>
                {% endif %}
                <!-- Table -->

                <div class="row">


                  <!-- <div class="col-md-12">
                    <ul class="nav nav-tabs">
                      <li class="active"><a href="/admin/createnewsletter">Create Newsletter</a></li>
                      <li><a href="/admin/newsletter">Subscribers</a></li>
                    </ul>
                  </div>
                  <br /> -->

                  <div class="col-md-12">
                    {{ content() }}


                        <div class="widget">

                          <div class="widget-head">
                            <div class="pull-left">Create Newsletter</div>
                            <div class="widget-icons pull-right">
                              <!-- <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>  -->
                              <!-- <a href="#" class="wclose"><i class="icon-remove"></i></a> -->
                            </div>
                            <div class="clearfix"></div>
                          </div>


                          <div class="widget-content">
                            <!-- {#{ hidden_field('csrf', 'value': security.getToken())}#}
 -->
                            <div class="padd">
                              <label>Subject</label>
                              <span class="asterisk">*</span>
                              <input type="text" name="subject" id="subject" class="form-control" value="<?php if(isset($_POST['reset'])){echo '';}else{echo $_POST['subject'];}?>" placeholder="Subject">
                              <div id="errsubject">{{ errNLSubject }}</div>
                              <br />
                              <label>Message</label>
                              <span class="asterisk">*</span>
                              {{ text_area("message", 'placeholder':'Message', 'class':'programPageText') }}
                              <div id="errdetails">{{ errNLMessage }}</div>
                            </div>

                            <div class="widget-foot">


                              <!-- <label><input type="checkbox" name="save" value="1" checked="checked"> Save</label> -->
                              <div class="btnFooter">
                              <input type="submit" name="send" class="btn btn-primary" value="Send Newsletter" >
                               <input type="submit" name="reset" id="reset" class="btn btn-danger" value="Reset">
                               </div>
                              <div class="clearfix"></div>
                            </div>

                          </div>

                        </div>


                      </div>

                    </div>
      <div class="row"><!--start row-->
      <div class="col-lg-12">
        <div class="widget">
          <div class="widget-head">
          <div class="pull-left">Digital Assets (<span id="fileCount"><?php echo count($pictures)?></span> files)</div>
            <div class="widget-icons pull-right">
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="widget-content">
            <div class="padd">
              <div id="imageError"></div>
              <span class="btn btn-success fileinput-button">
                <i class="glyphicon glyphicon-plus"></i>
                <span>Add files...</span>
                <!-- The file input field used as target for the file upload widget -->
                <input id="newsletterpictures" type="file" name="files[]" multiple accept="image/*">
              </span>
              <!-- The global progress bar -->
              <div id="progress" class="progress">
                <div class="progress-bar progress-bar-success"></div>
              </div>
              <div class="gallery" id="newsletter-gallery">
                {% for img in pictures %}
                <div class="program-digital-assets-library pull-left" style="position: relative">
                  <a href="{{ url("img/" ~ prog~ "/" ~ img.fileName) }}" class='prettyPhoto[pp_gal]'>
                    <img src="{{ url("img/" ~ prog ~ "/" ~ img.fileName) }}" alt=""></a>
                    <input type="text" onclick="this.focus();this.select()" name="picturename" class="form-control" value="{{ url("http://angbayanko.org/img/" ~ prog~ "/" ~ img.fileName) }}">
                    <button type="button" class="btn btn-xs btn-danger delete-recent-upload-newsletter" data-filename="{{ img.fileName }}" data-folder="{{ prog }}" data-picture-id="{{ img.imgID }}" style="position: absolute; top: 0px; left:0px; z-index:999999"><i class="icon-remove"></i> </button>
                  </div>
                  {% endfor %}
                  <div class="clearfix"></div>
                </div>
              </div>
              <div class="widget-foot">

              </div>
            </div>
          </div>
        </div>
      </div><!--end row-->

                  </div>
                </form>
              </div>
<script type="text/javascript">
  $("#reset").on("click", function(){
       //code here
       $('#message').val('');
       history.go(0);
});
  $('#subject').on('change keyup keydown',function(){
    var x = $('#subject').val();
    if(x == ''){
      $('#errsubject').html('<div class="label label-danger">Subject is required</div>');
    }else{
      $('#errsubject').html('');
    }
  });
</script>
<!-- Matter ends -->
