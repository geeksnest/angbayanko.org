 
<style type="text/css">
  input[type='file'] {
  color: transparent;
}
</style>

            <!-- Modal Prompt-->
            <div id="modalPrompt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div id="headerColor">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color: white;font-size: 17px">×</button>
                    <span  style="color: white;font-size: 17px" id="changewarning"></span>
                  </div>
                  <div class="modal-body">
                    <p class="modal-message"></p>
                    <span class="modal-list-names"></span>
                  </div>
                  <div class="modal-footer">
<!--                     <a href="#" type="button" class="btn btn-primary modal-btn-yes" data-form='userform'>Yes</a>
                    <a type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancel</a> -->
                <a href="#" type="button" class="btn btn-primary modal-btn-yes" data-form='userform'><span class="icon glyphicon glyphicon-ok"></span> Yes</a>
                <a type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><span class="icon glyphicon glyphicon-remove"></span> No</a>
                  </div>
                </div>
              </div>
            </div>
            <!-- Page heading -->
            <div class="page-head">
              <!-- Page heading -->
              <h2 class="pull-left">
                <!-- page meta -->
                <span class="page-meta">Programs</span>
              </h2>


              <!-- Breadcrumb -->
              <div class="bread-crumb pull-right">
                <a href="/admin"><i class="icon-home"></i> Home</a>
                <!-- Divider -->
                <span class="divider">/</span>
                <a href="/admin/programs/0" class="bread-current">Programs</a>
              </div>

              <div class="clearfix"></div>

            </div>
            <!-- Page heading ends -->
            <!-- Matter -->
            <div class="matter">
              <div class="container">
              <p><?php $this->flashSession->output(); ?></p>
                {{ content() }}
                {{ titletaken }}
                <div class="row">
                  <div class="col-md-12">
                    <div class="widget">
                      <div class="widget-head">
                        <div class="pull-left">{% if edit %} Edit {% else %} Create {% endif %} Program</div>
                        <div class="widget-icons pull-right">
                        </div>
                        <div class="clearfix"></div>
                      </div>
                      <div class="widget-content">
                      <!-- EDIT PROGRAM-->
                      <div class="padd">
                      {% if edit %}

                          <div class="form quick-post">
                            <!-- Edit profile form (not working)-->
                            <form action="{{ url('admin/programs/' ~ eprogs.programID ) }}" method="POST" class="form-horizontal" 
                            enctype="multipart/form-data">

                              <!-- Title Color -->
                            <div class="form-group">
                              <div class="col-lg-1">
                                <label class="control-label">Title Color</label>
                              </div>
                              <div class="col-lg-8">
                               <input type="color" name="color" value="<?php echo "$eprogs->programColor"; ?>">
                              </div>
                            </div>

                              <!-- Title Icon-->
                            <div class="form-group">
                              <div class="col-lg-1">
                                <label class="control-label">Title Icon</label>
                              </div>
                              <div class="col-lg-8">
                             <input type="file" name="icon" id="icon" accept="image" onchange="readURL(this);">
                             <img src="/img/icons/<?php echo "$eprogs->programIcon"; ?>" id="blah" src="http://placehold.it/80" alt="your image" width="80px" height= "80px"><br/>
                             <span id="file-upload-filename"></span>
                             <!-- <img src="/img/icons/<?php echo "$eprogs->programIcon"; ?>"  width="32px" height="32px"><span> <?php echo "$eprogs->programIcon"; ?> -->
                             </span>
                              </div>
                            </div>

                            <!-- Title -->
                            <div class="form-group">
                              <div class="col-lg-1">
                                <label class="control-label"  for="title">{{ form.label('title') }} <span class="asterisk">*</span></label>
                              </div>
                              <div class="col-lg-8">
                                <?php echo $form->render('title', array( 'value' => $eprogs->programName)); ?>
                                {{ form.messages('title') }}
                                 {{ titleErr }}
                              </div>
                            </div>
                            <!-- Content -->
                            <div class="form-group">
                              <div class="col-lg-1">
                              <label class="control-label" for="content">{{ form.label('tagline') }} <span class="asterisk">*</span></label>
                              </div>

                              <div class="col-lg-8">
                                <?php echo $form->render('tagline', array( 'value' => $eprogs->programTagline)); ?>
                                {{ form.messages('tagline') }}
                                  {{ taglineErr }}
                              </div>
                            </div>
                            <!-- Content -->
                            <div class="form-group">
                            <div class="col-lg-1">
                              <label class="control-label" for="tooltip">{{ form.label('tooltip') }} <span class="asterisk">*</span></label>
                            </div>
                              <div class="col-lg-8">
                                <?php echo $form->render('tooltip', array( 'value' => $eprogs->programTooltip)); ?>
                                {{ form.messages('tooltip') }}
                                 {{ tooltipErr }}
                              </div>
                            </div>
                            <!-- Content -->
                            <div class="form-group">
                            <div class="col-lg-1">
                              <label class="control-label" for="content">{{ form.label('programurl') }} <span class="asterisk">*</span></label>
                            </div>
                              <div class="col-lg-8">
                                <?php echo $form->render('programurl', array( 'value' => $eprogs->programPage)); ?>
                                {{ form.messages('programurl') }}
                                {{program_urlErr}}
                              </div>
                            </div>
                            <div id="imageError"></div>
                            <div class="form-group">
                              <div class="col-lg-1">
                              </div>
                              <div class="col-lg-8">
                                <span class="btn btn-success fileinput-button">
                                  <i class="glyphicon glyphicon-plus"></i>
                                  <span>Add Flipping Images</span>
                                  <!-- The file input field used as target for the file upload widget -->
                                  <input id="fileupload" type="file" name="files[]" multiple accept="image/*">
                                </span>
                              </div>
                            </div>

                            <div class="form-group">
                              <div class="col-lg-2">
                              </div>
                              <div class="col-lg-8">
                              <!-- {{ pictureerror }} -->
                               {{ program_pictureErr }}
                              <!-- The global progress bar -->
                              <div id="progress" class="progress">
                                <div class="progress-bar progress-bar-success"></div>
                              </div>
                              </div>
                            </div>

                            <!-- The container for the uploaded files -->
                            <div class="form-group">
                              <div class="col-lg-1">
                              </div>
                              <div class="col-lg-8">
                                <table id="files" class="table table-striped table-bordered table-hover"><tbody>
                                  {% for ep in eprogsi %}
                                    <tr>
                                      <td>
                                  {{ image("img/programs/thumbnail/" ~ ep.imgname, "alt": "alternative text") }}
                                       <br> {{ ep.imgname }} </td>

                                      <td><input type="hidden" name="program_picture" value="{{ ep.imgname }}"><a class="btn btn-xs btn-danger delete_program_picture modal-control-button" data-recorid=""><i class="icon-remove"></i> </a> </td>
                                    </tr>
                                  {% endfor %}
                                </tbody>
                              </table>
                            </div>

                            <div class="form-group">
                            </div>
                       <!--{#{ form.render('csrf', ['value': security.getToken()]) }}
                            {{ form.messages('csrf') }#}     -->
                            <br/>
                            <!-- Buttons -->
                            <div class="form-group">
                             <!-- Buttons -->
                             <div class="center col-lg-offset-2 col-lg-6">
                            <!--  <button type="submit" class="btn btn-success" name="update-program" value="true"><span class="glyphicon glyphicon-floppy-disk"></span> Save Changes</button> -->
                            {{ submit_button('Save Changes', 'class':'btn btn-success', 'name':'update-program') }}
                              <a href="/admin/programlist" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span> Cancel</a>

                            </div>
                            </div>
                          </form>
                        </div>
                      {% else %}
                      <!-- ADD PROGRAM-->
                          <div class="form quick-post">
                            <!-- Edit profile form (not working)-->
                            <form action="{{ url('admin/programs/0' ) }}" method="post" class="form-horizontal" 
                            enctype="multipart/form-data">

                              <!-- Title Color -->
                            <div class="form-group">
                              <div class="col-lg-1">
                                <label class="control-label">Title Color</label>
                              </div>
                              <div class="col-lg-8">
                               <input type="color" name="color" value="#1570a6">
                              </div>
                            </div>

                            <!-- Title Icon-->
                            <div class="form-group">
                              <div class="col-lg-1">
                                <label class="control-label">Title Icon</label>
                              </div>
                              <div class="col-lg-8">
                             <input type="file" name="icon" id="icon" accept="image" onchange="readURL(this);">
                             <img id="blah" src="http://placehold.it/80" alt="your image" width="80px" height= "80px"><br/>
                             <span id="file-upload-filename"></span>
                            <!--  <div id="file-upload-filename"></div> -->
                              </div>
                            </div>

                            <!-- Title -->
                            <div class="form-group">
                              <div class="col-lg-1">
                              <label class="control-label" for="title">{{ form.label('title') }} <span class="asterisk">*</span></label>
                            </div>
                              <div class="col-lg-8">
                                {{ form.render('title') }}
                             <!--   {{ titletaken }} -->
                            <span id="hidetitle"> {{ titleErr }} </span>
                              </div>
                            </div>
                            <!-- Content -->
                            <div class="form-group">
                              <div class="col-lg-1">
                              <label class="control-label" for="content">{{ form.label('tagline') }} <span class="asterisk">*</span></label>
                            </div>
                              <div class="col-lg-8">
                                {{ form.render('tagline') }}
                              <!--   {{ form.messages('tagline') }} -->
                            <span id="hidetagline">  {{ taglineErr }} </span>
                              </div>
                            </div>
                            <!-- Content -->
                            <div class="form-group">
                              <div class="col-lg-1">
                              <label class="control-label" for="tooltip">{{ form.label('tooltip') }} <span class="asterisk">*</span></label>
                            </div>
                              <div class="col-lg-8">
                                {{ form.render('tooltip') }}
                              <!--   {{ form.messages('tooltip') }} -->
                              <span id="hidetooltip">  {{ tooltipErr }} </span>
                              </div>
                            </div>
                            <!-- Content -->
                            <div class="form-group">
                              <div class="col-lg-1">
                              <label class="control-label" for="content">{{ form.label('programurl') }} <span class="asterisk">*</span></label>
                            </div>
                              <div class="col-lg-8">
                                {{ form.render('programurl') }}
                              <!--   {{ form.messages('programurl') }} -->
                              <span id="hideurl">  {{program_urlErr}} </span>
                              </div>
                            </div>

                            <div class="form-group">
                              <div class="col-lg-1">
                              <label class="control-label" for="content">&nbsp;</label>
                            </div>
                            <div class="col-lg-8">
                            <div id="imageError"></div>
                            <span class="btn btn-success fileinput-button">
                              <i class="glyphicon glyphicon-plus"></i>
                              <span>Add Flipping Images...</span>
                              <!-- The file input field used as target for the file upload widget -->
                              <input id="fileupload" type="file" name="files[]" multiple accept="image/*">
                            </span>
                          <span id="hidepic">  {{ program_pictureErr }} </span>
                           <!--  {{ form.messages('files[]') }} -->
                            <!-- {{ pictureerror }} -->
                            <!-- The global progress bar -->
                            <div id="progress" class="progress">
                              <div class="progress-bar progress-bar-success"></div>
                            </div>
                            <!-- The container for the uploaded files -->
                            <table id="files" class="table table-striped table-bordered table-hover">
                            <tbody></tbody></table>
                          </div>


                          <!--   {{ form.render('csrf', ['value': security.getToken()]) }}
                            {{ form.messages('csrf') }}        <br/> -->
                            <!-- Buttons -->

                          <!---{{ form.render('csrf', ['value': security.getToken()]) }}-->
                          <!--- {{ form.messages('csrf') }}        <br/>-->
                            <!--- Buttons -->

                            <div class="form-group">
                             <!-- Buttons -->
                             <div class="center col-lg-offset-2 col-lg-6">
                              <button type="submit" class="btn btn-primary" name="publish-program"><span class="glyphicon glyphicon-floppy-disk" ></span> Publish</button>
                              <a href="/admin/programs/" ><button type="button" class="btn btn-danger"><span class="glyphicon glyphicon-refresh" ></span> Reset</button></a>
                            </div>
                            </div>
                          </form>
                        </div>
                      {% endif %}
                      </div>
                    </div>
                  <!--   <div class="widget-foot">
 -->                      <!-- Footer goes here -->
                   <!--  </div> -->
                  </div>
                  </div>
                  <!-- Task widget -->
    <!-- <div class="col-md-5">
      <div class="widget">
        <div class="widget-head">
          <div class="pull-left">List of Programs</div>
            <div class="widget-icons pull-right">
            </div>
          <div class="clearfix"></div>
        </div>
          <div class="widget-content referrer">
            {{ form('admin/programs', 'class': 'form-horizontal', 'id':'main-table-form') }}
            <input type="hidden" class="tbl-action" name="action" value=""/>
            <input type="hidden" class="tbl-recordID" name="recordID" value=""/>
            <input type="hidden" class="tbl-edit-url" name="editurl" value=""/>
              <table class="table table-striped table-bordered table-hover">
                <tbody>
                <?php if($prog == true){ ?>
                {% for p in prog %}
                <tr>
                <td class="name">{{ p.programName }} <br/> URL: {{ p.programPage }}</td>
                <td>
                <a href="#modalPrompt" class="btn btn-xs btn-warning tbl_edit_row modal-control-button" data-toggle="modal" data-action="edit" data-recorID="{{ p.programID }}"><i class="icon-pencil"></i> Edit </a>
                <a href="#modalPrompt" class="btn btn-xs btn-danger tbl_delete_row modal-control-button" data-toggle="modal" data-action="delete" data-recorID="{{ p.programID }}"><i class="icon-remove"></i> Delete</a>
                </td>
                </tr>

                {% endfor %}
                <?php } else {
                echo "<tr><td>No Programs Yet.</td></tr>";
                } ?>
                </tbody>
              </table>
            </form>
          </div>
      </div>
    </div> -->
</div>

            </div>
          </div>
          <script type="text/javascript">
                 function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
          </script>

          <script type="text/javascript">
            var input = document.getElementById( 'icon' );
            var infoArea = document.getElementById( 'file-upload-filename' );

            input.addEventListener( 'change', showFileName );

            function showFileName( event ) {
  
  // the change event gives us the input it occurred in 
            var input = event.srcElement;
  
  // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
            var fileName = input.files[0].name;
  
  // use fileName however fits your app best, i.e. add it into a div
            infoArea.textContent = 'File name: ' + fileName;
            }
          </script>


		<!-- Matter ends -->
