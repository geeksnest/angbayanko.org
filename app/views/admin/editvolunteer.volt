	    <!-- Page heading -->
	    <div class="page-head">
        <!-- Page heading -->
	      <h2 class="pull-left">
          <!-- page meta -->
          <span class="page-meta">Volunteer</span>
        </h2>


        <!-- Breadcrumb -->
        <div class="bread-crumb pull-right">
          <a href="/admin"><i class="icon-home"></i> Home</a>
          <!-- Divider -->
          <span class="divider">/</span>
          <a href="/admin/abkrescueteam" class="bread-current">Volunteers</a>
          <!-- Divider -->
          <span class="divider">/</span>
          <a href="" class="bread-current">Edit</a>
        </div>

        <div class="clearfix"></div>

	    </div>
	    <!-- Page heading ends -->

	    <!-- Matter -->

	    <div class="matter">
        <div class="container">

          <div class="row">

            <div class="col-md-12">
              <!-- {{ content() }} -->

              <div class="widget wgreen">

                <div class="widget-head">
                  <div class="pull-left">Edit Volunteer Information</div>
                  <div class="widget-icons pull-right">
                    <!-- <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>  -->
                    <!-- <a href="#" class="wclose"><i class="icon-remove"></i></a> -->
                  </div>
                  <div class="clearfix"></div>
                </div>

                <div class="widget-content">
                  <div class="padd">

                    <h6>Volunteers Account</h6>
                    <hr />
                    <!-- Form starts.  -->
                     {{ form('admin/editvolunteer/' ~ volunteer.id, 'class': 'form-horizontal') }}
                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label>FirstName</label>
                                   <span class="asterisk">*</span>
                                 </div>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('firstname', array( 'value' => $volunteer->fname)); ?>
                                    {{ form.messages('firstname') }}
                                    {{ fname }}
                                  </div>
                                </div>

                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label>LastName</label>
                                   <span class="asterisk">*</span>
                                 </div>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('lastname', array( 'value' => $volunteer->lname)); ?>
                                    {{ form.messages('lastname') }}
                                    {{ lname }}
                                  </div>
                                </div>

                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label>Email</label>
                                  <span class="asterisk">*</span>
                                </div>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('email', array( 'value' => $volunteer->email)); ?>
                                    {{ form.messages('email') }}
                                    {{ email }}
                                  </div>
                                </div>

                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label>Address</label>
                                </div>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('address', array( 'value' => $volunteer->address)); ?>
                                    {{ form.messages('address') }}
                                  </div>
                                </div>

                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label>City</label>
                                </div>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('city', array( 'value' => $volunteer->city)); ?>
                                    {{ form.messages('city') }}
                                  </div>
                                </div>

                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label>Country</label>
                                </div>
                                  <div class="col-lg-8">
                                  {{ form.render('country', ["class":'form-control']) }}
                                    {{ form.messages('country') }}
                                  </div>
                                </div>

                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label>State</label>
                                </div>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('state', array( 'value' => $volunteer->state)); ?>
                                    {{ form.messages('state') }}
                                  </div>
                                </div>

                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label>Zip</label>
                                </div>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('zip', array( 'value' => $volunteer->zip)); ?>
                                    {{ form.messages('zip') }}
                                  </div>
                                </div>

                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label>Contact</label>
                                  <span class="asterisk">*</span>
                                </div>
                                  <div class="col-lg-8">
                                    <?php echo $form->render('contact', array( 'value' => $volunteer->contact)); ?>
                                    {{ form.messages('contact') }}
                                  </div>
                                </div>

                                  {# form.render('csrf', ['value': security.getToken()]) #}
                                  {#{ form.render('csrf', ['name': this.security.getTokenKey(), 'value': this.security.getToken()]) }#}
                                <div class="form-group">
                                  <div class="col-lg-offset-1 col-lg-9">
                                    {{ submit_button('Save Changes' , 'class':'btn btn-primary', 'name':'update_button') }}

                                    <!-- <button type="button" class="btn btn-danger">Reset</button> -->
                                    <button type="button" class="btn btn-default" onclick="window.location='/admin/abkrescueteam'">Cancel</button>
                                  </div>
                                </div>
                              </form>
                  </div>
                </div>
                  <div class="widget-foot">
                    <!-- Footer goes here -->
                  </div>
              </div>


            </div>

          </div>

        </div>
		  </div>

		<!-- Matter ends -->
