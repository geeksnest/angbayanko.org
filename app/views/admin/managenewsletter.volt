<style type="text/css">
               .modal-header-color {
                background-color:   #ff0000;
                /*background: -webkit-linear-gradient(top,  #ff0000,  #ff0000);*/
                border-top-right-radius: 3px;
                border-top-left-radius: 3px;
                /*text-shadow:0px 1px #fff;
                border-bottom: 1px solid  #ff0000;
                border-top: 1px solid   #ff0000;*/
                color: white;
                font-size: 17px;
                font-weight: bold;
                padding: 8px 15px;
                padding-top:7px;
                padding-bottom: 7px;
             }
</style>
<div id="modalView" class="modal fade modalView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" style="color: white;font-size: 17px" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title" style="color: white;font-size: 17px">View Record</h4>
        </div>
        <div class="modal-body">
          <p class="modal-message"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Close</button>
        </div>
      </div>
    </div>
  </div>
            <!-- Modal Prompt-->
            <div id="modalPrompt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">

                  <div id="headerColor">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color: white;font-size: 17px">×</button>
                    <h4 class="modal-title" style="font-size: 17px;color:white;">Alert!</h4>
                  </div>
                  <div class="modal-body">
                    <p class="modal-message"></p>
                    <span class="modal-list-names"></span>
                  </div>
                  <div class="modal-footer">
                   <a href="#" type="button" class="btn btn-primary modal-btn-yes" data-form='userform'><span class="icon glyphicon glyphicon-ok"></span> Yes</a>
                   <a type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><span class="icon glyphicon glyphicon-remove"></span> No</a>
                 </div>
                </div>
              </div>
            </div>

            <!-- Page heading -->
            <div class="page-head">
              <h2 class="pull-left"><i class="icon-table"></i> Subscribers</h2>

              <!-- Breadcrumb -->
              <div class="bread-crumb pull-right">
                <a href="/admin"><i class="icon-home"></i> Home</a>
                <!-- Divider -->
                <span class="divider">/</span>
                <a href="" class="bread-current">Subscribers</a>
              </div>

              <div class="clearfix"></div>

            </div>
            <!-- Page heading ends -->

            <!-- Matter -->

            <div class="matter" onunload="return myFunction()">
              {{ form('admin/managenewsletter', 'id':'main-table-form') }}
              <div class="container">

                <div class="row">
                  <div class="col-md-2">
                    <label>Search by Text</label>
                  </div>
                  <div class="col-md-6 form-group">
                    <div class="col-md-12">
                      {{ text_field('search_text' , 'class':'form-control') }}
                    </div>
                  </div>
                  <div class="col-md-3 form-group">
                    <!-- {{ submit_button('Search', 'class':'btn btn-default') }} -->
               <!--      <button type="submit" name="clear_search" class="btn btn-default" value="Clear Search"><span class="icon-refresh"></span></button> -->
                      <button name="Search" id="Search" class="btn btn-success"><span class="icon-search"></span> Search</button>
                      <button name="clear_search" id="btnclearRef" class="btn btn-danger"><span class="icon-refresh"></span> Refresh</button>
                  </div>
                </div>

                <!-- Table -->
                <div class="row">
                  <div class="col-md-2">
                    <label>Filter by Date</label>
                  </div>
                  <div class="col-md-4 form-group" id="fromDatepicker" class="input-append">
                    <label class="col-md-2 control-label">From</label>
                    <div class="col-md-10">
                      {{ text_field('fromDate', 'readonly': 'true', 'data-format':'yyyy-MM-dd','class':'form-control dtpicker', 'data-time-icon':'icon-time') }}
                      <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="btn btn-info btn-lg icon-calendar"></i>
                      </span>
                    </div>
                  </div>
                  <div class="col-md-3 form-group" id="toDatepicker" class="input-append" style="margin-left: -8.5%;">
                    <label class="col-md-2 control-label">To</label>
                    <div class="col-md-12">
                      {{ text_field('toDate', 'readonly': 'true', 'data-format':'yyyy-MM-dd','class':'form-control dtpicker', 'data-time-icon':'icon-time') }}
                      <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="btn btn-info btn-lg icon-calendar"></i>
                      </span>
                    </div>
                  </div>
                  <div class="col-md-2">
                    {{ submit_button('Filter', 'class':'btn btn-primary') }}
                  </div>
                </div>

                <!-- Table -->
                
                <!-- {{ content() }} *used to display flash->success but causes white screen in test server-->
                <!-- workaround. message is in _deleteLetter() in admincontroller -->
                {% if sucksez %}
                  <div class="alert alert-success alert-dismissable fade in">{{sucksez}}</div>
                {% endif %}
                {% if searchez %}
                  <div class="alert alert-info">{{searchez}}</div>
                {% endif %}

                <div class="row">

                  <div class="col-md-12">


                    <div class="widget">

                      <div class="widget-head">
                        <div class="pull-left">Subscribers</div>
                        <div class="widget-icons pull-right">
                          <!-- <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>  -->
                          <!-- <a href="#" class="wclose"><i class="icon-remove"></i></a> -->
                        </div>
                        <div class="clearfix"></div>
                      </div>


                      <div class="widget-content">
                        {{ hidden_field('csrf', 'value': security.getToken())}}
                        <input type="hidden" class="tbl-action" name="action" value=""/>
                        <input type="hidden" class="tbl-recordID" name="recordID" value=""/>
                        <input type="hidden" class="tbl-edit-url" name="editurl" value="editpage/"/>
                        <table class="table table-striped table-bordered table-hover tblusers">
                          <thead>
                            <tr>
                              <th width="6px">{{ check_field('select_all[]', 'class':'tbl_select_all') }}</th>
                              <th><a href="?sort={{ letterSubjectHref }}">Subject <i class="{{ letterSubectIndicator ? letterSubjectIndicator : "" }}"></i></a></th>
                              <th><a href="?sort={{ dateCreatedHref }}">Date Sent <i class="{{ dateCreatedIndicator ? dateCreatedIndicator : "" }}"></i></a></th>
                              <th width="10%">Control</th>
                            </tr>
                          </thead>
                          <tbody>

                          {% if page.total_pages == 0 %}
                                  <tr>
                                      <td colspan="5" align="center">No Records Found</td>
                                  </tr>
                              {% else %}
                                  {% for post in page.items %}
                                      <tr>
                                          <td><input type="checkbox" name="tbl_id[]" class="tbl_select_row" value="{{ post.letterID}}"> </td>
                                          <td class="name">{{ post.letterSubject }}</td>
                                          <td>{{ date("F j, Y", post.dateCreated) }}</td>
                                          <td>
                                            <center>
                                              <a  href="#modalView" data-toggle="modal" class="btn btn-xs btn-success modal-record-view" data-href="ajaxUserView/{{ post.letterID }}/letter" title="View"><i class="icon-ok"></i> </a>
                                              <a href="#modalPrompt" title="Delete" class="btn btn-xs btn-danger tbl_delete_row modal-control-button" data-toggle="modal" data-action="delete" data-recorID="{{ post.letterID }}"><span ><i class="fa fa-trash"></i></span></a>
                                            </center>
                                          </td>
                                      </tr>
                                  {% endfor %}
                              {% endif %}
                          </tbody>
                          </table>

                          <div class="tblbottomcontrol" style="display:none">
                            <a href="#modalPrompt" class="tbl_delete_all" data-toggle="modal" data-action="delete_selected" > Delete all Selected </a> |
                            <a href="#" class="tbl_unselect_all"> Unselect </a>
                          </div>

                            <div class="widget-foot">
                              {% set limit = 10 %}
                              {% set start = (limit * (page.current - 1)) + 1 %}
                              {% set end = (limit * (page.current-1)) + limit %}

                              {% if end > page.total_items %}
                              {% set end = page.total_items %}
                              {% endif %}
                              {% if limit %}
                              <div style="margin-left: 85.5%">
                                <span>&nbsp;Showing {{ start }} - {{ end  }} of {{ page.total_items }}</span>
                              </div>
                              {% endif %}
                              {% if page.items and page.total_pages > 0 %}
                                <ul class="pagination pull-right">
                                    <!---->
                                {% if page.current == 1 and page.total_pages >= 5 and page.total_pages > 0 %}
                                 <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                                  {% for index in 1..5 %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/managenewsletter?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/managenewsletter?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% elseif page.current == 1 and page.total_pages < 5 and page.total_pages > 0 %}
                                <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                                  {% for index in 1..page.total_pages %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/managenewsletter?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/managenewsletter?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% endif %}
                                <!---->
                                <!---->
                                {% if page.current != 1 and page.total_pages < 5 and page.total_pages > 0 %}
                                <li>{{ link_to("admin/managenewsletter", 'First') }}</li>
                                <li>{{ link_to("admin/managenewsletter?page=" ~ page.before, 'Prev') }}</li>
                                  {% for index in 1..page.total_pages %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/managenewsletter?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/managenewsletter?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% elseif page.current != 1 and page.current < 4 and page.total_pages >= 5 %}
                                <li>{{ link_to("admin/managenewsletter", 'First') }}</li>
                                <li>{{ link_to("admin/managenewsletter?page=" ~ page.before, 'Prev') }}</li>
                                  {% for index in 1..5 %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/managenewsletter?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/managenewsletter?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% elseif page.current >=4 and page.current+2 < page.total_pages%}
                                <li>{{ link_to("admin/managenewsletter", 'First') }}</li>
                                <li>{{ link_to("admin/managenewsletter?page=" ~ page.before, 'Prev') }}</li>
                                  {% for index in page.current-2..page.current+2 %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/managenewsletter?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/managenewsletter?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% elseif page.current >=4 and (page.current+2 == page.total_pages or page.current+2 > page.total_pages) %}
                                <li>{{ link_to("admin/managenewsletter", 'First') }}</li>
                                <li>{{ link_to("admin/managenewsletter?page=" ~ page.before, 'Prev') }}</li>
                                  {% for index in page.total_pages-4..page.total_pages %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/managenewsletter?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/managenewsletter?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% endif %}
                                <!---->
                                <!---->
                                {% if page.current != page.last %}
                                <li>{{ link_to("admin/managenewsletter?page=" ~ page.next, 'Next') }}</li>
                                <li>{{ link_to("admin/managenewsletter?page=" ~ page.last, 'Last') }}</li>
                                </ul>
                                {% else %}
                                <li>{{ link_to("admin/managenewsletter?page=" ~ page.next, 'Next','onclick':'return false','style':'cursor:default') }}</li>
                                <li>{{ link_to("admin/managenewsletter?page=" ~ page.last, 'Last','onclick':'return false','style':'cursor:default') }}</li>
                                </ul>
                                {% endif %}
                                <!---->
                              {% elseif page.total_pages == 0 %}
                              <ul class="pagination pull-right">
                              <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                              <li>{{ link_to("admin/managenewsletter?page=" ~ page.next, 'Next','onclick':'return false','style':'cursor:default') }}</li>
                                <li>{{ link_to("admin/managenewsletter?page=" ~ page.last, 'Last','onclick':'return false','style':'cursor:default') }}</li>
                                </ul>
                 {% endif %}

                              <div class="clearfix"></div>

                            </div>

                          </div>

                        </div>


                      </div>

                    </div>


                  </div>
                </form>
              </div>
<script type="text/javascript">
  $('#btnclearRef').click(function(){
      $('#search_text').val('');
      window.location.href = window.location.href; //This is a possibility
      window.location.reload(); //Another possiblity
      history.go(0); //And another
  });

</script>
<!-- Matter ends -->
