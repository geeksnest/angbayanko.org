 <!--Modal View-->
            <div id="modalPrompt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Alert!</h4>
                  </div>
                  <div class="modal-body">
                    <p class="modal-message"></p>
                    <span class="modal-list-names"></span>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
                    <a href="#" type="button" class="btn btn-primary modal-btn-yes" data-form='userform'>Yes</a>
                  </div>
                </div>
              </div>
            </div>

<?php $token = $this->security->getToken(); ?>
<div class="page-head">

  <h2 class="pull-left">
    <span class="page-meta">ABKRESCUE SETTINGS</span>
  </h2>

  <div class="bread-crumb pull-right">
    <a href="/admin"><i class="icon-home"></i> Home</a> 
    <span class="divider">/</span> 
    <a href="" class="bread-current">Create</a>          
  </div>
  <div class="clearfix"></div>
</div>

<div class="matter">
  <div class="container">
  {{ content() }}
  {{ form('admin/abkrescuesettings', 'class': 'form-horizontal') }}
    <!--start row-->
    <div class="row">
      <div class="col-md-8">
        <div class="widget">
          <div class="widget-head">
            Social Media
            <div class="widget-icons pull-right">
            </div>
          </div>

          <div class="widget-content">
            <div class="padd">

              
              <div>
              <!-- <label>Page Title</label>
              <span class="asterisk">*</span>{{ titleError }} -->
            </div>
            <?php 
                foreach($socialtbl as $key => $social){
            ?>
            <div class="form-group">
            <div class="col-sm-8">
            <div class="input-group">
              <span class="input-group-btn">
             <!--  <a href="">  <i class="fa fa-facebook-square size30"></i> </a> -->
                <button class="btn btn-social-icon btn-<?php echo $social['icon']; ?> color-facebook" type="button">
                <span class="fa fa-<?php echo $social['icon']; ?> size18"></span>
                </button>
              </span>
              <input class="input form-control " placeholder="<?php echo $social['alt']; ?>" type="text"
               name="socialicon['<?php echo $social['icon']; ?>']" value="<?php echo $social['link']; ?>">
            </div>
            </div>

<!--             <div class="col-sm-4 text-left">
              <div class="pagestatuscontent fade-in-out">
              {% if social['status'] == 1 %}
              <span class='label label-success'>Active</span>
              <a href='#modalPrompt' data-action='deactivate' data-recorID='".$social['id']."' data-toggle='modal' class='btn btn-xs btn-default tbl_delete_row modal-control-button'><i class='icon-ban-circle'></i> </a>
              {% else %}
              <span class='label label-default'>Inactive</span>
              <a href='#modalPrompt' class='btn btn-xs btn-success tbl_delete_row modal-control-button' data-toggle='modal' data-action='activate' data-recorID='".$social['id']."'><i class='icon-ok'></i></a>
              {% endif %}
              </div>

            </div> -->
            </div>
            <?php } ?>

              <br /><br />
              {{ submit_button("save_page", 'class':'btn btn-primary', 'value':'Update Settings', 'name':'save_page','id':'savepassBtn' ) }}
              <button type="button" class="btn btn-default" onclick="window.location='/admin'">Cancel</button>

              <input type="hidden" name="csrf"value="<?php echo $token ?>"/>


          </div>
        </div>
      </div>
    </div>

    <div class="col-md-4">
      <div class="widget">
        <div class="widget-head">
          Details
          <div class="widget-icons pull-right">
          </div>
        </div>

        <div class="widget-content">
          <div class="padd">
            
            <div class="clearfix"></div>
            <div class="form-horizontal">
              <div class="form-group">
                <div class="col-lg-4">
                  <div>
                  <label> Contact No. </label>
                </div>
                </div>
                <div class="col-lg-5">
                  <?php echo $form->render('contact', array( 'value' => $abkdetails['phone'])); ?>
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-4">
                  <div>
                    <label> Email </label>
                  </div>
                </div>
                <div class="col-lg-5">
                   <!-- <input id="email" type="email" name="email" placeholder="Enter Email" class="form-control"  value="<?php echo $abkdetails['email'] ?>"> -->
                   <?php echo $form->render('email', array( 'value' => $abkdetails['email'])); ?>
                   {{ passError }}
                </div>
              </div>
            </div>

            <button type="button" class="btn btn-default pull-right" id="removeSideBarImg" style="display:none">Remove Image</button>
            <div class="clearfix"></div>

          </div>
        </div>

      </div>
    </div>

    <div class="col-md-4">
      <div class="widget">
        <div class="widget-head">
          Details
          <div class="widget-icons pull-right">
          </div>
        </div>

        <div class="widget-content">
          <div class="padd">
            
            <div class="clearfix"></div>
            <div class="form-horizontal">
              <div class="form-group">
                <div class="col-lg-4">
                  <div>
                  <label> Global Members </label>
                </div>
                </div>
                <div class="col-lg-5">
               <?php echo $form->render('global_mem', array( 'value' => $abkdetails['global_mem'])); ?>
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-4">
                  <div>
                    <label> Accomplish Mission </label>
                  </div>
                </div>
                <div class="col-lg-5">
                   <?php echo $form->render('accomp_miss', array( 'value' => $abkdetails['accomp_miss'])); ?>
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-4">
                  <div>
                    <label> Worth of Donation </label>
                  </div>
                </div>
                <div class="col-lg-5">
                   <?php echo $form->render('worth_donat', array( 'value' => $abkdetails['worth_donat'])); ?>
                </div>
              </div>
            </div>

          </div>
        </div>

      </div>
    </div>


    </div><!--end row-->


      </form>
    </div><!--end container-->
</div><!--end matter