
      <div class="inner-programs pull-left">
		{% if program.programBanner %}
        <div class="dm-latest-news-head">
            <img src="{{ program.programBanner }}" height="300px" width="400px">
            <div class="dm-latest-news-label">
              {{ program.programTagline }}
            </div>
        </div>
		{% endif %}
        <div class="tabs inner-tabs">
 <!-- this is for scrollable menu  -->
 <div id="displaymenu">
<div class="scroller scroller-left"><i class="icon icon-chevron-left"></i></div>
  <div class="scroller scroller-right"><i class="icon icon-chevron-right"></i></div>
<div class="wrapper">
    <ul class="nav nav-tabs list" id="myTab">
       {% if program.specialPage == 1 %}
            <li class="active" id="pgs"><a href="#news" data-toggle="tab" onclick="window.location.reload()">Latest Breaking News</a></li>
      {% endif %}
       {% for page in prog_pages  %}
               {% if page.pageActive %}
                    <li id="pgs" class="{% if loop.first %} {% if program.specialPage != 1 %}  {% endif %} {% endif %}"><a href="#{{ page.pageSlug }}" data-toggle="tab">{{ page.pageTitle }}</a></li>
                {% endif %}
            {% endfor %}
  </ul>
  </div>
</div>
<!-- end of scrollable menu -->
{# <br>
<br>
<br>
<br> #}
<!-- hide menu to and display it for responsive page -->
{# <div class="btn1"> #}
<div class="dropdown hideme ">
    <!-- <button class="btn btn-danger" dropdown-toggle"collapsed" id="button" style="background-color:#b02c27;width: 98%;" type="button" data-toggle="dropdown"><span class="latest"> Latest Breaking News</span>
    <span class="caret lcaret" ></span></button>
     <ul  class="dropdown-menu ">
              Use unique name in anchor tag
                {% if program.specialPage == 1 %}
                     <li class="active" id="pgs"><a href="#news" data-toggle="tab" onclick="window.location.reload()">Latest Breaking News</a></li>
                     {# <li role="separator" class="divider"></li> #}
                     {% endif %}
                {% for page in prog_pages  %}
                        {% if page.pageActive %}
                             <li id="pgs {{page.ID}}" ><a href="#{{ page.pageSlug }}" class="deact" data-toggle="tab">{{ page.pageTitle }}</a></li>
                             {# <li role="separator" class="divider"></li> #}
                         {% endif %}
                     {% endfor %}
      </ul>-->
      <br>
      <br>
      <ul class="nav navbar-nav navbar-left nabbar">
        <li class="dropdown drp-nabbar">
          {# <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">More<span class="caret"></span></a> #}
          <button class="btn btn-danger" dropdown-toggle"collapsed" id="button" style="background-color:#b02c27;width: 98%;" type="button" data-toggle="dropdown"><span class="latest"> Latest Breaking News</span>
          <span class="caret lcaret" ></span></button>
          <ul class="dropdown-menu">
            {# <li><a href="#">Start Learning</a></li>
            <li><a href="#">View All Courses</a></li>
            <li><a href="#">Chat with a CodeGuide</a></li> #}
           {% if program.specialPage == 1 %}
                 <li id="pgs" class="" ><a href="#news" data-toggle="tab" onclick="window.location.reload()">Latest Breaking News</a></li>
                 {# <li role="separator" class="divider"></li> #}
                 {% endif %}
            {% for page in prog_pages  %}
                    {% if page.pageActive %}
                         <li id="pgs {{page.ID}}" ><a href="#{{ page.pageSlug }}"  data-toggle="tab">{{ page.pageTitle }}</a></li>
                         {# <li role="separator" class="divider"></li> #}
                     {% endif %}
                 {% endfor %}
          </ul>
        </li>
      </ul>
    </div>
{# </div> #}
<!-- end of hiding menu -->

          <!-- Tab conten -->
          <div class="tab-content">
                    {% if program.specialPage == 1 %}
                    <div class="tab-pane active inner-news" id="news">
                      {% if post.total_items == 0 %}
                        No News Added.
                      {% else %}
                        {% for p in post.items %}
                            {% if p.postStatus == 'publish' %}
                            <div class="entry">
                                     <h2><a href="{{ url('post/news/' ~ p.postSlug) }}">{{ p.postTitle }}</a></h2>
                                     <!-- Meta details -->

                                     <div class="meta">
                                        <div><i class="icon-calendar"></i> {{ date('d-m-Y', p.postPublishDate) }}</div>
                                        <div  class="share_buttons">
                                        <div class="fb-like" data-href="http://angbayanko.org/{{url('post/news/' ~ p.postSlug)}}" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></div>
                                        <div class="fb-share-button" data-href="{{ url('post/news/' ~ p.postSlug) }}" data-type="button"></div>
                                        &nbsp;
                                        <a href="https://twitter.com/share" class="twitter-share-button" data-count="none"></a>
                                        <!-- <span class="pull-right"><i class="icon-comment"></i> <a href="#">2 Comments</a></span> -->
                                     </div>
                                   </div>

                                     <!-- Thumbnail -->
                                     {% if p.postFeatureImage %}
                                     <div class="bthumb3">
                                        <a href="#"><img src="{{ p.postFeatureImage }}" alt="" class="img-responsive"></a>
                                     </div>
                                     {% endif %}
                                     <!-- Para -->
                                     <p class="post-content"><?php echo $out = strlen(strip_tags($p->postContent)) > 550 ? substr(strip_tags($p->postContent),0,550)."..." : $p->postContent; ?></p>

                                     <!-- Read more -->
                                     <div class="button readmore"><a href="{{url('post/news/' ~ p.postSlug)}}">Read More...</a></div>
                                     <div class="clearfix"></div>
                            </div>
                            {% endif %}

                        {% endfor %}
                        {% endif %}

                      {# {% if post.total_items > 1 %} #}
                      {% if post.total_items > 2 %}
                            <div class="paging">
                              {% if post.current != 1 %}
                                {{ link_to("admin/users?page=" ~ post.before, 'Prev') }}
                              {% endif %}

                              {% for index in 1..post.total_pages %}
                              {% if post.current == index %}
                                {{ link_to("programs/page/" ~ program.programPage ~ "?page=" ~ index, index, 'class':'current') }}
                              {% else %}
                                {{ link_to("programs/page/" ~ program.programPage ~ "?page=" ~ index, index) }}
                              {% endif %}
                              {% endfor %}

                              {% if post.current != post.total_pages %}
                                {{ link_to("programs/page/" ~ program.programPage ~ "?page=" ~ post.next, 'Next') }}
                              {% endif %}
                            </div>
                      {% endif %}

                    </div>

                    {% endif %}
              {% for page in prog_pages  %}
                {% if page.pageActive %}
                    {# <div class="tab-pane inside-contents {% if loop.first %} {% if program.programID != 1 %} active {% endif %} {% endif %}" id="{{ page.pageSlug }}"> #}
                    <div class="tab-pane inside-contents {% if loop.first %} {% if program.programID != 1 %}  {% endif %} {% endif %}" id="{{ page.pageSlug }}">
                     {{ page.pageContent }}

                    </div>
                {% endif %}
            {% endfor %}
          </div>
        </div>
      </div>
