<!Doctype html>
<html class="no-js" lang="">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Title and other stuffs -->
  <title> ABK RESCUE </title>

  <link rel="apple-touch-icon" href="apple-touch-icon.png">
  <!-- Place favicon.ico in the root directory -->

  <!-- <link rel="stylesheet" href="css/normalize.css"> -->
  <!-- <link rel="stylesheet" href="css/main.css"> -->
  <link rel="stylesheet" href="/css/bootstrap.min.css">

  <!--  <script src="//cdnjs.cloudflare.com/ajax/libs/less.js/2.6.1/less.min.js"></script> -->
  <link rel="stylesheet" type="text/less" href="/css/bootstrap-less/less/style.less">
  <script src="/js/vendor/less.min.js"></script>
  <style type="text/css">
    #myModalEvent{
      top:50px;
    }
  </style>
</head>
<body data-spy="scroll" data-offset="50">

  <!-- Header Starts -->
  <header class="top-bg open-sans">
    <div class="head-container">

      <div class="col-sm-3" id="header-copyright">
        <span class="size12 text-justify">
          A Program of AngBayanKo Foundation, <br>
          a US 501(c)(3) approved non-profit organization
        </span>
      </div>

      <div class="col-sm-4" id="header-details">
        <div class="head-details">
          <i class="ic-world size20"> </i>
          <span class="dropdown-toggle size16 open-sans" type="button" data-toggle="dropdown">U.S
            <label class=""></label></span>

            <div id="detail-email">
              <i class="ic-telephone size18"> </i>
              <span class=""> <?php echo $hello['phone']; ?> <!-- 626.205.3838 --> </span>
            </div>
            <div id="detail-email">
              <i class="ic-envelope size18"> </i>
              <span> <?php echo $hello['email']; ?> <!-- cert@abkrescue.com  --> </span>
            </div>

          </div>
        </div>

        <div class="col-sm-3" id="header-search">
          <div class="form-group has-feedback">
            <input type="text" class="form-control head-search" placeholder="search">
            <span class="glyphicon glyphicon-search form-control-feedback search-icon size18"></span>
          </div>
        </div>

        <div class="col-sm-2 text-center" id="header-icons">
          <div class="header-icons">
            <?php foreach($socialmedia as $soc){
              if($soc['alt'] == 'LinkedIn'){
               ?>
              <a href="https://www.linkedin.com" class="btn btn-rounded btn btn-icon">
              <i class="ic-<?php echo $soc['icon']; ?> size25 social-icon"></i></a>
               <?php
              }elseif($soc['alt'] == 'Instagram'){
              ?>
              <a href="https://www.instagram.com" class="btn btn-rounded btn btn-icon">
              <i class="ic-<?php echo $soc['icon']; ?> size25 social-icon"></i></a>
              <?php
              }else{
              ?>
              <a href="<?php echo $soc['link']; ?>" class="btn btn-rounded btn btn-icon">
              <i class="ic-<?php echo $soc['icon']; ?> size25 social-icon"></i></a>
              <?php
            }
            }
            ?>
            <!-- <a href="<?php echo $soc['link']; ?>" class="btn btn-rounded btn btn-icon">
              <i class="ic-<?php echo $soc['icon']; ?> size25 social-icon"></i></a> -->


            </div>
          </div>
        </div>
      </header>
      <!-- Header Ends -->

      <!-- START NAV auth.AC -->
      <!-- Use class navbar-static-top for sticky navigation -->
      <nav class="navbar navbar-default nav-container navbar-static-top" role="navigation" id="nav" data-spy="affix" data-offset-top="200">
        <div class="container-fluid">
          <div class="navbar-header col-sm-4" id="nav-logo">
            <a href="#" class="brand pull-left">
              <div id="nav-abklogo"><img src="/img/resource/abk-foundation-logo.png" class="square" alt="abk-foundation-logo"></div>
              <div id="nav-abkreslogo">
                <img src="/img/resource/abkrescue-logo.png" alt="abkrescue-logo"></div>
                <div id="nav-abkcertlogo">
                  <img src="/img/resource/cert-logo.png" alt="abkrescue-logo"></div>
                </a>
              </div>
              <!-- Put this for the toggle navigation on responsive -->
              <div class="navbar-header container" id="nav-toggle">
                <button type="button" data-target="#navbarCollapse" data-toggle="offcanvas" class="navbar-toggle
                offcanvas-toggle pull-right">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a href="/abkrescue/index" class="navbar-brand size18">ABKRESCUE</a>
            </div>
            <!-- Put this for the toggle navigation on responsive -->

            <div class="col-sm-7" id="nav-text">
              <div id="navbarCollapse" class="navbar-offcanvas navbar-offcanvas-touch nopadleft">
                <ul class="nav navbar-nav open-sans">
                  <li><a href="#" class="active">TAKE ACTION</a></li>
                  <li><a href="/abkrescue/#about_section">ABOUT US</a></li>
                  <li><a href="/abkrescue/abkblog">BLOG</a></li>
                  <li><a href="/abkrescue/#volunteer_section">VOLUNTEER</a></li>
                  <li><a href="/abkrescue/index">DONATE</a></li>
                </ul>
              </div>
            </div>

            <div class="navbar-header col-sm-1" id="nav-cert">
              <a href="#" class="brand pull-right">
                <span><img src="/img/resource/cert-logo.png" alt="cert-logo"></span>
              </a>
            </div>
          </div>
        </nav>
        <hr class="nav-hr"/>
        <!-- END NAV auth.AC -->

        <!-- START HEADLINE auth.AC -->
        <div class="headline-container">
          <!-- <a href="#" class="content-box"> -->
          <div class="col-sm-4 height100 nopad" id="frame-lg">
            <?php foreach($featured as $featured){
            if($featured['featured'] == '1'){
            ?>
            <div class="frame-lg" style="background-image: url('<?php echo $featured['banner']; ?>'), url('/img/resource/abk-foundation-logo.png');"  data-toggle="modal" data-target="#myModalEventfet<?php echo $i; ?>">
              <div class="content-container featured-container">
                <a>
                  <div class="content-title"><strong><h5><b><?php echo $featured['title']; ?></b></h5></strong></div>
                  <div class="content">
                    <span>
                      <?php
                      if (strlen($featured['activity_description']) > 70)
                      {
                      ?>
                      <a href="" data-toggle="modal" data-target="#myModalEventfet<?php echo $i; ?>">
                      <?=substr($featured['activity_description'], 0 , 70); ?> ...
                      <?php } else {
                      echo $featured['activity_description'];
                    }
                    ?>
                    </a>
                  </span>
                </div>
              </a>
            </div>
          </div>

          <div class="modal fade" id="myModalEventfet<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="color:#ff0000">&times;</span></button>
                <h4 class="modal-title">
                  <span id="titleTxt" class="size20" style="color: #10a800" value"">
                  <?php echo $featured['title']; ?>

                  </span></h4>
                </div>
                <div class="modal-body">
                  <div class="row">
                    <div class="col-sm-12">
                      <center>
                      <div id="imgEvt" style="padding-bottom:30px;">
                        <img class="img-responsive" src= "<?php echo $featured['banner']?> " height="500" width="500" overflow="hidden" onerror="javascript:imgError(this)" />
                      </div></center>
                    </div>
                    <div class="col-sm-12">
                     <div id="descTxt" style="color: #1d1d1d">
                      <?php echo $featured['content']; ?>
                     </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php break; } }?>
        </div>
        <!-- </a> -->
        <div class="col-sm-8 height100 nopad" id="frame-md" >
          <?php $i = 0; foreach($activities as $activities){
          ?>
          <?php $title = $activities['title']; ?>
          <!-- <a href="#" class="content-box"> -->
          <div class="col-sm-6 height50 activitypad <?php echo ($i == 2 || $i == 3 ? 'nobot' : '') ;?> " data-toggle="modal" data-target="#myModalEvent<?php echo $i; ?>">
            <div class="frame-md" style="background-image: url('<?php echo $activities['banner']; ?>'), url('/img/resource/abk-foundation-logo.png');">
              <div class="content-container <?php echo ($i == 0 || $i == 1 ? 'absolutebot' : '') ;?> other-container">
                <a>
                  <div class="content-title"><strong>
                  <h5><b><?php echo $title; ?></b></h5></strong></div>
                    <div class="content">
                      <span>
                        <a href="" data-toggle="modal" data-target="#myModalEvent<?php echo $i; ?>">

                        <?php
                        if (strlen($activities['activity_description']) > 70)
                        { ?>
                        <?=substr($activities['activity_description'], 0 , 70)?> ...
                        <?php } else {
                        echo $activities['activity_description'];
                      }
                      ?>
                    </a>
                  </span>
                </div>
              </a>
            </div>
          </div>
        </div>

          <div class="modal fade" id="myModalEvent<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="color:#ff0000">&times;</span></button>
                <h4 class="modal-title">
                  <span id="titleTxt" class="size20" style="color: #10a800" value"">
                  <?php echo $activities['title']; ?>

                  </span></h4>
                </div>
                <div class="modal-body">
                  <div class="row">
                    <div class="col-sm-12">
                      <center>
                      <div id="imgEvt" style="padding-bottom:30px;">
                        <img class="img-responsive" src= "<?php echo $activities['banner']?> " height="500" width="500" overflow="hidden" onerror="javascript:imgError(this)" />
                      </div></center>
                    </div>
                    <div class="col-sm-12">
                     <div id="descTxt" style="color: #1d1d1d">
                      <?php echo $activities['content']; ?>
                     </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <?php $i++; if($i==4) break; }?>

        <div id="donate"></div>
      </div>
    </div>
        <!-- END HEADLINE auth.AC -->
        <!-- START CALL-TO-ACTION auth.AC -->
        <div id="aboutus"></div>
        <div class="cta-container center-block" style="background-image: url('/img/resource/bg-cta.png');">
          <div class="cta-box">
            <div class="text-container col-sm-8">
              <div class="text"><span><?php echo $donate;  ?></span></div>
            </div>
            <div class="button-container col-sm-4">
              <div class="col-sm-12 open-sans">
                <div class="form-group col-sm-6">
                  <button class="form-control donate-submit">
                    <span class="size20"> DONATE </span>
                  </button>
                </div>

                <div class="form-group col-sm-6">
                 <a href="/abkrescue/#join-container" style="text-decoration: none;">
                  <button class="form-control volunteer-submit">
                    <span class="size20"> VOLUNTEER </span>
                  </button>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- END CALL-TO-ACTION auth.AC -->
        <div class="container">
          <h1 class="open-sans-bold text-center" style="text-shadow: 1px 1px #ff0000;"> BLOGS </h1>
          <div class="event-desc" style="background:#cacdcd">
            <?php
            if(count($blog) <= 5 ){
            ?>
            <style type="text/css">
              #load{ display: none; }
            </style><?php
          }
          foreach($blog as $blog){
          ?>
          <div class="col-lg-12" id="blog-content" style="display:none;">
            <div class="col-sm-2">
              <label class="event-num open-sans">
                <?php
                $image = $blog['blogFeatureImage'];
                echo "<img src='$image' height='120' width='120' onerror='javascript:imgError(this)'>"; ?>
              </label>
            </div>
            <div class="col-sm-7 event-text open-sans">
              <h3 class="open-sans-bold" style="word-break:break-all">
                <?php echo $blog['blogTitle']; ?>  </h3>

                <p class="size15">Author: <?php echo $blog['blogAuthor']; ?> </p>

                <p class="size14">Published Date: <?php echo date("F j, Y",$blog['blogPublishDate']); ?> </p>

                <p class="size14">
                 <?php if (strlen($blog['blogContent']) > 150) {
                 echo "<div id='trimmed'>".substr($blog['blogContent'], 0 , 150)."...</div>";
                 echo "<div id='demo' class='collapse'>";
                echo $blog['blogContent'];
                echo "</div>";
               }  else {
               echo $blog['blogContent'];
             }?>
           </p>

         </div>
         <div class="col-sm-12 mgtop5 mgbot5">
          <hr />
          <?php
          if (count($blog['blogContent'])) {
          if (strlen((string)$blog['blogContent']) < 141) {
          ?>
          <style type="text/css">
            #readmoretext{ display: none; }
          </style>
          <?php
        }
        else if(strlen((string)$blog['blogContent']) > 142) {
        ?>
        <!-- <a href="#" data-toggle="modal" data-target="#myModalBlog"
        onClick="loadModalBlog(
        '<?=trimTtxt($blog['blogContent'])?>',
        '<?=trimTtxt($blog['blogTitle'])?>' ,
        '<?=trimTtxt($blog['blogAuthor'])?>' ,
        '<?=trimTtxt(date('F j, Y',$blog['blogPublishDate']))?>' ,
        '<?=$blog['blogFeatureImage']?>')">

        <span class="open-sans-bold size13" id='readmoretext'><i class="fa fa-play text-info">Read More</i></span>

      </a> -->
      <a href="/abkrescue/readmore/<?php echo $blog['blogID'];?>" class="a a-info">Read More</a>
      <?php
      echo "";
    }
  }
  ?>


</div>
</div>
<?php
} ?>
</div>
</div>
<p align="center">
  <a href="#" id="load"> <button class="btn btn-secondary" type="button" style="background-color: #238f23; color: white">SHOW MORE BLOGS</button> </a>
</p>

<!-- Count Starts -->
<div class="count-container">
  <div class="count-bg" style="background-image: url('/img/resource/bg-s.png');"></div>
  <div class="count-body">
    <div class="container mgtop20 open-sans">
      <div class="col-sm-4 text-left">
        <span> <?php echo $hello['global_mem']; ?></span>
        <p class="size20"> GLOBAL MEMBERS </p>
      </div>
      <div class="col-sm-4 text-center">
        <span> <?php echo $hello['accomp_miss']; ?></span>
        <p class="size20"> ACCOMPLISHED MISSIONS </p>
      </div>
      <div class="col-sm-4 text-right">
        <span> <?php echo $hello['worth_donat']; ?></span>
        <p class="size20"> WORTH OF DONATIONS </p>
      </div>
    </div>
  </div>
</div>
<!-- Count Starts -->

<!-- Article Starts -->
<div class="article-container">
  <article class="container height100">
<?php echo $expert;  ?>
    <hr align="left" />

      <div class="sect-body">
      </div>
      <div class="sect-button col-sm-12 text-right">
      </div>
  </article>
</div>
<!-- Article Ends -->

<!-- Event Starts -->
<div class="event-container">
  <section class="event-left">
    <div class="event-bg" style="background-image:url('/img/resource/bg-ue-fill.png');">
      <div class="event-content">
        <hr class="event-hr" />
        <h1 class="open-sans-bold text-center" style="text-shadow: 1px 1px #ff0000;"> UPCOMING EVENTS </h1>

        <div class="event-desc">
          <?php foreach($events as $events){
          ?>
          <div class="col-sm-12 nopadleft">
            <div class="col-sm-5 nopadleft">
              <label class="event-num open-sans"> <?php echo date('d', strtotime($events['startdate'])); ?> </label><br>
              <label class="event-date open-sans"> <b class="open-sans-bold"> <?php echo date('F', strtotime($events['startdate'])); ?> </b> <span class="size21"> <?php echo date('l', strtotime($events['startdate'])); ?> </span> </label>
            </div>
            <div class="col-sm-7 event-text open-sans">
              <h3 class="open-sans-bold" style="word-break:break-all"> <?php echo $events['eventname']; ?>  </h3>
              <p class="size14">
                <?php if (strlen($events['shortdesc']) > 30) {
                echo substr($events['shortdesc'], 0 , 30)."...";
              }  else {
              echo $events['shortdesc'];
            }?>
          </p>
          <span class="pull-left size10">Location: <i> <?php echo $events['location']; ?> </i> </span>
          <span class="pull-right size10">Time: <i><?php echo date('h:i a', strtotime($events['starttime'])); ?></i> </span>
        </div>
        <div class="col-sm-12 mgtop5 mgbot5">
          <hr />
          <a class="sect-button" data-toggle="modal" data-target="#myModalActivities"
          onclick="loadModalActivities(
          '<?=trimTtxt($events['shortdesc'])?>',
          '<?=trimTtxt($events['eventname'])?>',
          '<?=date('h:i a', strtotime($events['starttime']))?>',
          '<?=date('l, d/m/Y', strtotime($events['startdate']))?>',
          '<?=$events['location']?>',
          '<?=date('h:i a', strtotime($events['endtime']))?>',
          '<?=date('l, d/m/Y', strtotime($events['enddate']))?>')">
          <i class="fa fa-play text-success"></i>
          <span class="open-sans-bold size16">Details</span>
        </a>
      </div>

      <?php } ?>
    </div>
  </div>
</div>
<h1 class="text-center" >
  <a href="/abkrescue/abkactivity" style="color: white;">
    <button class="btn" type="button" style="background-color: #238f23;">Show More</button>
  </a></h1>
</div>

</section>
<section class="event-right">
  <div class="event-right-bg"
  style="background-image:url('/img/resource/img-1-545-800.png');">
</div>
</section>
</div>
<!-- Event Ends -->


<!-- Footer Starts -->
<footer>
  <div class="container footer-container">

    <div class="col-sm-12 nopadleft footer-logo">
      <div class="col-sm-1 abklogo">
        <span class="helper"></span>
        <img src="/img/resource/abk-foundation-logo.png" class="foot-img" />
      </div>
      <div class="col-sm-3 nopadleft height100" id="abkrescue-logo">
        <span class="helper"></span>
        <img src="/img/resource/abkrescue-logo.png" class="foot-img" />
      </div>
      <div class="col-sm-2 nopadleft height100 text-center" id="cert-logo">
        <span class="helper"></span>
        <img src="/img/resource/cert-logo.png" />
      </div>
      <div class="col-sm-5 height100 nopadright" id="subscribe-logo">
        <div class="footer-subscribe">
          <span class="helper"></span>
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Email" aria-describedby="basic-addon2">
            <span class="input-group-btn lato-regular">
              <button class="btn btn-lg btn-secondary" type="button">SUBSCRIBE</button>
            </span>
          </div>
        </div>
      </div>
    </div>

    <div class="col-sm-12 footer-text">
      <div class="col-sm-3">
        <h5 class="open-sans-bold"> get to know us </h5>
        <ul class="open-sans size15">
          <li> read our blog </li>
          <li> field stories </li>
          <li> meet our stories </li>
        </ul>
      </div>
      <div class="col-sm-3">
        <h5 class="open-sans-bold"> get to know us </h5>
        <ul class="open-sans size15">
          <li> read our blog </li>
          <li> field stories </li>
          <li> meet our stories </li>
        </ul>
      </div>
      <div class="col-sm-3">
        <h5 class="open-sans-bold"> get to know us </h5>
        <ul class="open-sans size15">
          <li> read our blog </li>
          <li> field stories </li>
          <li> meet our stories </li>
        </ul>
      </div>
      <div class="col-sm-3">
        <h5 class="open-sans-bold"> get to know us </h5>
        <ul class="open-sans size15">
          <li> read our blog </li>
          <li> field stories </li>
          <li> meet our stories </li>
        </ul>
      </div>
    </div>
    <div class="col-sm-12 text-center open-sans">
      <hr class="footer-hr" />
      <div class="footer-icon-container ">
        <p> A Program of AngBayanKo Foundation, a US 501(c)(3) approved non-profit organization</p>
        <p>privacy policy   •   © 2016   •  <a href=""> get our awesome emails </a> </p>
        <div class="footer-icons">
          <!-- <?php foreach($socialmedia as $soc){
          ?>
          <a href="<?php echo $soc['link']; ?>" class="btn btn-icon">
            <i class="ic-<?php echo $soc['icon']; ?>"></i></a>
            <?php } ?> -->
          <a href="https://facebook.com" class="btn btn-icon">
            <i class="ic-facebook"></i></a>
            <a href="https://twitter.com" class="btn btn-icon">
              <i class="ic-twitter"></i></a>
              <a href="https://instagram.com" class="btn btn-icon">
                <i class="ic-instagram"></i></a>
                <a href="https://linkedin.com" class="btn btn-icon">
                  <i class="ic-linkedin"></i></a>
                </div>
              </div>
            </div>

          </div>
        </footer>

        <!-- Modal Event-->
        <div class="modal fade" id="myModalEvent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true" style="color:#ff0000">&times;</span></button>
                  <h4 class="modal-title"><span id="titleTxt" class="size20" style="color: #10a800"></span></h4>
                  <h4 class="modal-title"><span id="authorTxt" class="size20" style="color: #10a800"></span></h4>
                </div>

                <div class="modal-body">
                  <div class="row">
                    <div class="col-sm-12">
                      <center><div id="imgEvt" style="padding-bottom:30px;"></div></center>
                    </div>
                    <div class="col-sm-12">
                     <span id="descTxt" style="color: #1d1d1d"></span>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="modal fade" id="myModalBlog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color:#ff0000">&times;</span></button>
                    <h4 class="modal-title"><span id="titleblog" class="size30" style="color: #10a800"></span></h4>
                    Author:
                    <span id="authorblog" class="size15"></span>

                    <h5 class="modal-title">
                      Published Date:
                      <span id="pubdateblog" class="size15"></span></h4>
                    </div>

                    <div class="modal-body">
                      <div class="row">
                        <div class="col-sm-12">
                          <center><div id="imgblog" style="padding-bottom:30px;"></div></center>
                        </div>
                        <div class="col-sm-12">
                          <span id="descblog" style="color: #1d1d1d"></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Modal Activities-->
              <div class="modal fade" id="myModalActivities" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="color:#ff0000">&times;</span></button>
                      <h4 class="modal-title"><span id="titleTxtac" style="color: #10a800; font-weight:bolder;"></span></h4>
                    </div>
                    <div class="modal-body">
                      <div class="row">
                        <div class="col-sm-12">
                         <label> Date:</label> &nbsp;
                         <span id="dateac"></span>
                         &nbsp; - &nbsp;
                         <span id="enddate"></span>
                         <br>
                         <!--  <label> Date End: </label> &nbsp; <span id="enddate"></span> -->
                       </div>
                       <div class="col-sm-12">
                         <label> Time: </label> &nbsp;
                         <span id="timeac"></span>
                         &nbsp; - &nbsp;
                         <span id="endtime"></span>
                         <br>
                         <!-- <label> Time End: </label> &nbsp; <span id="endtime"></span> -->
                       </div>
                       <div class="col-sm-12">
                        <br>
                        <span id="descTxtac" style="color: #1d1d1d"></span>
                      </div>
                      <div class="col-sm-12" style="margin-top: 10px">
                        location:  <i><span id="locac" style="color: #1d1d1d"></span></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>


            <!-- Footer Ends -->
            <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
            <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
        <!--<script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
        <script src="js/vendor/jquery-1.12.0.min.js"></script>-->
        {{ javascript_include('js/vendor/jquery-1.12.0.min.js') }}
        {{ javascript_include('js/bootstrap.js') }}
        {{ javascript_include('js/join-validation.js') }}
        {{ javascript_include('js/vendor/bootstrap.offcanvas.min.js') }}

        <script type="text/javascript">
          $(function(){
        $("div#blog-content.col-lg-12").slice(0, 5).show(); // select the first ten
          $("#load").click(function(e){ // click event for load more
            e.preventDefault();
        $("div#blog-content.col-lg-12:hidden").slice(0, 5).show(); // select next 10 hidden divs and show them
        if($("div#blog-content.col-lg-12:hidden").length == 0){ // check if any hidden divs still exist
        }
      });
        });
        </script>
        <script type="text/javascript">
          function loadModalEvent(desc, title, img){
            $("#imgEvt").html('<img class="img-responsive" src= "'+ img +'" height="500" width="500" overflow="hidden" onerror="javascript:imgError(this)" />');
            $('#titleTxt').html(title);
            $('descTxt').html(desc);
            $('#titleTxt').css({"color":"#1d1d1d","font-size":"20"});
            $('#myModalEvent').modal('show');
          }
          function loadModalBlog(desc, title, author, pubdate, img){
            $("#imgblog").html('<img class="img-responsive" src= "'+ img +'" height="500" width="500" overflow="hidden" onerror="javascript:imgError(this)" />');
            $("#descblog").html(desc);
            $('#titleblog').html(title);
            $('#authorblog').html(author);
            $('#pubdateblog').html(pubdate);
            $('#titleblog').css({"color":"#1d1d1d","font-size":"20"});
            $('#myModalBlog').modal('show');
          }


          function loadModalActivities(desc, title,timeac,dateac, locac, endtime, enddate){
            $("#descTxtac").html(desc);
            $("#locac").html(locac);
            $('#titleTxtac').html(title);
            $('#timeac').html(timeac);
            $('#dateac').html(dateac);
            $('#endtime').html(endtime);
            $('#enddate').html(enddate);
            $('#myModalActivities').modal('show');
          }
        </script>
        <script>
          function imgError(img) {
            img.error="";
            img.src="/img/resource/abk-foundation-logo.png"
          }
        </script>



        <script type="text/javascript">
          $(window).scroll(function() {
            if ($(document).scrollTop() > 200) {
              $('nav').addClass('shrink');
            } else {
              $('nav').removeClass('shrink');
            }
          });

          $( window ).resize(function() {
        //console.log($(window).width());
        if($(window).width() < 753){
          // console.log("shing!")
          $('.join-block').addClass('join-block-add');
          $('.join-block').removeClass('join-block');
        }else{
          $('.join-block-add').addClass('join-block');
        }
        if($(window).innerWidth() <= 767){
          // console.log("shing!")
          $('.join-block').addClass('join-block-add');
          $('.join-block').removeClass('join-block');
        }else{
          $('.join-block-add').addClass('join-block');
        }

      })
          $(document).ready(function(){
           if($(window).width() < 753 || $(window).innerWidth() <= 767){
            console.log("shing!")
            $('.join-block').addClass('join-block-add');
            $('.join-block').removeClass('join-block');
          }else{
            $('.join-block-add').addClass('join-block');
          }
        });
        </script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
          (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
          e=o.createElement(i);r=o.getElementsByTagName(i)[0];
          e.src='https://www.google-analytics.com/analytics.js';
          r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
          ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
        <?php
        function trimTtxt($data){
        return rtrim(str_replace("'", "", $data));
      }
      ?>
    </body>
    </html>
