<!Doctype html>
<html class="no-js" lang="">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Title and other stuffs -->
  <title> ABK RESCUE </title>

  <link rel="apple-touch-icon" href="apple-touch-icon.png">
  <!-- Place favicon.ico in the root directory -->

  <!-- <link rel="stylesheet" href="css/normalize.css"> -->
  <!-- <link rel="stylesheet" href="css/main.css"> -->
  <link rel="stylesheet" href="/css/bootstrap.min.css">

  <!--  <script src="//cdnjs.cloudflare.com/ajax/libs/less.js/2.6.1/less.min.js"></script> -->
  <link rel="stylesheet" type="text/less" href="/css/bootstrap-less/less/style.less">
  <script src="/js/vendor/less.min.js"></script>
  <style type="text/css">
    #myModalEvent{
      top:50px;
    }
  </style>
</head>
<body data-spy="scroll" data-offset="50">

  <!-- Header Starts -->
  <header class="top-bg open-sans">
    <div class="head-container">

      <div class="col-sm-3" id="header-copyright">
        <span class="size12 text-justify">
          A Program of AngBayanKo Foundation, <br>
          a US 501(c)(3) approved non-profit organization
        </span>
      </div>

      <div class="col-sm-4" id="header-details">
        <div class="head-details">
          <i class="ic-world size20"> </i>
          <span class="dropdown-toggle size16 open-sans" type="button" data-toggle="dropdown">U.S
            <label class=""></label></span>

            <div id="detail-email">
              <i class="ic-telephone size18"> </i>
              <span class=""> (123) 123-4444 <!-- 626.205.3838 --> </span>
            </div>
            <div id="detail-email">
              <i class="ic-envelope size18"> </i>
              <span> angbayankofoundation.abk@gmail.com <!-- cert@abkrescue.com  --> </span>
            </div>

          </div>
        </div>

        <div class="col-sm-3" id="header-search">
          <div class="form-group has-feedback">
            <input type="text" class="form-control head-search" placeholder="search">
            <span class="glyphicon glyphicon-search form-control-feedback search-icon size18"></span>
          </div>
        </div>

        <div class="col-sm-2 text-center" id="header-icons">
          <div class="header-icons">
            <?php foreach($socialmedia as $soc){
              if($soc['alt'] == 'LinkedIn'){
               ?>
              <a href="https://www.linkedin.com" class="btn btn-rounded btn btn-icon">
              <i class="ic-<?php echo $soc['icon']; ?> size25 social-icon"></i></a>
               <?php
              }elseif($soc['alt'] == 'Instagram'){
              ?>
              <a href="https://www.instagram.com" class="btn btn-rounded btn btn-icon">
              <i class="ic-<?php echo $soc['icon']; ?> size25 social-icon"></i></a>
              <?php
              }else{
              ?>
              <a href="<?php echo $soc['link']; ?>" class="btn btn-rounded btn btn-icon">
              <i class="ic-<?php echo $soc['icon']; ?> size25 social-icon"></i></a>
              <?php
            }
            }
            ?>
            <!-- <a href="<?php echo $soc['link']; ?>" class="btn btn-rounded btn btn-icon">
              <i class="ic-<?php echo $soc['icon']; ?> size25 social-icon"></i></a> -->


            </div>
          </div>
        </div>
      </header>
      <!-- Header Ends -->
       <!-- START NAV auth.AC -->
      <!-- Use class navbar-static-top for sticky navigation -->
      <nav class="navbar navbar-default nav-container navbar-static-top" role="navigation" id="nav" data-spy="affix" data-offset-top="200">
        <div class="container-fluid">
          <div class="navbar-header col-sm-4" id="nav-logo">
            <a href="#" class="brand pull-left">
              <div id="nav-abklogo"><img src="/img/resource/abk-foundation-logo.png" class="square" alt="abk-foundation-logo"></div>
              <div id="nav-abkreslogo">
                <img src="/img/resource/abkrescue-logo.png" alt="abkrescue-logo"></div>
                <div id="nav-abkcertlogo">
                  <img src="/img/resource/cert-logo.png" alt="abkrescue-logo"></div>
                </a>
              </div>
              <!-- Put this for the toggle navigation on responsive -->
              <div class="navbar-header container" id="nav-toggle">
                <button type="button" data-target="#navbarCollapse" data-toggle="offcanvas" class="navbar-toggle
                offcanvas-toggle pull-right">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a href="/abkrescue/index" class="navbar-brand size18">ABKRESCUE</a>
            </div>
            <!-- Put this for the toggle navigation on responsive -->

            <div class="col-sm-7" id="nav-text">
              <div id="navbarCollapse" class="navbar-offcanvas navbar-offcanvas-touch nopadleft">
                <ul class="nav navbar-nav open-sans">
                  <li><a href="#" class="active">TAKE ACTION</a></li>
                  <li><a href="/abkrescue/#about_section">ABOUT US</a></li>
                  <li><a href="/abkrescue/abkblog">BLOG</a></li>
                  <li><a href="/abkrescue/#volunteer_section">VOLUNTEER</a></li>
                  <li><a href="/abkrescue/index">DONATE</a></li>
                </ul>
              </div>
            </div>

            <div class="navbar-header col-sm-1" id="nav-cert">
              <a href="#" class="brand pull-right">
                <span><img src="/img/resource/cert-logo.png" alt="cert-logo"></span>
              </a>
            </div>
          </div>
        </nav>
        <hr class="nav-hr"/>
        <!-- END NAV auth.AC -->
      <div class="containerBlog" style="background-color: #f2f7f7;margin-left: 20%;margin-right: 20%; ">
        <div class="row">
          <div class="blogtitle">
            <p align="center" style="text-shadow: 1px 1px #ff0000;font-size: 40px;font-weight: 550;">
              {{ form.blogTitle }}
            </p>
            <p align="center">
              Author: {{ form.blogAuthor }}
              </br>
              Publish Date: <?php foreach($form as $blog => $key){if($blog == 'blogPublishDate'){ echo date("F j, Y",$key);}} ?>
            </p>
          </div>
        </div>
        <div class="row col-sm-12" style="font-size:13px;text-align: justify;margin-bottom: 5%;">
          <div class="contentblog" style="margin-left: 10%;margin-right: 10%;">
            <p align="center"><img src='{{ form.blogFeatureImage }}'/></p>
            <p>{{ form.blogContent }}</p>
          </div>
        </div>

         <a href="/abkrescue/abkblog" style="text-decoration: none; margin-left: 10%;"><button class="btn btn-primary"><span class="glyphicon glyphicon-circle-arrow-left"></span> Back to Blogs</button></a>
      </div>

<hr/>
 <!-- <div class="row">
        <div class="col-sm-12 title">
         <p align="center" style="text-shadow: 1px 1px #ff0000;font-size: 40px;font-weight: 550;">{{ form.blogTitle }}</p>
         <p align="center">Author: {{ form.blogAuthor }}
         </br>
         Publish Date: <?php foreach($form as $blog => $key){if($blog == 'blogPublishDate'){ echo date("F j, Y",$key);}} ?></p>

        <div class="col-sm-12" align="center">
          <p align="center"><img src='{{ form.blogFeatureImage }}'/></p>
          <div class="col-sm-6" align="center"><p align="justify">{{ form.blogContent }}</p></div>
        </div>
          </div>
        </div> -->
      <!-- Footer Starts -->
<footer>
  <div class="container footer-container">

    <div class="col-sm-12 nopadleft footer-logo">
      <div class="col-sm-1 abklogo">
        <span class="helper"></span>
        <img src="/img/resource/abk-foundation-logo.png" class="foot-img" />
      </div>
      <div class="col-sm-3 nopadleft height100" id="abkrescue-logo">
        <span class="helper"></span>
        <img src="/img/resource/abkrescue-logo.png" class="foot-img" />
      </div>
      <div class="col-sm-2 nopadleft height100 text-center" id="cert-logo">
        <span class="helper"></span>
        <img src="/img/resource/cert-logo.png" />
      </div>
      <div class="col-sm-5 height100 nopadright" id="subscribe-logo">
        <div class="footer-subscribe">
          <span class="helper"></span>
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Email" aria-describedby="basic-addon2">
            <span class="input-group-btn lato-regular">
              <button class="btn btn-lg btn-secondary" type="button">SUBSCRIBE</button>
            </span>
          </div>
        </div>
      </div>
    </div>

    <div class="col-sm-12 footer-text">
      <div class="col-sm-3">
        <h5 class="open-sans-bold"> get to know us </h5>
        <ul class="open-sans size15">
          <li> read our blog </li>
          <li> field stories </li>
          <li> meet our stories </li>
        </ul>
      </div>
      <div class="col-sm-3">
        <h5 class="open-sans-bold"> get to know us </h5>
        <ul class="open-sans size15">
          <li> read our blog </li>
          <li> field stories </li>
          <li> meet our stories </li>
        </ul>
      </div>
      <div class="col-sm-3">
        <h5 class="open-sans-bold"> get to know us </h5>
        <ul class="open-sans size15">
          <li> read our blog </li>
          <li> field stories </li>
          <li> meet our stories </li>
        </ul>
      </div>
      <div class="col-sm-3">
        <h5 class="open-sans-bold"> get to know us </h5>
        <ul class="open-sans size15">
          <li> read our blog </li>
          <li> field stories </li>
          <li> meet our stories </li>
        </ul>
      </div>
    </div>
    <div class="col-sm-12 text-center open-sans">
      <hr class="footer-hr" />
      <div class="footer-icon-container ">
        <p> A Program of AngBayanKo Foundation, a US 501(c)(3) approved non-profit organization</p>
        <p>privacy policy   •   © 2016   •  <a href=""> get our awesome emails </a> </p>
        <div class="footer-icons">
          <?php foreach($socialmedia as $soc){
          ?>
          <a href="<?php echo $soc['link']; ?>" class="btn btn-icon">
            <i class="ic-<?php echo $soc['icon']; ?>"></i></a>
            <?php } ?>
          <!-- <a href="#" class="btn btn-icon">
            <i class="ic-facebook"></i></a>
            <a href="#" class="btn btn-icon">
              <i class="ic-twitter"></i></a>
              <a href="#" class="btn btn-icon">
                <i class="ic-instagram"></i></a>
                <a href="#" class="btn btn-icon">
                  <i class="ic-linkedin"></i></a> -->
                </div>
              </div>
            </div>

          </div>
        </footer>
        <!-- Footer Ends -->
       </body>
    </html>
