    <style type="text/css">
      #about > p > img {
        display: none;
      }
    </style>
    <div class="inner-programs pull-left">
     <!--  {% if page.pageBanner != '' %}
      <div class="dm-latest-news-head">
        <img src="{{ page.pageBanner }}" id="page-img">
      </div>
      <div class="tab-content" id="page-content">
        {{ page.pageContent }}
      </div>
      {% endif %}
              {% if page.pageBanner == '' %} -->
              <!-- <div class="dm-latest-news-head">
                <img src="{{ page.pageBanner }}" id="page-img">
              </div> -->
              <!-- <div class="tab-content" id="page-content" align="justify">
                {{ page.pageContent }}
              </div>
              {% endif %} -->
      <div class="tabs inner-tabs pageinner-tabs">
        <ul class="nav nav-tabs">
          <!-- Use unique name in anchor tag -->

          {% if page.specialPage == 1 %}
          <li class="active" id="pgs"><a href="#news" data-toggle="tab" onclick="window.location.reload()" >Latest Breaking News</a></li>
          <li id="pgs"><a href="#about" data-toggle="tab">About This Page</a></li>

          <?php
          if(empty($abk_vol_username)){
            ?>

            <li id="pgs"><a href="#signup" data-toggle="tab">Sign up</a></li>
            <?php
          }
          ?>

          {% endif %}

          <!-- {{ subpages }} -->
        </ul>
        <div class="tab-content">
          {{ ptry.postTitle }}
          {{ actOptions }}
          {% if page.specialPage == 1 %}
          <div class="tab-pane inner-news active" id="news">
            {% if post.total_items == 0 %}
            No News Added.
            {% else %}
            {% for p in post.items %}
            {% if p.postStatus == 'publish' %}
            <div class="entry">
             <h2><a href="{{ url('post/news/' ~ p.postSlug) }}">{{ p.postTitle }}</a></h2>
             <!-- Meta details -->
             <div class="meta">
              <div><i class="icon-calendar"></i> {{ date('d-m-Y', p.postPublishDate) }}</div>
              <div  class="share_buttons">
              <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></div>
              <div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-type="button"></div>  &nbsp;
               <a href="https://twitter.com/share" class="twitter-share-button" data-count="none">Tweet</a>
              </div>
              <span class="pull-right">
                <a href="http://angbayanko.org/post/news/{{ p.postSlug }}#disqus_thread"></a>
              </span>
            </div>

            <!-- Thumbnail -->
            {% if p.postFeatureImage %}
            <div class="bthumb3">
              <a href="#"><img src="{{ p.postFeatureImage }}" alt="" class="img-responsive"></a>
            </div>
            {% endif %}
            <!-- Para -->
            <p class="post-content"><?php echo $out = strlen(strip_tags($p->postContent)) > 550 ? substr(strip_tags($p->postContent),0,550)."..." : $p->postContent; ?></p>

            <!-- Read more -->
            <div class="button readmore"><a href="{{ url('post/news/' ~ p.postSlug) }}">Read More...</a></div>
            <div class="clearfix"></div>
          </div>
          {% endif %}
          {% endfor %}
          {% endif %}

          {% if post.total_items > 1 %}
          <div class="paging">
            {% if post.current != 1 %}
            {{ link_to("admin/users?page=" ~ post.before, 'Prev') }}
            {% endif %}

            {% for index in 1..post.total_pages %}
            {% if post.current == index %}
            {{ link_to("site/pages/"~page.pageSlug~"?page=" ~ index, index, 'class':'current') }}
            {% else %}
            {{ link_to("site/pages/"~page.pageSlug~"?page=" ~ index, index) }}
            {% endif %}
            {% endfor %}

            {% if post.current != post.total_pages %}
            {{ link_to("site/pages/"~page.pageSlug~"?page=" ~ post.next, 'Next') }}
            {% endif %}
          </div>
          {% endif %}
        </div>
        {% endif %}

        {% if page.pageParent != 0 %}
          <div class="inner-news" id="about">
          </div>
        {% else %}
        <div class="tab-pane inner-news" id="about">
          {# <label> <img lt="" src="http://images4.fanpop.com/image/photos/16000000/Beautiful-Cat-cats-16095933-1280-800.jpg" style="height: 390px;width: 590px;margin-left: 2%;"> </label> #}
            <label> <img lt="" src="http://images4.fanpop.com/image/photos/16000000/Beautiful-Cat-cats-16095933-1280-800.jpg" style="width:100%;margin-left: 2%;"> </label>

          {{ page.pageContent }}
        </div>
             {% endif %}


        <div class="tab-pane inner-news" id="signup">
          <div class="text-right">

            I already have my account. <a href="#loginModal" data-toggle="modal">Log in</a></strong>
          </div>

{{ Errasd }}

          <form method="post" id="signupForm" action="/site/signup">
            <h3 class="fontNormal">1. Profile Information</h3>
            <small>Step 1 of 2</small>
            <hr />
            <div id="formResult"></div>


            <div class="well well-sm border-flat">
              Personal Profile
            </div>

            <div class="signupRows">
              <div class="form-group">
                <label>Your Title</label>
                <div class="row">
                  <div class="col-xs-4 title">
                    <select name="title" class="form-control">
                      <option value="Mr.">Mr.</option>
                      <option value="Ms.">Ms.</option>
                      <option value="Mrs.">Mrs.</option>
                      <option value="" selected="selected">Select Title</option>
                    </select>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label>First Name</label>
                <input name="fname" type="text" class="form-control border-flat" placeholder="Enter first name">
              </div>

              <div class="form-group">
                <label>Middle Name</label>
                <input name="mname" type="text" class="form-control border-flat" placeholder="Enter middle name">
              </div>

              <div class="form-group">
                <label>Last Name</label>
                <input name="lname" type="text" class="form-control border-flat" placeholder="Enter last name">
              </div>

              <div class="form-group">
                <label>Extension Name <small class="text-muted">(optional)</small></label>
                <div class="row">
                  <div class="col-xs-4">
                    <input name="extname" type="text" class="form-control border-flat" placeholder="Enter extension name">
                  </div>
                </div>
              </div>
            </div>

            <div class="well well-sm border-flat">
              Contact Details
            </div>

            <div class="signupRows">
              <div class="form-group">
                <label>Complete Address</label>
                <input name="address" type="text" class="form-control border-flat" placeholder="Enter complete address">
              </div>

              <div class="form-group">
                <label>Email address</label>
                <input name="email" type="text" class="form-control border-flat" placeholder="Enter email">
              </div>

              <div class="form-group">
                <label>Phone Number <small class="text-muted">(optional)</small></label>
                <input name="phone" type="text" class="form-control border-flat phone" placeholder="Enter phone number">
              </div>
            </div>

            <div class="well well-sm border-flat">
              Account Information
            </div>

            <div class="signupRows">
              <div class="form-group">
                <label>Username</label>
                <input name="username" type="text" class="form-control border-flat" placeholder="Enter username">
              </div>

              <div class="form-group">
                <label>Password</label>
                <input name="password" type="password" class="form-control border-flat" placeholder="Enter password">
              </div>

              <div class="form-group">
                <label>Confirm Password</label>
                <input name="repassword" type="password" class="form-control border-flat" placeholder="Re-type password">
              </div>
            </div>


            <button type="submit" class="btn btn-primary" id="signupNext" style="float:right;">Next</button>

          </form>

          <div id="finalForm"></div>

        </div>


      </div>
    </div>
  </div>

