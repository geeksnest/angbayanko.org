

        


          <h3><?php echo $albumName ?></h3>

            <?php if(empty($page->items)){ ?>
              <span>No albums found</span>
            <?php } ?>
            



            <?php foreach ($page->items as $pic) { ?>
              <a class="prettyPhoto[pp_gal]" href="<?php echo $pic['picturePath'] ?>" title="<?php echo $pic['pictureCaption'] ?>">
                <div title="<?php echo $pic['pictureCaption'] ?>" class="square bg" style="background-image:url('<?php echo $pic['picturePath'] ?>')">
              </div>
              </a>
            <?php } ?>
            
            
            
            

            <div class="">
              <!-- Footer goes here -->
              {% if page.total_pages > 1 %}   
              <ul class="pagination pull-right">
                {% if page.current != 1 %}
                <li>{{ link_to("partners/showpictures/"~albumID~"?page=" ~ page.before, 'Prev') }}</li>
                {% endif %}

                {% for index in 1..page.total_pages %}
                {% if page.current == index %}
                <li>{{ link_to("partners/showpictures/"~albumID~"?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                {% else %}
                <li>{{ link_to("partners/showpictures/"~albumID~"?page=" ~ index, index) }}</li>
                {% endif %}
                {% endfor %}         

                {% if page.current != page.total_pages %}                 
                <li>{{ link_to("partners/showpictures/"~albumID~"?page=" ~ page.next, 'Next') }}</li>
                {% endif %}
              </ul>
              <div class="clearfix"></div>
              {% endif %}

        </div>
      