
        
        
          

            <?php if(empty($page->items)){ ?>
              <div><span>No albums found</span></div>
            <?php } ?>
            

            
            <?php foreach ($page->items as $album) { ?>

              <a href="<?php echo $this->url->get().'partners/showpictures/'.$album['albumID'] ?>">
                <div title="<?php echo $album['albumName'] ?>" class="square bg" style="background-image:url('<?php echo $album['coverFileName'] ?>')">
                 <div class="content">
                      <div class="table">
                          <div class="table-cell">
                              <div class="albumTitle"><?php echo $album['albumName'] ?></div>
                          </div>
                      </div>
                  </div>
              </div>
              </a>

            <?php } ?>
            

            <div class="">
              <!-- Footer goes here -->
              {% if page.total_pages > 1 %}
              <ul class="pagination pull-right">
                {% if page.current != 1 %}
                <li>{{ link_to("partners/pictures/"~partnerID~"?page=" ~ page.before, 'Prev') }}</li>
                {% endif %}

                {% for index in 1..page.total_pages %}
                {% if page.current == index %}
                <li>{{ link_to("partners/pictures/"~partnerID~"?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                {% else %}
                <li>{{ link_to("partners/pictures/"~partnerID~"?page=" ~ index, index) }}</li>
                {% endif %}
                {% endfor %}         

                {% if page.current != page.total_pages %}                 
                <li>{{ link_to("partners/pictures/"~partnerID~"?page=" ~ page.next, 'Next') }}</li>
                {% endif %}
              </ul>
              <div class="clearfix"></div>
              {% endif %}
            </div>
