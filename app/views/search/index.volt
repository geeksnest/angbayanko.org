<div class="inner-programs pull-left">
	<div class="tabs inner-tabs">
	        {{ content() }}

	        <?php if(!empty($page)){ ?>
		        <?php if(empty($page->items)){
		        	echo '<h3>No search result found.</h3>Please enter your serach keyword.';
		        } ?>
	            <?php foreach ($page->items as $s) { ?>
	              <h4 class="fontNormal"><a href="<?php echo $s['searchUrl'] ?>"><?php echo $s['searchTitle'] ?></a></h4>
	              <?php echo $s['searchDesc'] ?>
	              <hr />
	            <?php } ?>

	            <div class="">
	              <!-- Footer goes here -->
	              {% if page.total_pages > 1 %}   
	              <ul class="pagination pull-right">
	                {% if page.current != 1 %}
	                <li>{{ link_to("search?page=" ~ page.before ~"&&s="~searchString, 'Prev') }}</li>
	                {% endif %}

	                {% for index in 1..page.total_pages %}
	                {% if page.current == index %}
	                <li>{{ link_to("search?page=" ~ index ~"&&s="~searchString, index, 'style':'background-color:#eee') }}</li>
	                {% else %}
	                <li>{{ link_to("search?page=" ~ index ~"&&s="~searchString, index) }}</li>
	                {% endif %}
	                {% endfor %}         

	                {% if page.current != page.total_pages %}                 
	                <li>{{ link_to("search?page=" ~ page.next ~"&&s="~searchString, 'Next') }}</li>
	                {% endif %}
	              </ul>
	              <div class="clearfix"></div>
	              {% endif %}
	            </div>
            <?php } ?>

    </div>


</div>