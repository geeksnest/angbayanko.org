<div class="inner-programs pull-left">
	<div class="tabs inner-tabs">
  		{{ townTab }}
	        
    	{% if list %}
	    	{% if page.total_pages == 0 %}
            	<p>No albums found</p>
        	{% else %}
            {% for post in page.items %}
			    <a href="{{ post['albumURL'] }}">
			    <div title="{{ post['albumName'] }}" class="square bg" style="background-image:url('{{ post['coverFileName'] }}')">
				   <div class="content">
				        <div class="table">
				            <div class="table-cell">
				                <div class="albumTitle">{{ post['albumName'] }}</div>
				            </div>
				        </div>
				    </div>
				</div>
				</a>
            {% endfor %}
            <div class="widget-foot">

                {% if page.total_pages > 1 %}

                <ul class="pagination pull-right">

                  {% if page.current != 1 %}
                  <li>{{ link_to("towns/gallery/"~townID~"?page=" ~ page.before, 'Prev') }}</li>
                  {% endif %}

                  {% for index in 1..page.total_pages %}
                  {% if page.current == index %}
                  <li>{{ link_to("towns/gallery/"~townID~"?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                  {% else %}
                  <li>{{ link_to("towns/gallery/"~townID~"?page=" ~ index, index) }}</li>
                  {% endif %}
                  {% endfor %}         

                  {% if page.current != page.total_pages %}                 
                  <li>{{ link_to("towns/gallery/"~townID~"?page=" ~ page.next, 'Next') }}</li>
                  {% endif %}
                </ul>
                {% endif %}

                <div class="clearfix"></div> 

              </div>

          {% endif %}
      {% else %}
      	{% if page.total_pages == 0 %}
        	<p>No pictures found</p>
    	{% else %}
    		<h3>{{ albumInfo.albumName }}</h3>
            {% for post in page.items %}
				<a class="prettyPhoto[pp_gal]" href="{{ url('img/towns/'~townID~'/'~albumInfo.albumName) }}/{{ post.pictureFilename }}" title="{{ post.pictureCaption }}"><div title="{{ post.pictureCaption }}" class="square bg" style="background-image:url('{{ url('img/towns/'~townID~'/'~albumInfo.albumName) }}/{{ post.pictureFilename }}')"></div></a>
            {% endfor %}
		{% endif %}	
		<div class="clearfix"></div>
		<div>{{ link_to('towns/gallery/'~townID, 'Back to Album List') }}</div>
		
      {% endif %}

    </div>
</div>