<div class="inner-programs pull-left">
	<div class="tabs inner-tabs">
  		{{ townTab }}
  		<div class="tab-content">
	        <div class="tab-pane inner-news active">
	        	{% if list %}
			    	{% if page.total_pages == 0 %}  
		            	<p>No events found</p>
		        	{% else %}

		            {% for post in page.items %}
			              <span class="pull-right">{{ post['eventDate'] }}</span>
			              <h4 class="fontNormal">
			              	{{ link_to('towns/events/'~post['townID']~'/'~post['eventID'], post['eventTitle']) }}
			              </h4>
			              <!-- <div>{{ post['eventVenue'] }}</div> -->
			              {{ post['eventDetails'] }}
			              <hr>
		            {% endfor %}

		            <div class="widget-foot">

                        {% if page.total_pages > 1 %}

                        <ul class="pagination pull-right">

                          {% if page.current != 1 %}
                          <li>{{ link_to("towns/events/"~post['townID']~"?page=" ~ page.before, 'Prev') }}</li>
                          {% endif %}

                          {% for index in 1..page.total_pages %}
                          {% if page.current == index %}
                          <li>{{ link_to("towns/events/"~post['townID']~"?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                          {% else %}
                          <li>{{ link_to("towns/events/"~post['townID']~"?page=" ~ index, index) }}</li>
                          {% endif %}
                          {% endfor %}         

                          {% if page.current != page.total_pages %}                 
                          <li>{{ link_to("towns/events/"~post['townID']~"?page=" ~ page.next, 'Next') }}</li>
                          {% endif %}
                        </ul>
                        {% endif %}

                        <div class="clearfix"></div> 

                      </div>

		          {% endif %}
	          {% else %}
	          	<span class="pull-right">{{ date("F j, Y", event.eventDate) }}</span>
				<h3><a href="">
					{{ event.eventTitle }}
				</a></h3>
				<div><strong>{{ event.eventVenue }}</strong></div>
				{{ event.eventDetails }}
				
				<br />
				<br />
				{{ link_to('towns/events/'~event.townID, 'Back to Event List') }}
				
	          {% endif %}
		    </div>
	    </div>
    </div>
</div>