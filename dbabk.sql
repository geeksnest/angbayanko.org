-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: dbabk
-- ------------------------------------------------------
-- Server version	5.5.59-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `abkresdetails`
--

DROP TABLE IF EXISTS `abkresdetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `abkresdetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `date_updated` datetime NOT NULL,
  `global_mem` int(30) DEFAULT NULL,
  `accomp_miss` int(30) DEFAULT NULL,
  `worth_donat` int(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `abkresdetails`
--

LOCK TABLES `abkresdetails` WRITE;
/*!40000 ALTER TABLE `abkresdetails` DISABLE KEYS */;
INSERT INTO `abkresdetails` VALUES (1,'(123) 123-4444','angbayankofoundation.abk@gmail.com','2017-12-11 09:44:34',2,1,8);
/*!40000 ALTER TABLE `abkresdetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aboutus`
--

DROP TABLE IF EXISTS `aboutus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aboutus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  `date_updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aboutus`
--

LOCK TABLES `aboutus` WRITE;
/*!40000 ALTER TABLE `aboutus` DISABLE KEYS */;
INSERT INTO `aboutus` VALUES (1,'<h2><strong>Where does it come from?</strong></h2>\r\n\r\n<p><br />\r\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</p>\r\n\r\n<p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from &quot;de Finibus Bonorum et Malorum&quot; by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>\r\n','2017-01-16 10:20:09');
/*!40000 ALTER TABLE `aboutus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `activities`
--

DROP TABLE IF EXISTS `activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `slugs` varchar(255) NOT NULL,
  `content` text,
  `keyword` varchar(255) DEFAULT NULL,
  `status` smallint(6) NOT NULL,
  `banner` varchar(255) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `sort` varchar(255) DEFAULT NULL,
  `featured` varchar(255) DEFAULT NULL,
  `activity_description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activities`
--

LOCK TABLES `activities` WRITE;
/*!40000 ALTER TABLE `activities` DISABLE KEYS */;
INSERT INTO `activities` VALUES (6,'lolo and lola','lolo-and-lola','<p>Loremyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy</p>\r\n','Third Part',1,'/img/activities/Vortex Healing ATV Rental-01.jpg','2017-11-23 14:54:56','2017-11-23 14:54:56','3',NULL,'Lorem XD'),(8,'My Activities','my-activities','<p>asdasdasd</p>\r\n\r\n<p>asad</p>\r\n','My Activities',1,'/img/activities/cat2 (1).jpg','2017-01-13 11:53:07','2017-01-13 11:53:07','5',NULL,'desc'),(10,'5th Part','5th-part','<p>walalsaldasd</p>\r\n','5th Part',1,'/img/activities/atv.png','2017-01-13 11:53:45','2017-01-13 11:53:45','0',NULL,'walng desc'),(13,'sample Activity','sample-activity','<p>asdasdasd suoer sample</p>\r\n','activity ko now',1,'actkonow','2017-01-13 11:54:27','2017-01-13 11:54:27','0',NULL,'asdasdsa'),(18,'super title','super-title','<p><strong>lurem</strong><br />\r\n<br />\r\nCommon names for&nbsp;<strong>Lorem ipsum</strong>&nbsp;text include: - blind text. ...&nbsp;<strong>Lorem ipsum</strong>&nbsp;is latin, slightly jumbled, the remnants of a passage from Cicero&#39;s &#39;De finibus bonorum et malorum&#39; 1.10.32, which begins &#39;Neque porro quisquam est qui dolorem&nbsp;<strong>ipsum</strong>&nbsp;quia dolor sit amet, consectetur, adipisci velit...&#39;</p>\r\n','asdasdasd',1,'/img/activities/cat2 (1).jpg','2017-01-13 11:25:46','2017-01-13 11:25:46','0','1','may decription.. mahaaaaaanaaaaaaabaaaaaaaaaaaaaaaaaaaaaaaaabababababaa'),(19,'new activitiiiiiewewew','new-activitiiiiiewewew','<p>zDxa as xd xd</p>\r\n','dasdad',1,'/img/activities/img-2-425x200.png','2017-01-13 11:55:33','2017-01-13 11:55:33','0',NULL,'saas dessdcs 123'),(20,'sdgsdgsdgsdg','sdgsdgsdgsdg','<p>sdgsdg&nbsp;</p>\r\n\r\n<p><img alt=\"\" src=\"/img/activities/cat2 (1).jpg\" style=\"height:177px; width:284px\" /></p>\r\n','dsdgsdg',1,'/img/activities/cat2 (1).jpg','2017-01-13 11:59:36','2017-01-13 11:59:36','0',NULL,'safafagagahadhdha 1111111111111111111111111111111111 11111111 sd sdsdhsdhs sdhsdhsd'),(21,'sample activitiy100','sample-activitiy100','<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>\r\n','sample activitiy100',1,'/img/activities/img-2-425x200.png','2017-01-13 14:12:58','2017-01-13 14:12:58','0',NULL,'sample activitiy100'),(22,'jacinto','jacinto','<p><img alt=\"\" src=\"/img/activities/img-2-425x200.png\" style=\"height:200px; width:425px\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>jacintojacintojacintojacintojacinto</p>\r\n','jacinto',1,'/img/activities/img-2-425x200.png','2017-01-17 09:29:34','2017-01-17 09:29:34','0',NULL,'jacinto jacinto jacintojacintojacintojacintojacintojacintojacinto'),(23,'aa','aa','<p>asdasd</p>\r\n','ss',1,'/img/activities/cat2 (1).jpg','2017-01-22 22:51:54','2017-01-22 22:51:54','0',NULL,'dasdsad'),(24,'sample ojt active','sample-ojt-active','<p>dasd</p>\r\n','',1,'/img/programs/post/fuchs-1310826_960_720.jpg','2017-11-21 10:33:19','2017-11-21 10:33:19','0',NULL,'sdsad'),(25,'abkrescreateactivity','abkrescreateactivity','<p>abkrescreateactivity</p>\r\n','abkrescreateactivity',0,'abkrescreateactivity','2017-11-21 10:36:50','2017-11-21 10:36:50','0',NULL,'abkrescreateactivity'),(26,'sample','sample','<p>ffsf</p>\r\n','',1,'/img/activities/cat2 (1).jpg','2017-11-21 10:52:42','2017-11-21 10:52:42','0',NULL,'sfdf'),(27,'asdfghjkl,hjgfdgfsaa','asdfghjklhjgfdgfsaa','<p>SADGFHGJKGHGFDHGSFA</p>\r\n','',1,'/img/programs/post/IMG_175945 (2).jpg','2017-12-06 14:06:25','2017-12-06 14:06:25','0',NULL,'ASDFGTHJKLJKHGFD'),(28,'adsdasd','adsdasd','<p>asdasdsa</p>\r\n','',1,'asdsad','2017-12-07 09:58:34','2017-12-07 09:58:34','0',NULL,'dsds'),(29,'adsdasda','adsdasda','<p>asdasdsa</p>\r\n','',1,'asdsad','2017-12-07 09:58:51','2017-12-07 09:58:51','0',NULL,'dsds'),(30,'adda ba','adda-ba','<p>sfdsfsdfsdfsdf</p>\r\n','',1,'adfdsdsdfdsf','2017-12-07 09:59:46','2017-12-07 09:59:46','0',NULL,'adafdsfsf'),(31,'new activity','new-activity','<p>dsdsd</p>\r\n','dsdsd',1,'dsds','2018-04-02 13:53:50','2018-04-02 13:53:50','0',NULL,'dsds'),(32,'another','another','<p>dsdsd</p>\r\n','dsds',1,'dsdsd','2018-04-02 13:55:19','2018-04-02 13:55:19','0',NULL,'dsds'),(33,'another activity','another-activity','<p>ewewe</p>\r\n','eew',1,'dfdfd','2018-04-02 13:55:43','2018-04-02 13:55:43','0',NULL,'ewew'),(34,'bago again','bago-again','<p>fdfdf</p>\r\n','fdfd',1,'fdf','2018-04-02 13:56:37','2018-04-02 13:56:37','0',NULL,'fdfd'),(35,'new ','new-','<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>\r\n','dsds',1,'dsdsd','2018-04-02 14:00:24','2018-04-02 14:00:24','0',NULL,'sdsdsds'),(36,'bagong activity','bagong-activity','<p>new<br />\r\n&nbsp;</p>\r\n','new',1,'fdfdf','2018-04-02 14:14:55','2018-04-02 14:14:55','0',NULL,'new'),(37,'another1','another1','<p>dsdsd</p>\r\n','dsdsd',1,'sdsds','2018-04-02 14:16:52','2018-04-02 14:16:52','0',NULL,'dsdsd'),(38,'another1','another1','<p>new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activitynew activity&nbsp;new activity&nbsp;new activitynew activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activityv&nbsp;new activity v&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity&nbsp;new activity</p>\r\n','dfdfdfdf',1,'dsds','2018-04-04 10:36:35','2018-04-04 10:36:35','0',NULL,'sdsds'),(39,'sadfdgfhgjh','sadfdgfhgjh','<p>asfdgfhjgkrtjnbnetdhfcvberdfbgbdb dfv t te d</p>\r\n','adfsgfhjhkjlk',1,'/img/programs/post/Snake_River_(5mb).jpg','2018-04-12 16:00:10','2018-04-12 16:00:10','0',NULL,NULL),(40,'sample tite','sample-tite','<p>sample content</p>\r\n','sample keyword',1,'/img/activities/nature-hd-background-10.jpg','2018-05-11 11:25:53','2018-05-11 11:25:53','0',NULL,'sample description');
/*!40000 ALTER TABLE `activities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `donate`
--

DROP TABLE IF EXISTS `donate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `donate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  `date_updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `donate`
--

LOCK TABLES `donate` WRITE;
/*!40000 ALTER TABLE `donate` DISABLE KEYS */;
INSERT INTO `donate` VALUES (1,'<p>&nbsp;ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque sed aliquam odio, sit amet aliquam risus. Vivamus imperdiet dapibus ipsum, quis commodo sapien efficitur sit amet. Duis et ultrices ipsum, vitae pharetra odiosss. &nbsp;asdsa faf aafafasfafasf afa</p>\r\n\r\n<p>asfasfasfasf</p>\r\n\r\n<p>asfas</p>\r\n\r\n<p>fas</p>\r\n\r\n<p>fas</p>\r\n\r\n<p>fasf</p>\r\n\r\n<p>&nbsp;</p>\r\n','2018-04-13 12:01:48');
/*!40000 ALTER TABLE `donate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eventname` varchar(255) NOT NULL,
  `slugs` varchar(255) DEFAULT NULL,
  `shortdesc` text NOT NULL,
  `longdesc` text NOT NULL,
  `location` varchar(255) NOT NULL,
  `startdate` date NOT NULL,
  `enddate` date NOT NULL,
  `starttime` time DEFAULT NULL,
  `endtime` time DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `status` smallint(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` VALUES (13,'PITCON','another','hhh','<p>hhhhhhhhhhhh</p>\r\n','hhhhhhhhhhhh','2018-04-04','2018-04-05','08:00:00','08:00:00','2018-04-04 11:05:42','2018-04-04 11:05:42',1),(14,'Merry Christmas and A Happy New Year','merry-christmas-and-a-happy-new-year','Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas si','<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis a</p>\r\n','Henry Lee Irwin Theater Ateneo de Manila University','2017-11-17','2017-11-18',NULL,NULL,'2017-11-17 17:36:29','2017-11-17 17:36:29',1),(15,'event mo to','event-mo-to','shoooooooort description','<p>looooooooong description</p>\r\n','mapukpukaw street di mahagilap city','2017-11-21','2018-01-31',NULL,NULL,'2017-11-20 16:38:22','2017-11-20 16:38:22',1),(17,'sample','','a','<p>a</p>\r\n','a','2017-11-21','2017-11-22',NULL,NULL,'2017-11-21 10:03:07','2017-11-21 10:03:07',1),(18,'erwer','','ewr','<p>rerwr</p>\r\n','wer','2017-11-21','2017-11-21',NULL,NULL,'2017-11-21 14:07:04','2017-11-21 14:07:04',1),(19,'dssa','asdsad','asdsd','<p>sdadsad</p>\r\n','dasd','2017-11-29','2017-12-30',NULL,NULL,'2017-11-23 11:27:26','2017-11-23 11:27:26',1),(20,'dssa','dasd','dasd','<p>asdasdasdasdasd</p>\r\n','dsads','2017-11-29','2018-01-05',NULL,NULL,'2017-11-24 17:16:19','2017-11-24 17:16:19',1),(21,'uuuiuiu','yyhhhhhhhh','hhhhhhh','<p>hhhhhhhhhhhhh</p>\r\n','ghhghg','2017-11-24','2017-11-25',NULL,NULL,'2017-11-24 17:35:36','2017-11-24 17:35:36',1),(22,'sample event','','saafdfdsfsd','<p>fdsfsdfdd</p>\r\n','fsdfasd','2017-12-06','2017-12-06',NULL,NULL,'2017-12-06 17:20:54','2017-12-06 17:20:54',1),(23,'PITCON2','ddffdfddsdd','dfdfdfdfdsds','<p>dsdsdsdsd</p>\r\n','ggfgfgfffgfgfg','2018-03-19','2018-03-21','08:00:00','08:00:00','2018-04-12 13:20:53','2018-04-12 13:20:53',1),(24,'ITConference','fdfdfd','dfdfdfdf','<p>fdfdfdfdf</p>\r\n','fdfdfdfdf','2018-03-22','2018-03-23',NULL,NULL,'2018-03-19 16:24:38','2018-03-19 16:24:38',1),(25,'                                       Fundrasing     e   vent ','                       hfhhfhfhfhf','                                   hhh       ','<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; fgfgf&nbsp; &nbsp; &nbsp; &nbsp;uhu</p>\r\n','gfggggggff','2018-03-20','2018-03-22',NULL,NULL,'2018-03-20 14:07:24','2018-03-20 14:07:24',1),(26,'                                                            ','         nnnn                                                      ','                                             ','<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</p>\r\n','                                                                             ','2018-03-20','2018-03-22',NULL,NULL,'2018-03-20 14:11:46','2018-03-20 14:11:46',1),(27,'Run For A Cause','fdfdf','hdsjhdjs','<p>dsdsds</p>\r\n','sdsdsd','2018-03-20','2018-03-21',NULL,NULL,'2018-03-20 14:29:59','2018-03-20 14:29:59',1),(29,'new','','dsds','<p>sdsdsd</p>\r\n','dsds','2018-03-23','2018-03-24',NULL,NULL,'2018-03-23 09:31:56','2018-03-23 09:31:56',1),(30,'bago','','dsdsdsd','<p>sdsdsds</p>\r\n','dsdsd','2018-03-23','2018-03-24',NULL,NULL,'2018-03-23 11:11:22','2018-03-23 11:11:22',1),(31,'                            PITCON','pitcon','Pangasinan IT Conference','<p>Pangasinan IT Conference</p>\r\n','dsdsdsdsdsd','2018-03-27','2018-03-27',NULL,NULL,'2018-03-26 10:06:35','2018-03-26 10:06:35',1),(32,'                            PITCON','                                   pitcon     sds','P        an         g    asinan         IT ','<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Pang&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; asinan &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; IT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Confe&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; rence</p>\r\n','dsdsdsdsdsd','2018-03-27','2018-03-28',NULL,NULL,'2018-03-26 10:07:38','2018-03-26 10:07:38',1),(33,'                                                                                                                                        ','                                                                                                                                                              ','                                             ','<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\r\n','                                                                                                                         ','2018-03-27','2018-03-28',NULL,NULL,'2018-03-26 10:08:59','2018-03-26 10:08:59',1),(34,'panibago','dsds','sdsd','<p>sdsds</p>\r\n','                        ^&^%^%^% ','2018-03-27','2018-03-27',NULL,NULL,'2018-03-27 16:32:54','2018-03-27 16:32:54',1),(35,'sample event','dsds','dsds','<p>dsdsd</p>\r\n','dsd','2018-04-02','2018-04-04',NULL,NULL,'2018-04-02 11:25:07','2018-04-02 11:25:07',1),(36,'new','new','new','<p>new new newnewnewnewnewnewnewnewnewnewnewnewnew newnew</p>\r\n','hjhgjjkh','2018-04-03','2018-04-05',NULL,NULL,'2018-04-02 11:43:30','2018-04-02 11:43:30',1),(37,'new again','new','new','<p>new</p>\r\n','new','2018-04-04','2018-04-06',NULL,NULL,'2018-04-02 12:00:38','2018-04-02 12:00:38',1),(39,'new1','new','new','<p>new</p>\r\n','ewewewewe','2018-04-03','2018-04-04',NULL,NULL,'2018-04-02 14:44:28','2018-04-02 14:44:28',1),(40,'bagong event','sasa','sasas','<p>dsdsdsd</p>\r\n','     ^%^%','2018-04-03','2018-04-05',NULL,NULL,'2018-04-02 14:57:15','2018-04-02 14:57:15',1),(41,'new event','dfdfd','fdfdf','<p>fdfdfdf</p>\r\n','fdfdf','2018-04-06','2018-04-07','05:23:00','17:23:00','2018-04-03 17:27:52','2018-04-03 17:27:52',1),(42,'another1','another','another ','<p>another ano</p>\r\n','fdfdf','2018-04-05','2018-04-06','10:23:00','20:00:00','2018-04-04 10:26:36','2018-04-04 10:26:36',1),(43,'Charity event','charity event','charity that can help others','<p>dfdfdfdfdfdf</p>\r\n','dsdsd','2018-04-12','2018-04-14','09:41:00','19:41:00','2018-04-11 10:58:34','2018-04-11 10:58:34',1),(44,'another new event','dsdsd','sdsdsd','<p>sdsdsdddsdsdsdsdsdsdsdsdsdsdsddsddsdsdsdsddssdsd</p>\r\n','dsdsdsdsd','2018-04-12','2018-04-14','10:59:00','20:00:00','2018-04-11 10:59:49','2018-04-11 10:59:49',1),(45,'ghhgg','ghghgh','ghgghgh','<p>ghghg</p>\r\n','ghghg','2018-04-12','2018-04-13','09:57:00','18:57:00','2018-04-12 14:56:59','2018-04-12 14:56:59',1),(46,'hghgh','hghgh','hghghg','<p>hghghg</p>\r\n','hghg','2018-04-12','2018-04-13','14:57:00','20:57:00','2018-04-12 14:58:02','2018-04-12 14:58:02',1);
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `expert`
--

DROP TABLE IF EXISTS `expert`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `expert` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  `date_updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `expert`
--

LOCK TABLES `expert` WRITE;
/*!40000 ALTER TABLE `expert` DISABLE KEYS */;
INSERT INTO `expert` VALUES (1,'<h1><span class=\"marker\"><strong>AN EXPERT IN EMERGENCY RESPONSE</strong></span></h1>\r\n\r\n<blockquote>\r\n<p><em>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</em></p>\r\n</blockquote>\r\n\r\n<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.</p>\r\n\r\n<p>sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat.</p>\r\n\r\n<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit. sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet.</p>\r\n\r\n<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit. sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet.</p>\r\n','2017-11-22 14:05:56');
/*!40000 ALTER TABLE `expert` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jointeam`
--

DROP TABLE IF EXISTS `jointeam`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jointeam` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  `date_updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jointeam`
--

LOCK TABLES `jointeam` WRITE;
/*!40000 ALTER TABLE `jointeam` DISABLE KEYS */;
INSERT INTO `jointeam` VALUES (1,'<p>There are many ways to contribute to our mission in helping to serve to the community. Fill out the form below and your volunteer coordinator will be in touch.ss</p>\r\n','2018-04-20 14:33:17');
/*!40000 ALTER TABLE `jointeam` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_acl_groups`
--

DROP TABLE IF EXISTS `phpbb_acl_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_acl_groups` (
  `group_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `forum_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `auth_option_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `auth_role_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `auth_setting` tinyint(2) NOT NULL DEFAULT '0',
  KEY `group_id` (`group_id`),
  KEY `auth_opt_id` (`auth_option_id`),
  KEY `auth_role_id` (`auth_role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_acl_groups`
--

LOCK TABLES `phpbb_acl_groups` WRITE;
/*!40000 ALTER TABLE `phpbb_acl_groups` DISABLE KEYS */;
INSERT INTO `phpbb_acl_groups` VALUES (1,0,85,0,1),(1,0,93,0,1),(1,0,111,0,1),(5,0,0,5,0),(5,0,0,1,0),(2,0,0,6,0),(3,0,0,6,0),(4,0,0,5,0),(4,0,0,10,0),(1,1,0,17,0),(2,1,0,17,0),(3,1,0,17,0),(6,1,0,17,0),(1,2,0,17,0),(2,2,0,15,0),(3,2,0,15,0),(4,2,0,21,0),(5,2,0,14,0),(5,2,0,10,0),(6,2,0,19,0),(7,0,0,23,0),(7,2,0,24,0);
/*!40000 ALTER TABLE `phpbb_acl_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_acl_options`
--

DROP TABLE IF EXISTS `phpbb_acl_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_acl_options` (
  `auth_option_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `auth_option` varchar(50) COLLATE utf8_bin NOT NULL DEFAULT '',
  `is_global` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_local` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `founder_only` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`auth_option_id`),
  UNIQUE KEY `auth_option` (`auth_option`)
) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_acl_options`
--

LOCK TABLES `phpbb_acl_options` WRITE;
/*!40000 ALTER TABLE `phpbb_acl_options` DISABLE KEYS */;
INSERT INTO `phpbb_acl_options` VALUES (1,'f_',0,1,0),(2,'f_announce',0,1,0),(3,'f_attach',0,1,0),(4,'f_bbcode',0,1,0),(5,'f_bump',0,1,0),(6,'f_delete',0,1,0),(7,'f_download',0,1,0),(8,'f_edit',0,1,0),(9,'f_email',0,1,0),(10,'f_flash',0,1,0),(11,'f_icons',0,1,0),(12,'f_ignoreflood',0,1,0),(13,'f_img',0,1,0),(14,'f_list',0,1,0),(15,'f_noapprove',0,1,0),(16,'f_poll',0,1,0),(17,'f_post',0,1,0),(18,'f_postcount',0,1,0),(19,'f_print',0,1,0),(20,'f_read',0,1,0),(21,'f_reply',0,1,0),(22,'f_report',0,1,0),(23,'f_search',0,1,0),(24,'f_sigs',0,1,0),(25,'f_smilies',0,1,0),(26,'f_sticky',0,1,0),(27,'f_subscribe',0,1,0),(28,'f_user_lock',0,1,0),(29,'f_vote',0,1,0),(30,'f_votechg',0,1,0),(31,'m_',1,1,0),(32,'m_approve',1,1,0),(33,'m_chgposter',1,1,0),(34,'m_delete',1,1,0),(35,'m_edit',1,1,0),(36,'m_info',1,1,0),(37,'m_lock',1,1,0),(38,'m_merge',1,1,0),(39,'m_move',1,1,0),(40,'m_report',1,1,0),(41,'m_split',1,1,0),(42,'m_ban',1,0,0),(43,'m_warn',1,0,0),(44,'a_',1,0,0),(45,'a_aauth',1,0,0),(46,'a_attach',1,0,0),(47,'a_authgroups',1,0,0),(48,'a_authusers',1,0,0),(49,'a_backup',1,0,0),(50,'a_ban',1,0,0),(51,'a_bbcode',1,0,0),(52,'a_board',1,0,0),(53,'a_bots',1,0,0),(54,'a_clearlogs',1,0,0),(55,'a_email',1,0,0),(56,'a_fauth',1,0,0),(57,'a_forum',1,0,0),(58,'a_forumadd',1,0,0),(59,'a_forumdel',1,0,0),(60,'a_group',1,0,0),(61,'a_groupadd',1,0,0),(62,'a_groupdel',1,0,0),(63,'a_icons',1,0,0),(64,'a_jabber',1,0,0),(65,'a_language',1,0,0),(66,'a_mauth',1,0,0),(67,'a_modules',1,0,0),(68,'a_names',1,0,0),(69,'a_phpinfo',1,0,0),(70,'a_profile',1,0,0),(71,'a_prune',1,0,0),(72,'a_ranks',1,0,0),(73,'a_reasons',1,0,0),(74,'a_roles',1,0,0),(75,'a_search',1,0,0),(76,'a_server',1,0,0),(77,'a_styles',1,0,0),(78,'a_switchperm',1,0,0),(79,'a_uauth',1,0,0),(80,'a_user',1,0,0),(81,'a_userdel',1,0,0),(82,'a_viewauth',1,0,0),(83,'a_viewlogs',1,0,0),(84,'a_words',1,0,0),(85,'u_',1,0,0),(86,'u_attach',1,0,0),(87,'u_chgavatar',1,0,0),(88,'u_chgcensors',1,0,0),(89,'u_chgemail',1,0,0),(90,'u_chggrp',1,0,0),(91,'u_chgname',1,0,0),(92,'u_chgpasswd',1,0,0),(93,'u_download',1,0,0),(94,'u_hideonline',1,0,0),(95,'u_ignoreflood',1,0,0),(96,'u_masspm',1,0,0),(97,'u_masspm_group',1,0,0),(98,'u_pm_attach',1,0,0),(99,'u_pm_bbcode',1,0,0),(100,'u_pm_delete',1,0,0),(101,'u_pm_download',1,0,0),(102,'u_pm_edit',1,0,0),(103,'u_pm_emailpm',1,0,0),(104,'u_pm_flash',1,0,0),(105,'u_pm_forward',1,0,0),(106,'u_pm_img',1,0,0),(107,'u_pm_printpm',1,0,0),(108,'u_pm_smilies',1,0,0),(109,'u_readpm',1,0,0),(110,'u_savedrafts',1,0,0),(111,'u_search',1,0,0),(112,'u_sendemail',1,0,0),(113,'u_sendim',1,0,0),(114,'u_sendpm',1,0,0),(115,'u_sig',1,0,0),(116,'u_viewonline',1,0,0),(117,'u_viewprofile',1,0,0);
/*!40000 ALTER TABLE `phpbb_acl_options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_acl_roles`
--

DROP TABLE IF EXISTS `phpbb_acl_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_acl_roles` (
  `role_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `role_description` text COLLATE utf8_bin NOT NULL,
  `role_type` varchar(10) COLLATE utf8_bin NOT NULL DEFAULT '',
  `role_order` smallint(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`role_id`),
  KEY `role_type` (`role_type`),
  KEY `role_order` (`role_order`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_acl_roles`
--

LOCK TABLES `phpbb_acl_roles` WRITE;
/*!40000 ALTER TABLE `phpbb_acl_roles` DISABLE KEYS */;
INSERT INTO `phpbb_acl_roles` VALUES (1,'ROLE_ADMIN_STANDARD','ROLE_DESCRIPTION_ADMIN_STANDARD','a_',1),(2,'ROLE_ADMIN_FORUM','ROLE_DESCRIPTION_ADMIN_FORUM','a_',3),(3,'ROLE_ADMIN_USERGROUP','ROLE_DESCRIPTION_ADMIN_USERGROUP','a_',4),(4,'ROLE_ADMIN_FULL','ROLE_DESCRIPTION_ADMIN_FULL','a_',2),(5,'ROLE_USER_FULL','ROLE_DESCRIPTION_USER_FULL','u_',3),(6,'ROLE_USER_STANDARD','ROLE_DESCRIPTION_USER_STANDARD','u_',1),(7,'ROLE_USER_LIMITED','ROLE_DESCRIPTION_USER_LIMITED','u_',2),(8,'ROLE_USER_NOPM','ROLE_DESCRIPTION_USER_NOPM','u_',4),(9,'ROLE_USER_NOAVATAR','ROLE_DESCRIPTION_USER_NOAVATAR','u_',5),(10,'ROLE_MOD_FULL','ROLE_DESCRIPTION_MOD_FULL','m_',3),(11,'ROLE_MOD_STANDARD','ROLE_DESCRIPTION_MOD_STANDARD','m_',1),(12,'ROLE_MOD_SIMPLE','ROLE_DESCRIPTION_MOD_SIMPLE','m_',2),(13,'ROLE_MOD_QUEUE','ROLE_DESCRIPTION_MOD_QUEUE','m_',4),(14,'ROLE_FORUM_FULL','ROLE_DESCRIPTION_FORUM_FULL','f_',7),(15,'ROLE_FORUM_STANDARD','ROLE_DESCRIPTION_FORUM_STANDARD','f_',5),(16,'ROLE_FORUM_NOACCESS','ROLE_DESCRIPTION_FORUM_NOACCESS','f_',1),(17,'ROLE_FORUM_READONLY','ROLE_DESCRIPTION_FORUM_READONLY','f_',2),(18,'ROLE_FORUM_LIMITED','ROLE_DESCRIPTION_FORUM_LIMITED','f_',3),(19,'ROLE_FORUM_BOT','ROLE_DESCRIPTION_FORUM_BOT','f_',9),(20,'ROLE_FORUM_ONQUEUE','ROLE_DESCRIPTION_FORUM_ONQUEUE','f_',8),(21,'ROLE_FORUM_POLLS','ROLE_DESCRIPTION_FORUM_POLLS','f_',6),(22,'ROLE_FORUM_LIMITED_POLLS','ROLE_DESCRIPTION_FORUM_LIMITED_POLLS','f_',4),(23,'ROLE_USER_NEW_MEMBER','ROLE_DESCRIPTION_USER_NEW_MEMBER','u_',6),(24,'ROLE_FORUM_NEW_MEMBER','ROLE_DESCRIPTION_FORUM_NEW_MEMBER','f_',10);
/*!40000 ALTER TABLE `phpbb_acl_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_acl_roles_data`
--

DROP TABLE IF EXISTS `phpbb_acl_roles_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_acl_roles_data` (
  `role_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `auth_option_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `auth_setting` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`role_id`,`auth_option_id`),
  KEY `ath_op_id` (`auth_option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_acl_roles_data`
--

LOCK TABLES `phpbb_acl_roles_data` WRITE;
/*!40000 ALTER TABLE `phpbb_acl_roles_data` DISABLE KEYS */;
INSERT INTO `phpbb_acl_roles_data` VALUES (1,44,1),(1,46,1),(1,47,1),(1,48,1),(1,50,1),(1,51,1),(1,52,1),(1,56,1),(1,57,1),(1,58,1),(1,59,1),(1,60,1),(1,61,1),(1,62,1),(1,63,1),(1,66,1),(1,68,1),(1,70,1),(1,71,1),(1,72,1),(1,73,1),(1,79,1),(1,80,1),(1,81,1),(1,82,1),(1,83,1),(1,84,1),(2,44,1),(2,47,1),(2,48,1),(2,56,1),(2,57,1),(2,58,1),(2,59,1),(2,66,1),(2,71,1),(2,79,1),(2,82,1),(2,83,1),(3,44,1),(3,47,1),(3,48,1),(3,50,1),(3,60,1),(3,61,1),(3,62,1),(3,72,1),(3,79,1),(3,80,1),(3,82,1),(3,83,1),(4,44,1),(4,45,1),(4,46,1),(4,47,1),(4,48,1),(4,49,1),(4,50,1),(4,51,1),(4,52,1),(4,53,1),(4,54,1),(4,55,1),(4,56,1),(4,57,1),(4,58,1),(4,59,1),(4,60,1),(4,61,1),(4,62,1),(4,63,1),(4,64,1),(4,65,1),(4,66,1),(4,67,1),(4,68,1),(4,69,1),(4,70,1),(4,71,1),(4,72,1),(4,73,1),(4,74,1),(4,75,1),(4,76,1),(4,77,1),(4,78,1),(4,79,1),(4,80,1),(4,81,1),(4,82,1),(4,83,1),(4,84,1),(5,85,1),(5,86,1),(5,87,1),(5,88,1),(5,89,1),(5,90,1),(5,91,1),(5,92,1),(5,93,1),(5,94,1),(5,95,1),(5,96,1),(5,97,1),(5,98,1),(5,99,1),(5,100,1),(5,101,1),(5,102,1),(5,103,1),(5,104,1),(5,105,1),(5,106,1),(5,107,1),(5,108,1),(5,109,1),(5,110,1),(5,111,1),(5,112,1),(5,113,1),(5,114,1),(5,115,1),(5,116,1),(5,117,1),(6,85,1),(6,86,1),(6,87,1),(6,88,1),(6,89,1),(6,92,1),(6,93,1),(6,94,1),(6,96,1),(6,97,1),(6,98,1),(6,99,1),(6,100,1),(6,101,1),(6,102,1),(6,103,1),(6,106,1),(6,107,1),(6,108,1),(6,109,1),(6,110,1),(6,111,1),(6,112,1),(6,113,1),(6,114,1),(6,115,1),(6,117,1),(7,85,1),(7,87,1),(7,88,1),(7,89,1),(7,92,1),(7,93,1),(7,94,1),(7,99,1),(7,100,1),(7,101,1),(7,102,1),(7,105,1),(7,106,1),(7,107,1),(7,108,1),(7,109,1),(7,114,1),(7,115,1),(7,117,1),(8,85,1),(8,87,1),(8,88,1),(8,89,1),(8,92,1),(8,93,1),(8,94,1),(8,96,0),(8,97,0),(8,109,0),(8,114,0),(8,115,1),(8,117,1),(9,85,1),(9,87,0),(9,88,1),(9,89,1),(9,92,1),(9,93,1),(9,94,1),(9,99,1),(9,100,1),(9,101,1),(9,102,1),(9,105,1),(9,106,1),(9,107,1),(9,108,1),(9,109,1),(9,114,1),(9,115,1),(9,117,1),(10,31,1),(10,32,1),(10,33,1),(10,34,1),(10,35,1),(10,36,1),(10,37,1),(10,38,1),(10,39,1),(10,40,1),(10,41,1),(10,42,1),(10,43,1),(11,31,1),(11,32,1),(11,34,1),(11,35,1),(11,36,1),(11,37,1),(11,38,1),(11,39,1),(11,40,1),(11,41,1),(11,43,1),(12,31,1),(12,34,1),(12,35,1),(12,36,1),(12,40,1),(13,31,1),(13,32,1),(13,35,1),(14,1,1),(14,2,1),(14,3,1),(14,4,1),(14,5,1),(14,6,1),(14,7,1),(14,8,1),(14,9,1),(14,10,1),(14,11,1),(14,12,1),(14,13,1),(14,14,1),(14,15,1),(14,16,1),(14,17,1),(14,18,1),(14,19,1),(14,20,1),(14,21,1),(14,22,1),(14,23,1),(14,24,1),(14,25,1),(14,26,1),(14,27,1),(14,28,1),(14,29,1),(14,30,1),(15,1,1),(15,3,1),(15,4,1),(15,5,1),(15,6,1),(15,7,1),(15,8,1),(15,9,1),(15,11,1),(15,13,1),(15,14,1),(15,15,1),(15,17,1),(15,18,1),(15,19,1),(15,20,1),(15,21,1),(15,22,1),(15,23,1),(15,24,1),(15,25,1),(15,27,1),(15,29,1),(15,30,1),(16,1,0),(17,1,1),(17,7,1),(17,14,1),(17,19,1),(17,20,1),(17,23,1),(17,27,1),(18,1,1),(18,4,1),(18,7,1),(18,8,1),(18,9,1),(18,13,1),(18,14,1),(18,15,1),(18,17,1),(18,18,1),(18,19,1),(18,20,1),(18,21,1),(18,22,1),(18,23,1),(18,24,1),(18,25,1),(18,27,1),(18,29,1),(19,1,1),(19,7,1),(19,14,1),(19,19,1),(19,20,1),(20,1,1),(20,3,1),(20,4,1),(20,7,1),(20,8,1),(20,9,1),(20,13,1),(20,14,1),(20,15,0),(20,17,1),(20,18,1),(20,19,1),(20,20,1),(20,21,1),(20,22,1),(20,23,1),(20,24,1),(20,25,1),(20,27,1),(20,29,1),(21,1,1),(21,3,1),(21,4,1),(21,5,1),(21,6,1),(21,7,1),(21,8,1),(21,9,1),(21,11,1),(21,13,1),(21,14,1),(21,15,1),(21,16,1),(21,17,1),(21,18,1),(21,19,1),(21,20,1),(21,21,1),(21,22,1),(21,23,1),(21,24,1),(21,25,1),(21,27,1),(21,29,1),(21,30,1),(22,1,1),(22,4,1),(22,7,1),(22,8,1),(22,9,1),(22,13,1),(22,14,1),(22,15,1),(22,16,1),(22,17,1),(22,18,1),(22,19,1),(22,20,1),(22,21,1),(22,22,1),(22,23,1),(22,24,1),(22,25,1),(22,27,1),(22,29,1),(23,96,0),(23,97,0),(23,114,0),(24,15,0);
/*!40000 ALTER TABLE `phpbb_acl_roles_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_acl_users`
--

DROP TABLE IF EXISTS `phpbb_acl_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_acl_users` (
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `forum_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `auth_option_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `auth_role_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `auth_setting` tinyint(2) NOT NULL DEFAULT '0',
  KEY `user_id` (`user_id`),
  KEY `auth_option_id` (`auth_option_id`),
  KEY `auth_role_id` (`auth_role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_acl_users`
--

LOCK TABLES `phpbb_acl_users` WRITE;
/*!40000 ALTER TABLE `phpbb_acl_users` DISABLE KEYS */;
INSERT INTO `phpbb_acl_users` VALUES (2,0,0,5,0);
/*!40000 ALTER TABLE `phpbb_acl_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_attachments`
--

DROP TABLE IF EXISTS `phpbb_attachments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_attachments` (
  `attach_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `post_msg_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `topic_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `in_message` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `poster_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `is_orphan` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `physical_filename` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `real_filename` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `download_count` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `attach_comment` text COLLATE utf8_bin NOT NULL,
  `extension` varchar(100) COLLATE utf8_bin NOT NULL DEFAULT '',
  `mimetype` varchar(100) COLLATE utf8_bin NOT NULL DEFAULT '',
  `filesize` int(20) unsigned NOT NULL DEFAULT '0',
  `filetime` int(11) unsigned NOT NULL DEFAULT '0',
  `thumbnail` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`attach_id`),
  KEY `filetime` (`filetime`),
  KEY `post_msg_id` (`post_msg_id`),
  KEY `topic_id` (`topic_id`),
  KEY `poster_id` (`poster_id`),
  KEY `is_orphan` (`is_orphan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_attachments`
--

LOCK TABLES `phpbb_attachments` WRITE;
/*!40000 ALTER TABLE `phpbb_attachments` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_attachments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_banlist`
--

DROP TABLE IF EXISTS `phpbb_banlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_banlist` (
  `ban_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `ban_userid` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ban_ip` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '',
  `ban_email` varchar(100) COLLATE utf8_bin NOT NULL DEFAULT '',
  `ban_start` int(11) unsigned NOT NULL DEFAULT '0',
  `ban_end` int(11) unsigned NOT NULL DEFAULT '0',
  `ban_exclude` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ban_reason` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `ban_give_reason` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`ban_id`),
  KEY `ban_end` (`ban_end`),
  KEY `ban_user` (`ban_userid`,`ban_exclude`),
  KEY `ban_email` (`ban_email`,`ban_exclude`),
  KEY `ban_ip` (`ban_ip`,`ban_exclude`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_banlist`
--

LOCK TABLES `phpbb_banlist` WRITE;
/*!40000 ALTER TABLE `phpbb_banlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_banlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_bbcodes`
--

DROP TABLE IF EXISTS `phpbb_bbcodes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_bbcodes` (
  `bbcode_id` smallint(4) unsigned NOT NULL DEFAULT '0',
  `bbcode_tag` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT '',
  `bbcode_helpline` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `display_on_posting` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `bbcode_match` text COLLATE utf8_bin NOT NULL,
  `bbcode_tpl` mediumtext COLLATE utf8_bin NOT NULL,
  `first_pass_match` mediumtext COLLATE utf8_bin NOT NULL,
  `first_pass_replace` mediumtext COLLATE utf8_bin NOT NULL,
  `second_pass_match` mediumtext COLLATE utf8_bin NOT NULL,
  `second_pass_replace` mediumtext COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`bbcode_id`),
  KEY `display_on_post` (`display_on_posting`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_bbcodes`
--

LOCK TABLES `phpbb_bbcodes` WRITE;
/*!40000 ALTER TABLE `phpbb_bbcodes` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_bbcodes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_bookmarks`
--

DROP TABLE IF EXISTS `phpbb_bookmarks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_bookmarks` (
  `topic_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`topic_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_bookmarks`
--

LOCK TABLES `phpbb_bookmarks` WRITE;
/*!40000 ALTER TABLE `phpbb_bookmarks` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_bookmarks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_bots`
--

DROP TABLE IF EXISTS `phpbb_bots`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_bots` (
  `bot_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `bot_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `bot_name` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `bot_agent` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `bot_ip` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`bot_id`),
  KEY `bot_active` (`bot_active`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_bots`
--

LOCK TABLES `phpbb_bots` WRITE;
/*!40000 ALTER TABLE `phpbb_bots` DISABLE KEYS */;
INSERT INTO `phpbb_bots` VALUES (1,1,'AdsBot [Google]',3,'AdsBot-Google',''),(2,1,'Alexa [Bot]',4,'ia_archiver',''),(3,1,'Alta Vista [Bot]',5,'Scooter/',''),(4,1,'Ask Jeeves [Bot]',6,'Ask Jeeves',''),(5,1,'Baidu [Spider]',7,'Baiduspider',''),(6,1,'Bing [Bot]',8,'bingbot/',''),(7,1,'Exabot [Bot]',9,'Exabot',''),(8,1,'FAST Enterprise [Crawler]',10,'FAST Enterprise Crawler',''),(9,1,'FAST WebCrawler [Crawler]',11,'FAST-WebCrawler/',''),(10,1,'Francis [Bot]',12,'http://www.neomo.de/',''),(11,1,'Gigabot [Bot]',13,'Gigabot/',''),(12,1,'Google Adsense [Bot]',14,'Mediapartners-Google',''),(13,1,'Google Desktop',15,'Google Desktop',''),(14,1,'Google Feedfetcher',16,'Feedfetcher-Google',''),(15,1,'Google [Bot]',17,'Googlebot',''),(16,1,'Heise IT-Markt [Crawler]',18,'heise-IT-Markt-Crawler',''),(17,1,'Heritrix [Crawler]',19,'heritrix/1.',''),(18,1,'IBM Research [Bot]',20,'ibm.com/cs/crawler',''),(19,1,'ICCrawler - ICjobs',21,'ICCrawler - ICjobs',''),(20,1,'ichiro [Crawler]',22,'ichiro/',''),(21,1,'Majestic-12 [Bot]',23,'MJ12bot/',''),(22,1,'Metager [Bot]',24,'MetagerBot/',''),(23,1,'MSN NewsBlogs',25,'msnbot-NewsBlogs/',''),(24,1,'MSN [Bot]',26,'msnbot/',''),(25,1,'MSNbot Media',27,'msnbot-media/',''),(26,1,'Nutch [Bot]',28,'http://lucene.apache.org/nutch/',''),(27,1,'Online link [Validator]',29,'online link validator',''),(28,1,'psbot [Picsearch]',30,'psbot/0',''),(29,1,'Sensis [Crawler]',31,'Sensis Web Crawler',''),(30,1,'SEO Crawler',32,'SEO search Crawler/',''),(31,1,'Seoma [Crawler]',33,'Seoma [SEO Crawler]',''),(32,1,'SEOSearch [Crawler]',34,'SEOsearch/',''),(33,1,'Snappy [Bot]',35,'Snappy/1.1 ( http://www.urltrends.com/ )',''),(34,1,'Steeler [Crawler]',36,'http://www.tkl.iis.u-tokyo.ac.jp/~crawler/',''),(35,1,'Telekom [Bot]',37,'crawleradmin.t-info@telekom.de',''),(36,1,'TurnitinBot [Bot]',38,'TurnitinBot/',''),(37,1,'Voyager [Bot]',39,'voyager/',''),(38,1,'W3 [Sitesearch]',40,'W3 SiteSearch Crawler',''),(39,1,'W3C [Linkcheck]',41,'W3C-checklink/',''),(40,1,'W3C [Validator]',42,'W3C_Validator',''),(41,1,'YaCy [Bot]',43,'yacybot',''),(42,1,'Yahoo MMCrawler [Bot]',44,'Yahoo-MMCrawler/',''),(43,1,'Yahoo Slurp [Bot]',45,'Yahoo! DE Slurp',''),(44,1,'Yahoo [Bot]',46,'Yahoo! Slurp',''),(45,1,'YahooSeeker [Bot]',47,'YahooSeeker/','');
/*!40000 ALTER TABLE `phpbb_bots` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_config`
--

DROP TABLE IF EXISTS `phpbb_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_config` (
  `config_name` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `config_value` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `is_dynamic` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`config_name`),
  KEY `is_dynamic` (`is_dynamic`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_config`
--

LOCK TABLES `phpbb_config` WRITE;
/*!40000 ALTER TABLE `phpbb_config` DISABLE KEYS */;
INSERT INTO `phpbb_config` VALUES ('active_sessions','0',0),('allow_attachments','1',0),('allow_autologin','1',0),('allow_avatar','1',0),('allow_avatar_local','0',0),('allow_avatar_remote','0',0),('allow_avatar_remote_upload','0',0),('allow_avatar_upload','1',0),('allow_bbcode','1',0),('allow_birthdays','1',0),('allow_bookmarks','1',0),('allow_emailreuse','0',0),('allow_forum_notify','1',0),('allow_mass_pm','1',0),('allow_name_chars','USERNAME_CHARS_ANY',0),('allow_namechange','0',0),('allow_nocensors','0',0),('allow_pm_attach','0',0),('allow_pm_report','1',0),('allow_post_flash','1',0),('allow_post_links','1',0),('allow_privmsg','1',0),('allow_quick_reply','1',0),('allow_sig','1',0),('allow_sig_bbcode','1',0),('allow_sig_flash','0',0),('allow_sig_img','1',0),('allow_sig_links','1',0),('allow_sig_pm','1',0),('allow_sig_smilies','1',0),('allow_smilies','1',0),('allow_topic_notify','1',0),('attachment_quota','52428800',0),('auth_bbcode_pm','1',0),('auth_flash_pm','0',0),('auth_img_pm','1',0),('auth_method','db',0),('auth_smilies_pm','1',0),('avatar_filesize','6144',0),('avatar_gallery_path','images/avatars/gallery',0),('avatar_max_height','90',0),('avatar_max_width','90',0),('avatar_min_height','20',0),('avatar_min_width','20',0),('avatar_path','images/avatars/upload',0),('avatar_salt','4c4442d7ba4b7bddb82c0f01c5fa6cc6',0),('board_contact','efrenbautistajr@gmail.com',0),('board_disable','0',0),('board_disable_msg','',0),('board_dst','0',0),('board_email','efrenbautistajr@gmail.com',0),('board_email_form','0',0),('board_email_sig','Thanks, The Management',0),('board_hide_emails','1',0),('board_startdate','1403110152',0),('board_timezone','0',0),('browser_check','1',0),('bump_interval','10',0),('bump_type','d',0),('cache_gc','7200',0),('cache_last_gc','0',1),('captcha_gd','1',0),('captcha_gd_3d_noise','1',0),('captcha_gd_fonts','1',0),('captcha_gd_foreground_noise','0',0),('captcha_gd_wave','0',0),('captcha_gd_x_grid','25',0),('captcha_gd_y_grid','25',0),('captcha_plugin','phpbb_captcha_gd',0),('check_attachment_content','1',0),('check_dnsbl','0',0),('chg_passforce','0',0),('confirm_refresh','1',0),('cookie_domain','localhost',0),('cookie_name','phpbb3_grjrw',0),('cookie_path','/',0),('cookie_secure','0',0),('coppa_enable','0',0),('coppa_fax','',0),('coppa_mail','',0),('cron_lock','0',1),('database_gc','604800',0),('database_last_gc','0',1),('dbms_version','5.5.24-log',0),('default_dateformat','D M d, Y g:i a',0),('default_lang','en',0),('default_style','2',0),('delete_time','0',0),('display_last_edited','1',0),('display_order','0',0),('edit_time','0',0),('email_check_mx','1',0),('email_enable','1',0),('email_function_name','mail',0),('email_max_chunk_size','50',0),('email_package_size','20',0),('enable_confirm','1',0),('enable_pm_icons','1',0),('enable_post_confirm','1',0),('feed_enable','1',0),('feed_forum','1',0),('feed_http_auth','0',0),('feed_item_statistics','1',0),('feed_limit_post','15',0),('feed_limit_topic','10',0),('feed_overall','1',0),('feed_overall_forums','0',0),('feed_topic','1',0),('feed_topics_active','0',0),('feed_topics_new','1',0),('flood_interval','15',0),('force_server_vars','0',0),('form_token_lifetime','7200',0),('form_token_mintime','0',0),('form_token_sid_guests','1',0),('forward_pm','1',0),('forwarded_for_check','0',0),('full_folder_action','2',0),('fulltext_mysql_max_word_len','254',0),('fulltext_mysql_min_word_len','4',0),('fulltext_native_common_thres','5',0),('fulltext_native_load_upd','1',0),('fulltext_native_max_chars','14',0),('fulltext_native_min_chars','3',0),('gzip_compress','0',0),('hot_threshold','25',0),('icons_path','images/icons',0),('img_create_thumbnail','0',0),('img_display_inlined','1',0),('img_imagick','',0),('img_link_height','0',0),('img_link_width','0',0),('img_max_height','0',0),('img_max_thumb_width','400',0),('img_max_width','0',0),('img_min_thumb_filesize','12000',0),('ip_check','3',0),('ip_login_limit_max','50',0),('ip_login_limit_time','21600',0),('ip_login_limit_use_forwarded','0',0),('jab_enable','0',0),('jab_host','',0),('jab_package_size','20',0),('jab_password','',0),('jab_port','5222',0),('jab_use_ssl','0',0),('jab_username','',0),('last_queue_run','0',1),('ldap_base_dn','',0),('ldap_email','',0),('ldap_password','',0),('ldap_port','',0),('ldap_server','',0),('ldap_uid','',0),('ldap_user','',0),('ldap_user_filter','',0),('limit_load','0',0),('limit_search_load','0',0),('load_anon_lastread','0',0),('load_birthdays','1',0),('load_cpf_memberlist','0',0),('load_cpf_viewprofile','1',0),('load_cpf_viewtopic','0',0),('load_db_lastread','1',0),('load_db_track','1',0),('load_jumpbox','1',0),('load_moderators','1',0),('load_online','1',0),('load_online_guests','1',0),('load_online_time','5',0),('load_onlinetrack','1',0),('load_search','1',0),('load_tplcompile','0',0),('load_unreads_search','1',0),('load_user_activity','1',0),('max_attachments','3',0),('max_attachments_pm','1',0),('max_autologin_time','0',0),('max_filesize','262144',0),('max_filesize_pm','262144',0),('max_login_attempts','3',0),('max_name_chars','20',0),('max_num_search_keywords','10',0),('max_pass_chars','100',0),('max_poll_options','10',0),('max_post_chars','60000',0),('max_post_font_size','200',0),('max_post_img_height','0',0),('max_post_img_width','0',0),('max_post_smilies','0',0),('max_post_urls','0',0),('max_quote_depth','3',0),('max_reg_attempts','5',0),('max_sig_chars','255',0),('max_sig_font_size','200',0),('max_sig_img_height','0',0),('max_sig_img_width','0',0),('max_sig_smilies','0',0),('max_sig_urls','5',0),('mime_triggers','body|head|html|img|plaintext|a href|pre|script|table|title',0),('min_name_chars','3',0),('min_pass_chars','6',0),('min_post_chars','1',0),('min_search_author_chars','3',0),('new_member_group_default','0',0),('new_member_post_limit','3',0),('newest_user_colour','AA0000',1),('newest_user_id','2',1),('newest_username','admin',1),('num_files','0',1),('num_posts','1',1),('num_topics','1',1),('num_users','1',1),('override_user_style','0',0),('pass_complex','PASS_TYPE_ANY',0),('pm_edit_time','0',0),('pm_max_boxes','4',0),('pm_max_msgs','50',0),('pm_max_recipients','0',0),('posts_per_page','10',0),('print_pm','1',0),('questionnaire_unique_id','cebf0e21cfef334a',0),('queue_interval','60',0),('rand_seed','fff5f0a5c6eb194870a3ac67e4709e67',1),('rand_seed_last_update','1403110241',1),('ranks_path','images/ranks',0),('record_online_date','0',1),('record_online_users','0',1),('referer_validation','1',0),('require_activation','0',0),('script_path','/forum',0),('search_anonymous_interval','0',0),('search_block_size','250',0),('search_gc','7200',0),('search_indexing_state','',1),('search_interval','0',0),('search_last_gc','0',1),('search_store_results','1800',0),('search_type','fulltext_native',0),('secure_allow_deny','1',0),('secure_allow_empty_referer','1',0),('secure_downloads','0',0),('server_name','localhost',0),('server_port','80',0),('server_protocol','http://',0),('session_gc','3600',0),('session_last_gc','0',1),('session_length','3600',0),('site_desc','A short text to describe your forum',0),('sitename','yourdomain.com',0),('smilies_path','images/smilies',0),('smilies_per_page','50',0),('smtp_auth_method','PLAIN',0),('smtp_delivery','0',0),('smtp_host','',0),('smtp_password','',0),('smtp_port','25',0),('smtp_username','',0),('topics_per_page','25',0),('tpl_allow_php','0',0),('upload_dir_size','0',1),('upload_icons_path','images/upload_icons',0),('upload_path','files',0),('version','3.0.12',0),('warnings_expire_days','90',0),('warnings_gc','14400',0),('warnings_last_gc','0',1);
/*!40000 ALTER TABLE `phpbb_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_confirm`
--

DROP TABLE IF EXISTS `phpbb_confirm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_confirm` (
  `confirm_id` char(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `session_id` char(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `confirm_type` tinyint(3) NOT NULL DEFAULT '0',
  `code` varchar(8) COLLATE utf8_bin NOT NULL DEFAULT '',
  `seed` int(10) unsigned NOT NULL DEFAULT '0',
  `attempts` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`session_id`,`confirm_id`),
  KEY `confirm_type` (`confirm_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_confirm`
--

LOCK TABLES `phpbb_confirm` WRITE;
/*!40000 ALTER TABLE `phpbb_confirm` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_confirm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_disallow`
--

DROP TABLE IF EXISTS `phpbb_disallow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_disallow` (
  `disallow_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `disallow_username` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`disallow_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_disallow`
--

LOCK TABLES `phpbb_disallow` WRITE;
/*!40000 ALTER TABLE `phpbb_disallow` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_disallow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_drafts`
--

DROP TABLE IF EXISTS `phpbb_drafts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_drafts` (
  `draft_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `topic_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `forum_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `save_time` int(11) unsigned NOT NULL DEFAULT '0',
  `draft_subject` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `draft_message` mediumtext COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`draft_id`),
  KEY `save_time` (`save_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_drafts`
--

LOCK TABLES `phpbb_drafts` WRITE;
/*!40000 ALTER TABLE `phpbb_drafts` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_drafts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_extension_groups`
--

DROP TABLE IF EXISTS `phpbb_extension_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_extension_groups` (
  `group_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `group_name` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `cat_id` tinyint(2) NOT NULL DEFAULT '0',
  `allow_group` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `download_mode` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `upload_icon` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `max_filesize` int(20) unsigned NOT NULL DEFAULT '0',
  `allowed_forums` text COLLATE utf8_bin NOT NULL,
  `allow_in_pm` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_extension_groups`
--

LOCK TABLES `phpbb_extension_groups` WRITE;
/*!40000 ALTER TABLE `phpbb_extension_groups` DISABLE KEYS */;
INSERT INTO `phpbb_extension_groups` VALUES (1,'IMAGES',1,1,1,'',0,'',0),(2,'ARCHIVES',0,1,1,'',0,'',0),(3,'PLAIN_TEXT',0,0,1,'',0,'',0),(4,'DOCUMENTS',0,0,1,'',0,'',0),(5,'REAL_MEDIA',3,0,1,'',0,'',0),(6,'WINDOWS_MEDIA',2,0,1,'',0,'',0),(7,'FLASH_FILES',5,0,1,'',0,'',0),(8,'QUICKTIME_MEDIA',6,0,1,'',0,'',0),(9,'DOWNLOADABLE_FILES',0,0,1,'',0,'',0);
/*!40000 ALTER TABLE `phpbb_extension_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_extensions`
--

DROP TABLE IF EXISTS `phpbb_extensions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_extensions` (
  `extension_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `extension` varchar(100) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`extension_id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_extensions`
--

LOCK TABLES `phpbb_extensions` WRITE;
/*!40000 ALTER TABLE `phpbb_extensions` DISABLE KEYS */;
INSERT INTO `phpbb_extensions` VALUES (1,1,'gif'),(2,1,'png'),(3,1,'jpeg'),(4,1,'jpg'),(5,1,'tif'),(6,1,'tiff'),(7,1,'tga'),(8,2,'gtar'),(9,2,'gz'),(10,2,'tar'),(11,2,'zip'),(12,2,'rar'),(13,2,'ace'),(14,2,'torrent'),(15,2,'tgz'),(16,2,'bz2'),(17,2,'7z'),(18,3,'txt'),(19,3,'c'),(20,3,'h'),(21,3,'cpp'),(22,3,'hpp'),(23,3,'diz'),(24,3,'csv'),(25,3,'ini'),(26,3,'log'),(27,3,'js'),(28,3,'xml'),(29,4,'xls'),(30,4,'xlsx'),(31,4,'xlsm'),(32,4,'xlsb'),(33,4,'doc'),(34,4,'docx'),(35,4,'docm'),(36,4,'dot'),(37,4,'dotx'),(38,4,'dotm'),(39,4,'pdf'),(40,4,'ai'),(41,4,'ps'),(42,4,'ppt'),(43,4,'pptx'),(44,4,'pptm'),(45,4,'odg'),(46,4,'odp'),(47,4,'ods'),(48,4,'odt'),(49,4,'rtf'),(50,5,'rm'),(51,5,'ram'),(52,6,'wma'),(53,6,'wmv'),(54,7,'swf'),(55,8,'mov'),(56,8,'m4v'),(57,8,'m4a'),(58,8,'mp4'),(59,8,'3gp'),(60,8,'3g2'),(61,8,'qt'),(62,9,'mpeg'),(63,9,'mpg'),(64,9,'mp3'),(65,9,'ogg'),(66,9,'ogm');
/*!40000 ALTER TABLE `phpbb_extensions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_forums`
--

DROP TABLE IF EXISTS `phpbb_forums`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_forums` (
  `forum_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `left_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `right_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `forum_parents` mediumtext COLLATE utf8_bin NOT NULL,
  `forum_name` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `forum_desc` text COLLATE utf8_bin NOT NULL,
  `forum_desc_bitfield` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `forum_desc_options` int(11) unsigned NOT NULL DEFAULT '7',
  `forum_desc_uid` varchar(8) COLLATE utf8_bin NOT NULL DEFAULT '',
  `forum_link` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `forum_password` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '',
  `forum_style` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `forum_image` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `forum_rules` text COLLATE utf8_bin NOT NULL,
  `forum_rules_link` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `forum_rules_bitfield` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `forum_rules_options` int(11) unsigned NOT NULL DEFAULT '7',
  `forum_rules_uid` varchar(8) COLLATE utf8_bin NOT NULL DEFAULT '',
  `forum_topics_per_page` tinyint(4) NOT NULL DEFAULT '0',
  `forum_type` tinyint(4) NOT NULL DEFAULT '0',
  `forum_status` tinyint(4) NOT NULL DEFAULT '0',
  `forum_posts` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `forum_topics` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `forum_topics_real` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `forum_last_post_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `forum_last_poster_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `forum_last_post_subject` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `forum_last_post_time` int(11) unsigned NOT NULL DEFAULT '0',
  `forum_last_poster_name` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `forum_last_poster_colour` varchar(6) COLLATE utf8_bin NOT NULL DEFAULT '',
  `forum_flags` tinyint(4) NOT NULL DEFAULT '32',
  `forum_options` int(20) unsigned NOT NULL DEFAULT '0',
  `display_subforum_list` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `display_on_index` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `enable_indexing` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `enable_icons` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `enable_prune` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `prune_next` int(11) unsigned NOT NULL DEFAULT '0',
  `prune_days` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `prune_viewed` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `prune_freq` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`forum_id`),
  KEY `left_right_id` (`left_id`,`right_id`),
  KEY `forum_lastpost_id` (`forum_last_post_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_forums`
--

LOCK TABLES `phpbb_forums` WRITE;
/*!40000 ALTER TABLE `phpbb_forums` DISABLE KEYS */;
INSERT INTO `phpbb_forums` VALUES (1,0,1,4,'','Your first category','','',7,'','','',0,'','','','',7,'',0,0,0,1,1,1,1,2,'',1403110152,'admin','AA0000',32,0,1,1,1,1,0,0,0,0,0),(2,1,2,3,'','Your first forum','Description of your first forum.','',7,'','','',0,'','','','',7,'',0,1,0,1,1,1,1,2,'Welcome to phpBB3',1403110152,'admin','AA0000',48,0,1,1,1,1,0,0,0,0,0);
/*!40000 ALTER TABLE `phpbb_forums` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_forums_access`
--

DROP TABLE IF EXISTS `phpbb_forums_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_forums_access` (
  `forum_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `session_id` char(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`forum_id`,`user_id`,`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_forums_access`
--

LOCK TABLES `phpbb_forums_access` WRITE;
/*!40000 ALTER TABLE `phpbb_forums_access` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_forums_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_forums_track`
--

DROP TABLE IF EXISTS `phpbb_forums_track`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_forums_track` (
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `forum_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `mark_time` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`forum_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_forums_track`
--

LOCK TABLES `phpbb_forums_track` WRITE;
/*!40000 ALTER TABLE `phpbb_forums_track` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_forums_track` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_forums_watch`
--

DROP TABLE IF EXISTS `phpbb_forums_watch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_forums_watch` (
  `forum_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `notify_status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  KEY `forum_id` (`forum_id`),
  KEY `user_id` (`user_id`),
  KEY `notify_stat` (`notify_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_forums_watch`
--

LOCK TABLES `phpbb_forums_watch` WRITE;
/*!40000 ALTER TABLE `phpbb_forums_watch` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_forums_watch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_groups`
--

DROP TABLE IF EXISTS `phpbb_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_groups` (
  `group_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `group_type` tinyint(4) NOT NULL DEFAULT '1',
  `group_founder_manage` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `group_skip_auth` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `group_name` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `group_desc` text COLLATE utf8_bin NOT NULL,
  `group_desc_bitfield` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `group_desc_options` int(11) unsigned NOT NULL DEFAULT '7',
  `group_desc_uid` varchar(8) COLLATE utf8_bin NOT NULL DEFAULT '',
  `group_display` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `group_avatar` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `group_avatar_type` tinyint(2) NOT NULL DEFAULT '0',
  `group_avatar_width` smallint(4) unsigned NOT NULL DEFAULT '0',
  `group_avatar_height` smallint(4) unsigned NOT NULL DEFAULT '0',
  `group_rank` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `group_colour` varchar(6) COLLATE utf8_bin NOT NULL DEFAULT '',
  `group_sig_chars` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `group_receive_pm` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `group_message_limit` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `group_max_recipients` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `group_legend` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`group_id`),
  KEY `group_legend_name` (`group_legend`,`group_name`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_groups`
--

LOCK TABLES `phpbb_groups` WRITE;
/*!40000 ALTER TABLE `phpbb_groups` DISABLE KEYS */;
INSERT INTO `phpbb_groups` VALUES (1,3,0,0,'GUESTS','','',7,'',0,'',0,0,0,0,'',0,0,0,5,0),(2,3,0,0,'REGISTERED','','',7,'',0,'',0,0,0,0,'',0,0,0,5,0),(3,3,0,0,'REGISTERED_COPPA','','',7,'',0,'',0,0,0,0,'',0,0,0,5,0),(4,3,0,0,'GLOBAL_MODERATORS','','',7,'',0,'',0,0,0,0,'00AA00',0,0,0,0,1),(5,3,1,0,'ADMINISTRATORS','','',7,'',0,'',0,0,0,0,'AA0000',0,0,0,0,1),(6,3,0,0,'BOTS','','',7,'',0,'',0,0,0,0,'9E8DA7',0,0,0,5,0),(7,3,0,0,'NEWLY_REGISTERED','','',7,'',0,'',0,0,0,0,'',0,0,0,5,0);
/*!40000 ALTER TABLE `phpbb_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_icons`
--

DROP TABLE IF EXISTS `phpbb_icons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_icons` (
  `icons_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `icons_url` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `icons_width` tinyint(4) NOT NULL DEFAULT '0',
  `icons_height` tinyint(4) NOT NULL DEFAULT '0',
  `icons_order` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `display_on_posting` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`icons_id`),
  KEY `display_on_posting` (`display_on_posting`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_icons`
--

LOCK TABLES `phpbb_icons` WRITE;
/*!40000 ALTER TABLE `phpbb_icons` DISABLE KEYS */;
INSERT INTO `phpbb_icons` VALUES (1,'misc/fire.gif',16,16,1,1),(2,'smile/redface.gif',16,16,9,1),(3,'smile/mrgreen.gif',16,16,10,1),(4,'misc/heart.gif',16,16,4,1),(5,'misc/star.gif',16,16,2,1),(6,'misc/radioactive.gif',16,16,3,1),(7,'misc/thinking.gif',16,16,5,1),(8,'smile/info.gif',16,16,8,1),(9,'smile/question.gif',16,16,6,1),(10,'smile/alert.gif',16,16,7,1);
/*!40000 ALTER TABLE `phpbb_icons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_lang`
--

DROP TABLE IF EXISTS `phpbb_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_lang` (
  `lang_id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `lang_iso` varchar(30) COLLATE utf8_bin NOT NULL DEFAULT '',
  `lang_dir` varchar(30) COLLATE utf8_bin NOT NULL DEFAULT '',
  `lang_english_name` varchar(100) COLLATE utf8_bin NOT NULL DEFAULT '',
  `lang_local_name` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `lang_author` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`lang_id`),
  KEY `lang_iso` (`lang_iso`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_lang`
--

LOCK TABLES `phpbb_lang` WRITE;
/*!40000 ALTER TABLE `phpbb_lang` DISABLE KEYS */;
INSERT INTO `phpbb_lang` VALUES (1,'en','en','British English','British English','phpBB Group');
/*!40000 ALTER TABLE `phpbb_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_log`
--

DROP TABLE IF EXISTS `phpbb_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_log` (
  `log_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `log_type` tinyint(4) NOT NULL DEFAULT '0',
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `forum_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `topic_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `reportee_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `log_ip` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '',
  `log_time` int(11) unsigned NOT NULL DEFAULT '0',
  `log_operation` text COLLATE utf8_bin NOT NULL,
  `log_data` mediumtext COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`log_id`),
  KEY `log_type` (`log_type`),
  KEY `forum_id` (`forum_id`),
  KEY `topic_id` (`topic_id`),
  KEY `reportee_id` (`reportee_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_log`
--

LOCK TABLES `phpbb_log` WRITE;
/*!40000 ALTER TABLE `phpbb_log` DISABLE KEYS */;
INSERT INTO `phpbb_log` VALUES (1,2,2,0,0,0,'::1',1403110234,'LOG_ERROR_EMAIL','a:1:{i:0;s:360:\"<strong>EMAIL/PHP/mail()</strong><br /><em>/forum/install/index.php</em><br /><br />Errno 2: mail() [<a href=\'function.mail\'>function.mail</a>]: Failed to connect to mailserver at &quot;localhost&quot; port 25, verify your &quot;SMTP&quot; and &quot;smtp_port&quot; setting in php.ini or use ini_set() at [ROOT]/includes/functions_messenger.php line 1680<br />\";}'),(2,0,2,0,0,0,'::1',1403110235,'LOG_INSTALL_INSTALLED','a:1:{i:0;s:6:\"3.0.12\";}'),(3,0,2,0,0,0,'::1',1403110494,'LOG_TEMPLATE_ADD_FS','a:1:{i:0;s:10:\"Metro Blue\";}'),(4,0,2,0,0,0,'::1',1403110495,'LOG_THEME_ADD_DB','a:1:{i:0;s:10:\"Metro Blue\";}'),(5,0,2,0,0,0,'::1',1403110496,'LOG_IMAGESET_ADD_FS','a:1:{i:0;s:10:\"Metro Blue\";}'),(6,0,2,0,0,0,'::1',1403110496,'LOG_STYLE_ADD','a:1:{i:0;s:10:\"Metro Blue\";}');
/*!40000 ALTER TABLE `phpbb_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_login_attempts`
--

DROP TABLE IF EXISTS `phpbb_login_attempts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_login_attempts` (
  `attempt_ip` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '',
  `attempt_browser` varchar(150) COLLATE utf8_bin NOT NULL DEFAULT '',
  `attempt_forwarded_for` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `attempt_time` int(11) unsigned NOT NULL DEFAULT '0',
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `username_clean` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '0',
  KEY `att_ip` (`attempt_ip`,`attempt_time`),
  KEY `att_for` (`attempt_forwarded_for`,`attempt_time`),
  KEY `att_time` (`attempt_time`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_login_attempts`
--

LOCK TABLES `phpbb_login_attempts` WRITE;
/*!40000 ALTER TABLE `phpbb_login_attempts` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_login_attempts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_moderator_cache`
--

DROP TABLE IF EXISTS `phpbb_moderator_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_moderator_cache` (
  `forum_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `username` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `group_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `group_name` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `display_on_index` tinyint(1) unsigned NOT NULL DEFAULT '1',
  KEY `disp_idx` (`display_on_index`),
  KEY `forum_id` (`forum_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_moderator_cache`
--

LOCK TABLES `phpbb_moderator_cache` WRITE;
/*!40000 ALTER TABLE `phpbb_moderator_cache` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_moderator_cache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_modules`
--

DROP TABLE IF EXISTS `phpbb_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_modules` (
  `module_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `module_enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `module_display` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `module_basename` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `module_class` varchar(10) COLLATE utf8_bin NOT NULL DEFAULT '',
  `parent_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `left_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `right_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `module_langname` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `module_mode` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `module_auth` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`module_id`),
  KEY `left_right_id` (`left_id`,`right_id`),
  KEY `module_enabled` (`module_enabled`),
  KEY `class_left_id` (`module_class`,`left_id`)
) ENGINE=InnoDB AUTO_INCREMENT=199 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_modules`
--

LOCK TABLES `phpbb_modules` WRITE;
/*!40000 ALTER TABLE `phpbb_modules` DISABLE KEYS */;
INSERT INTO `phpbb_modules` VALUES (1,1,1,'','acp',0,1,64,'ACP_CAT_GENERAL','',''),(2,1,1,'','acp',1,4,17,'ACP_QUICK_ACCESS','',''),(3,1,1,'','acp',1,18,41,'ACP_BOARD_CONFIGURATION','',''),(4,1,1,'','acp',1,42,49,'ACP_CLIENT_COMMUNICATION','',''),(5,1,1,'','acp',1,50,63,'ACP_SERVER_CONFIGURATION','',''),(6,1,1,'','acp',0,65,84,'ACP_CAT_FORUMS','',''),(7,1,1,'','acp',6,66,71,'ACP_MANAGE_FORUMS','',''),(8,1,1,'','acp',6,72,83,'ACP_FORUM_BASED_PERMISSIONS','',''),(9,1,1,'','acp',0,85,110,'ACP_CAT_POSTING','',''),(10,1,1,'','acp',9,86,99,'ACP_MESSAGES','',''),(11,1,1,'','acp',9,100,109,'ACP_ATTACHMENTS','',''),(12,1,1,'','acp',0,111,166,'ACP_CAT_USERGROUP','',''),(13,1,1,'','acp',12,112,145,'ACP_CAT_USERS','',''),(14,1,1,'','acp',12,146,153,'ACP_GROUPS','',''),(15,1,1,'','acp',12,154,165,'ACP_USER_SECURITY','',''),(16,1,1,'','acp',0,167,216,'ACP_CAT_PERMISSIONS','',''),(17,1,1,'','acp',16,170,179,'ACP_GLOBAL_PERMISSIONS','',''),(18,1,1,'','acp',16,180,191,'ACP_FORUM_BASED_PERMISSIONS','',''),(19,1,1,'','acp',16,192,201,'ACP_PERMISSION_ROLES','',''),(20,1,1,'','acp',16,202,215,'ACP_PERMISSION_MASKS','',''),(21,1,1,'','acp',0,217,230,'ACP_CAT_STYLES','',''),(22,1,1,'','acp',21,218,221,'ACP_STYLE_MANAGEMENT','',''),(23,1,1,'','acp',21,222,229,'ACP_STYLE_COMPONENTS','',''),(24,1,1,'','acp',0,231,250,'ACP_CAT_MAINTENANCE','',''),(25,1,1,'','acp',24,232,241,'ACP_FORUM_LOGS','',''),(26,1,1,'','acp',24,242,249,'ACP_CAT_DATABASE','',''),(27,1,1,'','acp',0,251,276,'ACP_CAT_SYSTEM','',''),(28,1,1,'','acp',27,252,255,'ACP_AUTOMATION','',''),(29,1,1,'','acp',27,256,267,'ACP_GENERAL_TASKS','',''),(30,1,1,'','acp',27,268,275,'ACP_MODULE_MANAGEMENT','',''),(31,1,1,'','acp',0,277,278,'ACP_CAT_DOT_MODS','',''),(32,1,1,'attachments','acp',3,19,20,'ACP_ATTACHMENT_SETTINGS','attach','acl_a_attach'),(33,1,1,'attachments','acp',11,101,102,'ACP_ATTACHMENT_SETTINGS','attach','acl_a_attach'),(34,1,1,'attachments','acp',11,103,104,'ACP_MANAGE_EXTENSIONS','extensions','acl_a_attach'),(35,1,1,'attachments','acp',11,105,106,'ACP_EXTENSION_GROUPS','ext_groups','acl_a_attach'),(36,1,1,'attachments','acp',11,107,108,'ACP_ORPHAN_ATTACHMENTS','orphan','acl_a_attach'),(37,1,1,'ban','acp',15,155,156,'ACP_BAN_EMAILS','email','acl_a_ban'),(38,1,1,'ban','acp',15,157,158,'ACP_BAN_IPS','ip','acl_a_ban'),(39,1,1,'ban','acp',15,159,160,'ACP_BAN_USERNAMES','user','acl_a_ban'),(40,1,1,'bbcodes','acp',10,87,88,'ACP_BBCODES','bbcodes','acl_a_bbcode'),(41,1,1,'board','acp',3,21,22,'ACP_BOARD_SETTINGS','settings','acl_a_board'),(42,1,1,'board','acp',3,23,24,'ACP_BOARD_FEATURES','features','acl_a_board'),(43,1,1,'board','acp',3,25,26,'ACP_AVATAR_SETTINGS','avatar','acl_a_board'),(44,1,1,'board','acp',3,27,28,'ACP_MESSAGE_SETTINGS','message','acl_a_board'),(45,1,1,'board','acp',10,89,90,'ACP_MESSAGE_SETTINGS','message','acl_a_board'),(46,1,1,'board','acp',3,29,30,'ACP_POST_SETTINGS','post','acl_a_board'),(47,1,1,'board','acp',10,91,92,'ACP_POST_SETTINGS','post','acl_a_board'),(48,1,1,'board','acp',3,31,32,'ACP_SIGNATURE_SETTINGS','signature','acl_a_board'),(49,1,1,'board','acp',3,33,34,'ACP_FEED_SETTINGS','feed','acl_a_board'),(50,1,1,'board','acp',3,35,36,'ACP_REGISTER_SETTINGS','registration','acl_a_board'),(51,1,1,'board','acp',4,43,44,'ACP_AUTH_SETTINGS','auth','acl_a_server'),(52,1,1,'board','acp',4,45,46,'ACP_EMAIL_SETTINGS','email','acl_a_server'),(53,1,1,'board','acp',5,51,52,'ACP_COOKIE_SETTINGS','cookie','acl_a_server'),(54,1,1,'board','acp',5,53,54,'ACP_SERVER_SETTINGS','server','acl_a_server'),(55,1,1,'board','acp',5,55,56,'ACP_SECURITY_SETTINGS','security','acl_a_server'),(56,1,1,'board','acp',5,57,58,'ACP_LOAD_SETTINGS','load','acl_a_server'),(57,1,1,'bots','acp',29,257,258,'ACP_BOTS','bots','acl_a_bots'),(58,1,1,'captcha','acp',3,37,38,'ACP_VC_SETTINGS','visual','acl_a_board'),(59,1,0,'captcha','acp',3,39,40,'ACP_VC_CAPTCHA_DISPLAY','img','acl_a_board'),(60,1,1,'database','acp',26,243,244,'ACP_BACKUP','backup','acl_a_backup'),(61,1,1,'database','acp',26,245,246,'ACP_RESTORE','restore','acl_a_backup'),(62,1,1,'disallow','acp',15,161,162,'ACP_DISALLOW_USERNAMES','usernames','acl_a_names'),(63,1,1,'email','acp',29,259,260,'ACP_MASS_EMAIL','email','acl_a_email && cfg_email_enable'),(64,1,1,'forums','acp',7,67,68,'ACP_MANAGE_FORUMS','manage','acl_a_forum'),(65,1,1,'groups','acp',14,147,148,'ACP_GROUPS_MANAGE','manage','acl_a_group'),(66,1,1,'icons','acp',10,93,94,'ACP_ICONS','icons','acl_a_icons'),(67,1,1,'icons','acp',10,95,96,'ACP_SMILIES','smilies','acl_a_icons'),(68,1,1,'inactive','acp',13,115,116,'ACP_INACTIVE_USERS','list','acl_a_user'),(69,1,1,'jabber','acp',4,47,48,'ACP_JABBER_SETTINGS','settings','acl_a_jabber'),(70,1,1,'language','acp',29,261,262,'ACP_LANGUAGE_PACKS','lang_packs','acl_a_language'),(71,1,1,'logs','acp',25,233,234,'ACP_ADMIN_LOGS','admin','acl_a_viewlogs'),(72,1,1,'logs','acp',25,235,236,'ACP_MOD_LOGS','mod','acl_a_viewlogs'),(73,1,1,'logs','acp',25,237,238,'ACP_USERS_LOGS','users','acl_a_viewlogs'),(74,1,1,'logs','acp',25,239,240,'ACP_CRITICAL_LOGS','critical','acl_a_viewlogs'),(75,1,1,'main','acp',1,2,3,'ACP_INDEX','main',''),(76,1,1,'modules','acp',30,269,270,'ACP','acp','acl_a_modules'),(77,1,1,'modules','acp',30,271,272,'UCP','ucp','acl_a_modules'),(78,1,1,'modules','acp',30,273,274,'MCP','mcp','acl_a_modules'),(79,1,1,'permission_roles','acp',19,193,194,'ACP_ADMIN_ROLES','admin_roles','acl_a_roles && acl_a_aauth'),(80,1,1,'permission_roles','acp',19,195,196,'ACP_USER_ROLES','user_roles','acl_a_roles && acl_a_uauth'),(81,1,1,'permission_roles','acp',19,197,198,'ACP_MOD_ROLES','mod_roles','acl_a_roles && acl_a_mauth'),(82,1,1,'permission_roles','acp',19,199,200,'ACP_FORUM_ROLES','forum_roles','acl_a_roles && acl_a_fauth'),(83,1,1,'permissions','acp',16,168,169,'ACP_PERMISSIONS','intro','acl_a_authusers || acl_a_authgroups || acl_a_viewauth'),(84,1,0,'permissions','acp',20,203,204,'ACP_PERMISSION_TRACE','trace','acl_a_viewauth'),(85,1,1,'permissions','acp',18,181,182,'ACP_FORUM_PERMISSIONS','setting_forum_local','acl_a_fauth && (acl_a_authusers || acl_a_authgroups)'),(86,1,1,'permissions','acp',18,183,184,'ACP_FORUM_PERMISSIONS_COPY','setting_forum_copy','acl_a_fauth && acl_a_authusers && acl_a_authgroups && acl_a_mauth'),(87,1,1,'permissions','acp',18,185,186,'ACP_FORUM_MODERATORS','setting_mod_local','acl_a_mauth && (acl_a_authusers || acl_a_authgroups)'),(88,1,1,'permissions','acp',17,171,172,'ACP_USERS_PERMISSIONS','setting_user_global','acl_a_authusers && (acl_a_aauth || acl_a_mauth || acl_a_uauth)'),(89,1,1,'permissions','acp',13,117,118,'ACP_USERS_PERMISSIONS','setting_user_global','acl_a_authusers && (acl_a_aauth || acl_a_mauth || acl_a_uauth)'),(90,1,1,'permissions','acp',18,187,188,'ACP_USERS_FORUM_PERMISSIONS','setting_user_local','acl_a_authusers && (acl_a_mauth || acl_a_fauth)'),(91,1,1,'permissions','acp',13,119,120,'ACP_USERS_FORUM_PERMISSIONS','setting_user_local','acl_a_authusers && (acl_a_mauth || acl_a_fauth)'),(92,1,1,'permissions','acp',17,173,174,'ACP_GROUPS_PERMISSIONS','setting_group_global','acl_a_authgroups && (acl_a_aauth || acl_a_mauth || acl_a_uauth)'),(93,1,1,'permissions','acp',14,149,150,'ACP_GROUPS_PERMISSIONS','setting_group_global','acl_a_authgroups && (acl_a_aauth || acl_a_mauth || acl_a_uauth)'),(94,1,1,'permissions','acp',18,189,190,'ACP_GROUPS_FORUM_PERMISSIONS','setting_group_local','acl_a_authgroups && (acl_a_mauth || acl_a_fauth)'),(95,1,1,'permissions','acp',14,151,152,'ACP_GROUPS_FORUM_PERMISSIONS','setting_group_local','acl_a_authgroups && (acl_a_mauth || acl_a_fauth)'),(96,1,1,'permissions','acp',17,175,176,'ACP_ADMINISTRATORS','setting_admin_global','acl_a_aauth && (acl_a_authusers || acl_a_authgroups)'),(97,1,1,'permissions','acp',17,177,178,'ACP_GLOBAL_MODERATORS','setting_mod_global','acl_a_mauth && (acl_a_authusers || acl_a_authgroups)'),(98,1,1,'permissions','acp',20,205,206,'ACP_VIEW_ADMIN_PERMISSIONS','view_admin_global','acl_a_viewauth'),(99,1,1,'permissions','acp',20,207,208,'ACP_VIEW_USER_PERMISSIONS','view_user_global','acl_a_viewauth'),(100,1,1,'permissions','acp',20,209,210,'ACP_VIEW_GLOBAL_MOD_PERMISSIONS','view_mod_global','acl_a_viewauth'),(101,1,1,'permissions','acp',20,211,212,'ACP_VIEW_FORUM_MOD_PERMISSIONS','view_mod_local','acl_a_viewauth'),(102,1,1,'permissions','acp',20,213,214,'ACP_VIEW_FORUM_PERMISSIONS','view_forum_local','acl_a_viewauth'),(103,1,1,'php_info','acp',29,263,264,'ACP_PHP_INFO','info','acl_a_phpinfo'),(104,1,1,'profile','acp',13,121,122,'ACP_CUSTOM_PROFILE_FIELDS','profile','acl_a_profile'),(105,1,1,'prune','acp',7,69,70,'ACP_PRUNE_FORUMS','forums','acl_a_prune'),(106,1,1,'prune','acp',15,163,164,'ACP_PRUNE_USERS','users','acl_a_userdel'),(107,1,1,'ranks','acp',13,123,124,'ACP_MANAGE_RANKS','ranks','acl_a_ranks'),(108,1,1,'reasons','acp',29,265,266,'ACP_MANAGE_REASONS','main','acl_a_reasons'),(109,1,1,'search','acp',5,59,60,'ACP_SEARCH_SETTINGS','settings','acl_a_search'),(110,1,1,'search','acp',26,247,248,'ACP_SEARCH_INDEX','index','acl_a_search'),(111,1,1,'send_statistics','acp',5,61,62,'ACP_SEND_STATISTICS','send_statistics','acl_a_server'),(112,1,1,'styles','acp',22,219,220,'ACP_STYLES','style','acl_a_styles'),(113,1,1,'styles','acp',23,223,224,'ACP_TEMPLATES','template','acl_a_styles'),(114,1,1,'styles','acp',23,225,226,'ACP_THEMES','theme','acl_a_styles'),(115,1,1,'styles','acp',23,227,228,'ACP_IMAGESETS','imageset','acl_a_styles'),(116,1,1,'update','acp',28,253,254,'ACP_VERSION_CHECK','version_check','acl_a_board'),(117,1,1,'users','acp',13,113,114,'ACP_MANAGE_USERS','overview','acl_a_user'),(118,1,0,'users','acp',13,125,126,'ACP_USER_FEEDBACK','feedback','acl_a_user'),(119,1,0,'users','acp',13,127,128,'ACP_USER_WARNINGS','warnings','acl_a_user'),(120,1,0,'users','acp',13,129,130,'ACP_USER_PROFILE','profile','acl_a_user'),(121,1,0,'users','acp',13,131,132,'ACP_USER_PREFS','prefs','acl_a_user'),(122,1,0,'users','acp',13,133,134,'ACP_USER_AVATAR','avatar','acl_a_user'),(123,1,0,'users','acp',13,135,136,'ACP_USER_RANK','rank','acl_a_user'),(124,1,0,'users','acp',13,137,138,'ACP_USER_SIG','sig','acl_a_user'),(125,1,0,'users','acp',13,139,140,'ACP_USER_GROUPS','groups','acl_a_user && acl_a_group'),(126,1,0,'users','acp',13,141,142,'ACP_USER_PERM','perm','acl_a_user && acl_a_viewauth'),(127,1,0,'users','acp',13,143,144,'ACP_USER_ATTACH','attach','acl_a_user'),(128,1,1,'words','acp',10,97,98,'ACP_WORDS','words','acl_a_words'),(129,1,1,'users','acp',2,5,6,'ACP_MANAGE_USERS','overview','acl_a_user'),(130,1,1,'groups','acp',2,7,8,'ACP_GROUPS_MANAGE','manage','acl_a_group'),(131,1,1,'forums','acp',2,9,10,'ACP_MANAGE_FORUMS','manage','acl_a_forum'),(132,1,1,'logs','acp',2,11,12,'ACP_MOD_LOGS','mod','acl_a_viewlogs'),(133,1,1,'bots','acp',2,13,14,'ACP_BOTS','bots','acl_a_bots'),(134,1,1,'php_info','acp',2,15,16,'ACP_PHP_INFO','info','acl_a_phpinfo'),(135,1,1,'permissions','acp',8,73,74,'ACP_FORUM_PERMISSIONS','setting_forum_local','acl_a_fauth && (acl_a_authusers || acl_a_authgroups)'),(136,1,1,'permissions','acp',8,75,76,'ACP_FORUM_PERMISSIONS_COPY','setting_forum_copy','acl_a_fauth && acl_a_authusers && acl_a_authgroups && acl_a_mauth'),(137,1,1,'permissions','acp',8,77,78,'ACP_FORUM_MODERATORS','setting_mod_local','acl_a_mauth && (acl_a_authusers || acl_a_authgroups)'),(138,1,1,'permissions','acp',8,79,80,'ACP_USERS_FORUM_PERMISSIONS','setting_user_local','acl_a_authusers && (acl_a_mauth || acl_a_fauth)'),(139,1,1,'permissions','acp',8,81,82,'ACP_GROUPS_FORUM_PERMISSIONS','setting_group_local','acl_a_authgroups && (acl_a_mauth || acl_a_fauth)'),(140,1,1,'','mcp',0,1,10,'MCP_MAIN','',''),(141,1,1,'','mcp',0,11,18,'MCP_QUEUE','',''),(142,1,1,'','mcp',0,19,32,'MCP_REPORTS','',''),(143,1,1,'','mcp',0,33,38,'MCP_NOTES','',''),(144,1,1,'','mcp',0,39,48,'MCP_WARN','',''),(145,1,1,'','mcp',0,49,56,'MCP_LOGS','',''),(146,1,1,'','mcp',0,57,64,'MCP_BAN','',''),(147,1,1,'ban','mcp',146,58,59,'MCP_BAN_USERNAMES','user','acl_m_ban'),(148,1,1,'ban','mcp',146,60,61,'MCP_BAN_IPS','ip','acl_m_ban'),(149,1,1,'ban','mcp',146,62,63,'MCP_BAN_EMAILS','email','acl_m_ban'),(150,1,1,'logs','mcp',145,50,51,'MCP_LOGS_FRONT','front','acl_m_ || aclf_m_'),(151,1,1,'logs','mcp',145,52,53,'MCP_LOGS_FORUM_VIEW','forum_logs','acl_m_,$id'),(152,1,1,'logs','mcp',145,54,55,'MCP_LOGS_TOPIC_VIEW','topic_logs','acl_m_,$id'),(153,1,1,'main','mcp',140,2,3,'MCP_MAIN_FRONT','front',''),(154,1,1,'main','mcp',140,4,5,'MCP_MAIN_FORUM_VIEW','forum_view','acl_m_,$id'),(155,1,1,'main','mcp',140,6,7,'MCP_MAIN_TOPIC_VIEW','topic_view','acl_m_,$id'),(156,1,1,'main','mcp',140,8,9,'MCP_MAIN_POST_DETAILS','post_details','acl_m_,$id || (!$id && aclf_m_)'),(157,1,1,'notes','mcp',143,34,35,'MCP_NOTES_FRONT','front',''),(158,1,1,'notes','mcp',143,36,37,'MCP_NOTES_USER','user_notes',''),(159,1,1,'pm_reports','mcp',142,20,21,'MCP_PM_REPORTS_OPEN','pm_reports','aclf_m_report'),(160,1,1,'pm_reports','mcp',142,22,23,'MCP_PM_REPORTS_CLOSED','pm_reports_closed','aclf_m_report'),(161,1,1,'pm_reports','mcp',142,24,25,'MCP_PM_REPORT_DETAILS','pm_report_details','aclf_m_report'),(162,1,1,'queue','mcp',141,12,13,'MCP_QUEUE_UNAPPROVED_TOPICS','unapproved_topics','aclf_m_approve'),(163,1,1,'queue','mcp',141,14,15,'MCP_QUEUE_UNAPPROVED_POSTS','unapproved_posts','aclf_m_approve'),(164,1,1,'queue','mcp',141,16,17,'MCP_QUEUE_APPROVE_DETAILS','approve_details','acl_m_approve,$id || (!$id && aclf_m_approve)'),(165,1,1,'reports','mcp',142,26,27,'MCP_REPORTS_OPEN','reports','aclf_m_report'),(166,1,1,'reports','mcp',142,28,29,'MCP_REPORTS_CLOSED','reports_closed','aclf_m_report'),(167,1,1,'reports','mcp',142,30,31,'MCP_REPORT_DETAILS','report_details','acl_m_report,$id || (!$id && aclf_m_report)'),(168,1,1,'warn','mcp',144,40,41,'MCP_WARN_FRONT','front','aclf_m_warn'),(169,1,1,'warn','mcp',144,42,43,'MCP_WARN_LIST','list','aclf_m_warn'),(170,1,1,'warn','mcp',144,44,45,'MCP_WARN_USER','warn_user','aclf_m_warn'),(171,1,1,'warn','mcp',144,46,47,'MCP_WARN_POST','warn_post','acl_m_warn && acl_f_read,$id'),(172,1,1,'','ucp',0,1,12,'UCP_MAIN','',''),(173,1,1,'','ucp',0,13,22,'UCP_PROFILE','',''),(174,1,1,'','ucp',0,23,30,'UCP_PREFS','',''),(175,1,1,'','ucp',0,31,42,'UCP_PM','',''),(176,1,1,'','ucp',0,43,48,'UCP_USERGROUPS','',''),(177,1,1,'','ucp',0,49,54,'UCP_ZEBRA','',''),(178,1,1,'attachments','ucp',172,10,11,'UCP_MAIN_ATTACHMENTS','attachments','acl_u_attach'),(179,1,1,'groups','ucp',176,44,45,'UCP_USERGROUPS_MEMBER','membership',''),(180,1,1,'groups','ucp',176,46,47,'UCP_USERGROUPS_MANAGE','manage',''),(181,1,1,'main','ucp',172,2,3,'UCP_MAIN_FRONT','front',''),(182,1,1,'main','ucp',172,4,5,'UCP_MAIN_SUBSCRIBED','subscribed',''),(183,1,1,'main','ucp',172,6,7,'UCP_MAIN_BOOKMARKS','bookmarks','cfg_allow_bookmarks'),(184,1,1,'main','ucp',172,8,9,'UCP_MAIN_DRAFTS','drafts',''),(185,1,0,'pm','ucp',175,32,33,'UCP_PM_VIEW','view','cfg_allow_privmsg'),(186,1,1,'pm','ucp',175,34,35,'UCP_PM_COMPOSE','compose','cfg_allow_privmsg'),(187,1,1,'pm','ucp',175,36,37,'UCP_PM_DRAFTS','drafts','cfg_allow_privmsg'),(188,1,1,'pm','ucp',175,38,39,'UCP_PM_OPTIONS','options','cfg_allow_privmsg'),(189,1,0,'pm','ucp',175,40,41,'UCP_PM_POPUP_TITLE','popup','cfg_allow_privmsg'),(190,1,1,'prefs','ucp',174,24,25,'UCP_PREFS_PERSONAL','personal',''),(191,1,1,'prefs','ucp',174,26,27,'UCP_PREFS_POST','post',''),(192,1,1,'prefs','ucp',174,28,29,'UCP_PREFS_VIEW','view',''),(193,1,1,'profile','ucp',173,14,15,'UCP_PROFILE_PROFILE_INFO','profile_info',''),(194,1,1,'profile','ucp',173,16,17,'UCP_PROFILE_SIGNATURE','signature','acl_u_sig'),(195,1,1,'profile','ucp',173,18,19,'UCP_PROFILE_AVATAR','avatar','cfg_allow_avatar && (cfg_allow_avatar_local || cfg_allow_avatar_remote || cfg_allow_avatar_upload || cfg_allow_avatar_remote_upload)'),(196,1,1,'profile','ucp',173,20,21,'UCP_PROFILE_REG_DETAILS','reg_details',''),(197,1,1,'zebra','ucp',177,50,51,'UCP_ZEBRA_FRIENDS','friends',''),(198,1,1,'zebra','ucp',177,52,53,'UCP_ZEBRA_FOES','foes','');
/*!40000 ALTER TABLE `phpbb_modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_poll_options`
--

DROP TABLE IF EXISTS `phpbb_poll_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_poll_options` (
  `poll_option_id` tinyint(4) NOT NULL DEFAULT '0',
  `topic_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `poll_option_text` text COLLATE utf8_bin NOT NULL,
  `poll_option_total` mediumint(8) unsigned NOT NULL DEFAULT '0',
  KEY `poll_opt_id` (`poll_option_id`),
  KEY `topic_id` (`topic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_poll_options`
--

LOCK TABLES `phpbb_poll_options` WRITE;
/*!40000 ALTER TABLE `phpbb_poll_options` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_poll_options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_poll_votes`
--

DROP TABLE IF EXISTS `phpbb_poll_votes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_poll_votes` (
  `topic_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `poll_option_id` tinyint(4) NOT NULL DEFAULT '0',
  `vote_user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `vote_user_ip` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '',
  KEY `topic_id` (`topic_id`),
  KEY `vote_user_id` (`vote_user_id`),
  KEY `vote_user_ip` (`vote_user_ip`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_poll_votes`
--

LOCK TABLES `phpbb_poll_votes` WRITE;
/*!40000 ALTER TABLE `phpbb_poll_votes` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_poll_votes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_posts`
--

DROP TABLE IF EXISTS `phpbb_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_posts` (
  `post_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `topic_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `forum_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `poster_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `icon_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `poster_ip` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '',
  `post_time` int(11) unsigned NOT NULL DEFAULT '0',
  `post_approved` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `post_reported` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `enable_bbcode` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `enable_smilies` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `enable_magic_url` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `enable_sig` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `post_username` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `post_subject` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `post_text` mediumtext COLLATE utf8_bin NOT NULL,
  `post_checksum` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `post_attachment` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `bbcode_bitfield` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `bbcode_uid` varchar(8) COLLATE utf8_bin NOT NULL DEFAULT '',
  `post_postcount` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `post_edit_time` int(11) unsigned NOT NULL DEFAULT '0',
  `post_edit_reason` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `post_edit_user` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `post_edit_count` smallint(4) unsigned NOT NULL DEFAULT '0',
  `post_edit_locked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`post_id`),
  KEY `forum_id` (`forum_id`),
  KEY `topic_id` (`topic_id`),
  KEY `poster_ip` (`poster_ip`),
  KEY `poster_id` (`poster_id`),
  KEY `post_approved` (`post_approved`),
  KEY `post_username` (`post_username`),
  KEY `tid_post_time` (`topic_id`,`post_time`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_posts`
--

LOCK TABLES `phpbb_posts` WRITE;
/*!40000 ALTER TABLE `phpbb_posts` DISABLE KEYS */;
INSERT INTO `phpbb_posts` VALUES (1,1,2,2,0,'::1',1403110152,1,0,1,1,1,1,'','Welcome to phpBB3','This is an example post in your phpBB3 installation. Everything seems to be working. You may delete this post if you like and continue to set up your board. During the installation process your first category and your first forum are assigned an appropriate set of permissions for the predefined usergroups administrators, bots, global moderators, guests, registered users and registered COPPA users. If you also choose to delete your first category and your first forum, do not forget to assign permissions for all these usergroups for all new categories and forums you create. It is recommended to rename your first category and your first forum and copy permissions from these while creating new categories and forums. Have fun!','5dd683b17f641daf84c040bfefc58ce9',0,'','',1,0,'',0,0,0);
/*!40000 ALTER TABLE `phpbb_posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_privmsgs`
--

DROP TABLE IF EXISTS `phpbb_privmsgs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_privmsgs` (
  `msg_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `root_level` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `author_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `icon_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `author_ip` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '',
  `message_time` int(11) unsigned NOT NULL DEFAULT '0',
  `enable_bbcode` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `enable_smilies` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `enable_magic_url` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `enable_sig` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `message_subject` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `message_text` mediumtext COLLATE utf8_bin NOT NULL,
  `message_edit_reason` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `message_edit_user` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `message_attachment` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `bbcode_bitfield` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `bbcode_uid` varchar(8) COLLATE utf8_bin NOT NULL DEFAULT '',
  `message_edit_time` int(11) unsigned NOT NULL DEFAULT '0',
  `message_edit_count` smallint(4) unsigned NOT NULL DEFAULT '0',
  `to_address` text COLLATE utf8_bin NOT NULL,
  `bcc_address` text COLLATE utf8_bin NOT NULL,
  `message_reported` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`msg_id`),
  KEY `author_ip` (`author_ip`),
  KEY `message_time` (`message_time`),
  KEY `author_id` (`author_id`),
  KEY `root_level` (`root_level`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_privmsgs`
--

LOCK TABLES `phpbb_privmsgs` WRITE;
/*!40000 ALTER TABLE `phpbb_privmsgs` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_privmsgs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_privmsgs_folder`
--

DROP TABLE IF EXISTS `phpbb_privmsgs_folder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_privmsgs_folder` (
  `folder_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `folder_name` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `pm_count` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`folder_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_privmsgs_folder`
--

LOCK TABLES `phpbb_privmsgs_folder` WRITE;
/*!40000 ALTER TABLE `phpbb_privmsgs_folder` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_privmsgs_folder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_privmsgs_rules`
--

DROP TABLE IF EXISTS `phpbb_privmsgs_rules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_privmsgs_rules` (
  `rule_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `rule_check` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `rule_connection` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `rule_string` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `rule_user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `rule_group_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `rule_action` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `rule_folder_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`rule_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_privmsgs_rules`
--

LOCK TABLES `phpbb_privmsgs_rules` WRITE;
/*!40000 ALTER TABLE `phpbb_privmsgs_rules` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_privmsgs_rules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_privmsgs_to`
--

DROP TABLE IF EXISTS `phpbb_privmsgs_to`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_privmsgs_to` (
  `msg_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `author_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `pm_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `pm_new` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `pm_unread` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `pm_replied` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `pm_marked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `pm_forwarded` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `folder_id` int(11) NOT NULL DEFAULT '0',
  KEY `msg_id` (`msg_id`),
  KEY `author_id` (`author_id`),
  KEY `usr_flder_id` (`user_id`,`folder_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_privmsgs_to`
--

LOCK TABLES `phpbb_privmsgs_to` WRITE;
/*!40000 ALTER TABLE `phpbb_privmsgs_to` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_privmsgs_to` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_profile_fields`
--

DROP TABLE IF EXISTS `phpbb_profile_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_profile_fields` (
  `field_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `field_name` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `field_type` tinyint(4) NOT NULL DEFAULT '0',
  `field_ident` varchar(20) COLLATE utf8_bin NOT NULL DEFAULT '',
  `field_length` varchar(20) COLLATE utf8_bin NOT NULL DEFAULT '',
  `field_minlen` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `field_maxlen` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `field_novalue` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `field_default_value` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `field_validation` varchar(20) COLLATE utf8_bin NOT NULL DEFAULT '',
  `field_required` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `field_show_novalue` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `field_show_on_reg` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `field_show_on_vt` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `field_show_profile` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `field_hide` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `field_no_view` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `field_active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `field_order` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`field_id`),
  KEY `fld_type` (`field_type`),
  KEY `fld_ordr` (`field_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_profile_fields`
--

LOCK TABLES `phpbb_profile_fields` WRITE;
/*!40000 ALTER TABLE `phpbb_profile_fields` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_profile_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_profile_fields_data`
--

DROP TABLE IF EXISTS `phpbb_profile_fields_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_profile_fields_data` (
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_profile_fields_data`
--

LOCK TABLES `phpbb_profile_fields_data` WRITE;
/*!40000 ALTER TABLE `phpbb_profile_fields_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_profile_fields_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_profile_fields_lang`
--

DROP TABLE IF EXISTS `phpbb_profile_fields_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_profile_fields_lang` (
  `field_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `lang_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `option_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `field_type` tinyint(4) NOT NULL DEFAULT '0',
  `lang_value` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`field_id`,`lang_id`,`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_profile_fields_lang`
--

LOCK TABLES `phpbb_profile_fields_lang` WRITE;
/*!40000 ALTER TABLE `phpbb_profile_fields_lang` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_profile_fields_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_profile_lang`
--

DROP TABLE IF EXISTS `phpbb_profile_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_profile_lang` (
  `field_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `lang_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `lang_name` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `lang_explain` text COLLATE utf8_bin NOT NULL,
  `lang_default_value` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`field_id`,`lang_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_profile_lang`
--

LOCK TABLES `phpbb_profile_lang` WRITE;
/*!40000 ALTER TABLE `phpbb_profile_lang` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_profile_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_ranks`
--

DROP TABLE IF EXISTS `phpbb_ranks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_ranks` (
  `rank_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `rank_title` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `rank_min` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `rank_special` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `rank_image` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`rank_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_ranks`
--

LOCK TABLES `phpbb_ranks` WRITE;
/*!40000 ALTER TABLE `phpbb_ranks` DISABLE KEYS */;
INSERT INTO `phpbb_ranks` VALUES (1,'Site Admin',0,1,'');
/*!40000 ALTER TABLE `phpbb_ranks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_reports`
--

DROP TABLE IF EXISTS `phpbb_reports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_reports` (
  `report_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `reason_id` smallint(4) unsigned NOT NULL DEFAULT '0',
  `post_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `pm_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `user_notify` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `report_closed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `report_time` int(11) unsigned NOT NULL DEFAULT '0',
  `report_text` mediumtext COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`report_id`),
  KEY `post_id` (`post_id`),
  KEY `pm_id` (`pm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_reports`
--

LOCK TABLES `phpbb_reports` WRITE;
/*!40000 ALTER TABLE `phpbb_reports` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_reports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_reports_reasons`
--

DROP TABLE IF EXISTS `phpbb_reports_reasons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_reports_reasons` (
  `reason_id` smallint(4) unsigned NOT NULL AUTO_INCREMENT,
  `reason_title` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `reason_description` mediumtext COLLATE utf8_bin NOT NULL,
  `reason_order` smallint(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`reason_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_reports_reasons`
--

LOCK TABLES `phpbb_reports_reasons` WRITE;
/*!40000 ALTER TABLE `phpbb_reports_reasons` DISABLE KEYS */;
INSERT INTO `phpbb_reports_reasons` VALUES (1,'warez','The post contains links to illegal or pirated software.',1),(2,'spam','The reported post has the only purpose to advertise for a website or another product.',2),(3,'off_topic','The reported post is off topic.',3),(4,'other','The reported post does not fit into any other category, please use the further information field.',4);
/*!40000 ALTER TABLE `phpbb_reports_reasons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_search_results`
--

DROP TABLE IF EXISTS `phpbb_search_results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_search_results` (
  `search_key` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `search_time` int(11) unsigned NOT NULL DEFAULT '0',
  `search_keywords` mediumtext COLLATE utf8_bin NOT NULL,
  `search_authors` mediumtext COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`search_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_search_results`
--

LOCK TABLES `phpbb_search_results` WRITE;
/*!40000 ALTER TABLE `phpbb_search_results` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_search_results` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_search_wordlist`
--

DROP TABLE IF EXISTS `phpbb_search_wordlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_search_wordlist` (
  `word_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `word_text` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `word_common` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `word_count` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`word_id`),
  UNIQUE KEY `wrd_txt` (`word_text`),
  KEY `wrd_cnt` (`word_count`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_search_wordlist`
--

LOCK TABLES `phpbb_search_wordlist` WRITE;
/*!40000 ALTER TABLE `phpbb_search_wordlist` DISABLE KEYS */;
INSERT INTO `phpbb_search_wordlist` VALUES (1,'this',0,1),(2,'example',0,1),(3,'post',0,1),(4,'your',0,1),(5,'phpbb3',0,2),(6,'installation',0,1),(7,'everything',0,1),(8,'seems',0,1),(9,'working',0,1),(10,'you',0,1),(11,'may',0,1),(12,'delete',0,1),(13,'like',0,1),(14,'and',0,1),(15,'continue',0,1),(16,'set',0,1),(17,'board',0,1),(18,'during',0,1),(19,'the',0,1),(20,'process',0,1),(21,'first',0,1),(22,'category',0,1),(23,'forum',0,1),(24,'are',0,1),(25,'assigned',0,1),(26,'appropriate',0,1),(27,'permissions',0,1),(28,'for',0,1),(29,'predefined',0,1),(30,'usergroups',0,1),(31,'administrators',0,1),(32,'bots',0,1),(33,'global',0,1),(34,'moderators',0,1),(35,'guests',0,1),(36,'registered',0,1),(37,'users',0,1),(38,'coppa',0,1),(39,'also',0,1),(40,'choose',0,1),(41,'not',0,1),(42,'forget',0,1),(43,'assign',0,1),(44,'all',0,1),(45,'these',0,1),(46,'new',0,1),(47,'categories',0,1),(48,'forums',0,1),(49,'create',0,1),(50,'recommended',0,1),(51,'rename',0,1),(52,'copy',0,1),(53,'from',0,1),(54,'while',0,1),(55,'creating',0,1),(56,'have',0,1),(57,'fun',0,1),(58,'welcome',0,1);
/*!40000 ALTER TABLE `phpbb_search_wordlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_search_wordmatch`
--

DROP TABLE IF EXISTS `phpbb_search_wordmatch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_search_wordmatch` (
  `post_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `word_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `title_match` tinyint(1) unsigned NOT NULL DEFAULT '0',
  UNIQUE KEY `unq_mtch` (`word_id`,`post_id`,`title_match`),
  KEY `word_id` (`word_id`),
  KEY `post_id` (`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_search_wordmatch`
--

LOCK TABLES `phpbb_search_wordmatch` WRITE;
/*!40000 ALTER TABLE `phpbb_search_wordmatch` DISABLE KEYS */;
INSERT INTO `phpbb_search_wordmatch` VALUES (1,1,0),(1,2,0),(1,3,0),(1,4,0),(1,5,0),(1,5,1),(1,6,0),(1,7,0),(1,8,0),(1,9,0),(1,10,0),(1,11,0),(1,12,0),(1,13,0),(1,14,0),(1,15,0),(1,16,0),(1,17,0),(1,18,0),(1,19,0),(1,20,0),(1,21,0),(1,22,0),(1,23,0),(1,24,0),(1,25,0),(1,26,0),(1,27,0),(1,28,0),(1,29,0),(1,30,0),(1,31,0),(1,32,0),(1,33,0),(1,34,0),(1,35,0),(1,36,0),(1,37,0),(1,38,0),(1,39,0),(1,40,0),(1,41,0),(1,42,0),(1,43,0),(1,44,0),(1,45,0),(1,46,0),(1,47,0),(1,48,0),(1,49,0),(1,50,0),(1,51,0),(1,52,0),(1,53,0),(1,54,0),(1,55,0),(1,56,0),(1,57,0),(1,58,1);
/*!40000 ALTER TABLE `phpbb_search_wordmatch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_sessions`
--

DROP TABLE IF EXISTS `phpbb_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_sessions` (
  `session_id` char(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `session_user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `session_forum_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `session_last_visit` int(11) unsigned NOT NULL DEFAULT '0',
  `session_start` int(11) unsigned NOT NULL DEFAULT '0',
  `session_time` int(11) unsigned NOT NULL DEFAULT '0',
  `session_ip` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '',
  `session_browser` varchar(150) COLLATE utf8_bin NOT NULL DEFAULT '',
  `session_forwarded_for` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `session_page` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `session_viewonline` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `session_autologin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `session_admin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`session_id`),
  KEY `session_time` (`session_time`),
  KEY `session_user_id` (`session_user_id`),
  KEY `session_fid` (`session_forum_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_sessions`
--

LOCK TABLES `phpbb_sessions` WRITE;
/*!40000 ALTER TABLE `phpbb_sessions` DISABLE KEYS */;
INSERT INTO `phpbb_sessions` VALUES ('451639eabe6a268200c772ad842b99ce',1,0,1403110233,1403110233,1403110233,'::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36','','install/index.php?mode=install&sub=final',1,0,0),('a17afd31dcc499801810a261249dbe3b',2,0,1403110233,1403110233,1403110681,'::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36','','adm/index.php?i=6',1,0,1);
/*!40000 ALTER TABLE `phpbb_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_sessions_keys`
--

DROP TABLE IF EXISTS `phpbb_sessions_keys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_sessions_keys` (
  `key_id` char(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `last_ip` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '',
  `last_login` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`key_id`,`user_id`),
  KEY `last_login` (`last_login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_sessions_keys`
--

LOCK TABLES `phpbb_sessions_keys` WRITE;
/*!40000 ALTER TABLE `phpbb_sessions_keys` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_sessions_keys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_sitelist`
--

DROP TABLE IF EXISTS `phpbb_sitelist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_sitelist` (
  `site_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `site_ip` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '',
  `site_hostname` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `ip_exclude` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_sitelist`
--

LOCK TABLES `phpbb_sitelist` WRITE;
/*!40000 ALTER TABLE `phpbb_sitelist` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_sitelist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_smilies`
--

DROP TABLE IF EXISTS `phpbb_smilies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_smilies` (
  `smiley_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(50) COLLATE utf8_bin NOT NULL DEFAULT '',
  `emotion` varchar(50) COLLATE utf8_bin NOT NULL DEFAULT '',
  `smiley_url` varchar(50) COLLATE utf8_bin NOT NULL DEFAULT '',
  `smiley_width` smallint(4) unsigned NOT NULL DEFAULT '0',
  `smiley_height` smallint(4) unsigned NOT NULL DEFAULT '0',
  `smiley_order` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `display_on_posting` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`smiley_id`),
  KEY `display_on_post` (`display_on_posting`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_smilies`
--

LOCK TABLES `phpbb_smilies` WRITE;
/*!40000 ALTER TABLE `phpbb_smilies` DISABLE KEYS */;
INSERT INTO `phpbb_smilies` VALUES (1,':D','Very Happy','icon_e_biggrin.gif',15,17,1,1),(2,':-D','Very Happy','icon_e_biggrin.gif',15,17,2,1),(3,':grin:','Very Happy','icon_e_biggrin.gif',15,17,3,1),(4,':)','Smile','icon_e_smile.gif',15,17,4,1),(5,':-)','Smile','icon_e_smile.gif',15,17,5,1),(6,':smile:','Smile','icon_e_smile.gif',15,17,6,1),(7,';)','Wink','icon_e_wink.gif',15,17,7,1),(8,';-)','Wink','icon_e_wink.gif',15,17,8,1),(9,':wink:','Wink','icon_e_wink.gif',15,17,9,1),(10,':(','Sad','icon_e_sad.gif',15,17,10,1),(11,':-(','Sad','icon_e_sad.gif',15,17,11,1),(12,':sad:','Sad','icon_e_sad.gif',15,17,12,1),(13,':o','Surprised','icon_e_surprised.gif',15,17,13,1),(14,':-o','Surprised','icon_e_surprised.gif',15,17,14,1),(15,':eek:','Surprised','icon_e_surprised.gif',15,17,15,1),(16,':shock:','Shocked','icon_eek.gif',15,17,16,1),(17,':?','Confused','icon_e_confused.gif',15,17,17,1),(18,':-?','Confused','icon_e_confused.gif',15,17,18,1),(19,':???:','Confused','icon_e_confused.gif',15,17,19,1),(20,'8-)','Cool','icon_cool.gif',15,17,20,1),(21,':cool:','Cool','icon_cool.gif',15,17,21,1),(22,':lol:','Laughing','icon_lol.gif',15,17,22,1),(23,':x','Mad','icon_mad.gif',15,17,23,1),(24,':-x','Mad','icon_mad.gif',15,17,24,1),(25,':mad:','Mad','icon_mad.gif',15,17,25,1),(26,':P','Razz','icon_razz.gif',15,17,26,1),(27,':-P','Razz','icon_razz.gif',15,17,27,1),(28,':razz:','Razz','icon_razz.gif',15,17,28,1),(29,':oops:','Embarrassed','icon_redface.gif',15,17,29,1),(30,':cry:','Crying or Very Sad','icon_cry.gif',15,17,30,1),(31,':evil:','Evil or Very Mad','icon_evil.gif',15,17,31,1),(32,':twisted:','Twisted Evil','icon_twisted.gif',15,17,32,1),(33,':roll:','Rolling Eyes','icon_rolleyes.gif',15,17,33,1),(34,':!:','Exclamation','icon_exclaim.gif',15,17,34,1),(35,':?:','Question','icon_question.gif',15,17,35,1),(36,':idea:','Idea','icon_idea.gif',15,17,36,1),(37,':arrow:','Arrow','icon_arrow.gif',15,17,37,1),(38,':|','Neutral','icon_neutral.gif',15,17,38,1),(39,':-|','Neutral','icon_neutral.gif',15,17,39,1),(40,':mrgreen:','Mr. Green','icon_mrgreen.gif',15,17,40,1),(41,':geek:','Geek','icon_e_geek.gif',17,17,41,1),(42,':ugeek:','Uber Geek','icon_e_ugeek.gif',17,18,42,1);
/*!40000 ALTER TABLE `phpbb_smilies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_styles`
--

DROP TABLE IF EXISTS `phpbb_styles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_styles` (
  `style_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `style_name` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `style_copyright` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `style_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `template_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `theme_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `imageset_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`style_id`),
  UNIQUE KEY `style_name` (`style_name`),
  KEY `template_id` (`template_id`),
  KEY `theme_id` (`theme_id`),
  KEY `imageset_id` (`imageset_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_styles`
--

LOCK TABLES `phpbb_styles` WRITE;
/*!40000 ALTER TABLE `phpbb_styles` DISABLE KEYS */;
INSERT INTO `phpbb_styles` VALUES (1,'prosilver','&copy; phpBB Group',1,1,1,1),(2,'Metro Blue','&copy; pixelgoose.com, 2013',1,2,2,2);
/*!40000 ALTER TABLE `phpbb_styles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_styles_imageset`
--

DROP TABLE IF EXISTS `phpbb_styles_imageset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_styles_imageset` (
  `imageset_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `imageset_name` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `imageset_copyright` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `imageset_path` varchar(100) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`imageset_id`),
  UNIQUE KEY `imgset_nm` (`imageset_name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_styles_imageset`
--

LOCK TABLES `phpbb_styles_imageset` WRITE;
/*!40000 ALTER TABLE `phpbb_styles_imageset` DISABLE KEYS */;
INSERT INTO `phpbb_styles_imageset` VALUES (1,'prosilver','&copy; phpBB Group','prosilver'),(2,'Metro Blue','&copy; pixelgoose.com, 2013','metro_blue');
/*!40000 ALTER TABLE `phpbb_styles_imageset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_styles_imageset_data`
--

DROP TABLE IF EXISTS `phpbb_styles_imageset_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_styles_imageset_data` (
  `image_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `image_name` varchar(200) COLLATE utf8_bin NOT NULL DEFAULT '',
  `image_filename` varchar(200) COLLATE utf8_bin NOT NULL DEFAULT '',
  `image_lang` varchar(30) COLLATE utf8_bin NOT NULL DEFAULT '',
  `image_height` smallint(4) unsigned NOT NULL DEFAULT '0',
  `image_width` smallint(4) unsigned NOT NULL DEFAULT '0',
  `imageset_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`image_id`),
  KEY `i_d` (`imageset_id`)
) ENGINE=InnoDB AUTO_INCREMENT=157 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_styles_imageset_data`
--

LOCK TABLES `phpbb_styles_imageset_data` WRITE;
/*!40000 ALTER TABLE `phpbb_styles_imageset_data` DISABLE KEYS */;
INSERT INTO `phpbb_styles_imageset_data` VALUES (1,'site_logo','site_logo.gif','',52,139,1),(2,'forum_link','forum_link.gif','',27,27,1),(3,'forum_read','forum_read.gif','',27,27,1),(4,'forum_read_locked','forum_read_locked.gif','',27,27,1),(5,'forum_read_subforum','forum_read_subforum.gif','',27,27,1),(6,'forum_unread','forum_unread.gif','',27,27,1),(7,'forum_unread_locked','forum_unread_locked.gif','',27,27,1),(8,'forum_unread_subforum','forum_unread_subforum.gif','',27,27,1),(9,'topic_moved','topic_moved.gif','',27,27,1),(10,'topic_read','topic_read.gif','',27,27,1),(11,'topic_read_mine','topic_read_mine.gif','',27,27,1),(12,'topic_read_hot','topic_read_hot.gif','',27,27,1),(13,'topic_read_hot_mine','topic_read_hot_mine.gif','',27,27,1),(14,'topic_read_locked','topic_read_locked.gif','',27,27,1),(15,'topic_read_locked_mine','topic_read_locked_mine.gif','',27,27,1),(16,'topic_unread','topic_unread.gif','',27,27,1),(17,'topic_unread_mine','topic_unread_mine.gif','',27,27,1),(18,'topic_unread_hot','topic_unread_hot.gif','',27,27,1),(19,'topic_unread_hot_mine','topic_unread_hot_mine.gif','',27,27,1),(20,'topic_unread_locked','topic_unread_locked.gif','',27,27,1),(21,'topic_unread_locked_mine','topic_unread_locked_mine.gif','',27,27,1),(22,'sticky_read','sticky_read.gif','',27,27,1),(23,'sticky_read_mine','sticky_read_mine.gif','',27,27,1),(24,'sticky_read_locked','sticky_read_locked.gif','',27,27,1),(25,'sticky_read_locked_mine','sticky_read_locked_mine.gif','',27,27,1),(26,'sticky_unread','sticky_unread.gif','',27,27,1),(27,'sticky_unread_mine','sticky_unread_mine.gif','',27,27,1),(28,'sticky_unread_locked','sticky_unread_locked.gif','',27,27,1),(29,'sticky_unread_locked_mine','sticky_unread_locked_mine.gif','',27,27,1),(30,'announce_read','announce_read.gif','',27,27,1),(31,'announce_read_mine','announce_read_mine.gif','',27,27,1),(32,'announce_read_locked','announce_read_locked.gif','',27,27,1),(33,'announce_read_locked_mine','announce_read_locked_mine.gif','',27,27,1),(34,'announce_unread','announce_unread.gif','',27,27,1),(35,'announce_unread_mine','announce_unread_mine.gif','',27,27,1),(36,'announce_unread_locked','announce_unread_locked.gif','',27,27,1),(37,'announce_unread_locked_mine','announce_unread_locked_mine.gif','',27,27,1),(38,'global_read','announce_read.gif','',27,27,1),(39,'global_read_mine','announce_read_mine.gif','',27,27,1),(40,'global_read_locked','announce_read_locked.gif','',27,27,1),(41,'global_read_locked_mine','announce_read_locked_mine.gif','',27,27,1),(42,'global_unread','announce_unread.gif','',27,27,1),(43,'global_unread_mine','announce_unread_mine.gif','',27,27,1),(44,'global_unread_locked','announce_unread_locked.gif','',27,27,1),(45,'global_unread_locked_mine','announce_unread_locked_mine.gif','',27,27,1),(46,'pm_read','topic_read.gif','',27,27,1),(47,'pm_unread','topic_unread.gif','',27,27,1),(48,'icon_back_top','icon_back_top.gif','',11,11,1),(49,'icon_contact_aim','icon_contact_aim.gif','',20,20,1),(50,'icon_contact_email','icon_contact_email.gif','',20,20,1),(51,'icon_contact_icq','icon_contact_icq.gif','',20,20,1),(52,'icon_contact_jabber','icon_contact_jabber.gif','',20,20,1),(53,'icon_contact_msnm','icon_contact_msnm.gif','',20,20,1),(54,'icon_contact_www','icon_contact_www.gif','',20,20,1),(55,'icon_contact_yahoo','icon_contact_yahoo.gif','',20,20,1),(56,'icon_post_delete','icon_post_delete.gif','',20,20,1),(57,'icon_post_info','icon_post_info.gif','',20,20,1),(58,'icon_post_report','icon_post_report.gif','',20,20,1),(59,'icon_post_target','icon_post_target.gif','',9,11,1),(60,'icon_post_target_unread','icon_post_target_unread.gif','',9,11,1),(61,'icon_topic_attach','icon_topic_attach.gif','',10,7,1),(62,'icon_topic_latest','icon_topic_latest.gif','',9,11,1),(63,'icon_topic_newest','icon_topic_newest.gif','',9,11,1),(64,'icon_topic_reported','icon_topic_reported.gif','',14,16,1),(65,'icon_topic_unapproved','icon_topic_unapproved.gif','',14,16,1),(66,'icon_user_warn','icon_user_warn.gif','',20,20,1),(67,'subforum_read','subforum_read.gif','',9,11,1),(68,'subforum_unread','subforum_unread.gif','',9,11,1),(69,'icon_contact_pm','icon_contact_pm.gif','en',20,28,1),(70,'icon_post_edit','icon_post_edit.gif','en',20,42,1),(71,'icon_post_quote','icon_post_quote.gif','en',20,54,1),(72,'icon_user_online','icon_user_online.gif','en',58,58,1),(73,'button_pm_forward','button_pm_forward.gif','en',25,96,1),(74,'button_pm_new','button_pm_new.gif','en',25,84,1),(75,'button_pm_reply','button_pm_reply.gif','en',25,96,1),(76,'button_topic_locked','button_topic_locked.gif','en',25,88,1),(77,'button_topic_new','button_topic_new.gif','en',25,96,1),(78,'button_topic_reply','button_topic_reply.gif','en',25,96,1),(79,'site_logo','site_logo.png','',31,125,2),(80,'forum_link','forum_link.png','',38,38,2),(81,'forum_read','forum_read.png','',38,38,2),(82,'forum_read_locked','forum_read_locked.png','',38,38,2),(83,'forum_read_subforum','forum_read_subforum.png','',38,38,2),(84,'forum_unread','forum_unread.png','',38,38,2),(85,'forum_unread_locked','forum_unread_locked.png','',38,38,2),(86,'forum_unread_subforum','forum_unread_subforum.png','',38,38,2),(87,'topic_moved','topic_moved.png','',38,38,2),(88,'topic_read','topic_read.png','',38,38,2),(89,'topic_read_mine','topic_read_mine.png','',38,38,2),(90,'topic_read_hot','topic_read_hot.png','',38,38,2),(91,'topic_read_hot_mine','topic_read_hot_mine.png','',38,38,2),(92,'topic_read_locked','topic_read_locked.png','',38,38,2),(93,'topic_read_locked_mine','topic_read_locked_mine.png','',38,38,2),(94,'topic_unread','topic_unread.png','',38,38,2),(95,'topic_unread_mine','topic_unread_mine.png','',38,38,2),(96,'topic_unread_hot','topic_unread_hot.png','',38,38,2),(97,'topic_unread_hot_mine','topic_unread_hot_mine.png','',38,38,2),(98,'topic_unread_locked','topic_unread_locked.png','',38,38,2),(99,'topic_unread_locked_mine','topic_unread_locked_mine.png','',38,38,2),(100,'sticky_read','sticky_read.png','',38,38,2),(101,'sticky_read_mine','sticky_read_mine.png','',38,38,2),(102,'sticky_read_locked','sticky_read_locked.png','',38,38,2),(103,'sticky_read_locked_mine','sticky_read_locked_mine.png','',38,38,2),(104,'sticky_unread','sticky_unread.png','',38,38,2),(105,'sticky_unread_mine','sticky_unread_mine.png','',38,38,2),(106,'sticky_unread_locked','sticky_unread_locked.png','',38,38,2),(107,'sticky_unread_locked_mine','sticky_unread_locked_mine.png','',38,38,2),(108,'announce_read','announce_read.png','',38,38,2),(109,'announce_read_mine','announce_read_mine.png','',38,38,2),(110,'announce_read_locked','announce_read_locked.png','',38,38,2),(111,'announce_read_locked_mine','announce_read_locked_mine.png','',38,38,2),(112,'announce_unread','announce_unread.png','',38,38,2),(113,'announce_unread_mine','announce_unread_mine.png','',38,38,2),(114,'announce_unread_locked','announce_unread_locked.png','',38,38,2),(115,'announce_unread_locked_mine','announce_unread_locked_mine.png','',38,38,2),(116,'global_read','announce_read.png','',38,38,2),(117,'global_read_mine','announce_read_mine.png','',38,38,2),(118,'global_read_locked','announce_read_locked.png','',38,38,2),(119,'global_read_locked_mine','announce_read_locked_mine.png','',38,38,2),(120,'global_unread','announce_unread.png','',38,38,2),(121,'global_unread_mine','announce_unread_mine.png','',38,38,2),(122,'global_unread_locked','announce_unread_locked.png','',38,38,2),(123,'global_unread_locked_mine','announce_unread_locked_mine.png','',38,38,2),(124,'subforum_read','subforum_read.png','',11,12,2),(125,'subforum_unread','subforum_unread.png','',11,12,2),(126,'pm_read','topic_read.png','',38,38,2),(127,'pm_unread','topic_unread.png','',38,38,2),(128,'icon_back_top','icon_back_top.gif','',11,11,2),(129,'icon_contact_aim','icon_contact_aim.png','',20,20,2),(130,'icon_contact_email','icon_contact_email.png','',20,20,2),(131,'icon_contact_icq','icon_contact_icq.png','',20,20,2),(132,'icon_contact_jabber','icon_contact_jabber.png','',20,20,2),(133,'icon_contact_msnm','icon_contact_msnm.png','',20,20,2),(134,'icon_contact_www','icon_contact_www.png','',20,20,2),(135,'icon_contact_yahoo','icon_contact_yahoo.png','',20,20,2),(136,'icon_post_delete','icon_post_delete.png','',20,20,2),(137,'icon_post_info','icon_post_info.png','',20,20,2),(138,'icon_post_report','icon_post_report.png','',20,20,2),(139,'icon_post_target','icon_post_target.png','',9,11,2),(140,'icon_post_target_unread','icon_post_target_unread.png','',9,11,2),(141,'icon_topic_attach','icon_topic_attach.gif','',10,7,2),(142,'icon_topic_latest','icon_topic_latest.png','',10,12,2),(143,'icon_topic_newest','icon_topic_newest.png','',9,11,2),(144,'icon_topic_reported','icon_topic_reported.png','',14,16,2),(145,'icon_topic_unapproved','icon_topic_unapproved.png','',14,16,2),(146,'icon_user_warn','icon_user_warn.png','',20,20,2),(147,'icon_contact_pm','icon_contact_pm.png','en',20,28,2),(148,'icon_post_edit','icon_post_edit.png','en',20,52,2),(149,'icon_post_quote','icon_post_quote.png','en',20,60,2),(150,'icon_user_online','icon_user_online.png','en',58,58,2),(151,'button_pm_forward','button_pm_forward.png','en',32,100,2),(152,'button_pm_new','button_pm_new.png','en',32,100,2),(153,'button_pm_reply','button_pm_reply.png','en',32,115,2),(154,'button_topic_locked','button_topic_locked.png','en',32,97,2),(155,'button_topic_new','button_topic_new.png','en',32,114,2),(156,'button_topic_reply','button_topic_reply.png','en',32,116,2);
/*!40000 ALTER TABLE `phpbb_styles_imageset_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_styles_template`
--

DROP TABLE IF EXISTS `phpbb_styles_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_styles_template` (
  `template_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `template_name` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `template_copyright` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `template_path` varchar(100) COLLATE utf8_bin NOT NULL DEFAULT '',
  `bbcode_bitfield` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT 'kNg=',
  `template_storedb` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `template_inherits_id` int(4) unsigned NOT NULL DEFAULT '0',
  `template_inherit_path` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`template_id`),
  UNIQUE KEY `tmplte_nm` (`template_name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_styles_template`
--

LOCK TABLES `phpbb_styles_template` WRITE;
/*!40000 ALTER TABLE `phpbb_styles_template` DISABLE KEYS */;
INSERT INTO `phpbb_styles_template` VALUES (1,'prosilver','&copy; phpBB Group','prosilver','lNg=',0,0,''),(2,'Metro Blue','&copy; pixelgoose.com, 2013','metro_blue','lNg=',0,0,'');
/*!40000 ALTER TABLE `phpbb_styles_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_styles_template_data`
--

DROP TABLE IF EXISTS `phpbb_styles_template_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_styles_template_data` (
  `template_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `template_filename` varchar(100) COLLATE utf8_bin NOT NULL DEFAULT '',
  `template_included` text COLLATE utf8_bin NOT NULL,
  `template_mtime` int(11) unsigned NOT NULL DEFAULT '0',
  `template_data` mediumtext COLLATE utf8_bin NOT NULL,
  KEY `tid` (`template_id`),
  KEY `tfn` (`template_filename`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_styles_template_data`
--

LOCK TABLES `phpbb_styles_template_data` WRITE;
/*!40000 ALTER TABLE `phpbb_styles_template_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_styles_template_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_styles_theme`
--

DROP TABLE IF EXISTS `phpbb_styles_theme`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_styles_theme` (
  `theme_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `theme_name` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `theme_copyright` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `theme_path` varchar(100) COLLATE utf8_bin NOT NULL DEFAULT '',
  `theme_storedb` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `theme_mtime` int(11) unsigned NOT NULL DEFAULT '0',
  `theme_data` mediumtext COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`theme_id`),
  UNIQUE KEY `theme_name` (`theme_name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_styles_theme`
--

LOCK TABLES `phpbb_styles_theme` WRITE;
/*!40000 ALTER TABLE `phpbb_styles_theme` DISABLE KEYS */;
INSERT INTO `phpbb_styles_theme` VALUES (1,'prosilver','&copy; phpBB Group','prosilver',1,0,''),(2,'Metro Blue','&copy; pixelgoose.com, 2013','metro_blue',1,1365639543,'/*  phpBB3 Style Sheet\n    --------------------------------------------------------------\n	Style name:			Metro Blue\n	Based on style:		prosilver\n	Original author:	Tom Beddard ( http://www.subblue.com/ )\n	Modified by:		PixelGoose Studio ( http://pixelgoose.com/ )\n    --------------------------------------------------------------\n*/\n\n/* General Markup Styles\r\n---------------------------------------- */\r\n\r\n* {\r\n	/* Reset browsers default margin, padding and font sizes */\r\n	margin: 0;\r\n	padding: 0;\r\n}\r\n\r\nhtml {\r\n	/*font-size: 0.813em;*/\r\n	/* Always show a scrollbar for short pages - stops the jump when the scrollbar appears. non-IE browsers */\r\n	height: 101%;\r\n}\r\n\r\nbody {\r\n	/* Text-Sizing with ems: http://www.clagnut.com/blog/348/ */\r\n	font-family: \'Open Sans\', sans-serif;\r\n	color: #828282;\r\n	background-color: #F6F6F6;\r\n	/*font-size: 62.5%;			 This sets the default font size to be equivalent to 10px */\r\n	font-size: 0.813em;\r\n	margin: 0;\r\n	padding: 12px 0;\r\n}\r\n\r\nh1 {\r\n	/* Forum name */\r\n	font-family: \'Open Sans\', sans-serif;\r\n	margin-right: 200px;\r\n	color: #FFFFFF;\r\n	margin-top: 15px;\r\n	font-weight: bold;\r\n	font-size: 2em;\r\n	letter-spacing: -1px;\r\n}\r\n\r\nh2 {\r\n	/* Forum header titles */\r\n	font-family: \'Open Sans\', sans-serif;\r\n	font-weight: normal;\r\n	color: #3f3f3f;\r\n	font-size: 2em;\r\n	margin: 0.8em 0 0.2em 0;\r\n	letter-spacing: -1px;\r\n}\r\n\r\nh2.solo {\r\n	margin-bottom: 1em;\r\n}\r\n#message h2, #confirm h2 {\r\n	border-bottom: solid 1px #E4E4E4;\r\n}\r\nh3 {\r\n	/* Sub-headers (also used as post headers, but defined later) */\r\n	font-family: \'Open Sans\', sans-serif;\r\n	font-size: 1.077em;\r\n	color: #989898;\r\n}\r\n\r\nh4 {\r\n	/* Forum and topic list titles */\r\n	font-family: \'Open Sans\', sans-serif;\r\n	font-size: 1.3em;\r\n}\r\n\r\np {\r\n	line-height: 1.3em;\r\n	margin-bottom: 1.5em;\r\n}\r\n\r\nimg {\r\n	border-width: 0;\r\n    height: auto;\r\n    max-width: 100%;\r\n    vertical-align: middle;\r\n}\r\nhr {\r\n	/* Also see tweaks.css */\r\n	border: 0 none #FFFFFF;\r\n	border-top: 1px solid #E4E4E4;\r\n	height: 1px;\r\n	margin: 6px 0 12px;\r\n	display: block;\r\n	clear: both;\r\n}\r\n\r\nhr.dashed {\r\n	border-top: 1px solid #E4E4E4;\r\n	margin: 10px 0;\r\n}\r\n\r\nhr.divider {\r\n	display: none;\r\n}\r\n\r\np.right {\r\n	text-align: right;\r\n}\r\n\r\n/* Main blocks\r\n---------------------------------------- */\r\n#wrap {\r\n	width: 1180px;\r\n	margin: 0 auto;\r\n}\r\n\r\n#simple-wrap {\r\n	padding: 6px 12px;\r\n	background-color: #fff;\r\n	border: solid 1px #E4E4E4;\r\n	margin: 0 12px;\r\n}\r\n#simple-wrap h2 {\r\n	border-bottom: solid 1px #E4E4E4;\r\n}\r\n#page-body {\r\n	clear: both;\r\n	overflow: hidden;\r\n}\r\n\r\n#page-footer {\r\n	clear: both;\r\n}\r\n\r\n#page-footer h3 {\r\n	margin-top: 20px;\r\n}\r\n\r\n\r\n/* Search box\r\n--------------------------------------------- */\r\n.search-box {\r\n	color: #43A6DF;\r\n	float: left;\r\n	white-space: nowrap; /* For Opera */\r\n}\r\n.search-box-inner {\r\n	display: inline-block;\r\n    height: 28px;\r\n    width: 250px;\r\n    background-color: #FFFFFF;\r\n    border: 1px solid #E4E4E4;\r\n    height: 26px;\r\n	padding: 2px;	\r\n}\r\n.search-box #keywords {\r\n    height: 20px;\r\n    width: 200px;\r\n	background-color: #FFF;\r\n}\r\n\r\n.search-box input {\r\n	border: 1px solid #b0b0b0;\r\n}\r\n.search-box .inputbox {\r\n	padding: 2px 0 2px 6px;\r\n}\r\n.topic-actions .search-box {\r\n	margin: 0 0 12px;\r\n}\r\n/* .button1 style defined later, just a few tweaks for the search button version */\r\n.search-box input.button1 {\r\n	padding: 1px 5px;\r\n}\r\n\r\n.search-box li {\r\n	text-align: right;\r\n	margin-top: 4px;\r\n}\r\n\r\n.search-box img {\r\n	vertical-align: middle;\r\n	margin-right: 3px;\r\n}\r\n.search-adv-link {\r\n	line-height: 31px;\r\n	display: inline-block;\r\n\r\n}\r\n.search-form {\r\n	overflow: hidden;\r\n}\r\n/* Site description and logo */\r\n#site-description {\r\n	float: left;\r\n	width: 70%;\r\n}\r\n\r\n#site-description h1 {\r\n	margin-right: 0;\r\n}\r\n\r\n\r\n/* Header and tabs\r\n---------------------------------------- */\r\n\r\n#above-headerbar {\r\n	height: 36px;\r\n	margin-bottom: 12px;\r\n}\r\n#header {\r\n	height: 120px;\r\n	background-color: #43A6DF;\r\n	margin-bottom: 12px;\r\n	position: relative;\r\n}\r\n#header #logo {\r\n	position: absolute;\r\n	left: 12px;\r\n	bottom: 12px;\r\n}\r\n#header .tabs {\r\n	float: right;\r\n	padding: 0;\r\n	margin: 0;\r\n	list-style-type: none;\r\n}\r\n#header li {\r\n	float: left;\r\n	padding: 0;\r\n	margin: 0;\r\n	height: 120px;\r\n	width: 120px;\r\n	border-left: solid 12px #F6F6F6;\r\n}\r\n#header li a {\r\n	color: #fff;\r\n	height: 120px;\r\n	width: 120px;\r\n	display: block;\r\n	position: relative;\r\n	font-size: 1.231em;\r\n	text-transform: lowercase;\r\n}\r\n#header li a:hover {\r\n	background-color: #2E3539;\r\n}\r\n#header li a span {\r\n	position: absolute;\r\n	left: 12px;\r\n	bottom: 12px;\r\n}\r\n#header li#members-link a {\r\n	background-image: url(\"{T_IMAGESET_PATH}/tab-icon-members.png\");\r\n	background-attachment: scroll;\r\n	background-repeat: no-repeat;\r\n	background-position: left top;	\r\n}\r\n#header li#faq-link a {\r\n	background-image: url(\"{T_IMAGESET_PATH}/tab-icon-faq.png\");\r\n	background-attachment: scroll;\r\n	background-repeat: no-repeat;\r\n	background-position: left top;	\r\n}\r\n#header li#contacts-link a {\r\n	background-image: url(\"{T_IMAGESET_PATH}/tab-icon-contacts.png\");\r\n	background-attachment: scroll;\r\n	background-repeat: no-repeat;\r\n	background-position: left top;	\r\n}\r\n#header li#news-link a {\r\n	background-image: url(\"{T_IMAGESET_PATH}/tab-icon-news.png\");\r\n	background-attachment: scroll;\r\n	background-repeat: no-repeat;\r\n	background-position: left top;	\r\n}\r\n#header li#sample-link a {\r\n	background-image: url(\"{T_IMAGESET_PATH}/tab-icon-sample.png\");\r\n	background-attachment: scroll;\r\n	background-repeat: no-repeat;\r\n	background-position: left top;	\r\n}\r\n\r\n#subheader-menu {\r\n	background-color: #2E3539;\r\n	height: 34px;\r\n	margin-bottom: 12px;\r\n	-moz-box-sizing: border-box;\r\n	-webkit-box-sizing: border-box;\r\n	box-sizing: border-box;\r\n	padding: 7px 12px;\r\n}\r\n#subheader-menu .links.right {\r\n	float: right;\r\n}\r\n#subheader-menu .links.right li {\r\n	margin-right: 0;\r\n}\r\n#subheader-menu .links li {\r\n	float: left;\r\n	list-style: none;\r\n	margin-right: 12px;\r\n}\r\n#subheader-menu .links li a {\r\n	color: #ffffff;\r\n}\r\n\r\n#breadcrumbs {\r\n	margin-bottom: 12px;\r\n}\r\n.navbit-arrow {\r\n	margin: 0 6px;\r\n	vertical-align: middle;\r\n}\r\n/* Round cornered boxes and backgrounds\r\n---------------------------------------- */\r\n.headerbar {\r\n	background: #ebebeb none repeat-x 0 0;\r\n	color: #FFFFFF;\r\n	margin-bottom: 4px;\r\n	padding: 0 5px;\r\n}\r\n\r\n.navbar {\r\n	padding: 12px 0;\r\n	border-top: solid 1px #E4E4E4;\r\n	border-bottom: solid 1px #E4E4E4;\r\n	margin-bottom: 12px;\r\n}\r\n\r\n.forabg {\r\n	background: #b1b1b1 none repeat-x 0 0;\r\n	margin-bottom: 12px;\r\n	clear: both;\r\n}\r\n\r\n.forumbg {\r\n	background: #b1b1b1 none repeat-x 0 0;\r\n	margin-bottom: 12px;\r\n	clear: both;\r\n}\r\n.forum-moderators,\r\n.forum-subforums {\r\n	margin-top: 6px;\r\n	font-size: 0.846em;\r\n}\r\n.forum-moderators + .forum-subforums {\r\n	margin-top: 0;\r\n}\r\n.forum-name {\r\n	padding-left: 12px;\r\n}\r\n.panel {\r\n	margin-bottom: 12px;\r\n	padding: 0 10px;\r\n	color: #3f3f3f;\r\n}\r\n.panel.no-topics {\r\n	padding: 48px 0;\r\n	text-align: center;\r\n	font-size: 1.231em;\r\n	background-color: #fff;\r\n}\r\n.panel.bg2 {\r\n	border: 0;\r\n	background-color: transparent;\r\n	padding: 0;\r\n}\r\n.post {\r\n	padding: 12px;\r\n	margin-bottom: 12px;\r\n	background-repeat: no-repeat;\r\n	background-position: 100% 0;\r\n	border: solid 1px #E4E4E4;\r\n	position: relative;\r\n}\r\n#preview {\r\n	overflow: hidden;\r\n}\r\n.post:target .content {\r\n	color: #000000;\r\n}\r\n\r\n.post:target h3 a {\r\n	color: #000000;\r\n}\r\n\r\n.bg1	{ background-color: #f7f7f7;}\r\n.bg2	{ background-color: #f2f2f2; }\r\n.bg3	{ background-color: #ebebeb; }\r\n\r\n.rowbg {\r\n	margin: 5px 5px 2px 5px;\r\n}\r\n\r\n.ucprowbg {\r\n	background-color: #e2e2e2;\r\n}\r\n\r\n.fieldsbg {\r\n	background-color: #eaeaea;\r\n}\r\n\r\n\r\n\r\n/* Horizontal lists\r\n----------------------------------------*/\r\n\r\n.user-links {\r\n	float: right;\r\n}\r\n.user-links li {\r\n	float: left;\r\n	list-style-image: none;\r\n	list-style-type: none;\r\n	margin-right: 12px;\r\n}\r\n.user-links li:last-child {\r\n	margin-right: 0px;\r\n}\r\n\r\n.user-links {\r\n	/*font-size: 1.077em;*/\r\n}\r\n.user-links, .user-links a {\r\n	line-height: 31px;\r\n}\r\n\r\n\r\nul.linklist {\r\n	display: block;\r\n	margin: 0;\r\n}\r\n\r\nul.linklist li {\r\n	display: block;\r\n	list-style-type: none;\r\n	float: left;\r\n	width: auto;\r\n	margin-right: 5px;\r\n	line-height: 2.2em;\r\n}\r\n\r\nul.linklist li.rightside, p.rightside {\r\n	float: right;\r\n	margin-right: 0;\r\n	margin-left: 5px;\r\n	text-align: right;\r\n}\r\n\r\nul.navlinks {\r\n	padding-bottom: 1px;\r\n	margin-bottom: 1px;\r\n	border-bottom: 1px solid #FFFFFF;\r\n	font-weight: bold;\r\n}\r\n\r\nul.leftside {\r\n	float: left;\r\n	margin-left: 0;\r\n	margin-right: 5px;\r\n	text-align: left;\r\n}\r\n\r\nul.rightside {\r\n	float: right;\r\n	margin-left: 5px;\r\n	margin-right: -5px;\r\n	text-align: right;\r\n}\r\n.thread-icons {\r\n	margin: 0px;\r\n	padding: 0px;\r\n	list-style-type: none;\r\n	float: right;\r\n}\r\n.thread-icons li {\r\n	float: left;\r\n}\r\n\r\n/* Table styles\r\n----------------------------------------*/\r\ntable.table1 {\r\n	/* See tweaks.css */\r\n}\r\n\r\n#ucp-main table.table1 {\r\n	padding: 2px;\r\n}\r\n\r\ntable.table1 thead th {\r\n	font-weight: normal;\r\n	color: #FFFFFF;\r\n	line-height: 1.3em;\r\n}\r\n\r\ntable.table1 thead th span {\r\n	padding-left: 7px;\r\n}\r\n\r\ntable.table1 tbody tr {\r\n	border: 1px solid #E4E4E4;\r\n}\r\n\r\ntable.table1 tbody tr:hover, table.table1 tbody tr.hover {\r\n	background-color: #f6f6f6;\r\n	color: #000;\r\n}\r\n\r\ntable.table1 td {\r\n	color: #6a6a6a;\r\n}\r\n\r\ntable.table1 tbody td {\r\n	padding: 5px;\r\n	border-top: 1px solid #E4E4E4;\r\n}\r\n\r\ntable.table1 tbody th {\r\n	padding: 5px;\r\n	border-bottom: 1px solid #E4E4E4;\r\n	text-align: left;\r\n	color: #333333;\r\n	background-color: #FFFFFF;\r\n}\r\n\r\n/* Specific column styles */\r\ntable.table1 .name		{ text-align: left; }\r\ntable.table1 .posts		{ text-align: center !important; width: 7%; }\r\ntable.table1 .joined	{ text-align: left; width: 15%; }\r\ntable.table1 .active	{ text-align: left; width: 15%; }\r\ntable.table1 .mark		{ text-align: center; width: 7%; }\r\ntable.table1 .info		{ text-align: left; width: 30%; }\r\ntable.table1 .info div	{ width: 100%; white-space: normal; overflow: hidden; }\r\ntable.table1 .autocol	{ line-height: 2em; white-space: nowrap; }\r\ntable.table1 thead .autocol { padding-left: 1em; }\r\n\r\ntable.table1 span.rank-img {\r\n	float: right;\r\n	width: auto;\r\n}\r\n\r\ntable.info td {\r\n	padding: 3px;\r\n}\r\n\r\ntable.info tbody th {\r\n	padding: 3px;\r\n	text-align: right;\r\n	vertical-align: top;\r\n	color: #000000;\r\n	font-weight: normal;\r\n}\r\n\r\n.forumbg table.table1 {\r\n	margin: 0;\r\n}\r\n\r\n.forumbg-table > .inner {\r\n	margin: 0 -1px;\r\n}\r\n\r\n\r\n/* Misc layout styles\r\n---------------------------------------- */\r\n/* column[1-2] styles are containers for two column layouts \r\n   Also see tweaks.css */\r\n.column1 {\r\n	float: left;\r\n	clear: left;\r\n	width: 49%;\r\n}\r\n\r\n.column2 {\r\n	float: right;\r\n	clear: right;\r\n	width: 49%;\r\n}\r\n\r\n/* General classes for placing floating blocks */\r\n.left-box {\r\n	float: left;\r\n	width: auto;\r\n	text-align: left;\r\n}\r\n\r\n.right-box {\r\n	float: right;\r\n	width: auto;\r\n	text-align: right;\r\n}\r\n\r\ndl.details {\r\n	/*font-size: 1.1em;*/\r\n}\r\n\r\ndl.details dt {\r\n	float: left;\r\n	clear: left;\r\n	width: 30%;\r\n	text-align: right;\r\n	color: #000000;\r\n	display: block;\r\n}\r\n\r\ndl.details dd {\r\n	margin-left: 0;\r\n	padding-left: 5px;\r\n	margin-bottom: 5px;\r\n	float: left;\r\n	width: 65%;\r\n}\r\n\r\n.dark-header {\r\n	background-color: #2E3539;\r\n	line-height: 48px;\r\n	padding: 0 12px;\r\n	color: #ffffff;\r\n	font-weight: normal;\r\n	clear: both;\r\n}\r\n.dark-header a {\r\n	color: #ffffff !important;\r\n}\r\n.dark-header-content {\r\n	background-color: #ffffff;\r\n	padding: 12px;\r\n    border-bottom: 1px solid #E4E4E4;\r\n    border-left: 1px solid #E4E4E4;\r\n    border-right: 1px solid #E4E4E4;\r\n	margin-bottom: 12px;\r\n}\r\n/* Pagination\r\n---------------------------------------- */\r\n.pagination {\r\n	height: 1%; /* IE tweak (holly hack) */\r\n	width: auto;\r\n	text-align: right;\r\n	margin-top: 5px;\r\n	float: right;\r\n}\r\n\r\n.pagination span.page-sep {\r\n	display: none;\r\n}\r\n\r\nli.pagination {\r\n	margin-top: 0;\r\n}\r\n\r\n.pagination strong, .pagination b {\r\n	font-weight: normal;\r\n}\r\n\r\n.pagination span strong {\r\n	padding: 2px 6px;\r\n	margin: 0 1px;\r\n	font-weight: normal;\r\n	color: #FFFFFF;\r\n	background-color: #bfbfbf;\r\n	border: 1px solid #bfbfbf;\r\n	font-size: 0.9em;\r\n}\r\n\r\n.pagination span a, .pagination span a:link, .pagination span a:visited, .pagination span a:active {\r\n	font-weight: normal;\r\n	text-decoration: none;\r\n	color: #747474;\r\n	margin: 0 1px;\r\n	padding: 2px 6px;\r\n	background-color: #eeeeee;\r\n	border: 1px solid #bababa;\r\n	font-size: 0.9em;\r\n	line-height: 1.5em;\r\n}\r\n\r\n.pagination span a:hover {\r\n	border-color: #d2d2d2;\r\n	background-color: #d2d2d2;\r\n	color: #FFF;\r\n	text-decoration: none;\r\n}\r\n\r\n.pagination img {\r\n	vertical-align: middle;\r\n}\r\n\r\n/* Pagination in viewforum for multipage topics */\r\n.row .pagination {\r\n	display: block;\r\n	float: right;\r\n	width: auto;\r\n	margin-top: 0;\r\n	padding: 1px 0 1px 15px;\r\n	font-size: 0.9em;\r\n	background: none 0 50% no-repeat;\r\n}\r\n\r\n.row .pagination span a, li.pagination span a {\r\n	background-color: #FFFFFF;\r\n}\r\n\r\n.row .pagination span a:hover, li.pagination span a:hover {\r\n	background-color: #d2d2d2;\r\n}\r\n\r\n/* Miscellaneous styles\r\n---------------------------------------- */\r\n#forum-permissions {\r\n	float: right;\r\n	width: auto;\r\n	padding-left: 5px;\r\n	margin-left: 5px;\r\n	margin-top: 10px;\r\n	text-align: right;\r\n}\r\n\r\n.copyright {\r\n	padding: 5px;\r\n	text-align: center;\r\n	color: #555555;\r\n}\r\n\r\n.small {\r\n	font-size: 0.9em !important;\r\n}\r\n\r\n.titlespace {\r\n	margin-bottom: 15px;\r\n}\r\n\r\n.headerspace {\r\n	margin-top: 12px;\r\n}\r\n\r\n.error {\r\n	color: #bcbcbc;\r\n	font-weight: bold;\r\n	font-size: 1em;\r\n}\r\n\r\n.reported {\r\n	background-color: #f7f7f7;\r\n}\r\n\r\nli.reported:hover {\r\n	background-color: #ececec;\r\n}\r\n.reported + .reported {\r\n    border-top: none !important;\r\n}\r\ndiv.rules {\r\n	padding: 12px;\r\n	margin: 12px 0;\r\n}\r\n\r\ndiv.rules ul, div.rules ol {\r\n	margin-left: 20px;\r\n}\r\n\r\np.rules {\r\n	background-color: #ececec;\r\n	background-image: none;\r\n	padding: 5px;\r\n}\r\n\r\np.rules img {\r\n	vertical-align: middle;\r\n}\r\n\r\np.rules a {\r\n	vertical-align: middle;\r\n	clear: both;\r\n}\r\n.rules-header {\r\n	font-size: 1.231em;\r\n	display: block;\r\n	font-weight: 600;\r\n	margin-bottom: 6px;\r\n}\r\n.rules-header a {\r\n	font-weight: 600;\r\n	text-decoration: underline !important;\r\n}\r\n.rules-header a:hover {\r\n	text-decoration: none !important;\r\n}\r\n#top {\r\n	position: absolute;\r\n	top: -20px;\r\n}\r\n\r\n.clear {\r\n	display: block;\r\n	clear: both;\r\n	font-size: 1px;\r\n	line-height: 1px;\r\n	background: transparent;\r\n}\r\n\r\n/* Animation\r\n---------------------------------------- */\r\n\r\n#header .tabs li a,\r\n.topiclist.forums .row, .topiclist.topics .row,\r\n.button1, .button2,\r\n.inputbox,\r\n#tabs li span,\r\n#cp-menu #navigation li a,\r\ntable.table1 tbody tr:hover, table.table1 tbody tr.hover {\r\n	-webkit-transition: all 0.3s ease 0s;\r\n	-moz-transition: all 0.3s ease 0s;\r\n	-ms-transition: all 0.3s ease 0s;\r\n	-o-transition: all 0.3s ease 0s;\r\n	transition: all 0.3s ease 0s;\r\n}\r\n.search-box .button2 {\r\n	transition: none;\r\n	-webkit-transition: none;\r\n	-moz-transition: none;\r\n	-ms-transition: none;\r\n	-o-transition: none;\r\n}\r\n\r\n\r\n\r\n/* Popup Login\r\n---------------------------------------- */\r\n\r\n#mask {\r\n	display: none;\r\n	background: #000; \r\n	position: fixed; left: 0; top: 0; \r\n	z-index: 10;\r\n	width: 100%; height: 100%;\r\n	opacity: 0.6;\r\n	z-index: 999;\r\n}\r\n\r\n.login-popup {\r\n	display:none;\r\n	width: 364px;\r\n	background: #fff;\r\n	padding: 16px 0; 	\r\n	float: left;\r\n	font-size: 1.2em;\r\n	position: fixed;\r\n	top: 50%; left: 50%;\r\n	z-index: 99999;\r\n	box-shadow: 0px 0px 20px #434343; /* CSS3 */\r\n	-moz-box-shadow: 0px 0px 20px #434343; /* Firefox */\r\n	-webkit-box-shadow: 0px 0px 20px #434343; /* Safari, Chrome */\r\n}\r\n#navbar_username,\r\n#navbar_password_hint,\r\n#navbar_password {\r\n	width: 254px;\r\n	padding: 0 12px 0 32px;\r\n	border: solid 1px #ebebeb;\r\n	height: 40px;\r\n	line-height: 40px;\r\n	background-attachment: scroll;\r\n	background-image: url(\"{T_THEME_PATH}/images/login-icons.png\") !important;\r\n	background-repeat: no-repeat;\r\n	background-color: #FFF;\r\n	outline: none !important;\r\n	font-size: 11px;\r\n}\r\n#navbar_username {\r\n	background-position: 12px 0;\r\n}\r\n\r\n#navbar_password_hint,\r\n#navbar_password {\r\n	background-position: 12px -40px;\r\n}\r\n.navbar_username_outer,\r\n.navbar_password_outer {\r\n	padding: 12px 32px;\r\n}\r\n.navbar_username_outer label,\r\n.navbar_password_outer label {\r\n	font-size: 11px;\r\n}\r\n.navbar_username_outer.active,\r\n.navbar_password_outer.active {\r\n	padding: 12px 32px 12px 26px;\r\n	background-color: #f2f2f2;\r\n	border-left: solid 6px #43A6DF;\r\n	-webkit-box-sizing: border-box;\r\n	   -moz-box-sizing: border-box;\r\n	        box-sizing: border-box;\r\n}\r\n.quick-login {\r\n	border-bottom: solid 1px #ebebeb;\r\n	margin-bottom: 12px;\r\n}\r\n.login-buttons {\r\n	padding: 12px 32px;\r\n}\r\n#autologin_label {\r\n	font-size: 11px;\r\n	cursor: pointer;\r\n	float: right;\r\n}\r\n#login-box .register-link {\r\n	padding: 12px 32px;	\r\n}\r\n\r\nimg.close-button { \r\n	float: right; \r\n	margin: -10px 6px 0 0;\r\n}\r\n#login-text {\r\n	font: 24px \'Open Sans\',sans-serif;\r\n	color: #43A6DF;\r\n	text-transform: lowercase;\r\n	padding: 0 32px;\r\n	margin-bottom: 12px;\r\n}\r\n.login-popup fieldset { \r\n	border:none; \r\n}\r\n.login-link {\r\n	display: none;\r\n}\r\n\r\n/* Sidebar blocks\r\n---------------------------------------- */\r\n\r\n#sidebar {\r\n	float: left;\r\n	width: 252px;\r\n	margin-bottom: 12px;\r\n}\r\n#page-body-inner {\r\n	float: left;\r\n	width: 916px;\r\n	margin-right: 12px;\r\n}\r\n.no-sidebar#page-body-inner {\r\n	width: 100%;\r\n	margin-right: 0;\r\n}\r\n\r\n#sidebar + #page-body-inner .forabg,\r\n#sidebar + #page-body-inner .dark-header {\r\n	clear: none;\r\n}\r\n.sidebar-block {\r\n	margin-bottom: 12px;\r\n}\r\n.sidebar-block:last-child {\r\n	margin-bottom: 0;\r\n}\r\n.sidebar-block-header {\r\n	background-color: #2E3539;\r\n    color: #FFFFFF;\r\n    font-size: 14px;\r\n    font-weight: normal;\r\n    padding: 14px 12px;\r\n    letter-spacing: 0;\r\n    margin: 0;\r\n}\r\n.sidebar-block-content {\r\n    background: #FFFFFF;\r\n    border: solid 1px #E4E4E4;\r\n    color: #424242;\r\n    padding: 16px 12px;\r\n}\r\n#sidebar ul {\r\n	list-style-position: inside;\r\n	margin-bottom: 1.5em;\r\n}\r\n\r\n\r\n\r\n\r\n/* Prefooter blocks\r\n---------------------------------------- */\r\n\r\n.prefooter-blocks {\r\n	margin-bottom: 12px;\r\n	overflow: hidden;\r\n	clear: both;\r\n}\r\n.prefooter-blocks h2 {\r\n	color: #FFF;\r\n    font-size: 32px;\r\n    font-weight: 300;\r\n    margin-bottom: 6px;\r\n    margin-top: 0;\r\n    text-transform: lowercase;\r\n    line-height: 1.23;\r\n}\r\n.prefooter-blocks h2, .prefooter-blocks h3, .prefooter-blocks h4 {\r\n	color: #FFF;\r\n}\r\n#container1 {\r\n	width: 100%;\r\n	float: left;\r\n	background-color: #2E3539;\r\n	position: relative;\r\n	right: 12px;\r\n}\r\n#container2 {\r\n	width: 100%;\r\n	float: left;	\r\n	background-color: #F6F6F6;\r\n	position: relative;\r\n	right: 252px;\r\n}\r\n#container3 {\r\n	width: 100%;\r\n	float: left;\r\n	background-color: #43A6DF;\r\n	overflow: hidden;\r\n}\r\n#col1 {\r\n    float: left;\r\n    left: 264px;\r\n    margin-right: -264px;\r\n    padding-right: 276px !important;\r\n    position: relative;\r\n    width: 100%;\r\n}\r\n#col2 {\r\n	width: 252px;\r\n	float: right;\r\n	position: relative;\r\n	left: 264px;\r\n}\r\n#col1, #col2 {\r\n	-webkit-box-sizing: border-box;\r\n	   -moz-box-sizing: border-box;\r\n	        box-sizing: border-box;\r\n	padding: 24px 12px;\r\n	color: #FFF;\r\n}\r\n\r\n.prefooter-block-01 {\r\n	float: left;\r\n	width: 70%;\r\n	padding-right: 12px;\r\n}\r\n.prefooter-block-01.single {\r\n	width: auto;\r\n	padding-right: 0;\r\n}\r\n.prefooter-block-01 p {\r\n	margin-bottom: 1em;\r\n	line-height: 1.3em;\r\n}\r\n.about-block p:last-child {\r\n	margin-bottom: 0;\r\n}\r\n.prefooter-block-02 {\r\n	float: left;\r\n	width: 30%;\r\n}\r\n.prefooter-block-02 li {\r\n	line-height: 1.3em;\r\n	margin-bottom: 6px;\r\n}\r\n.prefooter-right {\r\n	float: right;\r\n	width: 244px;\r\n}\r\n.prefooter-block-03 {\r\n	margin-bottom: 24px;\r\n	overflow: hidden;\r\n}\r\n.social-icons ul li {\r\n	float: left;\r\n	margin-right: 6px;\r\n	list-style-type: none;\r\n}\r\n.social-icons ul li a {\r\n	display: block;\r\n	width: 37px;\r\n	height: 37px;\r\n	background-image: url(\"{T_THEME_PATH}/images/social-icons.png\");\r\n	background-attachment: scroll;\r\n	background-repeat: no-repeat;\r\n\r\n}\r\n.social-icons ul li a.twitter {\r\n	background-position: -76px top;\r\n}\r\n.social-icons ul li a.twitter:hover {\r\n	background-position: -76px -37px;\r\n}\r\n.social-icons ul li a.facebook {\r\n	background-position: -38px top;\r\n}\r\n.social-icons ul li a.facebook:hover {\r\n	background-position: -38px -37px;\r\n}\r\n.social-icons ul li a.youtube {\r\n	background-position: -152px top;\r\n}\r\n.social-icons ul li a.youtube:hover {\r\n	background-position: -152px -37px;\r\n}\r\n.social-icons ul li a.gplus {\r\n	background-position: 0 top;\r\n}\r\n.social-icons ul li a.gplus:hover {\r\n	background-position: 0 -37px;\r\n}\r\n.social-icons ul li a.myspace {\r\n	background-position: -114px top;\r\n}\r\n.social-icons ul li a.myspace:hover {\r\n	background-position: -114px -37px;\r\n}\r\n.prefooter-block-04 a {\r\n	color: #ffffff;\r\n	text-decoration: underline;\r\n}\r\n.prefooter-block-04 a:hover {\r\n	color: #ffffff;\r\n	text-decoration: none;\r\n}\r\n#footer_divider {\r\n	border-bottom: 1px solid #E4E4E4;\r\n	clear: both;\r\n	padding-top: 48px;\r\n}	\r\n\r\na.toggleMenuButton {\r\n    background-image: url(\"{T_THEME_PATH}/images/toggleMenubg.png\");\r\n    background-attachment: scroll;\r\n    background-repeat: no-repeat;\r\n    background-position: center center;\r\n}\r\n#subheader-menu li {\r\n    background-image: url(\"{T_THEME_PATH}/images/submenu-icons.png\");\r\n    background-attachment: scroll;\r\n    background-repeat: no-repeat;\r\n}\n/* Link Styles\r\n---------------------------------------- */\r\n\r\n/* Links adjustment to correctly display an order of rtl/ltr mixed content */\r\na {\r\n	direction: ltr;\r\n	unicode-bidi: embed;\r\n}\r\n\r\na:link	{ color: #898989; text-decoration: none; }\r\na:visited	{ color: #898989; text-decoration: none; }\r\na:hover	{ color: #d3d3d3; text-decoration: underline; }\r\na:active	{ color: #d2d2d2; text-decoration: none; }\r\n\r\n/* Coloured usernames */\r\n.username-coloured {\r\n	font-weight: bold;\r\n	display: inline !important;\r\n	padding: 0 !important;\r\n	word-wrap: break-word;\r\n}\r\n/* Links on gradient backgrounds */\r\n#search-box a:link, .navbg a:link, .forumbg .header a:link, .forabg .header a:link, th a:link {\r\n	color: #FFFFFF;\r\n	text-decoration: none;\r\n}\r\n\r\n#search-box a:visited, .navbg a:visited, .forumbg .header a:visited, .forabg .header a:visited, th a:visited {\r\n	color: #FFFFFF;\r\n	text-decoration: none;\r\n}\r\n\r\n#search-box a:hover, .navbg a:hover, .forumbg .header a:hover, .forabg .header a:hover, th a:hover {\r\n	color: #ffffff;\r\n	text-decoration: underline;\r\n}\r\n\r\n#search-box a:active, .navbg a:active, .forumbg .header a:active, .forabg .header a:active, th a:active {\r\n	color: #ffffff;\r\n	text-decoration: none;\r\n}\r\n\r\n/* Links for forum/topic lists */\r\na.forumtitle {\r\n	font-size: 1.077em;\r\n	font-weight: bold;\r\n	text-decoration: none;\r\n}\r\n\r\n/* a.forumtitle:visited { color: #898989; } */\r\n\r\na.forumtitle:hover {\r\n	color: #bcbcbc;\r\n	text-decoration: underline;\r\n}\r\n\r\na.forumtitle:active {\r\n	color: #898989;\r\n}\r\n\r\na.topictitle {\r\n    font-size: 1.077em;\r\n	font-weight: bold;\r\n	text-decoration: none;\r\n}\r\n\r\n/* a.topictitle:visited { color: #d2d2d2; } */\r\n\r\na.topictitle:hover {\r\n	color: #bcbcbc;\r\n	text-decoration: underline;\r\n}\r\n\r\na.topictitle:active {\r\n	color: #898989;\r\n}\r\n\r\n/* Post body links */\r\n.postlink {\r\n	text-decoration: none;\r\n	color: #d2d2d2;\r\n	border-bottom: 1px solid #d2d2d2;\r\n	padding-bottom: 0;\r\n}\r\n\r\n/* .postlink:visited { color: #bdbdbd; } */\r\n\r\n.postlink:active {\r\n	color: #d2d2d2;\r\n}\r\n\r\n.postlink:hover {\r\n	background-color: #f6f6f6;\r\n	text-decoration: none;\r\n	color: #404040;\r\n}\r\n\r\n.signature a, .signature a:visited, .signature a:hover, .signature a:active {\r\n	border: none;\r\n	text-decoration: underline;\r\n	background-color: transparent;\r\n}\r\n\r\n/* Profile links */\r\n.postprofile a:link, .postprofile a:visited, .postprofile dt.author a {\r\n	font-weight: bold;\r\n	color: #898989;\r\n	text-decoration: none;\r\n}\r\n\r\n.postprofile a:hover, .postprofile dt.author a:hover {\r\n	text-decoration: underline;\r\n	color: #d3d3d3;\r\n}\r\n\r\n/* CSS spec requires a:link, a:visited, a:hover and a:active rules to be specified in this order. */\r\n/* See http://www.phpbb.com/bugs/phpbb3/59685 */\r\n.postprofile a:active {\r\n	font-weight: bold;\r\n	color: #898989;\r\n	text-decoration: none;\r\n}\r\n\r\n\r\n/* Profile searchresults */	\r\n.search .postprofile a {\r\n	color: #898989;\r\n	text-decoration: none; \r\n	font-weight: normal;\r\n}\r\n\r\n.search .postprofile a:hover {\r\n	color: #d3d3d3;\r\n	text-decoration: underline; \r\n}\r\n\r\n/* Back to top of page */\r\n.back2top {\r\n	clear: both;\r\n	height: 11px;\r\n	text-align: right;\r\n}\r\n\r\na.top {\r\n	background: none no-repeat top left;\r\n	text-decoration: none;\r\n	width: {IMG_ICON_BACK_TOP_WIDTH}px;\r\n	height: {IMG_ICON_BACK_TOP_HEIGHT}px;\r\n	display: block;\r\n	float: right;\r\n	overflow: hidden;\r\n	letter-spacing: 1000px;\r\n	text-indent: 11px;\r\n}\r\n\r\na.top2 {\r\n	background: none no-repeat 0 50%;\r\n	text-decoration: none;\r\n	padding-left: 15px;\r\n}\r\n\r\n/* Arrow links  */\r\na.up		{ background: none no-repeat left center; }\r\na.down		{ background: none no-repeat right center; }\r\na.left		{ background: none no-repeat 0px 50%; }\r\na.right		{ background: none no-repeat 100% 50%; }\r\n\r\na.block-link {\r\n	display: block;\r\n	margin: 12px 0;\r\n}\r\na.up, a.up:link, a.up:active, a.up:visited {\r\n	padding-left: 10px;\r\n	text-decoration: none;\r\n	border-bottom-width: 0;\r\n}\r\n\r\na.up:hover {\r\n	background-position: left center;\r\n	background-color: transparent;\r\n}\r\n\r\na.down, a.down:link, a.down:active, a.down:visited {\r\n	padding-right: 10px;\r\n}\r\n\r\na.down:hover {\r\n	background-position: right bottom;\r\n	text-decoration: none;\r\n}\r\n\r\na.left, a.left:active, a.left:visited {\r\n	padding-left: 12px;\r\n}\r\n\r\na.left:hover {\r\n	color: #d2d2d2;\r\n	text-decoration: none;\r\n	background-position: 0 50%;\r\n}\r\n\r\na.right, a.right:active, a.right:visited {\r\n	padding-right: 12px;\r\n}\r\n\r\na.right:hover {\r\n	color: #d2d2d2;\r\n	text-decoration: none;\r\n	background-position: 100% 50%;\r\n}\r\n\r\n/* invisible skip link, used for accessibility  */\r\n.skiplink {\r\n	position: absolute;\r\n	left: -999px;\r\n	width: 990px;\r\n}\r\n\r\n/* Feed icon in forumlist_body.html */\r\na.feed-icon-forum {\r\n	float: right;\r\n	margin: 3px;\r\n}\n/* Content Styles\r\n---------------------------------------- */\r\n\r\nul.topiclist {\r\n	display: block;\r\n	list-style-type: none;\r\n	margin: 0;\r\n}\r\n\r\nul.forums {\r\n	background: #ffffff none repeat-x 0 0;\r\n}\r\n\r\nul.topiclist li {\r\n	display: block;\r\n	list-style-type: none;\r\n	color: #777777;\r\n	margin: 0;\r\n}\r\n\r\nul.topiclist dl {\r\n	position: relative;\r\n}\r\n\r\nul.topiclist li.row dl {\r\n	padding: 12px 0;\r\n}\r\n\r\nul.topiclist dt {\r\n	display: block;\r\n	float: left;\r\n	width: 50%;\r\n	padding-left: 5px;\r\n	padding-right: 5px;\r\n}\r\nul.topiclist dt > span {\r\n	padding-left: 12px;\r\n}\r\nul.topiclist dd {\r\n	display: block;\r\n	float: left;\r\n	padding: 4px 0;\r\n}\r\n\r\nul.topiclist dfn {\r\n	position: absolute;\r\n	left: -999px;\r\n	width: 990px;\r\n}\r\n\r\nul.topiclist li.row dt a.subforum {\r\n	background-image: none;\r\n	background-position: 0 50%;\r\n	background-repeat: no-repeat;\r\n	position: relative;\r\n	white-space: nowrap;\r\n	padding: 0 0 0 16px;\r\n}\r\n.topiclist .header dt {\r\n	font-size: 1.077em;\r\n	padding-top: 14px;\r\n}\r\n.topiclist .header dt a {\r\n	color: #ffffff !important;\r\n	padding-left: 12px;\r\n}\r\n.topiclist .header dd {\r\n	font-size: 0.923em;\r\n	line-height: 48px;\r\n}\r\n.forum-image {\r\n	float: left;\r\n	padding-top: 5px;\r\n	margin-right: 5px;\r\n}\r\n\r\nli.row {\r\n    border-bottom: 1px solid #E4E4E4;\r\n    border-left: 1px solid #E4E4E4;\r\n    border-right: 1px solid #E4E4E4;\r\n}\r\n\r\nli.row strong {\r\n	font-weight: normal;\r\n	color: #000000;\r\n}\r\n\r\nli.row:hover {\r\n	background-color: #F2F2F2;\r\n}\r\n\r\nli.row:hover dd {\r\n	\r\n}\r\n\r\nli.header dt, li.header dd {\r\n	border-left-width: 0;\r\n	margin: 0;\r\n	color: #FFFFFF;\r\n	padding-top: 0;\r\n	padding-bottom: 0;\r\n}\r\n\r\n\r\nli.header dd {\r\n	margin-left: 1px;\r\n}\r\n\r\nli.header dl.icon {\r\n	min-height: 0;\r\n}\r\n\r\nli.header dl.icon dt {\r\n	/* Tweak for headers alignment when folder icon used */\r\n	padding-left: 0;\r\n	padding-right: 50px;\r\n}\r\n\r\n/* Forum list column styles */\r\ndl.icon {\r\n	min-height: 35px;\r\n	background-position: 5px 12px;		/* Position of folder icon */\r\n	background-repeat: no-repeat;\r\n}\r\n\r\ndl.icon dt {\r\n	padding-left: 45px;					/* Space for folder icon */\r\n	background-repeat: no-repeat;\r\n	background-position: 5px 95%;		/* Position of topic icon */\r\n}\r\n\r\ndd.posts, dd.topics, dd.views {\r\n	width: 8%;\r\n	text-align: center;\r\n	line-height: 2.2em;\r\n	font-size: 0.846em;\r\n}\r\n\r\n/* List in forum description */\r\ndl.icon dt ol,\r\ndl.icon dt ul {\r\n	list-style-position: inside;\r\n	margin-left: 1em;\r\n}\r\n\r\ndl.icon dt li {\r\n	display: list-item;\r\n	list-style-type: inherit;\r\n}\r\n\r\ndd.lastpost {\r\n	width: 25%;\r\n	font-size: 0.846em;\r\n}\r\n\r\ndd.redirect {\r\n	font-size: 1.1em;\r\n	line-height: 2.5em;\r\n}\r\n\r\ndd.moderation {\r\n	font-size: 1.1em;\r\n}\r\n\r\ndd.lastpost span, ul.topiclist dd.searchby span, ul.topiclist dd.info span, ul.topiclist dd.time span, dd.redirect span, dd.moderation span {\r\n	display: block;\r\n	padding-left: 5px;\r\n}\r\n\r\ndd.time {\r\n	width: auto;\r\n	line-height: 200%;\r\n	font-size: 1.1em;\r\n}\r\n\r\ndd.extra {\r\n	width: 12%;\r\n	line-height: 200%;\r\n	text-align: center;\r\n	font-size: 1.1em;\r\n}\r\n\r\ndd.mark {\r\n	float: right !important;\r\n	width: 9%;\r\n	text-align: center;\r\n	line-height: 200%;\r\n	font-size: 1.2em;\r\n}\r\n\r\ndd.info {\r\n	width: 30%;\r\n}\r\n\r\ndd.option {\r\n	width: 15%;\r\n	line-height: 200%;\r\n	text-align: center;\r\n	font-size: 1.1em;\r\n}\r\n\r\ndd.searchby {\r\n	width: 47%;\r\n	font-size: 1.1em;\r\n	line-height: 1em;\r\n}\r\n\r\nul.topiclist dd.searchextra {\r\n	margin-left: 5px;\r\n	padding: 0.2em 0;\r\n	font-size: 1.1em;\r\n	color: #333333;\r\n	border-left: none;\r\n	clear: both;\r\n	width: 98%;\r\n	overflow: hidden;\r\n}\r\n\r\n/* Container for post/reply buttons and pagination */\r\n.topic-actions {\r\n	margin-bottom: 3px;\r\n	height: 28px;\r\n	min-height: 28px;\r\n	overflow: hidden;\r\n}\r\ndiv[class].topic-actions {\r\n	height: auto;\r\n}\r\n\r\n/* Post body styles\r\n----------------------------------------*/\r\n.postbody {\r\n	padding: 0;\r\n	line-height: 1.48em;\r\n	color: #333333;\r\n	width: 76%;\r\n	float: left;\r\n	clear: both;\r\n}\r\n\r\n.postbody .ignore {\r\n	font-size: 1.1em;\r\n}\r\n\r\n.postbody h3.first {\r\n	/* The first post on the page uses this */\r\n	font-size: 1.077em;\r\n}\r\n\r\n.postbody h3 {\r\n	/* Postbody requires a different h3 format - so change it here */\r\n	font-size: 1.077em;\r\n	font-weight: 300;\r\n	padding: 2px 0 0 0;\r\n	margin: 0 0 0.3em 0 !important;\r\n	text-transform: none;\r\n	border: none;\r\n	font-family: \"Trebuchet MS\", Verdana, Helvetica, Arial, sans-serif;\r\n	line-height: 125%;\r\n}\r\n\r\n.postbody h3 img {\r\n	/* Also see tweaks.css */\r\n	vertical-align: bottom;\r\n}\r\n\r\n.postbody .content {\r\n	border-top: solid 1px #E4E4E4;\r\n	padding-top: 12px;\r\n	margin-top: 6px;\r\n}\r\n\r\n.search .postbody {\r\n	width: 68%\r\n}\r\n\r\n/* Topic review panel\r\n----------------------------------------*/\r\n#review {\r\n	margin-top: 2em;\r\n}\r\nh3#review {\r\n	border-bottom: 0;\r\n}\r\n#topicreview {\r\n	overflow: auto;\r\n	height: 300px;\r\n}\r\n\r\n#topicreview .postbody {\r\n	width: auto;\r\n	float: none;\r\n	margin: 0;\r\n	height: auto;\r\n}\r\n\r\n#topicreview .post {\r\n	height: auto;\r\n}\r\n\r\n#topicreview h2 {\r\n	border-bottom-width: 0;\r\n}\r\n#topicreview h3 {\r\n	border-bottom-width: 0;\r\n}\r\n.post-ignore .postbody {\r\n	display: none;\r\n}\r\n\r\n/* MCP Post details\r\n----------------------------------------*/\r\n#post_details\r\n{\r\n	/* This will only work in IE7+, plus the others */\r\n	overflow: auto;\r\n	max-height: 300px;\r\n}\r\n\r\n#expand\r\n{\r\n	clear: both;\r\n}\r\n\r\n/* Content container styles\r\n----------------------------------------*/\r\n.content {\r\n	min-height: 3em;\r\n	overflow: hidden;\r\n	line-height: 1.5em;\r\n	font-size: 1em;\r\n	color: #333333;\r\n	padding-bottom: 1px;\r\n}\r\n\r\n.content h2, .panel h2 {\r\n	font-weight: normal;\r\n	color: #43A6DF;\r\n	font-size: 1.6em;\r\n	margin-top: 0.5em;\r\n	margin-bottom: 0.5em;\r\n	padding-bottom: 0.5em;\r\n}\r\n\r\n.panel h3 {\r\n	margin: 0.5em 0;\r\n	font-size: 1.231em;\r\n	border-bottom: solid 1px #E4E4E4;\r\n}\r\n.pm .postbody h3 {\r\n	border-bottom: none;\r\n}\r\n.panel p {\r\n	margin-bottom: 1em;\r\n	line-height: 1.4em;\r\n}\r\n#qr_editor_div {\r\n	overflow: hidden;\r\n}\r\n#qr_editor_div h2 {\r\n	margin: 0 0 12px;\r\n	border-bottom: solid 1px #E4E4E4;\r\n}\r\n.content p {\r\n	margin-bottom: 1em;\r\n	line-height: 1.4em;\r\n}\r\n#faqlinks {\r\n	overflow: hidden;\r\n}\r\ndl.faq {\r\n	margin-top: 1em;\r\n	margin-bottom: 2em;\r\n	line-height: 1.4em;\r\n}\r\n\r\ndl.faq dt {\r\n	font-weight: bold;\r\n	color: #333333;\r\n	margin-bottom: 12px;\r\n}\r\n\r\n.content dl.faq {\r\n	margin-bottom: 0.5em;\r\n}\r\n\r\n.content li {\r\n	list-style-type: inherit;\r\n}\r\n\r\n.content ul, .content ol {\r\n	margin-bottom: 1em;\r\n	margin-left: 3em;\r\n}\r\n\r\n.posthilit {\r\n	background-color: #f3f3f3;\r\n	color: #BCBCBC;\r\n	padding: 0 2px 1px 2px;\r\n}\r\n\r\n.announce, .unreadpost {\r\n	/* Highlight the announcements & unread posts box */\r\n}\r\n\r\n/* Post author */\r\np.author {\r\n	margin: 0 15em 0.6em 0;\r\n	padding: 0 0 5px 0;\r\n	line-height: 1.2em;\r\n	font-size: 0.846em;\r\n}\r\n\r\n/* Post signature */\r\n.signature {\r\n	margin-top: 24px;\r\n	padding-top: 12px;\r\n	border-top: 1px solid #E4E4E4;\r\n	clear: left;\r\n	line-height: 140%;\r\n	overflow: hidden;\r\n	width: 100%;\r\n}\r\n\r\ndd .signature {\r\n	margin: 0;\r\n	padding: 0;\r\n	clear: none;\r\n	border: none;\r\n}\r\n\r\n.signature li {\r\n	list-style-type: inherit;\r\n}\r\n\r\n.signature ul, .signature ol {\r\n	margin-bottom: 1em;\r\n	margin-left: 3em;\r\n}\r\n\r\n/* Post noticies */\r\n.notice {\r\n	font-family: \"Lucida Grande\", Verdana, Helvetica, Arial, sans-serif;\r\n	width: auto;\r\n	margin-top: 1.5em;\r\n	padding-top: 0.2em;\r\n	font-size: 1em;\r\n	border-top: 1px dashed #CCCCCC;\r\n	clear: left;\r\n	line-height: 130%;\r\n}\r\n\r\n/* Jump to post link for now */\r\nul.searchresults {\r\n	list-style: none;\r\n	text-align: right;\r\n	clear: both;\r\n}\r\n\r\n/* BB Code styles\r\n----------------------------------------*/\r\n/* Quote block */\r\nblockquote {\r\n	background: #ebebeb none 6px 8px no-repeat;\r\n	border: 1px solid #dbdbdb;\r\n	font-size: 0.95em;\r\n	margin: 0.5em 1px 0 25px;\r\n	overflow: hidden;\r\n	padding: 5px;\r\n}\r\n\r\nblockquote blockquote {\r\n	/* Nested quotes */\r\n	background-color: #bababa;\r\n	font-size: 1em;\r\n	margin: 0.5em 1px 0 15px;	\r\n}\r\n\r\nblockquote blockquote blockquote {\r\n	/* Nested quotes */\r\n	background-color: #e4e4e4;\r\n}\r\n\r\nblockquote cite {\r\n	/* Username/source of quoter */\r\n	font-style: normal;\r\n	font-weight: bold;\r\n	margin-left: 20px;\r\n	display: block;\r\n	font-size: 0.9em;\r\n}\r\n\r\nblockquote cite cite {\r\n	font-size: 1em;\r\n}\r\n\r\nblockquote.uncited {\r\n	padding-top: 25px;\r\n}\r\n\r\n/* Code block */\r\ndl.codebox {\r\n	padding: 3px;\r\n	background-color: #FFFFFF;\r\n	border: 1px solid #d8d8d8;\r\n	font-size: 1em;\r\n}\r\n\r\ndl.codebox dt {\r\n	text-transform: uppercase;\r\n	border-bottom: 1px solid #CCCCCC;\r\n	margin-bottom: 3px;\r\n	font-size: 0.8em;\r\n	font-weight: bold;\r\n	display: block;\r\n}\r\n\r\nblockquote dl.codebox {\r\n	margin-left: 0;\r\n}\r\n\r\ndl.codebox code {\r\n	/* Also see tweaks.css */\r\n	overflow: auto;\r\n	display: block;\r\n	height: auto;\r\n	max-height: 200px;\r\n	white-space: normal;\r\n	padding-top: 5px;\r\n	font: 0.9em Monaco, \"Andale Mono\",\"Courier New\", Courier, mono;\r\n	line-height: 1.3em;\r\n	color: #8b8b8b;\r\n	margin: 2px 0;\r\n}\r\n\r\n.syntaxbg		{ color: #FFFFFF; }\r\n.syntaxcomment	{ color: #000000; }\r\n.syntaxdefault	{ color: #bcbcbc; }\r\n.syntaxhtml		{ color: #000000; }\r\n.syntaxkeyword	{ color: #585858; }\r\n.syntaxstring	{ color: #a7a7a7; }\r\n\r\n/* Attachments\r\n----------------------------------------*/\r\n.attachbox {\r\n	float: left;\r\n	width: auto; \r\n	margin: 5px 5px 5px 0;\r\n	padding: 6px;\r\n	background-color: #FFFFFF;\r\n	border: 1px dashed #d8d8d8;\r\n	clear: left;\r\n}\r\n\r\n.pm-message .attachbox {\r\n	background-color: #f3f3f3;\r\n}\r\n\r\n.attachbox dt {\r\n	font-family: Arial, Helvetica, sans-serif;\r\n	text-transform: uppercase;\r\n}\r\n\r\n.attachbox dd {\r\n	margin-top: 4px;\r\n	padding-top: 4px;\r\n	clear: left;\r\n	border-top: 1px solid #d8d8d8;\r\n}\r\n\r\n.attachbox dd dd {\r\n	border: none;\r\n}\r\n\r\n.attachbox p {\r\n	line-height: 110%;\r\n	color: #666666;\r\n	font-weight: normal;\r\n	clear: left;\r\n}\r\n\r\n.attachbox p.stats\r\n{\r\n	line-height: 110%;\r\n	color: #666666;\r\n	font-weight: normal;\r\n	clear: left;\r\n}\r\n\r\n.attach-image {\r\n	margin: 3px 0;\r\n	width: 100%;\r\n	max-height: 350px;\r\n	overflow: auto;\r\n}\r\n\r\n.attach-image img {\r\n	border: 1px solid #999999;\r\n/*	cursor: move; */\r\n	cursor: default;\r\n}\r\n\r\n/* Inline image thumbnails */\r\ndiv.inline-attachment dl.thumbnail, div.inline-attachment dl.file {\r\n	display: block;\r\n	margin-bottom: 4px;\r\n}\r\n\r\ndiv.inline-attachment p {\r\n	font-size: 100%;\r\n}\r\n\r\ndl.file {\r\n	font-family: Verdana, Arial, Helvetica, sans-serif;\r\n	display: block;\r\n}\r\n\r\ndl.file dt {\r\n	text-transform: none;\r\n	margin: 0;\r\n	padding: 0;\r\n	font-weight: bold;\r\n	font-family: Verdana, Arial, Helvetica, sans-serif;\r\n}\r\n\r\ndl.file dd {\r\n	color: #666666;\r\n	margin: 0;\r\n	padding: 0;	\r\n}\r\n\r\ndl.thumbnail img {\r\n	padding: 3px;\r\n	border: 1px solid #666666;\r\n	background-color: #FFF;\r\n}\r\n\r\ndl.thumbnail dd {\r\n	color: #666666;\r\n	font-style: italic;\r\n	font-family: Verdana, Arial, Helvetica, sans-serif;\r\n}\r\n\r\n.attachbox dl.thumbnail dd {\r\n	font-size: 100%;\r\n}\r\n\r\ndl.thumbnail dt a:hover {\r\n	background-color: #EEEEEE;\r\n}\r\n\r\ndl.thumbnail dt a:hover img {\r\n	border: 1px solid #d2d2d2;\r\n}\r\n\r\n/* Post poll styles\r\n----------------------------------------*/\r\nfieldset.polls {\r\n	font-family: \"Trebuchet MS\", Verdana, Helvetica, Arial, sans-serif;\r\n}\r\n\r\nfieldset.polls dl {\r\n	margin-top: 5px;\r\n	border-top: 1px solid #e2e2e2;\r\n	padding: 5px 0 0 0;\r\n	line-height: 120%;\r\n	color: #666666;\r\n}\r\n\r\nfieldset.polls dl.voted {\r\n	font-weight: bold;\r\n	color: #000000;\r\n}\r\n\r\nfieldset.polls dt {\r\n	text-align: left;\r\n	float: left;\r\n	display: block;\r\n	width: 30%;\r\n	border-right: none;\r\n	padding: 0;\r\n	margin: 0;\r\n	font-size: 1.1em;\r\n}\r\n\r\nfieldset.polls dd {\r\n	float: left;\r\n	width: 10%;\r\n	border-left: none;\r\n	padding: 0 5px;\r\n	margin-left: 0;\r\n	font-size: 1.1em;\r\n}\r\n\r\nfieldset.polls dd.resultbar {\r\n	width: 50%;\r\n}\r\n\r\nfieldset.polls dd input {\r\n	margin: 2px 0;\r\n}\r\n\r\nfieldset.polls dd div {\r\n	text-align: right;\r\n	font-family: Arial, Helvetica, sans-serif;\r\n	color: #FFFFFF;\r\n	font-weight: bold;\r\n	padding: 0 2px;\r\n	overflow: visible;\r\n	min-width: 2%;\r\n}\r\n\r\n.pollbar1 {\r\n	background-color: #aaaaaa;\r\n	border-bottom: 1px solid #747474;\r\n	border-right: 1px solid #747474;\r\n}\r\n\r\n.pollbar2 {\r\n	background-color: #bebebe;\r\n	border-bottom: 1px solid #8c8c8c;\r\n	border-right: 1px solid #8c8c8c;\r\n}\r\n\r\n.pollbar3 {\r\n	background-color: #D1D1D1;\r\n	border-bottom: 1px solid #aaaaaa;\r\n	border-right: 1px solid #aaaaaa;\r\n}\r\n\r\n.pollbar4 {\r\n	background-color: #e4e4e4;\r\n	border-bottom: 1px solid #bebebe;\r\n	border-right: 1px solid #bebebe;\r\n}\r\n\r\n.pollbar5 {\r\n	background-color: #f8f8f8;\r\n	border-bottom: 1px solid #D1D1D1;\r\n	border-right: 1px solid #D1D1D1;\r\n}\r\n\r\n/* Poster profile block\r\n----------------------------------------*/\r\n.postprofile {\r\n	/* Also see tweaks.css */\r\n	margin: 5px 0 10px 0;\r\n	min-height: 80px;\r\n	color: #666666;\r\n	border-left: 1px solid #FFFFFF;\r\n	width: 22%;\r\n	float: right;\r\n	display: inline;\r\n}\r\n.pm .postprofile {\r\n	border-left: 1px solid #DDDDDD;\r\n}\r\n\r\n.postprofile dd, .postprofile dt {\r\n	line-height: 1.2em;\r\n	margin-left: 8px;\r\n}\r\n\r\n.postprofile strong {\r\n	font-weight: normal;\r\n	color: #000000;\r\n}\r\n\r\n.avatar {\r\n	border: none;\r\n	margin-bottom: 3px;\r\n}\r\n\r\n.online {\r\n	background-image: none;\r\n	background-position: 100% 0;\r\n	background-repeat: no-repeat;\r\n}\r\n.section-viewtopic .post .online {\r\n	position: absolute;\r\n	top: 0;\r\n	right: 0;\r\n	width: 58px;\r\n	height: 58px;\r\n}\r\n#viewprofile .panel {\r\n	overflow: hidden;\r\n}\r\n\r\n/* Poster profile used by search*/\r\n.search .postprofile {\r\n	width: 30%;\r\n}\r\n\r\n/* pm list in compose message if mass pm is enabled */\r\ndl.pmlist dt {\r\n	width: 60% !important;\r\n}\r\n\r\ndl.pmlist dt textarea {\r\n	width: 95%;\r\n}\r\n\r\ndl.pmlist dd {\r\n	margin-left: 65% !important;\r\n	margin-bottom: 2px;\r\n}\n/* Button Styles\r\n---------------------------------------- */\r\n\r\n/* Rollover buttons\r\n   Based on: http://wellstyled.com/css-nopreload-rollovers.html\r\n----------------------------------------*/\r\n.buttons {\r\n	float: left;\r\n	width: auto;\r\n	height: auto;\r\n}\r\n\r\n/* Rollover state */\r\n.buttons div {\r\n	float: left;\r\n	margin: 0 12px 0 0;\r\n	background-position: 0 100%;\r\n}\r\n\r\n/* Rolloff state */\r\n.buttons div a {\r\n	display: block;\r\n	width: 100%;\r\n	height: 100%;\r\n	background-position: 0 0;\r\n	position: relative;\r\n	overflow: hidden;\r\n}\r\n/* Hide <a> text and hide off-state image when rolling over (prevents flicker in IE) */\r\n/*.buttons div span		{ display: none; }*/\r\n/*.buttons div a:hover	{ background-image: none; }*/\r\n.buttons div span			{ position: absolute; width: 100%; height: 100%; cursor: pointer;}\r\n.buttons div a:hover span	{ background-position: 0 100%; }\r\n\r\n/* Big button images */\r\n.reply-icon span	{ background: transparent none 0 0 no-repeat; }\r\n.post-icon span		{ background: transparent none 0 0 no-repeat; }\r\n.locked-icon span	{ background: transparent none 0 0 no-repeat; }\r\n.pmreply-icon span	{ background: none 0 0 no-repeat; }\r\n.newpm-icon span 	{ background: none 0 0 no-repeat; }\r\n.forwardpm-icon span 	{ background: none 0 0 no-repeat; }\r\n\r\n/* Set big button dimensions */\r\n.buttons div.reply-icon		{ width: {IMG_BUTTON_TOPIC_REPLY_WIDTH}px; height: {IMG_BUTTON_TOPIC_REPLY_HEIGHT}px; }\r\n.buttons div.post-icon		{ width: {IMG_BUTTON_TOPIC_NEW_WIDTH}px; height: {IMG_BUTTON_TOPIC_NEW_HEIGHT}px; }\r\n.buttons div.locked-icon	{ width: {IMG_BUTTON_TOPIC_LOCKED_WIDTH}px; height: {IMG_BUTTON_TOPIC_LOCKED_HEIGHT}px; }\r\n.buttons div.pmreply-icon	{ width: {IMG_BUTTON_PM_REPLY_WIDTH}px; height: {IMG_BUTTON_PM_REPLY_HEIGHT}px; }\r\n.buttons div.newpm-icon		{ width: {IMG_BUTTON_PM_NEW_WIDTH}px; height: {IMG_BUTTON_PM_NEW_HEIGHT}px; }\r\n.buttons div.forwardpm-icon	{ width: {IMG_BUTTON_PM_FORWARD_WIDTH}px; height: {IMG_BUTTON_PM_FORWARD_HEIGHT}px; }\r\n\r\n/* Sub-header (navigation bar)\r\n--------------------------------------------- */\r\na.print, a.sendemail, a.fontsize {\r\n	display: block;\r\n	overflow: hidden;\r\n	height: 18px;\r\n	text-indent: -5000px;\r\n	text-align: left;\r\n	background-repeat: no-repeat;\r\n}\r\n\r\na.print {\r\n	background-image: none;\r\n	width: 22px;\r\n}\r\n\r\na.sendemail {\r\n	background-image: none;\r\n	width: 22px;\r\n}\r\n\r\na.fontsize {\r\n	background-image: none;\r\n	background-position: 0 -1px;\r\n	width: 29px;\r\n}\r\n\r\na.fontsize:hover {\r\n	background-position: 0 -20px;\r\n	text-decoration: none;\r\n}\r\n\r\n/* Icon images\r\n---------------------------------------- */\r\n.sitehome, .icon-faq, .icon-members, .icon-home, .icon-ucp, .icon-register, .icon-logout,\r\n.icon-bookmark, .icon-bump, .icon-subscribe, .icon-unsubscribe, .icon-pages, .icon-search {\r\n	background-position: 0 50%;\r\n	background-repeat: no-repeat;\r\n	background-image: none;\r\n	padding: 1px 0 0 17px;\r\n}\r\n\r\n/* Poster profile icons\r\n----------------------------------------*/\r\nul.profile-icons {\r\n	padding-top: 10px;\r\n	list-style: none;\r\n}\r\n\r\n/* Rollover state */\r\nul.profile-icons li {\r\n	float: left;\r\n	margin: 0 3px 3px 0;\r\n	background: none;\r\n}\r\n\r\n/* Rolloff state */\r\nul.profile-icons li a {\r\n	display: block;\r\n	width: 100%;\r\n	height: 100%;\r\n	background-position: 0 0;\r\n}\r\n\r\n/* Hide <a> text and hide off-state image when rolling over (prevents flicker in IE) */\r\nul.profile-icons li span { display:none; }\r\nul.profile-icons li a:hover { background-position: 0 100%; }\r\n\r\n/* Positioning of moderator icons */\r\n.postbody ul.profile-icons {\r\n	float: right;\r\n	width: auto;\r\n	padding: 0;\r\n}\r\n\r\n.postbody ul.profile-icons li {\r\n	margin: 0 3px;\r\n}\r\n\r\n/* Profile & navigation icons */\r\n.email-icon, .email-icon a		{ background: none top left no-repeat; }\r\n.aim-icon, .aim-icon a			{ background: none top left no-repeat; }\r\n.yahoo-icon, .yahoo-icon a		{ background: none top left no-repeat; }\r\n.web-icon, .web-icon a			{ background: none top left no-repeat; }\r\n.msnm-icon, .msnm-icon a			{ background: none top left no-repeat; }\r\n.icq-icon, .icq-icon a			{ background: none top left no-repeat; }\r\n.jabber-icon, .jabber-icon a		{ background: none top left no-repeat; }\r\n.pm-icon, .pm-icon a				{ background: none top left no-repeat; }\r\n.quote-icon, .quote-icon a		{ background: none top left no-repeat; }\r\n\r\n/* Moderator icons */\r\n.report-icon, .report-icon a		{ background: none top left no-repeat; }\r\n.warn-icon, .warn-icon a			{ background: none top left no-repeat; }\r\n.edit-icon, .edit-icon a			{ background: none top left no-repeat; }\r\n.delete-icon, .delete-icon a		{ background: none top left no-repeat; }\r\n.info-icon, .info-icon a			{ background: none top left no-repeat; }\r\n\r\n/* Set profile icon dimensions */\r\nul.profile-icons li.email-icon a		{ width: {IMG_ICON_CONTACT_EMAIL_WIDTH}px; height: {IMG_ICON_CONTACT_EMAIL_HEIGHT}px; }\r\nul.profile-icons li.aim-icon a	{ width: {IMG_ICON_CONTACT_AIM_WIDTH}px; height: {IMG_ICON_CONTACT_AIM_HEIGHT}px; }\r\nul.profile-icons li.yahoo-icon a	{ width: {IMG_ICON_CONTACT_YAHOO_WIDTH}px; height: {IMG_ICON_CONTACT_YAHOO_HEIGHT}px; }\r\nul.profile-icons li.web-icon a	{ width: {IMG_ICON_CONTACT_WWW_WIDTH}px; height: {IMG_ICON_CONTACT_WWW_HEIGHT}px; }\r\nul.profile-icons li.msnm-icon a	{ width: {IMG_ICON_CONTACT_MSNM_WIDTH}px; height: {IMG_ICON_CONTACT_MSNM_HEIGHT}px; }\r\nul.profile-icons li.icq-icon a	{ width: {IMG_ICON_CONTACT_ICQ_WIDTH}px; height: {IMG_ICON_CONTACT_ICQ_HEIGHT}px; }\r\nul.profile-icons li.jabber-icon a	{ width: {IMG_ICON_CONTACT_JABBER_WIDTH}px; height: {IMG_ICON_CONTACT_JABBER_HEIGHT}px; }\r\nul.profile-icons li.pm-icon a		{ width: {IMG_ICON_CONTACT_PM_WIDTH}px; height: {IMG_ICON_CONTACT_PM_HEIGHT}px; }\r\nul.profile-icons li.quote-icon a	{ width: {IMG_ICON_POST_QUOTE_WIDTH}px; height: {IMG_ICON_POST_QUOTE_HEIGHT}px; }\r\nul.profile-icons li.report-icon a	{ width: {IMG_ICON_POST_REPORT_WIDTH}px; height: {IMG_ICON_POST_REPORT_HEIGHT}px; }\r\nul.profile-icons li.edit-icon a	{ width: {IMG_ICON_POST_EDIT_WIDTH}px; height: {IMG_ICON_POST_EDIT_HEIGHT}px; }\r\nul.profile-icons li.delete-icon a	{ width: {IMG_ICON_POST_DELETE_WIDTH}px; height: {IMG_ICON_POST_DELETE_HEIGHT}px; }\r\nul.profile-icons li.info-icon a	{ width: {IMG_ICON_POST_INFO_WIDTH}px; height: {IMG_ICON_POST_INFO_HEIGHT}px; }\r\nul.profile-icons li.warn-icon a	{ width: {IMG_ICON_USER_WARN_WIDTH}px; height: {IMG_ICON_USER_WARN_HEIGHT}px; }\r\n\r\n/* Fix profile icon default margins */\r\nul.profile-icons li.edit-icon	{ margin: 0 0 0 3px; }\r\nul.profile-icons li.quote-icon	{ margin: 0 0 0 10px; }\r\nul.profile-icons li.info-icon, ul.profile-icons li.report-icon	{ margin: 0 3px 0 0; }\n/* Control Panel Styles\r\n---------------------------------------- */\r\n\r\n\r\n/* Main CP box\r\n----------------------------------------*/\r\n#cp-menu {\r\n	float:left;\r\n	width: 19%;\r\n	margin-top: 1em;\r\n	margin-bottom: 5px;\r\n}\r\n\r\n#cp-main {\r\n	float: left;\r\n	width: 81%;\r\n}\r\n\r\n#cp-main .content {\r\n	padding: 0;\r\n}\r\n\r\n#cp-main h3, #cp-main hr, #cp-menu hr {\r\n	border-color: #bfbfbf;\r\n}\r\n\r\n#cp-main .panel p {\r\n}\r\n\r\n#cp-main .panel ol {\r\n	margin-left: 2em;\r\n}\r\n\r\n#cp-main .panel li.row {\r\n	border-bottom: 1px solid #cbcbcb;\r\n	border-top: 1px solid #F9F9F9;\r\n}\r\n\r\nul.cplist {\r\n	margin-bottom: 5px;\r\n	border-top: 1px solid #cbcbcb;\r\n}\r\n\r\n#cp-main .panel li.header dd, #cp-main .panel li.header dt {\r\n}\r\n\r\n#cp-main table.table1 {\r\n	margin-bottom: 1em;\r\n}\r\n\r\n#cp-main table.table1 thead th {\r\n	color: #333333;\r\n	font-weight: bold;\r\n	border-bottom: 1px solid #333333;\r\n	padding: 5px;\r\n}\r\n\r\n#cp-main table.table1 tbody th {\r\n	font-style: italic;\r\n	background-color: transparent !important;\r\n	border-bottom: none;\r\n}\r\n\r\n#cp-main .pagination {\r\n	float: right;\r\n	width: auto;\r\n	padding-top: 1px;\r\n}\r\n\r\n#cp-main .postbody p {\r\n}\r\n\r\n#cp-main .pm-message {\r\n	border: 1px solid #e2e2e2;\r\n	margin: 10px 0;\r\n	background-color: #FFFFFF;\r\n	width: auto;\r\n	float: none;\r\n}\r\n\r\n.pm-message h2 {\r\n	padding-bottom: 5px;\r\n}\r\n\r\n#cp-main .postbody h3, #cp-main .box2 h3 {\r\n	margin-top: 0;\r\n}\r\n\r\n#cp-main .buttons {\r\n	margin-left: 0;\r\n	margin-bottom: 6px;\r\n}\r\n\r\n#cp-main ul.linklist {\r\n	margin: 0;\r\n}\r\n\r\n/* MCP Specific tweaks */\r\n.mcp-main .postbody {\r\n	width: 100%;\r\n}\r\n\r\n.tabs-container h2 {\r\n	float: left;\r\n	margin-bottom: 0px;\r\n}\r\n\r\n.tabs-container #minitabs {\r\n	float: right;\r\n	margin-top: 19px;\r\n}\r\n\r\n.tabs-container:after {\r\n	display: block;\r\n	clear: both;\r\n	content: \'\';\r\n}\r\n\r\n/* CP tabbed menu\r\n----------------------------------------*/\r\n#tabs {\r\n	line-height: normal;\r\n	margin: 20px 0 -1px;\r\n	min-width: 570px;\r\n}\r\n\r\n#tabs ul {\r\n	margin:0;\r\n	padding: 0;\r\n	list-style: none;\r\n}\r\n\r\n#tabs li {\r\n	display: inline;\r\n	margin: 0;\r\n	padding: 0;\r\n	font-size: 1em;\r\n	font-weight: bold;\r\n}\r\n#tabs li.activetab a span {\r\n	color: #ffffff;\r\n	background-color: #2E3539 !important;\r\n}\r\n#tabs a {\r\n	float: left;\r\n	margin: 0 1px 0 0;\r\n	padding: 0;\r\n	text-decoration: none;\r\n	position: relative;\r\n	cursor: pointer;\r\n}\r\n\r\n#tabs a span {\r\n	float: left;\r\n	display: block;\r\n	padding: 12px;\r\n	white-space: nowrap;\r\n	background-color: #43A6DF;\r\n	color: #FFFFFF;\r\n}\r\n\r\n\r\n#tabs .activetab a {\r\n	border-bottom: 1px solid #ebebeb;\r\n}\r\n\r\n#tabs .activetab a span {\r\n}\r\n\r\n#tabs a:hover {\r\n}\r\n\r\n#tabs a:hover span {\r\n	background-color: #2E3539;\r\n	color: #FFFFFF;\r\n}\r\n\r\n#tabs .activetab a:hover {\r\n	background-position: 0 0;\r\n}\r\n\r\n#tabs .activetab a:hover span {\r\n	color: #FFFFFF !important;\r\n	background-position: 100% 0;\r\n}\r\n\r\n/* Mini tabbed menu used in MCP\r\n----------------------------------------*/\r\n#minitabs {\r\n	line-height: normal;\r\n	margin: -20px 7px 0 0;\r\n}\r\n\r\n#minitabs ul {\r\n	margin:0;\r\n	padding: 0;\r\n	list-style: none;\r\n}\r\n\r\n#minitabs li {\r\n	display: block;\r\n	float: right;\r\n	padding: 0 10px 4px 10px;\r\n	font-size: 1em;\r\n	font-weight: bold;\r\n	background-color: #f2f2f2;\r\n	margin-left: 2px;\r\n}\r\n\r\n#minitabs a {\r\n}\r\n\r\n#minitabs a:hover {\r\n	text-decoration: none;\r\n}\r\n\r\n#minitabs li.activetab {\r\n	background-color: #F9F9F9;\r\n}\r\n\r\n#minitabs li.activetab a, #minitabs li.activetab a:hover {\r\n	color: #333333;\r\n}\r\n\r\n/* UCP navigation menu\r\n----------------------------------------*/\r\n/* Container for sub-navigation list */\r\n#navigation {\r\n	width: 100%;\r\n	padding-top: 37px;\r\n}\r\n\r\n#navigation ul {\r\n	list-style:none;\r\n}\r\n\r\n/* Default list state */\r\n#navigation li {\r\n	margin: 1px 0;\r\n	padding: 0;\r\n	font-weight: bold;\r\n	display: inline;\r\n}\r\n\r\n/* Link styles for the sub-section links */\r\n#navigation a {\r\n	display: block;\r\n	padding: 12px 6px;\r\n	margin: 1px 0;\r\n	text-decoration: none;\r\n	font-weight: bold;\r\n	color: #333;\r\n	background: #cfcfcf none repeat-y 100% 0;\r\n}\r\n\r\n#navigation a:hover {\r\n	text-decoration: none;\r\n	background-color: #c6c6c6;\r\n	color: #bcbcbc;\r\n	background-image: none;\r\n}\r\n\r\n#navigation #active-subsection a {\r\n	display: block;\r\n	color: #d3d3d3;\r\n	background-color: #F9F9F9;\r\n	background-image: none;\r\n}\r\n\r\n#navigation #active-subsection a:hover {\r\n	color: #d3d3d3;\r\n}\r\n\r\n/* Preferences pane layout\r\n----------------------------------------*/\r\n#cp-main h2 {\r\n	border-bottom: none;\r\n	padding: 0;\r\n	margin-left: 10px;\r\n	color: #333333;\r\n}\r\n\r\n#cp-main .panel {\r\n	overflow: hidden;\r\n}\r\n\r\n#cp-main .pm {\r\n	background-color: #FFFFFF;\r\n}\r\n\r\n#cp-main span.corners-top, #cp-menu span.corners-top {\r\n	background-image: none;\r\n}\r\n\r\n#cp-main span.corners-top span, #cp-menu span.corners-top span {\r\n	background-image: none;\r\n}\r\n\r\n#cp-main span.corners-bottom, #cp-menu span.corners-bottom {\r\n	background-image: none;\r\n}\r\n\r\n#cp-main span.corners-bottom span, #cp-menu span.corners-bottom span {\r\n	background-image: none;\r\n}\r\n\r\n/* Topicreview */\r\n#cp-main .panel #topicreview span.corners-top, #cp-menu .panel #topicreview span.corners-top {\r\n	background-image: none;\r\n}\r\n\r\n#cp-main .panel #topicreview span.corners-top span, #cp-menu .panel #topicreview span.corners-top span {\r\n	background-image: none;\r\n}\r\n\r\n#cp-main .panel #topicreview span.corners-bottom, #cp-menu .panel #topicreview span.corners-bottom {\r\n	background-image: none;\r\n}\r\n\r\n#cp-main .panel #topicreview span.corners-bottom span, #cp-menu .panel #topicreview span.corners-bottom span {\r\n	background-image: none;\r\n}\r\n\r\n/* Friends list */\r\n.cp-mini {\r\n	background-color: #f9f9f9;\r\n	padding: 0 5px;\r\n	margin: 10px 15px 10px 5px;\r\n}\r\n\r\n.cp-mini span.corners-top, .cp-mini span.corners-bottom {\r\n	margin: 0 -5px;\r\n}\r\n\r\ndl.mini dt {\r\n	font-weight: bold;\r\n	color: #676767;\r\n}\r\n\r\ndl.mini dd {\r\n	padding-top: 4px;\r\n}\r\n\r\n.friend-online {\r\n	font-weight: bold;\r\n}\r\n\r\n.friend-offline {\r\n	font-style: italic;\r\n}\r\n\r\n/* PM Styles\r\n----------------------------------------*/\r\n#pm-menu {\r\n	line-height: 2.5em;\r\n}\r\n\r\n/* PM panel adjustments */\r\n.reply-all a.left {\r\n	background-position: 3px 60%;\r\n}\r\n\r\n.reply-all a.left:hover {\r\n	background-position: 0px 60%;\r\n}\r\n\r\n.reply-all {\r\n	font-size: 11px;\r\n	padding-top: 5px;\r\n}\r\n\r\n/* PM Message history */\r\n.current {\r\n	color: #999999;\r\n}\r\n\r\n/* Defined rules list for PM options */\r\nol.def-rules {\r\n	padding-left: 0;\r\n}\r\n\r\nol.def-rules li {\r\n	line-height: 180%;\r\n	padding: 1px;\r\n}\r\n\r\n/* PM marking colours */\r\n.pmlist li.bg1 {\r\n	padding: 0 3px;\r\n}\r\n\r\n.pmlist li.bg2 {\r\n	padding: 0 3px;\r\n}\r\n\r\n.pmlist li.pm_message_reported_colour, .pm_message_reported_colour {\r\n	border-left-color: #bcbcbc;\r\n	border-right-color: #bcbcbc;\r\n}\r\n\r\n.pmlist li.pm_marked_colour, .pm_marked_colour {\r\n	padding: 0;\r\n	border: solid 3px #ffffff;\r\n	border-width: 0 3px;\r\n}\r\n\r\n.pmlist li.pm_replied_colour, .pm_replied_colour {\r\n	padding: 0;\r\n	border: solid 3px #c2c2c2;\r\n	border-width: 0 3px;\r\n}\r\n\r\n.pmlist li.pm_friend_colour, .pm_friend_colour {\r\n	padding: 0;\r\n	border: solid 3px #bdbdbd;\r\n	border-width: 0 3px;\r\n}\r\n\r\n.pmlist li.pm_foe_colour, .pm_foe_colour {\r\n	padding: 0;\r\n	border: solid 3px #000000;\r\n	border-width: 0 3px;\r\n}\r\n\r\n.pm-legend {\r\n	border-left-width: 10px;\r\n	border-left-style: solid;\r\n	border-right-width: 0;\r\n	margin-bottom: 3px;\r\n	padding-left: 3px;\r\n}\r\n\r\n/* Avatar gallery */\r\n#gallery label {\r\n	position: relative;\r\n	float: left;\r\n	margin: 10px;\r\n	padding: 5px;\r\n	width: auto;\r\n	background: #FFFFFF;\r\n	border: 1px solid #CCC;\r\n	text-align: center;\r\n}\r\n\r\n#gallery label:hover {\r\n	background-color: #EEE;\r\n}\n/* Form Styles\r\n---------------------------------------- */\r\n\r\n/* General form styles\r\n----------------------------------------*/\r\nfieldset {\r\n	border-width: 0;\r\n	font-family: \'Open Sans\', sans-serif;\r\n}\r\n\r\ninput {\r\n	font-weight: normal;\r\n	cursor: pointer;\r\n	vertical-align: middle;\r\n	padding: 0 3px;\r\n	font-family: \'Open Sans\', sans-serif;\r\n}\r\n\r\nselect {\r\n	font-family: \'Open Sans\', sans-serif;\r\n	font-weight: normal;\r\n	cursor: pointer;\r\n	vertical-align: middle;\r\n	border: 1px solid #DCDCDC;\r\n	padding: 1px;\r\n	background-color: #FAFAFA;\r\n}\r\n\r\noption {\r\n	padding-right: 1em;\r\n}\r\n\r\noption.disabled-option {\r\n	color: graytext;\r\n}\r\n\r\ntextarea {\r\n	font-family: \'Open Sans\', sans-serif;\r\n	width: 60%;\r\n	padding: 2px;\r\n	line-height: 1.4em;\r\n	font-size: 1em;\r\n}\r\n\r\nlabel {\r\n	cursor: default;\r\n	padding-right: 5px;\r\n	color: #676767;\r\n}\r\n\r\nlabel input {\r\n	vertical-align: middle;\r\n}\r\n\r\nlabel img {\r\n	vertical-align: middle;\r\n}\r\n\r\n/* Definition list layout for forms\r\n---------------------------------------- */\r\nfieldset dl {\r\n	padding: 4px 0;\r\n}\r\n\r\nfieldset dt {\r\n	float: left;	\r\n	width: 40%;\r\n	text-align: left;\r\n	display: block;\r\n}\r\n\r\nfieldset dd {\r\n	margin-left: 41%;\r\n	vertical-align: top;\r\n	margin-bottom: 3px;\r\n}\r\n\r\n/* Specific layout 1 */\r\nfieldset.fields1 dt {\r\n	width: 15em;\r\n	border-right-width: 0;\r\n}\r\n\r\nfieldset.fields1 dd {\r\n	margin-left: 15em;\r\n	border-left-width: 0;\r\n}\r\n\r\nfieldset.fields1 {\r\n	background-color: transparent;\r\n}\r\n\r\nfieldset.fields1 div {\r\n	margin-bottom: 3px;\r\n}\r\n\r\n/* Set it back to 0px for the reCaptcha divs: PHPBB3-9587 */\r\nfieldset.fields1 #recaptcha_widget_div div {\r\n	margin-bottom: 0;\r\n}\r\n\r\n/* Specific layout 2 */\r\nfieldset.fields2 dt {\r\n	width: 15em;\r\n	border-right-width: 0;\r\n}\r\n\r\nfieldset.fields2 dd {\r\n	margin-left: 16em;\r\n	border-left-width: 0;\r\n}\r\n\r\n/* Form elements */\r\ndt label {\r\n	font-weight: bold;\r\n	text-align: left;\r\n}\r\n\r\ndd label {\r\n	white-space: nowrap;\r\n	color: #333;\r\n}\r\n\r\ndd input, dd textarea {\r\n	margin-right: 3px;\r\n}\r\n\r\ndd select {\r\n	width: auto;\r\n}\r\n\r\ndd textarea {\r\n	width: 85%;\r\n}\r\n\r\n/* Hover effects */\r\nfieldset dl:hover dt label {\r\n	color: #000000;\r\n}\r\n\r\nfieldset.fields2 dl:hover dt label {\r\n	color: inherit;\r\n}\r\n\r\n#timezone {\r\n	width: 95%;\r\n}\r\n\r\n* html #timezone {\r\n	width: 50%;\r\n}\r\n\r\n/* Quick-login on index page */\r\n.homelogin {\r\n	background-color: #fff;\r\n	padding: 12px;\r\n	border: solid 1px #e4e4e4;\r\n	margin-bottom: 12px;\r\n}\r\nfieldset.quick-login {\r\n	margin-top: 5px;\r\n}\r\n\r\nfieldset.quick-login input {\r\n	width: auto;\r\n}\r\n\r\nfieldset.quick-login input.inputbox {\r\n	width: 15%;\r\n	vertical-align: middle;\r\n	margin-right: 5px;\r\n	background-color: #f3f3f3;\r\n}\r\n\r\nfieldset.quick-login label {\r\n	white-space: nowrap;\r\n	padding-right: 2px;\r\n}\r\n\r\n/* Display options on viewtopic/viewforum pages  */\r\nfieldset.display-options {\r\n	text-align: center;\r\n	margin: 3px 0 12px 0;\r\n}\r\n\r\nfieldset.display-options label {\r\n	white-space: nowrap;\r\n	padding-right: 2px;\r\n}\r\n\r\nfieldset.display-options a {\r\n	margin-top: 3px;\r\n}\r\n\r\n/* Display actions for ucp and mcp pages */\r\nfieldset.display-actions {\r\n	text-align: right;\r\n	line-height: 2em;\r\n	white-space: nowrap;\r\n	padding-right: 1em;\r\n}\r\n\r\nfieldset.display-actions label {\r\n	white-space: nowrap;\r\n	padding-right: 2px;\r\n}\r\n\r\nfieldset.sort-options {\r\n	line-height: 2em;\r\n}\r\n\r\n/* MCP forum selection*/\r\nfieldset.forum-selection {\r\n	margin: 5px 0 3px 0;\r\n	float: right;\r\n}\r\n\r\nfieldset.forum-selection2 {\r\n	margin: 13px 0 3px 0;\r\n	float: right;\r\n}\r\n\r\n/* Jumpbox */\r\nfieldset.jumpbox {\r\n	text-align: right;\r\n	margin-top: 12px;\r\n	margin-bottom: 12px;\r\n}\r\n\r\nfieldset.quickmod {\r\n	width: 50%;\r\n	float: right;\r\n	text-align: right;\r\n	height: 2.5em;\r\n}\r\n\r\n/* Submit button fieldset */\r\nfieldset.submit-buttons {\r\n	text-align: center;\r\n	vertical-align: middle;\r\n	margin: 5px 0;\r\n}\r\n\r\nfieldset.submit-buttons input {\r\n	vertical-align: middle;\r\n	padding-top: 3px;\r\n	padding-bottom: 3px;\r\n}\r\n\r\n/* Posting page styles\r\n----------------------------------------*/\r\n\r\n/* Buttons used in the editor */\r\n#format-buttons {\r\n	margin: 15px 0 2px 0;\r\n}\r\n\r\n#format-buttons input, #format-buttons select {\r\n	vertical-align: middle;\r\n}\r\n\r\n/* Main message box */\r\n#message-box {\r\n	width: 80%;\r\n}\r\n\r\n#message-box textarea {\r\n	width: 450px;\r\n	height: 270px;\r\n	min-width: 100%;\r\n	max-width: 100%;\r\n	color: #333333;\r\n}\r\n\r\n/* Emoticons panel */\r\n#smiley-box {\r\n	width: 17%;\r\n	float: right;\r\n}\r\n\r\n#smiley-box img {\r\n	margin: 3px;\r\n}\r\n\r\n/* Input field styles\r\n---------------------------------------- */\r\n.inputbox {\r\n	background-color: #FFFFFF;\r\n	border: 1px solid #c0c0c0;\r\n	color: #333333;\r\n	padding: 2px;\r\n	cursor: text;\r\n}\r\n\r\n.inputbox:hover {\r\n	border: 1px solid #eaeaea;\r\n}\r\n\r\n.inputbox:focus {\r\n	border: 1px solid #eaeaea;\r\n	color: #4b4b4b;\r\n}\r\n\r\ninput.inputbox	{ width: 85%; }\r\ninput.medium	{ width: 50%; }\r\ninput.narrow	{ width: 25%; }\r\ninput.tiny		{ width: 125px; }\r\n\r\ntextarea.inputbox {\r\n	width: 85%;\r\n}\r\n\r\n.autowidth {\r\n	width: auto !important;\r\n}\r\n\r\n/* Form button styles\r\n---------------------------------------- */\r\ninput.button1, input.button2 {\r\n	font-size: 1em;\r\n}\r\n\r\na.button1, input.button1, input.button3, a.button2, input.button2 {\r\n	width: auto !important;\r\n	padding: 3px 12px;\r\n	font-family: \'Open Sans\', sans-serif;\r\n	color: #000;\r\n	background: #FAFAFA none repeat-x top left;\r\n	border: 0;\r\n}\r\n\r\na.button1, input.button1 {\r\n}\r\n\r\ninput.button3 {\r\n	padding: 0;\r\n	margin: 0;\r\n	line-height: 5px;\r\n	height: 12px;\r\n	background-image: none;\r\n	font-variant: small-caps;\r\n}\r\n\r\n/* Alternative button */\r\na.button2, input.button2, input.button3 {\r\n	border: 0;\r\n}\r\n\r\n/* <a> button in the style of the form buttons */\r\na.button1, a.button1:link, a.button1:visited, a.button1:active, a.button2, a.button2:link, a.button2:visited, a.button2:active {\r\n	text-decoration: none;\r\n	color: #000000;\r\n	padding: 6px 12px;\r\n	line-height: 250%;\r\n	vertical-align: text-bottom;\r\n	background-position: 0 2px;\r\n}\r\n\r\n/* Hover states */\r\na.button1:hover, input.button1:hover, a.button2:hover, input.button2:hover, input.button3:hover {\r\n	/*border: 1px solid #BCBCBC;*/\r\n	background-color: #2E3539;\r\n	color: #BCBCBC;\r\n}\r\n\r\ninput.disabled {\r\n	font-weight: normal;\r\n	color: #666666;\r\n}\r\n\r\n/* Topic and forum Search */\r\n.search-box {\r\n	float: left;\r\n}\r\n\r\n.search-box .inputbox:hover,\r\n.search-box .inputbox:focus {\r\n	border: 0;\r\n}\r\n\r\ninput.search {\r\n	background-image: none;\r\n	background-repeat: no-repeat;\r\n	background-position: left 1px;\r\n	padding-left: 6px;\r\n}\r\n\r\n.full { width: 95%; }\r\n.medium { width: 50%;}\r\n.narrow { width: 25%;}\r\n.tiny { width: 10%;}\n/* Style Sheet Tweaks\n\nThese style definitions are mainly IE specific \ntweaks required due to its poor CSS support.\n-------------------------------------------------*/\n\n* html table, * html select, * html input { font-size: 100%; }\n* html hr { margin: 0; }\n* html span.corners-top, * html span.corners-bottom { background-image: url(\"{T_THEME_PATH}/images/corners_left.gif\"); }\n* html span.corners-top span, * html span.corners-bottom span { background-image: url(\"{T_THEME_PATH}/images/corners_right.gif\"); }\n\ntable.table1 {\n	width: 99%;		/* IE < 6 browsers */\n	/* Tantek hack */\n	voice-family: \"\\\"}\\\"\";\n	voice-family: inherit;\n	width: 100%;\n}\nhtml>body table.table1 { width: 100%; }	/* Reset 100% for opera */\n\n* html ul.topiclist li { position: relative; }\n* html .postbody h3 img { vertical-align: middle; }\n\n/* Form styles */\nhtml>body dd label input { vertical-align: text-bottom; }	/* Align checkboxes/radio buttons nicely */\n\n* html input.button1, * html input.button2 {\n	padding-bottom: 0;\n	margin-bottom: 1px;\n}\n\n/* Misc layout styles */\n* html .column1, * html .column2 { width: 45%; }\n\n/* Nice method for clearing floated blocks without having to insert any extra markup (like spacer above)\n   From http://www.positioniseverything.net/easyclearing.html \n#tabs:after, #minitabs:after, .post:after, .navbar:after, fieldset dl:after, ul.topiclist dl:after, ul.linklist:after, dl.polls:after {\n	content: \".\"; \n	display: block; \n	height: 0; \n	clear: both; \n	visibility: hidden;\n}*/\n\n.clearfix, #tabs, #minitabs, fieldset dl, ul.topiclist dl, dl.polls {\n	height: 1%;\n	overflow: hidden;\n}\n\n/* viewtopic fix */\n* html .post {\n	height: 25%;\n	overflow: hidden;\n}\n\n/* navbar fix */\n* html .clearfix, * html .navbar, ul.linklist {\n	height: 4%;\n	overflow: hidden;\n}\n\n/* Simple fix so forum and topic lists always have a min-height set, even in IE6\n	From http://www.dustindiaz.com/min-height-fast-hack */\ndl.icon {\n	min-height: 35px;\n	height: auto !important;\n	height: 35px;\n}\n\n* html li.row dl.icon dt {\n	height: 35px;\n	overflow: visible;\n}\n\n* html #search-box {\n	width: 25%;\n}\n\n/* Correctly clear floating for details on profile view */\n*:first-child+html dl.details dd {\n	margin-left: 30%;\n	float: none;\n}\n\n* html dl.details dd {\n	margin-left: 30%;\n	float: none;\n}\n\n/* Headerbar height fix for IE7 and below */\n* html #site-description p {\n	margin-bottom: 1.0em;\n}\n\n*:first-child+html #site-description p {\n	margin-bottom: 1.0em;\n}\n\n/* #minitabs fix for IE */\n.tabs-container {\n	zoom: 1;\n}\n\n#minitabs {\n	white-space: nowrap;\n	*min-width: 50%;\n}\n/*  	\r\n--------------------------------------------------------------\r\nColours and backgrounds for common.css\r\n-------------------------------------------------------------- */\r\n\r\nhtml, body {\r\n	color: #333333;\r\n	background-color: #F6F6F6;\r\n}\r\n\r\nh1 {\r\n	color: #FFFFFF;\r\n}\r\n\r\nh2 {\r\n	color: #3E3E3E;\r\n}\r\n\r\nh3 {\r\n	color: #3E3E3E;\r\n}\r\n\r\nhr {\r\n	border-color: #E4E4E4;\r\n}\r\n\r\nhr.dashed {\r\n	border-top-color: #E4E4E4;\r\n}\r\n\r\n/* Search box\r\n--------------------------------------------- */\r\n\r\n.search-box {\r\n	color: #FFFFFF;\r\n}\r\n\r\n.search-box #keywords {\r\n	background-color: #FFF;\r\n}\r\n\r\n.search-box input {\r\n	border: 0;\r\n}\r\n.search-box .search-box-inner .button2 {\r\n    border: 0;\r\n    cursor: pointer;\r\n    float: right;\r\n    height: 26px;\r\n    padding: 0;\r\n    text-indent: -9999px;\r\n    width: 26px !important;	\r\n	\r\n    background-attachment: scroll;\r\n    background-image: url(\"{T_THEME_PATH}/images/search-button-bg.png\");\r\n    background-position: left top;	\r\n}\r\n.search-box .search-box-inner .button2:hover {\r\n    background-position: left bottom;	\r\n}\r\n\r\n\r\n/* Round cornered boxes and backgrounds\r\n---------------------------------------- */\r\n.headerbar {\r\n	background-color: #12A3EB;\r\n	/*background-image: url(\"{T_THEME_PATH}/images/bg_header.gif\");*/\r\n	color: #FFFFFF;\r\n}\r\n\r\n.navbar {\r\n	/*background-color: #cadceb;*/\r\n}\r\n\r\n.forabg {\r\n	background-color: #43A6DF;\r\n}\r\n\r\n.forumbg {\r\n	background-color: #43A6DF;\r\n}\r\n\r\n.panel {\r\n	background-color: #FFFFFF;\r\n	border: solid 1px #E4E4E4;\r\n	padding: 12px;\r\n}\r\n\r\n.post:target .content {\r\n	color: #000000;\r\n}\r\n\r\n.post:target h3 a {\r\n	color: #000000;\r\n}\r\n\r\n.bg1	{ background-color: #fff; overflow: hidden; }\r\n.bg2	{ background-color: #fff; overflow: hidden; }\r\n.bg3	{ background-color: #fff; overflow: hidden; }\r\n\r\n.ucprowbg {\r\n	background-color: #DCDEE2;\r\n}\r\n\r\n.fieldsbg {\r\n	background-color: #E7E8EA;\r\n}\r\n\r\nspan.corners-top {\r\n	background-image: url(\"{T_THEME_PATH}/images/corners_left.png\");\r\n}\r\n\r\nspan.corners-top span {\r\n	background-image: url(\"{T_THEME_PATH}/images/corners_right.png\");\r\n}\r\n\r\nspan.corners-bottom {\r\n	background-image: url(\"{T_THEME_PATH}/images/corners_left.png\");\r\n}\r\n\r\nspan.corners-bottom span {\r\n	background-image: url(\"{T_THEME_PATH}/images/corners_right.png\");\r\n}\r\n\r\n/* Horizontal lists\r\n----------------------------------------*/\r\n\r\nul.navlinks {\r\n	border-bottom-color: #FFFFFF;\r\n}\r\n\r\n/* Table styles\r\n----------------------------------------*/\r\ntable.table1 thead th {\r\n	color: #FFFFFF;\r\n}\r\n\r\ntable.table1 tbody tr {\r\n	border-color: #E4E4E4;\r\n}\r\n\r\ntable.table1 tbody tr:hover, table.table1 tbody tr.hover {\r\n	background-color: #F2F2F2;\r\n	color: #000;\r\n}\r\n\r\ntable.table1 td {\r\n	color: #43A6DF;\r\n}\r\n\r\ntable.table1 tbody td {\r\n	border-top-color: #E4E4E4;\r\n}\r\n\r\ntable.table1 tbody th {\r\n	border-bottom-color: #E4E4E4;\r\n	color: #333333;\r\n	background-color: #FFFFFF;\r\n}\r\n\r\ntable.info tbody th {\r\n	color: #000000;\r\n}\r\n\r\n#memberlist, .table-list {\r\n	border-collapse: collapse;\r\n}\r\n#memberlist th, .table-list th {\r\n	background-color: #2E3539;\r\n	padding: 14px 0;\r\n}\r\n#memberlist th.name, .table-list th.name {\r\n	padding-left: 6px;\r\n}\r\n/* Misc layout styles\r\n---------------------------------------- */\r\ndl.details dt {\r\n	color: #000000;\r\n	font-weight: bold;\r\n}\r\n\r\ndl.details dd {\r\n}\r\n\r\n.sep {\r\n	color: #43A6DF;\r\n}\r\n\r\n/* Pagination\r\n---------------------------------------- */\r\n\r\n.pagination span strong {\r\n	color: #FFFFFF;\r\n	background-color: #2E3539;\r\n	border-color: #2E3539;\r\n}\r\n\r\n.pagination span a, .pagination span a:link, .pagination span a:visited {\r\n	background-color: #FFFFFF;\r\n	border-color: #E4E4E4;\r\n}\r\n\r\n.pagination span a:hover {\r\n	border-color: #43A6DF;\r\n	background-color: #43A6DF;\r\n	color: #FFF;\r\n}\r\n\r\n.pagination span a:active {\r\n	color: #5C758C;\r\n	background-color: #ECEDEE;\r\n	border-color: #B4BAC0;\r\n}\r\n\r\n/* Pagination in viewforum for multipage topics */\r\n.row .pagination {\r\n	background-image: url(\"{T_THEME_PATH}/images/icon_pages.png\");\r\n}\r\n\r\n.row .pagination span a, li.pagination span a {\r\n	background-color: #FFFFFF;\r\n}\r\n\r\n.row .pagination span a:hover, li.pagination span a:hover {\r\n	background-color: #368AD2;\r\n}\r\n\r\n/* Miscellaneous styles\r\n---------------------------------------- */\r\n\r\n.copyright {\r\n	color: #555555;\r\n}\r\n\r\n.error {\r\n	color: #BC2A4D;\r\n}\r\n\r\n.reported {\r\n	\r\n}\r\n.post.reported, .row.reported {\r\n	border: solid 3px #FF5500;\r\n}\r\n\r\nli.reported:hover {\r\n	\r\n}\r\n.sticky, .announce {\r\n	/* you can add a background for stickies and announcements*/\r\n}\r\n\r\ndiv.rules {\r\n	background-color: #2E3539;\r\n	color: #FFFFFF;\r\n	background-attachment: scroll;\r\n	background-image: url(\"{T_THEME_PATH}/images/notice-exclamation.png\");\r\n	background-repeat: no-repeat;\r\n	background-position: 12px 12px;\r\n	padding-left: 60px;\r\n}\r\ndiv.rules a {\r\n	color: #43A6DF;\r\n	font-weight: bold;\r\n	text-decoration: underline;\r\n}\r\ndiv.rules a:hover {\r\n	text-decoration: none;\r\n}\r\n\r\np.rules {\r\n	background-image: none;\r\n}\r\n\r\n/*  	\r\n--------------------------------------------------------------\r\nColours and backgrounds for links.css\r\n-------------------------------------------------------------- */\r\n\r\na:link	{ color: #43A6DF; }\r\na:visited	{ color: #43A6DF; }\r\na:hover	{ color: #43A6DF; }\r\na:active	{ color: #43A6DF; }\r\n\r\n/* Links on gradient backgrounds */\r\n.search-box a:link, .navbg a:link, .forumbg .header a:link, .forabg .header a:link, th a:link {\r\n	color: #43A6DF;\r\n}\r\n\r\n.search-box a:visited, .navbg a:visited, .forumbg .header a:visited, .forabg .header a:visited, th a:visited {\r\n	color: #43A6DF;\r\n}\r\n\r\n.search-box a:hover, .navbg a:hover, .forumbg .header a:hover, .forabg .header a:hover, th a:hover {\r\n	color: #43A6DF;\r\n}\r\n\r\n.search-box a:active, .navbg a:active, .forumbg .header a:active, .forabg .header a:active, th a:active {\r\n	color: #43A6DF;\r\n}\r\n\r\n/* Links for forum/topic lists */\r\na.forumtitle {\r\n	color: #2E3539;\r\n}\r\n\r\n/* a.forumtitle:visited { color: #105289; } */\r\n\r\na.forumtitle:hover {\r\n	color: #2E3539;\r\n}\r\n\r\na.forumtitle:active {\r\n	color: #2E3539;\r\n}\r\n\r\na.topictitle {\r\n	color: #43A6DF;\r\n}\r\n\r\n/* a.topictitle:visited { color: #368AD2; } */\r\n\r\na.topictitle:hover {\r\n	color: #43A6DF;\r\n}\r\n\r\na.topictitle:active {\r\n	color: #43A6DF;\r\n}\r\n\r\n/* Post body links */\r\n.postlink {\r\n	color: #43A6DF;\r\n	border-bottom-color: #43A6DF;\r\n}\r\n\r\n.postlink:visited {\r\n	color: #43A6DF;\r\n	border-bottom-color: #43A6DF;\r\n}\r\n\r\n.postlink:active {\r\n	color: #43A6DF;\r\n}\r\n\r\n.postlink:hover {\r\n	background-color: transparent;\r\n	color: #43A6DF;\r\n}\r\n\r\n.signature a, .signature a:visited, .signature a:hover, .signature a:active {\r\n	background-color: transparent;\r\n}\r\n\r\n/* Profile links */\r\n.postprofile a:link, .postprofile a:visited, .postprofile dt.author a {\r\n	color: #43A6DF;\r\n}\r\n\r\n.postprofile a:hover, .postprofile dt.author a:hover {\r\n	color: #43A6DF;\r\n}\r\n\r\n.postprofile a:active {\r\n	color: #43A6DF;\r\n}\r\n\r\n/* Profile searchresults */	\r\n.search .postprofile a {\r\n	color: #43A6DF;\r\n}\r\n\r\n.search .postprofile a:hover {\r\n	color: #43A6DF;\r\n}\r\n\r\n/* Back to top of page */\r\na.top {\r\n	background-image: url(\"{IMG_ICON_BACK_TOP_SRC}\");\r\n}\r\n\r\na.top2 {\r\n	background-image: url(\"{IMG_ICON_BACK_TOP_SRC}\");\r\n}\r\n\r\n/* Arrow links  */\r\na.up		{ background-image: url(\"{T_THEME_PATH}/images/arrow_up.png\") }\r\na.down		{ background-image: url(\"{T_THEME_PATH}/images/arrow_down.png\") }\r\na.left		{ background-image: url(\"{T_THEME_PATH}/images/arrow_left.png\") }\r\na.right		{ background-image: url(\"{T_THEME_PATH}/images/arrow_right.png\") }\r\n\r\na.up:hover {\r\n	background-color: transparent;\r\n	color: #43A6DF;\r\n}\r\n\r\na.left:hover {\r\n	text-decoration: underline;\r\n	color: #43A6DF;\r\n}\r\n\r\na.right:hover {\r\n	text-decoration: underline;\r\n	color: #43A6DF;\r\n}\r\n\r\n\r\n/*  	\r\n--------------------------------------------------------------\r\nColours and backgrounds for content.css\r\n-------------------------------------------------------------- */\r\n\r\nul.forums {\r\n	background-color: #ffffff;\r\n}\r\n.ucp-main ul.topiclist {\r\n	background-color: #2E3539;\r\n	color: #fff;\r\n	clear: both;\r\n}\r\nul.topiclist li {\r\n	\r\n}\r\n\r\nul.topiclist dd {\r\n	border-left-color: #FFFFFF;\r\n}\r\n\r\n.rtl ul.topiclist dd {\r\n	border-right-color: #fff;\r\n	border-left-color: transparent;\r\n}\r\n\r\nul.topiclist li.row dt a.subforum.read {\r\n	background-image: url(\"{IMG_SUBFORUM_READ_SRC}\");\r\n}\r\n\r\nul.topiclist li.row dt a.subforum.unread {\r\n	background-image: url(\"{IMG_SUBFORUM_UNREAD_SRC}\");\r\n}\r\n\r\nli.row {\r\n	\r\n}\r\n\r\nli.row strong {\r\n	color: #000000;\r\n}\r\n\r\nli.row:hover {\r\n	background-color: #F2F2F2;\r\n}\r\n\r\nli.row:hover dd {\r\n	\r\n}\r\n\r\n.rtl li.row:hover dd {\r\n	\r\n}\r\n\r\nli.header dt, li.header dd {\r\n	color: #FFFFFF;\r\n}\r\n\r\n/* Forum list column styles */\r\nul.topiclist dd.searchextra {\r\n	color: #333333;\r\n}\r\n.mcp-main .header {\r\n	background-color: #2E3539;\r\n    color: #FFFFFF;\r\n}\r\n/* Post body styles\r\n----------------------------------------*/\r\n.postbody {\r\n	color: #333333;\r\n}\r\n\r\n/* Content container styles\r\n----------------------------------------*/\r\n.content {\r\n	color: #333333;\r\n}\r\n\r\n.content h2, .panel h2 {\r\n	border-bottom-color:  #E4E4E4;\r\n}\r\n\r\ndl.faq dt {\r\n	color: #333333;\r\n}\r\n\r\n.posthilit {\r\n	background-color: #F3BFCC;\r\n	color: #BC2A4D;\r\n}\r\n\r\n/* Post signature */\r\n.signature {\r\n	border-top-color: #E4E4E4;\r\n}\r\n\r\n/* Post noticies */\r\n.notice {\r\n	border-top-color:  #E4E4E4;\r\n}\r\n\r\n/* BB Code styles\r\n----------------------------------------*/\r\n/* Quote block */\r\nblockquote {\r\n	background-color: #F7F7F7;\r\n	background-image: url(\"{T_THEME_PATH}/images/quote.gif\");\r\n	border-color:#E4E4E4;\r\n}\r\n\r\n.rtl blockquote {\r\n	background-image: url(\"{T_THEME_PATH}/images/quote_rtl.gif\");\r\n}\r\n\r\nblockquote blockquote {\r\n	/* Nested quotes */\r\n	background-color:#EFEED9;\r\n}\r\n\r\nblockquote blockquote blockquote {\r\n	/* Nested quotes */\r\n	background-color: #EBEADD;\r\n}\r\n\r\n/* Code block */\r\ndl.codebox {\r\n	background-color: #FFFFFF;\r\n	border-color: #C9D2D8;\r\n}\r\n\r\ndl.codebox dt {\r\n	border-bottom-color:  #CCCCCC;\r\n}\r\n\r\ndl.codebox code {\r\n	color: #2E8B57;\r\n}\r\n\r\n.syntaxbg		{ color: #FFFFFF; }\r\n.syntaxcomment	{ color: #FF8000; }\r\n.syntaxdefault	{ color: #0000BB; }\r\n.syntaxhtml		{ color: #000000; }\r\n.syntaxkeyword	{ color: #007700; }\r\n.syntaxstring	{ color: #DD0000; }\r\n\r\n/* Attachments\r\n----------------------------------------*/\r\n.attachbox {\r\n	background-color: #FFFFFF;\r\n	border-color:  #C9D2D8;\r\n}\r\n\r\n.pm-message .attachbox {\r\n	background-color: #F2F3F3;\r\n}\r\n\r\n.attachbox dd {\r\n	border-top-color: #E4E4E4;\r\n}\r\n\r\n.attachbox p {\r\n	color: #666666;\r\n}\r\n\r\n.attachbox p.stats {\r\n	color: #666666;\r\n}\r\n\r\n.attach-image img {\r\n	border-color: #999999;\r\n}\r\n\r\n/* Inline image thumbnails */\r\n\r\ndl.file dd {\r\n	color: #666666;\r\n}\r\n\r\ndl.thumbnail img {\r\n	border-color: #666666;\r\n	background-color: #FFFFFF;\r\n}\r\n\r\ndl.thumbnail dd {\r\n	color: #666666;\r\n}\r\n\r\ndl.thumbnail dt a:hover {\r\n	background-color: #EEEEEE;\r\n}\r\n\r\ndl.thumbnail dt a:hover img {\r\n	border-color: #368AD2;\r\n}\r\n\r\n/* Post poll styles\r\n----------------------------------------*/\r\n\r\nfieldset.polls dl {\r\n	border-top-color: #DCDEE2;\r\n	color: #666666;\r\n}\r\n\r\nfieldset.polls dl.voted {\r\n	color: #000000;\r\n}\r\n\r\nfieldset.polls dd div {\r\n	color: #FFFFFF;\r\n}\r\n\r\n.rtl .pollbar1, .rtl .pollbar2, .rtl .pollbar3, .rtl .pollbar4, .rtl .pollbar5 {\r\n	border-right-color: transparent;\r\n}\r\n\r\n.pollbar1 {\r\n	background-color: #AA2346;\r\n	border-bottom-color: #74162C;\r\n	border-right-color: #74162C;\r\n}\r\n\r\n.rtl .pollbar1 {\r\n	border-left-color: #74162C;\r\n}\r\n\r\n.pollbar2 {\r\n	background-color: #BE1E4A;\r\n	border-bottom-color: #8C1C38;\r\n	border-right-color: #8C1C38;\r\n}\r\n\r\n.rtl .pollbar2 {\r\n	border-left-color: #8C1C38;\r\n}\r\n\r\n.pollbar3 {\r\n	background-color: #D11A4E;\r\n	border-bottom-color: #AA2346;\r\n	border-right-color: #AA2346;\r\n}\r\n\r\n.rtl .pollbar3 {\r\n	border-left-color: #AA2346;\r\n}\r\n\r\n.pollbar4 {\r\n	background-color: #E41653;\r\n	border-bottom-color: #BE1E4A;\r\n	border-right-color: #BE1E4A;\r\n}\r\n\r\n.rtl .pollbar4 {\r\n	border-left-color: #BE1E4A;\r\n}\r\n\r\n.pollbar5 {\r\n	background-color: #F81157;\r\n	border-bottom-color: #D11A4E;\r\n	border-right-color: #D11A4E;\r\n}\r\n\r\n.rtl .pollbar5 {\r\n	border-left-color: #D11A4E;\r\n}\r\n\r\n/* Poster profile block\r\n----------------------------------------*/\r\n.postprofile {\r\n	color: #666666;\r\n	border-left-color: #FFFFFF;\r\n}\r\n\r\n.rtl .postprofile {\r\n	border-right-color: #FFFFFF;\r\n	border-left-color: transparent;\r\n}\r\n\r\n.pm .postprofile {\r\n	border-left-color: #DDDDDD;\r\n}\r\n\r\n.rtl .pm .postprofile {\r\n	border-right-color: #DDDDDD;\r\n	border-left-color: transparent;\r\n}\r\n\r\n.postprofile strong {\r\n	color: #000000;\r\n}\r\n\r\n.online {\r\n	background-image: url(\"{IMG_ICON_USER_ONLINE_SRC}\");\r\n}\r\n\r\n/*  	\r\n--------------------------------------------------------------\r\nColours and backgrounds for buttons.css\r\n-------------------------------------------------------------- */\r\n\r\n/* Big button images */\r\n.reply-icon span	{ background-image: url(\"{IMG_BUTTON_TOPIC_REPLY_SRC}\"); }\r\n.post-icon span		{ background-image: url(\"{IMG_BUTTON_TOPIC_NEW_SRC}\"); }\r\n.locked-icon span	{ background-image: url(\"{IMG_BUTTON_TOPIC_LOCKED_SRC}\"); }\r\n.pmreply-icon span	{ background-image: url(\"{IMG_BUTTON_PM_REPLY_SRC}\") ;}\r\n.newpm-icon span 	{ background-image: url(\"{IMG_BUTTON_PM_NEW_SRC}\") ;}\r\n.forwardpm-icon span	{ background-image: url(\"{IMG_BUTTON_PM_FORWARD_SRC}\") ;}\r\n\r\na.print {\r\n	background-image: url(\"{T_THEME_PATH}/images/icon_print.png\");\r\n}\r\n\r\na.sendemail {\r\n	background-image: url(\"{T_THEME_PATH}/images/icon_sendemail.png\");\r\n}\r\n\r\na.fontsize {\r\n	background-image: url(\"{T_THEME_PATH}/images/icon_fontsize.gif\");\r\n}\r\n\r\n/* Icon images\r\n---------------------------------------- */\r\n.sitehome						{ background-image: url(\"{T_THEME_PATH}/images/icon_home.png\"); }\r\n/*.icon-faq						{ background-image: url(\"{T_THEME_PATH}/images/icon_faq.gif\"); }*/\r\n/*.icon-members					{ background-image: url(\"{T_THEME_PATH}/images/icon_members.gif\"); }*/\r\n.icon-home						{ background-image: url(\"{T_THEME_PATH}/images/icon_home.png\"); }\r\n/*.icon-ucp						{ background-image: url(\"{T_THEME_PATH}/images/icon_ucp.gif\"); }*/\r\n/*.icon-register					{ background-image: url(\"{T_THEME_PATH}/images/icon_register.gif\"); }*/\r\n/*.icon-logout					{ background-image: url(\"{T_THEME_PATH}/images/icon_logout.gif\"); }*/\r\n.icon-bookmark					{ background-image: url(\"{T_THEME_PATH}/images/icon_bookmark.png\"); }\r\n.icon-bump						{ background-image: url(\"{T_THEME_PATH}/images/icon_bump.gif\"); }\r\n.icon-subscribe					{ background-image: url(\"{T_THEME_PATH}/images/icon_subscribe.png\"); }\r\n.icon-unsubscribe				{ background-image: url(\"{T_THEME_PATH}/images/icon_unsubscribe.png\"); }\r\n/*.icon-pages						{ background-image: url(\"{T_THEME_PATH}/images/icon_pages.gif\"); }*/\r\n/*.icon-search					{ background-image: url(\"{T_THEME_PATH}/images/icon_search.gif\"); }*/\r\n\r\n/* Profile & navigation icons */\r\n.email-icon, .email-icon a		{ background-image: url(\"{IMG_ICON_CONTACT_EMAIL_SRC}\"); }\r\n.aim-icon, .aim-icon a			{ background-image: url(\"{IMG_ICON_CONTACT_AIM_SRC}\"); }\r\n.yahoo-icon, .yahoo-icon a		{ background-image: url(\"{IMG_ICON_CONTACT_YAHOO_SRC}\"); }\r\n.web-icon, .web-icon a			{ background-image: url(\"{IMG_ICON_CONTACT_WWW_SRC}\"); }\r\n.msnm-icon, .msnm-icon a			{ background-image: url(\"{IMG_ICON_CONTACT_MSNM_SRC}\"); }\r\n.icq-icon, .icq-icon a			{ background-image: url(\"{IMG_ICON_CONTACT_ICQ_SRC}\"); }\r\n.jabber-icon, .jabber-icon a		{ background-image: url(\"{IMG_ICON_CONTACT_JABBER_SRC}\"); }\r\n.pm-icon, .pm-icon a				{ background-image: url(\"{IMG_ICON_CONTACT_PM_SRC}\"); }\r\n.quote-icon, .quote-icon a		{ background-image: url(\"{IMG_ICON_POST_QUOTE_SRC}\"); }\r\n\r\n/* Moderator icons */\r\n.report-icon, .report-icon a		{ background-image: url(\"{IMG_ICON_POST_REPORT_SRC}\"); }\r\n.edit-icon, .edit-icon a			{ background-image: url(\"{IMG_ICON_POST_EDIT_SRC}\"); }\r\n.delete-icon, .delete-icon a		{ background-image: url(\"{IMG_ICON_POST_DELETE_SRC}\"); }\r\n.info-icon, .info-icon a			{ background-image: url(\"{IMG_ICON_POST_INFO_SRC}\"); }\r\n.warn-icon, .warn-icon a			{ background-image: url(\"{IMG_ICON_USER_WARN_SRC}\"); } /* Need updated warn icon */\r\n\r\n/*  	\r\n--------------------------------------------------------------\r\nColours and backgrounds for cp.css\r\n-------------------------------------------------------------- */\r\n\r\n/* Main CP box\r\n----------------------------------------*/\r\n\r\n#cp-main h3, #cp-main hr, #cp-menu hr {\r\n	border-color: #E4E4E4;\r\n}\r\n\r\n#cp-main .panel li.row {\r\n	border-bottom-color: #E4E4E4;\r\n}\r\n\r\nul.cplist {\r\n	border-top-color: #E4E4E4;\r\n}\r\n\r\n#cp-main .panel li.header dd, #cp-main .panel li.header dt {\r\n	\r\n}\r\n\r\n#cp-main table.table1 thead th {\r\n	border-bottom-color: #E4E4E4;\r\n}\r\n\r\n#cp-main .pm-message {\r\n	border-color: #E4E4E4;\r\n	background-color: #FFFFFF;\r\n}\r\n\r\n/* CP tabbed menu\r\n----------------------------------------*/\r\n#tabs a {\r\n	\r\n}\r\n\r\n#tabs a span {\r\n	\r\n}\r\n\r\n#tabs a:hover span {\r\n	\r\n}\r\n\r\n#tabs .activetab a {\r\n	\r\n}\r\n\r\n#tabs .activetab a span {\r\n	color: #333333;\r\n}\r\n\r\n#tabs .activetab a:hover span {\r\n	color: #000000;\r\n}\r\n\r\n/* Mini tabbed menu used in MCP\r\n----------------------------------------*/\r\n#minitabs li {\r\n	background-color: #E1EBF2;\r\n}\r\n\r\n#minitabs li.activetab {\r\n	background-color: #F9F9F9;\r\n}\r\n\r\n#minitabs li.activetab a, #minitabs li.activetab a:hover {\r\n	color: #333333;\r\n}\r\n\r\n/* UCP navigation menu\r\n----------------------------------------*/\r\n\r\n/* Link styles for the sub-section links */\r\n#navigation a {\r\n	color: #fff;\r\n	background-color: #43A6DF;\r\n}\r\n\r\n.rtl #navigation a {\r\n}\r\n\r\n#navigation a:hover {\r\n	background-image: none;\r\n	background-color: #2E3539;\r\n	color: #fff;\r\n}\r\n\r\n#navigation #active-subsection a {\r\n	color: #fff;\r\n	background-color: #2E3539;\r\n	background-image: none;\r\n}\r\n\r\n#navigation #active-subsection a:hover {\r\n	color: #fff;\r\n}\r\n\r\n/* Preferences pane layout\r\n----------------------------------------*/\r\n#cp-main h2 {\r\n	\r\n}\r\n\r\n#cp-main .panel {\r\n	\r\n}\r\n\r\n#cp-main .pm {\r\n	background-color: #FFFFFF;\r\n}\r\n\r\n#cp-main span.corners-top, #cp-menu span.corners-top {\r\n	background-image: url(\"{T_THEME_PATH}/images/corners_left2.gif\");\r\n}\r\n\r\n#cp-main span.corners-top span, #cp-menu span.corners-top span {\r\n	background-image: url(\"{T_THEME_PATH}/images/corners_right2.gif\");\r\n}\r\n\r\n#cp-main span.corners-bottom, #cp-menu span.corners-bottom {\r\n	background-image: url(\"{T_THEME_PATH}/images/corners_left2.gif\");\r\n}\r\n\r\n#cp-main span.corners-bottom span, #cp-menu span.corners-bottom span {\r\n	background-image: url(\"{T_THEME_PATH}/images/corners_right2.gif\");\r\n}\r\n\r\n/* Topicreview */\r\n#cp-main .panel #topicreview span.corners-top, #cp-menu .panel #topicreview span.corners-top {\r\n	background-image: url(\"{T_THEME_PATH}/images/corners_left.gif\");\r\n}\r\n\r\n#cp-main .panel #topicreview span.corners-top span, #cp-menu .panel #topicreview span.corners-top span {\r\n	background-image: url(\"{T_THEME_PATH}/images/corners_right.gif\");\r\n}\r\n\r\n#cp-main .panel #topicreview span.corners-bottom, #cp-menu .panel #topicreview span.corners-bottom {\r\n	background-image: url(\"{T_THEME_PATH}/images/corners_left.gif\");\r\n}\r\n\r\n#cp-main .panel #topicreview span.corners-bottom span, #cp-menu .panel #topicreview span.corners-bottom span {\r\n	background-image: url(\"{T_THEME_PATH}/images/corners_right.gif\");\r\n}\r\n\r\n/* Friends list */\r\n.cp-mini {\r\n	background-color: #eef5f9;\r\n}\r\n\r\ndl.mini dt {\r\n	color: #425067;\r\n}\r\n\r\n/* PM Styles\r\n----------------------------------------*/\r\n/* PM Message history */\r\n.current {\r\n	color: #000000 !important;\r\n}\r\n\r\n/* PM marking colours */\r\n.pmlist li.pm_message_reported_colour, .pm_message_reported_colour {\r\n	border-left-color: #BC2A4D;\r\n	border-right-color: #BC2A4D;\r\n}\r\n\r\n.pmlist li.pm_marked_colour, .pm_marked_colour {\r\n	border-color: #FF6600;\r\n}\r\n\r\n.pmlist li.pm_replied_colour, .pm_replied_colour {\r\n	border-color: #A9B8C2;\r\n}\r\n\r\n.pmlist li.pm_friend_colour, .pm_friend_colour {\r\n	border-color: #5D8FBD;\r\n}\r\n\r\n.pmlist li.pm_foe_colour, .pm_foe_colour {\r\n	border-color: #000000;\r\n}\r\n\r\n/* Avatar gallery */\r\n#gallery label {\r\n	background-color: #FFFFFF;\r\n	border-color: #CCC;\r\n}\r\n\r\n#gallery label:hover {\r\n	background-color: #EEE;\r\n}\r\n\r\n/*  	\r\n--------------------------------------------------------------\r\nColours and backgrounds for forms.css\r\n-------------------------------------------------------------- */\r\n\r\n/* General form styles\r\n----------------------------------------*/\r\nselect {\r\n	border-color: #DCDCDC;\r\n	background-color: #FFFFFF;\r\n	color: #000;\r\n}\r\n\r\nlabel {\r\n	\r\n}\r\n\r\noption.disabled-option {\r\n	color: graytext;\r\n}\r\n\r\n/* Definition list layout for forms\r\n---------------------------------------- */\r\ndd label {\r\n	color: #333;\r\n}\r\n\r\n/* Hover effects */\r\nfieldset dl:hover dt label {\r\n	color: #000000;\r\n}\r\n\r\nfieldset.fields2 dl:hover dt label {\r\n	color: inherit;\r\n}\r\n\r\n/* Quick-login on index page */\r\nfieldset.quick-login input.inputbox {\r\n	background-color: #F2F3F3;\r\n}\r\n\r\n/* Posting page styles\r\n----------------------------------------*/\r\n\r\n#message-box textarea {\r\n	color: #333333;\r\n}\r\n\r\n/* Input field styles\r\n---------------------------------------- */\r\n.inputbox {\r\n	background-color: #F7F7F7; \r\n	border-color: #E4E4E4;\r\n	color: #333333;\r\n	padding: 6px;\r\n}\r\n\r\n.inputbox:hover {\r\n	border-color: #43A6DF;\r\n}\r\n\r\n.inputbox:focus {\r\n	border-color: #43A6DF;\r\n	background-color: #F7F7F7;\r\n}\r\n\r\n/* Form button styles\r\n---------------------------------------- */\r\n\r\na.button1, input.button1, input.button3, a.button2, input.button2 {\r\n	color: #FFFFFF;\r\n	background-color: #43A6DF;\r\n}\r\n\r\na.button1, input.button1 {\r\n	border-color: #666666;\r\n}\r\n\r\ninput.button3 {\r\n	background-image: none;\r\n}\r\n\r\n/* Alternative button */\r\na.button2, input.button2, input.button3 {\r\n	\r\n}\r\n\r\n/* <a> button in the style of the form buttons */\r\na.button1, a.button1:link, a.button1:visited, a.button1:active, a.button2, a.button2:link, a.button2:visited, a.button2:active {\r\n	color: #FFFFFF;\r\n}\r\n\r\n/* Hover states */\r\na.button1:hover, input.button1:hover, a.button2:hover, input.button2:hover, input.button3:hover {\r\n	\r\n	color: #FFFFFF;\r\n}\r\n\r\ninput.search {\r\n	\r\n}\r\n\r\ninput.disabled {\r\n	color: #666666;\r\n}\n/*  phpBB 3.0.11 Style Sheet\r\n    --------------------------------------------------------------\r\n	Style name: Metro Blue\r\n	Copyright (C) 2013 PixelGoose Studio\r\n    --------------------------------------------------------------\r\n*/\r\n@media only screen and (min-width: 1199px) {\r\n    #subheader-menu li {\r\n        background-image: none !important;\r\n    }\r\n\r\n}\r\n\r\n@media only screen and (min-width: 960px) and (max-width: 1199px) {\r\n    #wrap {\r\n    	width: 940px;\r\n    }\r\n    #page-body-inner {\r\n        width: 676px;\r\n    }\r\n    #subheader-menu li {\r\n        background-image: none !important;\r\n    }\r\n\r\n}\r\n\r\n@media only screen and (min-width: 768px) and (max-width: 959px) {\r\n    #wrap {\r\n    	width: 750px;\r\n    }\r\n    #page-body-inner {\r\n        width: 486px;\r\n    }\r\n    .forabg ul.topiclist dd.topics,\r\n    .forabg ul.topiclist dd.posts,\r\n    .forumbg ul.topiclist dd.views {\r\n        display: none;\r\n    }\r\n	.search-box-inner {\r\n	    width: 200px;\r\n	}\r\n	.search-box #keywords {\r\n	    width: 150px;\r\n	}\r\n    .search-adv-link {\r\n    	display: none;\r\n    }\r\n	ul.topiclist dt {               \r\n	    width: 60%;\r\n	}\r\n    #header {\r\n        height: 100px;\r\n    }\r\n    #header li {\r\n        height: 100px;\r\n        width: 100px;\r\n        border-left-width: 10px;\r\n    }\r\n    #header li a {\r\n        height: 100px;\r\n        width: 100px;\r\n        font-size: 1.077em;\r\n    }\r\n    .tabs li a {\r\n        background-size: 100%;\r\n    }\r\n    #header #logo, #header li a span {\r\n        bottom: 10px;\r\n        left: 10px;\r\n    }\r\n    #header #logo img {\r\n        height: auto;\r\n        width: 83.33333333333333% !important;\r\n    }\r\n    #subheader-menu li {\r\n        background-image: none !important;\r\n    }\r\n    .drafts dd.mark {\r\n        display: none;\r\n    }\r\n    dd.mark {\r\n        width: 5%;\r\n    }\r\n    ul.topiclist dt {\r\n        overflow: hidden;\r\n    }\r\n\r\n}\r\n\r\n\r\n@media only screen and (min-width: 480px) and (max-width: 767px) {\r\n    html { \r\n        -webkit-text-size-adjust: 100%;\r\n    }\r\n    #wrap {\r\n    	width: 95%;\r\n    }\r\n    #page-body-inner {\r\n        width: 100% !important;\r\n        float: none;\r\n        margin-right: 0;\r\n    }\r\n    #sidebar {\r\n        float: none;\r\n        width: 100%;\r\n        margin-bottom: 12px;\r\n    }\r\n    dd.topics, dd.posts,\r\n    .forumbg ul.topiclist li.header dd.views,\r\n    .forumbg ul.topiclist li.row dd.views {\r\n    	display: none !important;\r\n    }\r\n    ul.topiclist dt {               \r\n        width: 60%;\r\n    }\r\n    #above-headerbar {\r\n        height: auto;\r\n        overflow: hidden;\r\n    }\r\n    .user-links {\r\n        float: none;\r\n        overflow: hidden;\r\n        margin-bottom: 12px;\r\n        border-bottom: solid 1px #E4E4E4;\r\n        text-align: center;\r\n    }\r\n    .user-links li {\r\n        display: inline;\r\n        vertical-align: top;\r\n        zoom: 1; /* Fix for IE7 */\r\n        *display: inline; /* Fix for IE7 */\r\n        float: none;\r\n    }\r\n    .search-box {\r\n        width: 100%;\r\n    }\r\n	.search-box-inner {\r\n	    width: 100%;\r\n        -webkit-box-sizing: border-box;\r\n           -moz-box-sizing: border-box;\r\n                box-sizing: border-box;\r\n        height: 32px;\r\n	}\r\n	.search-box #keywords {\r\n	    width: 90%;\r\n	}\r\n    .search-adv-link {\r\n    	display: none;\r\n    }\r\n    #page-body-inner {\r\n    	width: 480px;\r\n    }\r\n\r\n    #header {\r\n        height: 120px;\r\n    }\r\n\r\n    /* Subheader menu links */\r\n\r\n    #subheader-menu li#submenu-mark-read {\r\n        background-position: right center;\r\n    }\r\n    #subheader-menu li#submenu-unanswered {\r\n        background-position: 0 center;\r\n    }\r\n    #subheader-menu li#submenu-unread {\r\n        background-position: -28px center;\r\n    }\r\n    #subheader-menu li#submenu-search-new {\r\n        background-position: -56px center;\r\n    }\r\n    #subheader-menu li#submenu-active-topics {\r\n        background-position: -84px center;\r\n    }\r\n    #subheader-menu li a {\r\n        display: block;\r\n        width: 28px;\r\n        height: 28px;\r\n        text-indent: -9999px;\r\n    }\r\n\r\n    /* Tab menu */\r\n    .tabs-outer {\r\n        overflow: hidden;\r\n    }\r\n    .toggleMenuButton {\r\n        display: block;\r\n        width: 36px;\r\n        height: 36px;\r\n\r\n        position: absolute;\r\n        right: 12px;\r\n        bottom: 12px;\r\n        border-radius: 2px;\r\n        border: solid 1px #FFF;\r\n    }\r\n    .toggleMenuButton.active {\r\n        background-color: #2E3539;\r\n    }\r\n    #header .tabs {\r\n        float: none;\r\n        position: absolute;\r\n        top: 120px;\r\n        z-index: 100;\r\n        width: 100%;\r\n        display: none;\r\n    }\r\n    #header li {\r\n        height: auto;\r\n        width: 100%;\r\n        border: none;\r\n    }\r\n    #header li:hover, #header li:active {\r\n    }\r\n    #header li a {\r\n        height: auto;\r\n        width: 100%;\r\n        font-size: 1.077em;\r\n        border-top: solid 1px #E4E4E4;\r\n        text-transform: none;\r\n    }\r\n    #header li a {\r\n        background-image: none !important;\r\n        padding: 12px 8px;\r\n        -webkit-box-sizing: border-box;\r\n           -moz-box-sizing: border-box;\r\n                box-sizing: border-box;\r\n        background-color: #2E3539;\r\n    }\r\n    #header li a:hover, #header li a:active {\r\n        text-decoration: none;\r\n        background-color: #43A6DF;\r\n    }\r\n    #header li a span {\r\n        position: static;\r\n    }\r\n    #header #logo img {\r\n        height: auto;\r\n    }\r\n\r\n\r\n    #subheader-menu {\r\n        height: auto;\r\n        overflow: hidden;\r\n    }\r\n    #container1, #container2, #container3,\r\n    #col1, #col2 {\r\n        float: none;\r\n        position: inherit;\r\n        margin: 0;\r\n    }\r\n    #col1, #col2 {\r\n        padding: 24px 12px !important;\r\n        overflow: hidden;\r\n    }\r\n    #col1 {\r\n        border-bottom: solid 12px #F6F6F6;\r\n    }\r\n    #col2 {\r\n        background-color: #43A6DF;\r\n        width: auto;\r\n    }\r\n    .linklist {\r\n        float: none;\r\n        text-align: center;\r\n    }\r\n    ul.linklist li {\r\n        float: none !important;\r\n        display: inline;\r\n    }\r\n    #faqlinks .column1, #faqlinks .column2 {\r\n        float: none;\r\n        width: 100%;\r\n    }\r\n    #memberlist .posts, #memberlist .info {\r\n        display: none;\r\n    }\r\n    #memberlist.table1 thead tr th.name,\r\n    #memberlist th.name, .table-list th.name {\r\n        padding: 14px 6px 14px 3px !important;\r\n    }\r\n    fieldset.jumpbox {\r\n        text-align: right;\r\n    }\r\n    fieldset.quickmod {\r\n        height: auto;\r\n        margin-bottom: 12px;\r\n        width: auto;\r\n    }\r\n    fieldset dt {\r\n        width: auto;\r\n    }\r\n    fieldset dd {\r\n        margin-left: 0;\r\n    }\r\n    dd label {\r\n        white-space: normal;\r\n    }\r\n    .topic-actions .pagination {\r\n        margin-bottom: 12px;\r\n    }\r\n    .postbody ul.profile-icons {\r\n        overflow: hidden;\r\n        float: none;\r\n    }\r\n    .postbody h3 {\r\n        margin: 12px 0 !important;\r\n    }\r\n    .panel fieldset dt {\r\n        float: none;\r\n    }\r\n    .panel fieldset.fields1 dd {\r\n        margin-left: 0;\r\n    }\r\n    #message-box {\r\n        width: 94%;\r\n    }\r\n    #message-box textarea {\r\n        width: 94%;\r\n    }\r\n    .column1, .column2 {\r\n        clear: none;\r\n        float: none;\r\n        width: 100%;\r\n    }\r\n    #smiley-box {\r\n        float: none;\r\n        width: 100%;\r\n    }\r\n    .editor-status {\r\n        display: none;\r\n    }\r\n    a.button1, input.button1, input.button3, a.button2, input.button2 {\r\n        margin-bottom: 6px;\r\n    }\r\n    #tabs {\r\n        margin: 20px 0 -1px;\r\n        min-width: inherit;\r\n    }\r\n    #tabs ul {\r\n        margin: 0;\r\n        padding: 0;\r\n        text-align: center;\r\n    }\r\n    #tabs li {\r\n        display: inline-block;\r\n    }\r\n    #tabs a span {\r\n        padding: 6px 8px;\r\n    }\r\n    #cp-menu, #cp-main {\r\n        float: none;\r\n        width: 100%;\r\n    }\r\n    th.ip, td.ip, th.date, td.date {\r\n        display: none;\r\n    }\r\n    td.username {\r\n        text-overflow: ellipsis;\r\n        white-space: nowrap;\r\n        overflow: hidden;\r\n    }\r\n    .panel .panel {\r\n        padding: 0;\r\n        border: none;\r\n    }\r\n    #minitabs li {\r\n        display: inline-block;\r\n        float: none;\r\n        padding: 0 2px 4px;\r\n    }\r\n    #ucp .topiclist .header dd.mark {\r\n        display: none;\r\n    }\r\n    dd.mark {\r\n        width: 5%;\r\n    }\r\n    .drafts dd.mark {\r\n        display: none;\r\n    }\r\n    ul.topiclist dt {\r\n        overflow: hidden;\r\n    }\r\n    fieldset.fields2 dd {\r\n        margin-left: 0 !important;\r\n    }\r\n    #cp-main h2 {\r\n        margin-left: 0 !important;\r\n    }\r\n    dt.leader {\r\n        padding: 14px 6px !important;\r\n    }\r\n    .info.leader {\r\n        display: none;\r\n    }\r\n    .login-window {\r\n        display: none;\r\n    }\r\n    .login-link {\r\n        display: inline;\r\n    }\r\n}\r\n\r\n\r\n@media only screen and (max-width: 479px) {\r\n    html { \r\n        -webkit-text-size-adjust: 100%;\r\n    }\r\n    #wrap {\r\n        width: 95%;\r\n    }\r\n    #page-body-inner {\r\n        width: 100% !important;\r\n        float: none;\r\n        margin-right: 0;\r\n    }\r\n    h2 {\r\n        font-size: 1.538em;\r\n    }\r\n    #sidebar {\r\n        float: none;\r\n        width: 100%;\r\n        margin-bottom: 12px;\r\n    }\r\n    .topiclist .header dt {\r\n        padding: 14px 6px !important;\r\n        width: 100%;\r\n        -webkit-box-sizing: border-box;\r\n           -moz-box-sizing: border-box;\r\n                box-sizing: border-box;\r\n    }\r\n    .topiclist .header dt a {\r\n        padding-left: 0;\r\n    }\r\n    li.header dd.topics,\r\n    li.header dd.posts,\r\n    li.header dd.lastpost,\r\n    li.header dd.views,\r\n    li.row dd.posts,\r\n    li.row dd.lastpost,\r\n    li.row dd.views {\r\n        display: none;\r\n    }\r\n    ul.topiclist dt {               \r\n        width: 65%;\r\n    }\r\n    .forumbg ul.topiclist li.row dt {\r\n        width: 80%;\r\n    }\r\n    li.header dl.icon {\r\n        min-height: 48px;\r\n    }\r\n    .row .pagination {\r\n        float: none;\r\n        width: auto;\r\n        text-align: left;\r\n        margin: 6px 0px;\r\n    }\r\n    dl.icon dt {\r\n        background-position: 5px 20px;\r\n    }\r\n    #above-headerbar {\r\n        height: auto;\r\n        overflow: hidden;\r\n    }\r\n    .user-links {\r\n        float: none;\r\n        overflow: hidden;\r\n        margin-bottom: 12px;\r\n        border-bottom: solid 1px #E4E4E4;\r\n        text-align: center;\r\n    }\r\n    .user-links li {\r\n        display: inline;\r\n        vertical-align: top;\r\n        zoom: 1; /* Fix for IE7 */\r\n        *display: inline; /* Fix for IE7 */\r\n        float: none;\r\n        white-space: nowrap;\r\n    }\r\n    .search-box {\r\n        width: 100%;\r\n    }\r\n    .search-box-inner {\r\n        width: 100%;\r\n        -webkit-box-sizing: border-box;\r\n           -moz-box-sizing: border-box;\r\n                box-sizing: border-box;\r\n        height: 32px;\r\n    }\r\n    .search-box #keywords {\r\n        width: 85%;\r\n    }\r\n    .search-adv-link {\r\n        display: none;\r\n    }\r\n    #page-body-inner {\r\n        width: 480px;\r\n    }\r\n\r\n    #header {\r\n        height: 120px;\r\n    }\r\n\r\n\r\n    /* Subheader menu links */\r\n\r\n    #subheader-menu li#submenu-mark-read {\r\n        background-position: right center;\r\n    }\r\n    #subheader-menu li#submenu-unanswered {\r\n        background-position: 0 center;\r\n    }\r\n    #subheader-menu li#submenu-unread {\r\n        background-position: -28px center;\r\n    }\r\n    #subheader-menu li#submenu-search-new {\r\n        background-position: -56px center;\r\n    }\r\n    #subheader-menu li#submenu-active-topics {\r\n        background-position: -84px center;\r\n    }\r\n    #subheader-menu li a {\r\n        display: block;\r\n        width: 28px;\r\n        height: 28px;\r\n        text-indent: -9999px;\r\n    }\r\n\r\n\r\n    /* Tab menu */\r\n    .tabs-outer {\r\n        overflow: hidden;\r\n    }\r\n    .toggleMenuButton {\r\n        display: block;\r\n        width: 36px;\r\n        height: 36px;\r\n\r\n        position: absolute;\r\n        right: 12px;\r\n        bottom: 12px;\r\n        border-radius: 2px;\r\n        border: solid 1px #FFF;\r\n    }\r\n    .toggleMenuButton.active {\r\n        background-color: #2E3539;\r\n    }\r\n    #header .tabs {\r\n        float: none;\r\n        position: absolute;\r\n        top: 120px;\r\n        z-index: 100;\r\n        width: 100%;\r\n        display: none;\r\n    }\r\n    #header li {\r\n        height: auto;\r\n        width: 100%;\r\n        border: none;\r\n    }\r\n    #header li:hover, #header li:active {\r\n    }\r\n    #header li a {\r\n        height: auto;\r\n        width: 100%;\r\n        font-size: 1.077em;\r\n        border-top: solid 1px #E4E4E4;\r\n        text-transform: none;\r\n    }\r\n    #header li a {\r\n        background-image: none !important;\r\n        padding: 12px 8px;\r\n        -webkit-box-sizing: border-box;\r\n           -moz-box-sizing: border-box;\r\n                box-sizing: border-box;\r\n        background-color: #2E3539;\r\n    }\r\n    #header li a:hover, #header li a:active {\r\n        text-decoration: none;\r\n        background-color: #43A6DF;\r\n    }\r\n    #header li a span {\r\n        position: static;\r\n    }\r\n    #header #logo img {\r\n        height: auto;\r\n    }\r\n\r\n\r\n    #subheader-menu {\r\n        height: auto;\r\n        overflow: hidden;\r\n    }\r\n    #container1, #container2, #container3,\r\n    #col1, #col2 {\r\n        float: none;\r\n        position: inherit;\r\n        margin: 0;\r\n    }\r\n    #col1, #col2 {\r\n        padding: 24px 12px !important;\r\n        overflow: hidden;\r\n    }\r\n    #col1 {\r\n        border-bottom: solid 12px #F6F6F6;\r\n    }\r\n    #col2 {\r\n        background-color: #43A6DF;\r\n        width: auto;\r\n    }\r\n    .linklist {\r\n        float: none;\r\n        text-align: center;\r\n    }\r\n    .linklist a {\r\n        white-space: nowrap;\r\n    }\r\n    ul.linklist li {\r\n        float: none !important;\r\n        display: inline;\r\n    }\r\n    #faqlinks .column1, #faqlinks .column2 {\r\n        float: none;\r\n        width: 100%;\r\n    }\r\n    #memberlist .posts, #memberlist .info {\r\n        display: none;\r\n    }\r\n    #memberlist.table1 thead tr th.name,\r\n    #memberlist th.name, .table-list th.name {\r\n        padding: 14px 6px 14px 3px !important;\r\n    }\r\n    fieldset.jumpbox {\r\n        text-align: left;\r\n    }\r\n    .jumpbox label, .quickmod label {\r\n        display: block;\r\n    }\r\n    fieldset.quickmod {\r\n        height: auto;\r\n        width: 100%;\r\n        text-align: left;\r\n        margin-bottom: 12px;\r\n    }\r\n    fieldset dt {\r\n        width: auto;\r\n    }\r\n    fieldset dd {\r\n        margin-left: 0;\r\n    }\r\n    dd label {\r\n        white-space: normal;\r\n    }\r\n    .rules {\r\n        background-image: none !important;\r\n        padding: 12px !important;\r\n    }\r\n    .display-options label {\r\n        margin-bottom: 12px;\r\n        display: block;\r\n    }\r\n    #jumpbox {\r\n        clear: both;\r\n        width: 100%;\r\n        margin: 6px 0;\r\n    }\r\n    .topic-actions .buttons {\r\n        margin-bottom: 12px;\r\n    }\r\n    .topic-actions .pagination {\r\n        margin-bottom: 12px;\r\n    }\r\n    .postbody ul.profile-icons {\r\n        overflow: hidden;\r\n        float: none;\r\n    }\r\n    .postbody h3 {\r\n        margin: 12px 0 !important;\r\n    }\r\n    p.author {\r\n        margin: 0 0 0.6em;\r\n    }\r\n    .postprofile dd {\r\n        display: none;\r\n    }\r\n    .section-viewtopic form#viewtopic {\r\n        display: none;\r\n    }\r\n    fieldset dt {\r\n        display: block;\r\n        float: none;\r\n        width: auto !important;\r\n    }\r\n    fieldset.fields2 dd {\r\n        margin-left: 0;\r\n    }\r\n    .autowidth {\r\n        width: 94% !important;\r\n    }\r\n    textarea#message {\r\n        height: 150px;\r\n    }\r\n    fieldset.polls dt {\r\n        width: 30% !important;\r\n    }\r\n    .panel fieldset dt {\r\n        float: none;\r\n    }\r\n    .panel fieldset.fields1 dd {\r\n        margin-left: 0;\r\n    }\r\n    #message-box {\r\n        width: 94%;\r\n    }\r\n    #message-box textarea {\r\n        width: 94%;\r\n    }\r\n    dl.details dt {\r\n        clear: left;\r\n        float: none;\r\n        text-align: left;\r\n        width: auto;\r\n    }\r\n    dl.details dd {\r\n        float: left;\r\n        margin-bottom: 12px;\r\n        margin-left: 0;\r\n        padding-left: 0;\r\n        width: 100%;\r\n    }\r\n\r\n    .column1, .column2 {\r\n        clear: none;\r\n        float: none;\r\n        width: 100%;\r\n    }\r\n\r\n    #smiley-box {\r\n        float: none;\r\n        width: 100%;\r\n    }\r\n    .editor-status {\r\n        display: none;\r\n    }\r\n    a.button1, input.button1, input.button3, a.button2, input.button2 {\r\n        margin-bottom: 6px;\r\n    }\r\n    #tabs {\r\n        margin: 20px 0 -1px;\r\n        min-width: inherit;\r\n    }\r\n    #tabs ul {\r\n        margin: 0;\r\n        padding: 0;\r\n        text-align: center;\r\n    }\r\n    #tabs li {\r\n        display: inline-block;\r\n    }\r\n    #tabs a span {\r\n        padding: 6px 8px;\r\n    }\r\n    #cp-menu, #cp-main {\r\n        float: none;\r\n        width: 100%;\r\n    }\r\n    th.ip, td.ip, th.date, td.date {\r\n        display: none;\r\n    }\r\n    td.username {\r\n        text-overflow: ellipsis;\r\n        white-space: nowrap;\r\n        overflow: hidden;\r\n    }\r\n    .panel .panel {\r\n        padding: 0;\r\n        border: none;\r\n    }\r\n    #minitabs li {\r\n        display: inline-block;\r\n        float: none;\r\n        padding: 0 2px 4px;\r\n    }\r\n    #ucp .topiclist .header dd.mark,\r\n    #cp-main .topiclist .header dd.mark {\r\n        display: none;\r\n    }\r\n    .drafts dd.mark {\r\n        display: none;\r\n    }\r\n    .drafts dd.info,\r\n    .drafts + .cplist > li > dl > dd.info,\r\n    .pmlist dd.info {\r\n        display: none;\r\n    }\r\n    ul.topiclist dt {\r\n        overflow: hidden;\r\n    }\r\n    .attachments .time, .attachments + .cplist .time,\r\n    .attachments .extra, .attachments + .cplist .extra {\r\n        display: none;\r\n    }\r\n    .topiclist.attachments-list li.row dl dt {\r\n        width: 85% !important;\r\n    }\r\n    #dateoptions {\r\n        width: 100%;\r\n    }\r\n    dl.pmlist dt {\r\n        width: 100% !important;\r\n    }\r\n    dl.pmlist dt textarea {\r\n        width: 94%;\r\n    }\r\n    dl.pmlist dd {\r\n        margin-left: 0 !important;\r\n    }\r\n    #search_memberlist .panel {\r\n        padding: 0 !important;\r\n        border: none;\r\n    }\r\n    .ucp-main dd select {\r\n        width: 100%;\r\n    }\r\n    #cp-main h2 {\r\n        margin-left: 0 !important;\r\n    }\r\n    dt.leader {\r\n        padding: 14px 6px !important;\r\n    }\r\n    .info.leader {\r\n        display: none;\r\n    }\r\n    .topiclist.leader li dl dt {\r\n        width: 100%;\r\n    }\r\n    .topiclist.leader li dl dd.option {\r\n        font-size: 1em;\r\n        margin-right: 6px;\r\n        padding: 4px 5px !important;\r\n        text-align: left;\r\n        width: auto;\r\n    }\r\n    .cp-mini.friends {\r\n        margin: 0;\r\n        padding: 12px 6px;\r\n    }\r\n    .display-actions {\r\n        white-space: normal !important;\r\n        text-align: center;\r\n    }\r\n    .display-actions select {\r\n        width: 100% !important;\r\n        margin-bottom: 6px;\r\n    }\r\n    .mcp-main .header dd.moderation {\r\n        display: none;\r\n    }\r\n    .captcha {\r\n        width: 100%;\r\n    }\r\n    .login-window {\r\n        display: none;\r\n    }\r\n    .login-link {\r\n        display: inline;\r\n    }\r\n}');
/*!40000 ALTER TABLE `phpbb_styles_theme` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_topics`
--

DROP TABLE IF EXISTS `phpbb_topics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_topics` (
  `topic_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `forum_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `icon_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `topic_attachment` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `topic_approved` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `topic_reported` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `topic_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `topic_poster` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `topic_time` int(11) unsigned NOT NULL DEFAULT '0',
  `topic_time_limit` int(11) unsigned NOT NULL DEFAULT '0',
  `topic_views` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `topic_replies` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `topic_replies_real` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `topic_status` tinyint(3) NOT NULL DEFAULT '0',
  `topic_type` tinyint(3) NOT NULL DEFAULT '0',
  `topic_first_post_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `topic_first_poster_name` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `topic_first_poster_colour` varchar(6) COLLATE utf8_bin NOT NULL DEFAULT '',
  `topic_last_post_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `topic_last_poster_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `topic_last_poster_name` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `topic_last_poster_colour` varchar(6) COLLATE utf8_bin NOT NULL DEFAULT '',
  `topic_last_post_subject` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `topic_last_post_time` int(11) unsigned NOT NULL DEFAULT '0',
  `topic_last_view_time` int(11) unsigned NOT NULL DEFAULT '0',
  `topic_moved_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `topic_bumped` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `topic_bumper` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `poll_title` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `poll_start` int(11) unsigned NOT NULL DEFAULT '0',
  `poll_length` int(11) unsigned NOT NULL DEFAULT '0',
  `poll_max_options` tinyint(4) NOT NULL DEFAULT '1',
  `poll_last_vote` int(11) unsigned NOT NULL DEFAULT '0',
  `poll_vote_change` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`topic_id`),
  KEY `forum_id` (`forum_id`),
  KEY `forum_id_type` (`forum_id`,`topic_type`),
  KEY `last_post_time` (`topic_last_post_time`),
  KEY `topic_approved` (`topic_approved`),
  KEY `forum_appr_last` (`forum_id`,`topic_approved`,`topic_last_post_id`),
  KEY `fid_time_moved` (`forum_id`,`topic_last_post_time`,`topic_moved_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_topics`
--

LOCK TABLES `phpbb_topics` WRITE;
/*!40000 ALTER TABLE `phpbb_topics` DISABLE KEYS */;
INSERT INTO `phpbb_topics` VALUES (1,2,0,0,1,0,'Welcome to phpBB3',2,1403110152,0,0,0,0,0,0,1,'admin','AA0000',1,2,'admin','AA0000','Welcome to phpBB3',1403110152,972086460,0,0,0,'',0,0,1,0,0);
/*!40000 ALTER TABLE `phpbb_topics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_topics_posted`
--

DROP TABLE IF EXISTS `phpbb_topics_posted`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_topics_posted` (
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `topic_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `topic_posted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`topic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_topics_posted`
--

LOCK TABLES `phpbb_topics_posted` WRITE;
/*!40000 ALTER TABLE `phpbb_topics_posted` DISABLE KEYS */;
INSERT INTO `phpbb_topics_posted` VALUES (2,1,1);
/*!40000 ALTER TABLE `phpbb_topics_posted` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_topics_track`
--

DROP TABLE IF EXISTS `phpbb_topics_track`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_topics_track` (
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `topic_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `forum_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `mark_time` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`topic_id`),
  KEY `topic_id` (`topic_id`),
  KEY `forum_id` (`forum_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_topics_track`
--

LOCK TABLES `phpbb_topics_track` WRITE;
/*!40000 ALTER TABLE `phpbb_topics_track` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_topics_track` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_topics_watch`
--

DROP TABLE IF EXISTS `phpbb_topics_watch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_topics_watch` (
  `topic_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `notify_status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  KEY `topic_id` (`topic_id`),
  KEY `user_id` (`user_id`),
  KEY `notify_stat` (`notify_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_topics_watch`
--

LOCK TABLES `phpbb_topics_watch` WRITE;
/*!40000 ALTER TABLE `phpbb_topics_watch` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_topics_watch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_user_group`
--

DROP TABLE IF EXISTS `phpbb_user_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_user_group` (
  `group_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `group_leader` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `user_pending` tinyint(1) unsigned NOT NULL DEFAULT '1',
  KEY `group_id` (`group_id`),
  KEY `user_id` (`user_id`),
  KEY `group_leader` (`group_leader`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_user_group`
--

LOCK TABLES `phpbb_user_group` WRITE;
/*!40000 ALTER TABLE `phpbb_user_group` DISABLE KEYS */;
INSERT INTO `phpbb_user_group` VALUES (1,1,0,0),(2,2,0,0),(4,2,0,0),(5,2,1,0),(6,3,0,0),(6,4,0,0),(6,5,0,0),(6,6,0,0),(6,7,0,0),(6,8,0,0),(6,9,0,0),(6,10,0,0),(6,11,0,0),(6,12,0,0),(6,13,0,0),(6,14,0,0),(6,15,0,0),(6,16,0,0),(6,17,0,0),(6,18,0,0),(6,19,0,0),(6,20,0,0),(6,21,0,0),(6,22,0,0),(6,23,0,0),(6,24,0,0),(6,25,0,0),(6,26,0,0),(6,27,0,0),(6,28,0,0),(6,29,0,0),(6,30,0,0),(6,31,0,0),(6,32,0,0),(6,33,0,0),(6,34,0,0),(6,35,0,0),(6,36,0,0),(6,37,0,0),(6,38,0,0),(6,39,0,0),(6,40,0,0),(6,41,0,0),(6,42,0,0),(6,43,0,0),(6,44,0,0),(6,45,0,0),(6,46,0,0),(6,47,0,0);
/*!40000 ALTER TABLE `phpbb_user_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_users`
--

DROP TABLE IF EXISTS `phpbb_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_users` (
  `user_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `user_type` tinyint(2) NOT NULL DEFAULT '0',
  `group_id` mediumint(8) unsigned NOT NULL DEFAULT '3',
  `user_permissions` mediumtext COLLATE utf8_bin NOT NULL,
  `user_perm_from` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `user_ip` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_regdate` int(11) unsigned NOT NULL DEFAULT '0',
  `username` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `username_clean` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_password` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_passchg` int(11) unsigned NOT NULL DEFAULT '0',
  `user_pass_convert` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `user_email` varchar(100) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_email_hash` bigint(20) NOT NULL DEFAULT '0',
  `user_birthday` varchar(10) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_lastvisit` int(11) unsigned NOT NULL DEFAULT '0',
  `user_lastmark` int(11) unsigned NOT NULL DEFAULT '0',
  `user_lastpost_time` int(11) unsigned NOT NULL DEFAULT '0',
  `user_lastpage` varchar(200) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_last_confirm_key` varchar(10) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_last_search` int(11) unsigned NOT NULL DEFAULT '0',
  `user_warnings` tinyint(4) NOT NULL DEFAULT '0',
  `user_last_warning` int(11) unsigned NOT NULL DEFAULT '0',
  `user_login_attempts` tinyint(4) NOT NULL DEFAULT '0',
  `user_inactive_reason` tinyint(2) NOT NULL DEFAULT '0',
  `user_inactive_time` int(11) unsigned NOT NULL DEFAULT '0',
  `user_posts` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `user_lang` varchar(30) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_timezone` decimal(5,2) NOT NULL DEFAULT '0.00',
  `user_dst` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `user_dateformat` varchar(30) COLLATE utf8_bin NOT NULL DEFAULT 'd M Y H:i',
  `user_style` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `user_rank` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `user_colour` varchar(6) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_new_privmsg` int(4) NOT NULL DEFAULT '0',
  `user_unread_privmsg` int(4) NOT NULL DEFAULT '0',
  `user_last_privmsg` int(11) unsigned NOT NULL DEFAULT '0',
  `user_message_rules` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `user_full_folder` int(11) NOT NULL DEFAULT '-3',
  `user_emailtime` int(11) unsigned NOT NULL DEFAULT '0',
  `user_topic_show_days` smallint(4) unsigned NOT NULL DEFAULT '0',
  `user_topic_sortby_type` varchar(1) COLLATE utf8_bin NOT NULL DEFAULT 't',
  `user_topic_sortby_dir` varchar(1) COLLATE utf8_bin NOT NULL DEFAULT 'd',
  `user_post_show_days` smallint(4) unsigned NOT NULL DEFAULT '0',
  `user_post_sortby_type` varchar(1) COLLATE utf8_bin NOT NULL DEFAULT 't',
  `user_post_sortby_dir` varchar(1) COLLATE utf8_bin NOT NULL DEFAULT 'a',
  `user_notify` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `user_notify_pm` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `user_notify_type` tinyint(4) NOT NULL DEFAULT '0',
  `user_allow_pm` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `user_allow_viewonline` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `user_allow_viewemail` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `user_allow_massemail` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `user_options` int(11) unsigned NOT NULL DEFAULT '230271',
  `user_avatar` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_avatar_type` tinyint(2) NOT NULL DEFAULT '0',
  `user_avatar_width` smallint(4) unsigned NOT NULL DEFAULT '0',
  `user_avatar_height` smallint(4) unsigned NOT NULL DEFAULT '0',
  `user_sig` mediumtext COLLATE utf8_bin NOT NULL,
  `user_sig_bbcode_uid` varchar(8) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_sig_bbcode_bitfield` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_from` varchar(100) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_icq` varchar(15) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_aim` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_yim` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_msnm` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_jabber` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_website` varchar(200) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_occ` text COLLATE utf8_bin NOT NULL,
  `user_interests` text COLLATE utf8_bin NOT NULL,
  `user_actkey` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_newpasswd` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_form_salt` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_new` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `user_reminded` tinyint(4) NOT NULL DEFAULT '0',
  `user_reminded_time` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username_clean` (`username_clean`),
  KEY `user_birthday` (`user_birthday`),
  KEY `user_email_hash` (`user_email_hash`),
  KEY `user_type` (`user_type`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_users`
--

LOCK TABLES `phpbb_users` WRITE;
/*!40000 ALTER TABLE `phpbb_users` DISABLE KEYS */;
INSERT INTO `phpbb_users` VALUES (1,2,1,'',0,'',1403110152,'Anonymous','anonymous','',0,0,'',0,'',0,0,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'d M Y H:i',2,0,'',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','d96680833b20feb3',1,0,0),(2,3,5,'zik0zjzik0zjzik0xs\ni1cjyo000000\nzik0zjzhb2tc',0,'::1',1403110152,'admin','admin','$H$9dFQ11dmuZm0yIxWT4CbZuJCV2Xay80',0,0,'efrenbautistajr@gmail.com',97417957325,'',0,0,0,'','',0,0,0,0,0,0,1,'en',0.00,0,'D M d, Y g:i a',2,1,'AA0000',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,1,1,1,1,230271,'',0,0,0,'','','','','','','','','','','','','','','dab616f60212f46f',1,0,0),(3,2,6,'',0,'',1403110212,'AdsBot [Google]','adsbot [google]','',1403110212,0,'',0,'',0,1403110212,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'D M d, Y g:i a',2,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,0,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','9f11a07490033925',0,0,0),(4,2,6,'',0,'',1403110212,'Alexa [Bot]','alexa [bot]','',1403110212,0,'',0,'',0,1403110212,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'D M d, Y g:i a',2,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,0,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','6e4cfc4b597b4357',0,0,0),(5,2,6,'',0,'',1403110213,'Alta Vista [Bot]','alta vista [bot]','',1403110213,0,'',0,'',0,1403110213,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'D M d, Y g:i a',2,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,0,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','934e443a135ad3a5',0,0,0),(6,2,6,'',0,'',1403110214,'Ask Jeeves [Bot]','ask jeeves [bot]','',1403110214,0,'',0,'',0,1403110214,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'D M d, Y g:i a',2,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,0,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','2d352d106b0095d7',0,0,0),(7,2,6,'',0,'',1403110214,'Baidu [Spider]','baidu [spider]','',1403110214,0,'',0,'',0,1403110214,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'D M d, Y g:i a',2,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,0,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','4fddcadb8434b176',0,0,0),(8,2,6,'',0,'',1403110215,'Bing [Bot]','bing [bot]','',1403110215,0,'',0,'',0,1403110215,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'D M d, Y g:i a',2,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,0,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','6145685d97512c00',0,0,0),(9,2,6,'',0,'',1403110216,'Exabot [Bot]','exabot [bot]','',1403110216,0,'',0,'',0,1403110216,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'D M d, Y g:i a',2,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,0,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','e1d6215959057b52',0,0,0),(10,2,6,'',0,'',1403110216,'FAST Enterprise [Crawler]','fast enterprise [crawler]','',1403110216,0,'',0,'',0,1403110216,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'D M d, Y g:i a',2,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,0,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','db9e5c3e72d190e2',0,0,0),(11,2,6,'',0,'',1403110216,'FAST WebCrawler [Crawler]','fast webcrawler [crawler]','',1403110216,0,'',0,'',0,1403110216,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'D M d, Y g:i a',2,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,0,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','9563bae21533a431',0,0,0),(12,2,6,'',0,'',1403110217,'Francis [Bot]','francis [bot]','',1403110217,0,'',0,'',0,1403110217,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'D M d, Y g:i a',2,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,0,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','eead8da56e2c7d35',0,0,0),(13,2,6,'',0,'',1403110219,'Gigabot [Bot]','gigabot [bot]','',1403110219,0,'',0,'',0,1403110219,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'D M d, Y g:i a',2,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,0,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','972dcb4508514faf',0,0,0),(14,2,6,'',0,'',1403110220,'Google Adsense [Bot]','google adsense [bot]','',1403110220,0,'',0,'',0,1403110220,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'D M d, Y g:i a',2,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,0,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','5f0647a9995d9469',0,0,0),(15,2,6,'',0,'',1403110220,'Google Desktop','google desktop','',1403110220,0,'',0,'',0,1403110220,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'D M d, Y g:i a',2,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,0,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','797ef4b66d4ffcd6',0,0,0),(16,2,6,'',0,'',1403110221,'Google Feedfetcher','google feedfetcher','',1403110221,0,'',0,'',0,1403110221,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'D M d, Y g:i a',2,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,0,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','95ec398d9a3addfa',0,0,0),(17,2,6,'',0,'',1403110221,'Google [Bot]','google [bot]','',1403110221,0,'',0,'',0,1403110221,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'D M d, Y g:i a',2,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,0,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','179ca8043e4c3c48',0,0,0),(18,2,6,'',0,'',1403110222,'Heise IT-Markt [Crawler]','heise it-markt [crawler]','',1403110222,0,'',0,'',0,1403110222,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'D M d, Y g:i a',2,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,0,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','21409d10696cdc2f',0,0,0),(19,2,6,'',0,'',1403110222,'Heritrix [Crawler]','heritrix [crawler]','',1403110222,0,'',0,'',0,1403110222,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'D M d, Y g:i a',2,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,0,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','c52dc93d1118545d',0,0,0),(20,2,6,'',0,'',1403110222,'IBM Research [Bot]','ibm research [bot]','',1403110222,0,'',0,'',0,1403110222,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'D M d, Y g:i a',2,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,0,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','530adcd8a5cfa59e',0,0,0),(21,2,6,'',0,'',1403110223,'ICCrawler - ICjobs','iccrawler - icjobs','',1403110223,0,'',0,'',0,1403110223,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'D M d, Y g:i a',2,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,0,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','899634c57c474ee4',0,0,0),(22,2,6,'',0,'',1403110224,'ichiro [Crawler]','ichiro [crawler]','',1403110224,0,'',0,'',0,1403110224,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'D M d, Y g:i a',2,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,0,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','481d5e2654c1469a',0,0,0),(23,2,6,'',0,'',1403110224,'Majestic-12 [Bot]','majestic-12 [bot]','',1403110224,0,'',0,'',0,1403110224,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'D M d, Y g:i a',2,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,0,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','c9c7eae8ac887c78',0,0,0),(24,2,6,'',0,'',1403110225,'Metager [Bot]','metager [bot]','',1403110225,0,'',0,'',0,1403110225,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'D M d, Y g:i a',2,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,0,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','6edb17cee80f42ed',0,0,0),(25,2,6,'',0,'',1403110225,'MSN NewsBlogs','msn newsblogs','',1403110225,0,'',0,'',0,1403110225,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'D M d, Y g:i a',2,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,0,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','c934605cea12192c',0,0,0),(26,2,6,'',0,'',1403110226,'MSN [Bot]','msn [bot]','',1403110226,0,'',0,'',0,1403110226,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'D M d, Y g:i a',2,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,0,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','2850019981a1772c',0,0,0),(27,2,6,'',0,'',1403110227,'MSNbot Media','msnbot media','',1403110227,0,'',0,'',0,1403110227,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'D M d, Y g:i a',2,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,0,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','8106d75df47262b0',0,0,0),(28,2,6,'',0,'',1403110227,'Nutch [Bot]','nutch [bot]','',1403110227,0,'',0,'',0,1403110227,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'D M d, Y g:i a',2,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,0,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','42cd8b9c6f2da257',0,0,0),(29,2,6,'',0,'',1403110227,'Online link [Validator]','online link [validator]','',1403110227,0,'',0,'',0,1403110227,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'D M d, Y g:i a',2,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,0,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','b7b16f44e52cbfe0',0,0,0),(30,2,6,'',0,'',1403110227,'psbot [Picsearch]','psbot [picsearch]','',1403110227,0,'',0,'',0,1403110227,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'D M d, Y g:i a',2,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,0,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','9cdcb98c945551d7',0,0,0),(31,2,6,'',0,'',1403110228,'Sensis [Crawler]','sensis [crawler]','',1403110228,0,'',0,'',0,1403110228,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'D M d, Y g:i a',2,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,0,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','11cb659afa991204',0,0,0),(32,2,6,'',0,'',1403110228,'SEO Crawler','seo crawler','',1403110228,0,'',0,'',0,1403110228,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'D M d, Y g:i a',2,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,0,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','8a6e93d35229bc51',0,0,0),(33,2,6,'',0,'',1403110229,'Seoma [Crawler]','seoma [crawler]','',1403110229,0,'',0,'',0,1403110229,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'D M d, Y g:i a',2,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,0,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','db65b850ed9aeaef',0,0,0),(34,2,6,'',0,'',1403110229,'SEOSearch [Crawler]','seosearch [crawler]','',1403110229,0,'',0,'',0,1403110229,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'D M d, Y g:i a',2,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,0,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','0f99dcb573a264c4',0,0,0),(35,2,6,'',0,'',1403110229,'Snappy [Bot]','snappy [bot]','',1403110229,0,'',0,'',0,1403110229,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'D M d, Y g:i a',2,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,0,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','d6affeeab09a0c2b',0,0,0),(36,2,6,'',0,'',1403110230,'Steeler [Crawler]','steeler [crawler]','',1403110230,0,'',0,'',0,1403110230,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'D M d, Y g:i a',2,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,0,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','f40118c38e6b8612',0,0,0),(37,2,6,'',0,'',1403110230,'Telekom [Bot]','telekom [bot]','',1403110230,0,'',0,'',0,1403110230,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'D M d, Y g:i a',2,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,0,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','3784636800aafdc2',0,0,0),(38,2,6,'',0,'',1403110230,'TurnitinBot [Bot]','turnitinbot [bot]','',1403110230,0,'',0,'',0,1403110230,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'D M d, Y g:i a',2,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,0,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','68bd02981837c5e8',0,0,0),(39,2,6,'',0,'',1403110230,'Voyager [Bot]','voyager [bot]','',1403110230,0,'',0,'',0,1403110230,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'D M d, Y g:i a',2,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,0,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','920d04e54c6a3ae3',0,0,0),(40,2,6,'',0,'',1403110230,'W3 [Sitesearch]','w3 [sitesearch]','',1403110230,0,'',0,'',0,1403110230,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'D M d, Y g:i a',2,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,0,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','98c29b19f1bcca48',0,0,0),(41,2,6,'',0,'',1403110231,'W3C [Linkcheck]','w3c [linkcheck]','',1403110231,0,'',0,'',0,1403110231,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'D M d, Y g:i a',2,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,0,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','74dc4ea8b02666cf',0,0,0),(42,2,6,'',0,'',1403110231,'W3C [Validator]','w3c [validator]','',1403110231,0,'',0,'',0,1403110231,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'D M d, Y g:i a',2,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,0,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','c9fea8f43e20cfa8',0,0,0),(43,2,6,'',0,'',1403110231,'YaCy [Bot]','yacy [bot]','',1403110231,0,'',0,'',0,1403110231,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'D M d, Y g:i a',2,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,0,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','44da1a2491a7504f',0,0,0),(44,2,6,'',0,'',1403110232,'Yahoo MMCrawler [Bot]','yahoo mmcrawler [bot]','',1403110232,0,'',0,'',0,1403110232,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'D M d, Y g:i a',2,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,0,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','3136375b0775b07f',0,0,0),(45,2,6,'',0,'',1403110232,'Yahoo Slurp [Bot]','yahoo slurp [bot]','',1403110232,0,'',0,'',0,1403110232,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'D M d, Y g:i a',2,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,0,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','d3dbdc098f80e071',0,0,0),(46,2,6,'',0,'',1403110232,'Yahoo [Bot]','yahoo [bot]','',1403110232,0,'',0,'',0,1403110232,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'D M d, Y g:i a',2,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,0,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','85a7f734cb96fcb1',0,0,0),(47,2,6,'',0,'',1403110232,'YahooSeeker [Bot]','yahooseeker [bot]','',1403110232,0,'',0,'',0,1403110232,0,'','',0,0,0,0,0,0,0,'en',0.00,0,'D M d, Y g:i a',2,0,'9E8DA7',0,0,0,0,-3,0,0,'t','d',0,'t','a',0,1,0,0,1,1,0,230271,'',0,0,0,'','','','','','','','','','','','','','','d11b9aee64ef27db',0,0,0);
/*!40000 ALTER TABLE `phpbb_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_warnings`
--

DROP TABLE IF EXISTS `phpbb_warnings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_warnings` (
  `warning_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `post_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `log_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `warning_time` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`warning_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_warnings`
--

LOCK TABLES `phpbb_warnings` WRITE;
/*!40000 ALTER TABLE `phpbb_warnings` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_warnings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_words`
--

DROP TABLE IF EXISTS `phpbb_words`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_words` (
  `word_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `word` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `replacement` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`word_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_words`
--

LOCK TABLES `phpbb_words` WRITE;
/*!40000 ALTER TABLE `phpbb_words` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_words` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phpbb_zebra`
--

DROP TABLE IF EXISTS `phpbb_zebra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phpbb_zebra` (
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `zebra_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `friend` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `foe` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`zebra_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phpbb_zebra`
--

LOCK TABLES `phpbb_zebra` WRITE;
/*!40000 ALTER TABLE `phpbb_zebra` DISABLE KEYS */;
/*!40000 ALTER TABLE `phpbb_zebra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rescueteam`
--

DROP TABLE IF EXISTS `rescueteam`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rescueteam` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address` text,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `zip` varchar(255) DEFAULT NULL,
  `contact` varchar(255) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rescueteam`
--

LOCK TABLES `rescueteam` WRITE;
/*!40000 ALTER TABLE `rescueteam` DISABLE KEYS */;
INSERT INTO `rescueteam` VALUES (3,'safasfaf','afasfsaf','asflkasf@email.com','sdgsdgsdg','sdgsdgsdg','Azerbaijan','sgsdg','24124','2345678','2016-08-25 04:40:57','2016-10-03 15:02:36'),(4,'Janine','JJanine','janinelabadia023@gmail.com','xsccscdcsc','xscsdc','Philippines','AR','1234','(111) 111-1111','2016-08-26 04:20:09','2016-08-26 04:20:09'),(5,'ertert','ert','hazelsed@mailinator.com','asdasd','asd','Bahrain','asdasd','4324','234234324','2016-09-19 23:35:28','2016-09-19 23:35:28'),(6,'jessseee','nassstreerrr','samplaemail@yahoo.com','qwert','urdanetas','Bahamas','united','2435','(223) 434-5345','2016-09-30 15:51:38','2016-09-30 15:51:38'),(7,'jacinto','escano','email@email.com','','','0','','','(212) 412-4124','2016-09-30 16:02:02','2016-09-30 16:02:02'),(8,'khk:;{})(/','6566?\":9){}','khk@mailinator.com','Rochester Hills','Rochester Hills','Colombia','miami','4567890888343434','870','2016-10-28 17:07:03','2016-10-28 17:07:03'),(9,'shanong','wilba','wilba@mailinator.com','3697 S Livernois Rd','Rochester Hills','Aruba','miami','48307','0985634782','2016-10-28 17:15:31','2016-10-28 17:15:31'),(10,'dolly','wolly','dolly@mailinator.com','93 Calypso Shrs','Novato','United States','california','94949','6758765432','2016-10-28 17:28:59','2016-10-28 17:28:59'),(11,'asdasd','jaseen','asda@mail.mail','','','','','','123111323','2017-01-17 02:26:38','2017-01-17 02:26:38'),(12,'asdasd','jaseen','asda@mail.mail','','','','','','123111323','2017-01-17 02:36:55','2017-01-17 02:36:55'),(13,'jack','escano','jacintogeekers@gmail.com','','','Bangladesh','','','2141243532','2017-01-17 02:37:09','2017-01-17 02:37:09'),(14,'asd','jason','nastors@mailinator.com','aa','ss','Anguilla','aa','22','1234','2017-01-17 02:37:25','2017-01-17 02:37:25'),(15,'asd','jason','nastors@mailinator.com','aa','ss','Anguilla','aa','22','1234','2017-01-17 02:53:58','2017-01-17 02:53:58'),(16,'jeremay','pedigloria','jmay@mailinator.com','','','0','','','7888888888888888888','2017-11-21 10:11:17','2017-11-21 10:11:17'),(18,'maaay','pedigloria','eee@mailinator.com','','','0','','','3342343244','2017-11-22 11:00:45','2017-11-22 11:00:45'),(19,'@$$5','@%#%$','dwsfas@mailinator.com','','','0','','','22343423424244','2017-11-27 10:34:49','2017-11-27 10:34:49'),(20,'dgdvhdbjdomdlwnw','wrffqefqegqeeq','fafqqe@maiinator.com','ffwfqwfqwgtwrhetrtjhfdv','rgrygewgwe','Austria','twe','466543','2345678','2017-12-06 13:22:03','2017-12-06 13:22:03'),(21,'Anna','Eugenio','zelly@mailinator.com','sasas','sasa','Philippines','asasa','21212','1212121','2018-03-27 15:15:58','2018-03-27 15:15:58'),(22,'&&k','^^euge','zel@mailinator.com','sasa','sasas','Armenia','sasas','3232','21212','2018-03-27 15:19:00','2018-03-27 15:19:00'),(23,'Kezel Ann','Eugenio','kezel@mailinator.com','fdfdhfdhfjhfjdhjdhfdjhfdjfh','dfdfdfdf','Philippines','fdfdfdf','100000','09434253647','2018-04-11 10:10:05','2018-04-11 10:10:05'),(24,'dasdasdasd','sadasd','adasd@asdas.com','asdasd','asdasd','Afghanistan','asdasd','21','12345678912','2018-04-11 16:52:35','2018-04-11 16:52:35'),(26,'Ariel','Bermudez','arielbermudez25@rocketmail.com','asdasdas,asdasd,asdasda','City Z','Japan','Tokyo','1000000000000','09307241476','2018-04-11 16:57:28','2018-04-11 16:57:28'),(27,'ellyy','dsdsd','sdsds','dsd','dsdsdd','0','32232','23232','09232323232','2018-04-13 13:52:53','2018-04-13 13:52:53'),(28,'larshhh','larsgg','dfdfdf','fdfdfd','fdfdf','Algeria','fdfdf','32323','09767676778','2018-04-13 14:00:33','2018-04-13 14:00:33'),(29,'Lorraine#%$#','Manzano@#$#@_','manzano@mailinator.com','Urdaneta','Urdaneta','Philippines','Pangasinan','2428','09151656545','2018-05-02 16:08:51','2018-05-02 16:08:51'),(30,'dxfasdfd','sdfsdf','dsfsdfs@mailinator.com','sdfsdf','dfsd','Qatar','dfsdf','4364564564','54564564564','2018-05-08 17:02:38','2018-05-08 17:02:38'),(31,'Harris','Mariano','harris@mailinator.com','Binalonan','Urdaneta','Philippines','PH','2428','09998765432','2018-05-11 05:40:31','2018-05-11 05:40:31');
/*!40000 ALTER TABLE `rescueteam` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `socialmedia`
--

DROP TABLE IF EXISTS `socialmedia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `socialmedia` (
  `id` varchar(255) NOT NULL DEFAULT '',
  `icon` varchar(255) NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  `alt` varchar(255) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `socialmedia`
--

LOCK TABLES `socialmedia` WRITE;
/*!40000 ALTER TABLE `socialmedia` DISABLE KEYS */;
INSERT INTO `socialmedia` VALUES ('4D95374D-95C8-4A48-A491-4F6DA228D09C','twitter','https://twitter.com','Twitter',1),('554B2457-D22C-4307-A6D9-05217588EB43','instagram','https://youtube.com','Instagram',NULL),('9484B30E-643B-452B-9993-E9834426514D','linkedin','https://yahoo.com','LinkedIn',NULL),('A74EA81E-1515-4CE4-80B0-0D0E963648FA','facebook','https://facebook.com','Facebook',NULL);
/*!40000 ALTER TABLE `socialmedia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblannouncements`
--

DROP TABLE IF EXISTS `tblannouncements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblannouncements` (
  `annID` int(11) NOT NULL AUTO_INCREMENT,
  `annTitle` varchar(255) NOT NULL,
  `annDesc` text NOT NULL,
  `annDate` int(11) DEFAULT NULL,
  `annStart` int(11) DEFAULT NULL,
  `annEnd` int(11) DEFAULT NULL,
  `annStatus` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`annID`)
) ENGINE=InnoDB AUTO_INCREMENT=134 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblannouncements`
--

LOCK TABLES `tblannouncements` WRITE;
/*!40000 ALTER TABLE `tblannouncements` DISABLE KEYS */;
INSERT INTO `tblannouncements` VALUES (1,'This is a sample announcement','<p>13 Million Filipino kids live below poverty line, representing&nbsp;over 44 percent of those aged 15 and below, according to the&nbsp;U it d N ti &rsquo; Child &rsquo; Ed ti F d (UNICEF) d thUnited Nations&rsquo; Children&rsquo;s Education Fund (UNICEF) and the&nbsp;Philippine Institute for Development Studies (PIDS).13 Million Filipino kids live below poverty line, representing&nbsp;over 44 percent of those aged 15 and below, according to the&nbsp;U it d N ti &rsquo; Child &rsquo; Ed ti F d (UNICEF) d thUnited Nations&rsquo; Children&rsquo;s Education Fund (UNICEF) and the&nbsp;Philippine Institute for Development Studies (PIDS).</p>\r\n\r\n<p>13 Million Filipino kids live below poverty line, representing&nbsp;over 44 percent of those aged 15 and below, according to the&nbsp;U it d N ti &rsquo; Child &rsquo; Ed ti F d (UNICEF) d thUnited Nations&rsquo; Children&rsquo;s Education Fund (UNICEF) and the&nbsp;Philippine Institute for Development Studies (PIDS).</p>\r\n',1412859973,1412784000,1419955200,'deactivate'),(2,'Chronically malnourished children lack the nutrients','<p>Chronically malnourished children lack the nutrients&nbsp;needed for their proper health and physical and mental&nbsp;d ldevelopment.&nbsp;tThey become underweight, they are short in height,&nbsp;lack energy, perform poorly in school and consequently,&nbsp;have very little means of breaking away from the cycle&nbsp;of poverty and making a better future for themselvesof poverty and making a better future for themselves.</p>\r\n\r\n<p>Chronically malnourished children lack the nutrients&nbsp;needed for their proper health and physical and mental&nbsp;d ldevelopment.&nbsp;tThey become underweight, they are short in height,&nbsp;lack energy, perform poorly in school and consequently,&nbsp;have very little means of breaking away from the cycle&nbsp;of poverty and making a better future for themselvesof poverty and making a better future for themselves.</p>\r\n\r\n<p>Chronically malnourished children lack the nutrients&nbsp;needed for their proper health and physical and mental&nbsp;d ldevelopment.&nbsp;tThey become underweight, they are short in height,&nbsp;lack energy, perform poorly in school and consequently,&nbsp;have very little means of breaking away from the cycle&nbsp;of poverty and making a better future for themselvesof poverty and making a better future for themselves.</p>\r\n',1412860040,1412784000,1427472000,'deactivate'),(3,'Planet Impossible Development Plan','<p>There are two types of users: Agents, and Public. Agents are users that registers to the site. Agents can create own maps, add pictures, make comments and responses, adopt an action and put pins in other maps and view other user agents map. User Agent account can be use to PI forums and blog sites. Agents can create threads, post message, reply to a message and vote messages in forums. User Agents can submit blog articles, post comments to article, and share articles across social media platforms. On the other hand Public users are not allow to make any actions except for viewing and social media sharing. There can only be one Super Agent which is the Administrator of the PI Site.</p>\r\n\r\n<p>There will be a provided registration page for users who wanted to register, the user required information are provided in SLIDE 22, or I suggest we should user Login Using Facebook account or Login Using Gmail Account. We will add a security questions for registering users. When a user register he needs to activate his/her account through confirmation link send to his/her email account. Users who are not yet activated are still considered inactive.</p>\r\n\r\n<p>User Agents can easily retrieve forgotten password through a secret question or through password request through email, then a link to a page for change of password is given.</p>\r\n\r\n<p>Each Agent have their own account tools, such as each user has their own dashboard, account or profile, mission manager, and the community page, where agents view the threads they made and messages posted, and a blog panel. There will also have a user setting to public his profile or hide himself from other user.</p>\r\n\r\n<p>There will be a login page for Registered Users.</p>\r\n\r\n<p><strong>Messaging (Suggestion)</strong></p>\r\n\r\n<p>Each user agent can send a message to another user message. Agents can also reply to messages that was sent to them. As well as users can message the administrator for issues and other related matters. The messaging feature is the same with the messaging in facebook, except that there is no group messaging involve (we can add that in the future). Maybe we can also add file attachements to messages which only limits to office documents, images, and music.</p>\r\n\r\n<p>&nbsp;</p>\r\n',1412860187,1412784000,1419955200,'deactivate'),(4,'Creating Missions / Adding Action Pins','<p>In the create mission page, at first system will detect the current location of the user in the map. The current location of the user will be viewed at first, thereafter the map has been loaded &ldquo;ADD ACTION PIN&rdquo; will become enabled.</p>\r\n\r\n<p>Once the &ldquo;ADD ACTION PIN&rdquo; is clicked, the map is enabled for navigating to drop the action pin, the button message will change to &ldquo;DROP PIN&rdquo; and a notification above the map will display &ldquo;Navigate to the location by panning and zooming on the map&rdquo;. Once pin is dropped it can be move from one place to the other to change position. When clicked the button &ldquo;DROP PIN&rdquo; a pop up box will appear for the user agent to enter information such as the title of the Mission, Description, Uploading of Pictures and Video Documentation and also options if the agent wants to hide his/her profile, and if the agent want the Mission to be shown only to registered users, which means it cannot be search by public users, and an option to enable confirm first other action pin being added. Then when the user agent clicked save and the mission is now published. To cancel everything just click close to cancel everything from Drop Pin action. The origin pin will be different from other pins.</p>\r\n\r\n<p>The agent who authors the Mission can add more pins in the Map as well as other user agents can add pins to his map. When other user agents wants to add action pins to the authors map it will be the same process with the above paragraph except that when the action pin is saved I suggest that the author will confirm other action pin added before being posted or published if that option is enabled.</p>\r\n\r\n<p>We will be using Google Maps API V3 which is the latest google map available. In our preparation I also wanted to share you this user limit rule though we might not experience it at first but once it propagated and have many users at least you are informed about this. Link: <a href=\"https://developers.google.com/maps/documentation/javascript/usage\">https://developers.google.com/maps/documentation/javascript/usage</a></p>\r\n\r\n<p>When author views a map that he owns, he has also a feature to filter pins by author only, others, and secret agents. But when viewing other maps, views are all pins. A user agent can click on the pin then a pop up will appear about the action pin, other information will be displayed like Location, Uploaded Pictures and Videos.</p>\r\n',1412860220,1412784000,1422633600,'deactivate'),(5,'News & Events (why not change to announcements)','<p>News &amp; Events are like announcements, this are links posted by the super agent that links to a blog or to a certain link. The top page will display at least 5 announcements from the super agent. An announcement contains title, description with the link. The inside page for the News &amp; Events shall display all other news &amp; Events posted the by the Super Agent. We will include an automatic loading when scrolling down to display other news and events from the super agent.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Search</strong></p>\r\n\r\n<p>User Agents and Public users can use the following search function. The search function will crawl and retrieve the information base on keywords. Public users and agents can both search information from the site. We will add an auto suggest feature for the searching.</p>\r\n\r\n<p>For the advance search we can apply filtering options such as all, forum, blog, missions, category, agent, location, filtering by date and if possible by agents. Also base on the slide we can display popular tags which can be use for filtering by keyword.</p>\r\n',1412860256,1412784000,1424880000,'deactivate'),(6,'Back End Management Modules','<p>This part will be the Content Management System for the Project PI, Blog and Forum. The following modules will be the main tools to manage the content and information in the Front End Module.</p>\r\n\r\n<p><strong>Login</strong></p>\r\n\r\n<p>The CMS can only be accessed by the Super Agent.</p>\r\n\r\n<p><strong>User Management Module</strong></p>\r\n\r\n<p>Users Agents are listed in this module. The administrator can view all user agents registered and pending for activation. They will be listed in a table format with paging to shortened list and optimize query. The administrator can also make filter for pending users, active users, inactive users, blocked users and filter by keyword.</p>\r\n\r\n<p>The system shall consider users inactive after 1 year of no activity from action maps, forum and blog. The administrator also has the capability to suspend or block user agents as well as lift suspension. When a user is suspended his/her mission started will still be in public. The administrator can also activate user even without email confirmation done by the user.</p>\r\n\r\n<p>When viewing a user agent profile you can see all his activities and also all maps he/she has created.</p>\r\n\r\n<p><strong>Messaging (Suggestion)</strong></p>\r\n\r\n<p>The super agent can also make a direct message to a user agent. The same features in the front end module messaging.</p>\r\n',1412860325,1412784000,1422633600,'deactivate'),(7,'This will provide a loging feature for the PI system user Activities. This is like a live update on what are the current acitivity of the System.','<p>There are two types of users: Agents, and Public. Agents are users that registers to the site. Agents can create own maps, add pictures, make comments and responses, adopt an action and put pins in other maps and view other user agents map. User Agent account can be use to PI forums and blog sites. Agents can create threads, post message, reply to a message and vote messages in forums. User Agents can submit blog articles, post comments to article, and share articles across social media platforms. On the other hand Public users are not allow to make any actions except for viewing and social media sharing. There can only be one Super Agent which is the Administrator of the PI Site.</p>\r\n\r\n<p>There will be a provided registration page for users who wanted to register, the user required information are provided in SLIDE 22, or I suggest we should user Login Using Facebook account or Login Using Gmail Account. We will add a security questions for registering users. When a user register he needs to activate his/her account through confirmation link send to his/her email account. Users who are not yet activated are still considered inactive.</p>\r\n\r\n<p>User Agents can easily retrieve forgotten password through a secret question or through password request through email, then a link to a page for change of password is given.</p>\r\n\r\n<p>Each Agent have their own account tools, such as each user has their own dashboard, account or profile, mission manager, and the community page, where agents view the threads they made and messages posted, and a blog panel. There will also have a user setting to public his profile or hide himself from other user.</p>\r\n',1412860374,1412784000,1422633600,'deactivate'),(8,'SOFTWARE DETAILED DESIGN','<p>The following are the consolidated detailed design of the PI, Blog and forum functions. I also included the suggestions and what we have agreed upon by Sir. Paul</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Front End Module</strong></p>\r\n\r\n<p>The Front End Module will be the front face of the Planet Impossible site. The following list below are the Software Detailed Design for the Project Planet Impossible Site. These are story lines that explains how each function works and how it will be implemented.</p>\r\n',1412861585,1412784000,1422633600,'deactivate'),(9,'User Agents can easily retrieve forgotten password through a secret question or through password request through email, then a link to a page for change of password is given.','<p>Each Agent have their own account tools, such as each user has their own dashboard, account or profile, mission manager, and the community page, where agents view the threads they made and messages posted, and a blog panel. There will also have a user setting to public his profile or hide himself from other user.</p>\r\n\r\n<p>There will be a login page for Registered Users.</p>\r\n\r\n<p><strong>Messaging (Suggestion)</strong></p>\r\n\r\n<p>Each user agent can send a message to another user message. Agents can also reply to messages that was sent to them. As well as users can message the administrator for issues and other related matters. The messaging feature is the same with the messaging in facebook, except that there is no group messaging involve (we can add that in the future). Maybe we can also add file attachements to messages which only limits to office documents, images, and music.</p>\r\n',1412861630,1412784000,1423238400,'deactivate'),(10,'The mission category will display of Categories created by the Administrator as well as categories generated through tags.','<p><strong>Creating Missions / Adding Action Pins</strong></p>\r\n\r\n<p>In the create mission page, at first system will detect the current location of the user in the map. The current location of the user will be viewed at first, thereafter the map has been loaded &ldquo;ADD ACTION PIN&rdquo; will become enabled.</p>\r\n\r\n<p>Once the &ldquo;ADD ACTION PIN&rdquo; is clicked, the map is enabled for navigating to drop the action pin, the button message will change to &ldquo;DROP PIN&rdquo; and a notification above the map will display &ldquo;Navigate to the location by panning and zooming on the map&rdquo;. Once pin is dropped it can be move from one place to the other to change position. When clicked the button &ldquo;DROP PIN&rdquo; a pop up box will appear for the user agent to enter information such as the title of the Mission, Description, Uploading of Pictures and Video Documentation and also options if the agent wants to hide his/her profile, and if the agent want the Mission to be shown only to registered users, which means it cannot be search by public users, and an option to enable confirm first other action pin being added. Then when the user agent clicked save and the mission is now published. To cancel everything just click close to cancel everything from Drop Pin action. The origin pin will be different from other pins.</p>\r\n',1412861700,1412784000,1413475200,'deactivate'),(11,'When author views a map that he owns, he has also a feature to filter pins by author only, others, and secret agents. But when viewing other maps,','<p>When author views a map that he owns, he has also a feature to filter pins by author only, others, and secret agents. But when viewing other maps, views are all pins. A user agent can click on the pin then a pop up will appear about the action pin, other information will be displayed like Location, Uploaded Pictures and Videos.</p>\r\n\r\n<p>All action pin information added by other users agents to the owner of the map will also be added under responses below the mission map, also you can add responses through simple comments only (with Images or Videos) even without dropping a pin in the map.</p>\r\n\r\n<p>There will also be a social network links provided for each mission map.</p>\r\n\r\n<p>In the Owner View of the mission map, the author will have an interface for adding Mission Announcements for the mission map. He can manage content, add, update and remove announcements for the map, announcements can be in a form of video or images. This will be simple announcements with limitation such like twitter.</p>\r\n\r\n<p>I cannot confirm yet about the PIN CLUSTER click, I see it will not be fitted in the current process but we will try to find a work around for this.</p>\r\n\r\n<p>I also suggest that we will have our own style of Google Maps, since the Google Map API v3 has an enable styled maps.</p>\r\n\r\n<p>All missions that are being created are stored in the database, and notifies the super agent or administrator in the CMS as well as through his Email Address.</p>\r\n\r\n<p>I suggest that we also put Thumbs Up and Thumbs down and Report to an action pin. So that it can be easily take the attention of the super agent when reported or having so many thumbs down so that he can easily review the action pin.</p>\r\n',1412861735,1412697600,1422460800,'deactivate'),(13,'This is a Announcement!!!','<p>This is a Announcement!!!This is a Announcement!!!This is a Announcement!!!This is a Announcement!!!This is a Announcement!!!</p>\r\n',1447288392,1447304400,1447390800,'deactivate'),(15,'Paris Terror Attacks','<p>Falli pericula eu mel. Per partem deserunt complectitur cu, aeque antiopam per an. Doctus ceteros no vel. Eam iusto argumentum temporibus in. Mazim apeirian instructior ad eos, duo timeam facilis ad.</p>\r\n\r\n<p>Eos duis scribentur et. Qui ad ipsum accusam. Usu docendi mentitum definiebas an. Ponderum perpetua comprehensam ei nam, laboramus elaboraret dissentiet nec ne, ea mel delicata pericula.</p>\r\n\r\n<p>Et usu debet timeam, mentitum voluptaria sententiae his at, prima utamur dissentias ei cum. Sed ei invenire persequeris, illud augue periculis id eum, audire nonumes detraxit per ea. Eos possim adipiscing ei. Ne brute alterum nominavi cum, admodum legendos intellegat has no, minim appetere no pri. At duo graecis electram intellegat.</p>\r\n\r\n<p>Mea nominavi reprehendunt id. Ei paulo ponderum vis, utinam consulatu referrentur vel te, oratio appellantur no mea. Fastidii philosophia mei no. Maiorum adipisci contentiones vim ut, id viris perfecto mel, salutandi dignissim dissentiunt et nec. At eos prima posse dolor. Aperiam admodum volumus ex pro, ea diceret definitionem mea, has brute minim tacimates at.</p>\r\n\r\n<p>Ne idque legendos pertinacia cum, ea mei eloquentiam accommodare interpretaris, id scaevola deserunt pro. No suscipit appetere detraxit ius, nobis instructior cum ei. Qui iracundia voluptatum in, pro novum errem instructior eu. Gloriatur ullamcorper sit te, dicant legendos repudiare has an, omnis aperiri at pri. Pri audiam timeam in, ea sit diam wisi deleniti, mea molestie interpretaris ut.</p>\r\n\r\n<p>Pro duis eligendi ut, cum noster omittam rationibus ex. Eu tollit persecuti contentiones nec, has dolorum iracundia ad, ad sanctus docendi est. Munere minimum adipiscing no vel, ex usu alia inciderint. Ut facete recusabo qui.</p>\r\n\r\n<p>Conceptam assueverit duo te, vix ut populo scripta liberavisse. Has constituam reformidans consequuntur ex, an wisi semper mea. Sed mazim recusabo cu, te dicta iusto mea. Stet expetenda cum te, te mea tale aliquam.</p>\r\n\r\n<p>Id nonumes petentium urbanitas vel. Te nisl commune eam, est meis magna delenit an. Doming legendos dissentiunt no nec, eu qui etiam meliore. Sed ex putent numquam, populo maiorum his et. Modus putent ad sit, ex duo quodsi accusam periculis.</p>\r\n\r\n<p>Ad vis quis omnes platonem. Accumsan imperdiet vim ea, ei causae vocent invenire nam. Est atqui accusata eu, vix te regione molestiae. Quodsi vidisse contentiones cu qui. At quo regione legendos sapientem, an fugit reprimique repudiandae est.</p>\r\n\r\n<p>Sed legimus mediocrem voluptatum an. Ceteros similique eu his, erant ignota mel et. Utamur facilisis quo ut, ad offendit mediocritatem mei. Eu feugait epicuri mel, ad vitae eleifend mel, in est ridens iuvaret detracto. His ex iisque euripidis, ex qui suas complectitur. Et quem complectitur vix, sea veri dicta tritani ad.</p>\r\n\r\n<p>Quo at elit regione delectus, aperiri bonorum maiorum sit te. Clita aliquip platonem per at, te erat temporibus mei, ex labore numquam has. Soleat deleniti in eos, eos te option ocurreret. Detraxit suavitate cu quo, te quo laboramus intellegam.</p>\r\n\r\n<p>Ex has zril putent aperiri, clita semper id nec. Pri in zril populo. An his tamquam maiorum, ad dicit epicurei cum. In alia invidunt gubergren duo, ei est sumo meis perpetua. Te vel aperiri nostrud quaerendum, alterum referrentur suscipiantur at vis, ei nibh meis aeterno sed. Eos option gubergren no, pri ignota praesent sapientem eu.</p>\r\n\r\n<p>Eum at nullam semper, ut mel wisi primis abhorreant. Ne pri timeam posidonium, vivendum pericula argumentum vix te. Nam causae aliquid complectitur in. Ut ipsum dolorum utroque pri, ad suas elitr nominavi vix, quis agam sonet an mel. Posse contentiones et duo, eu has commodo incorrupte assueverit.</p>\r\n\r\n<p>Ei eripuit adversarium liberavisse usu, an facilisis sadipscing sit, eu qui illum mollis quaestio. Minim timeam omnesque qui an. Dictas consequuntur id sit, et modo detraxit vis, tamquam impedit at eam. Legere insolens reprimique no pro. Sea et dico agam, vim illum scribentur ei.</p>\r\n\r\n<p>Etiam aeterno accusamus qui eu. Qui ea vidit fierent, usu tollit fabellas mnesarchum in. Nostrud assentior cotidieque mea ea, impetus tincidunt qui eu. Est ipsum everti sensibus ut. Tamquam suscipit ut per. Meliore vituperata at mea, sed ea omnium invidunt voluptatibus, tale maluisset repudiandae pro te.</p>\r\n\r\n<p>Eum ei quem iudico quaestio. Te iriure discere dolores vis. Ex eam persius suscipit. Dicit copiosae abhorreant qui cu. Eos oratio nostro id, est fuisset molestie corrumpit no. Liber accumsan consectetuer ut qui.</p>\r\n\r\n<p>Postea conceptam cu nec, pri utamur interpretaris ad, ei movet invidunt dissentiunt his. Ad ius verear volumus, harum periculis adolescens nam ea. Vim ex quando graeco numquam. Prompta constituam ullamcorper ut sea, in numquam eruditi suscipit usu.</p>\r\n\r\n<p>Tota exerci cetero sit ne, his ad noluisse neglegentur. Idque sapientem pertinacia cum in, idque delectus argumentum eam eu. Cu augue iisque vis. Aperiam recteque voluptatibus eam cu, cu per oratio mollis, partem omnium delenit vim no. Nam modo reprimique in.</p>\r\n',1447650694,1448254800,1448341200,'deactivate'),(16,'Lorem Ipsum Dimsum!!2','<p>Id omnium iriure oporteat has, eu mei nisl munere percipit. Qui eu quem utinam principes, duis veniam maluisset ex pri. Duo diam graeco tractatos in, nam eu veniam populo, doctus senserit ea has. Cum ne errem doctus. Eu vel labitur utroque nostrum, per dico unum pericula no.</p>\r\n\r\n<p>Qui id saepe suscipit. Eu mel mandamus inciderint, vix choro cetero te, has in salutatus sadipscing liberavisse. Affert feugiat an per, nam an vitae oportere vituperatoribus. Cu vix dolor homero audiam, id aliquip regione tibique sed. Est atomorum recteque abhorreant id, vel an nemore antiopam.</p>\r\n\r\n<p>Semper omittam mentitum no cum, modus epicurei principes his cu, ei liber oporteat sit. Quot alienum suscipit ea sed. Eros abhorreant sententiae eum ad, rebum tollit eu nam. Mei at dicit dolorem. Dolor laboramus ius an. Populo pericula eam at.</p>\r\n\r\n<p>Nihil dolor tacimates in pri. Eros referrentur pri te. Quaestio concludaturque nam at. Eos mundi honestatis ut, mea no rebum doming phaedrum. Id ius facete diceret theophrastus, eam mutat dolores postulant ex, ex wisi audiam repudiare pro.</p>\r\n\r\n<p>Nam amet justo recusabo cu, ex per tota utinam conceptam. Vero aeque mea te, cum diceret partiendo ex, eum et prompta oportere. Aliquam feugiat pro at, zril aperiri te mel. Vix docendi inciderint persequeris ei, labores omittam ut sit. Ei eum accumsan vulputate delicatissimi, nihil essent quo in. Vis salutatus eloquentiam philosophia an, nam at etiam dolorum.</p>\r\n\r\n<p>Diam alia iudico an vel, tacimates cotidieque eloquentiam duo at. An ullum soluta fabellas mel. Esse constituto quo in, id nam regione prompta diceret. Ea vel latine placerat concludaturque, movet verear tacimates eos id, ad qui doming consequat interpretaris. Ut nam fabellas sensibus. Lucilius definiebas vim cu, quas laboramus persecuti id eos, prima verear definitiones ea eam. In mea partem laoreet, albucius delicata complectitur sea an, ut odio impedit per.</p>\r\n\r\n<p>In eum adhuc reque denique. At pro tempor audiam, malorum perpetua persequeris in est. Duo ut everti quaeque sapientem. Pri at debitis suscipit, vis no cibo mucius periculis. Nec at praesent voluptaria, cu cum aperiam conceptam.</p>\r\n\r\n<p>Sea eu nonumes oportere iudicabit. Viderer adipisci invenire an duo, assum novum impedit id vim, ex tale semper appellantur eos. Legere lobortis vis in, animal sensibus delicata ea ius, cu mea malis saepe. Suas dicant eu duo. Mei cu libris alterum epicurei, mea cu dissentiunt disputationi, saperet reformidans ei mei. Alia delenit ut his, est ut nisl mazim animal.</p>\r\n\r\n<p>Adhuc iriure nusquam duo no, an mel brute docendi electram, pro nibh corpora ne. Nec ea mollis impedit cotidieque, affert option pertinax ei mei. An vix sensibus maiestatis, ex vis posse scripserit. Pro illum efficiendi ne, eu mea fuisset prodesset intellegat. Sanctus corpora an est.</p>\r\n\r\n<p>Omittam mnesarchum sit at, ponderum honestatis mei ex. Id clita animal pro. Quis munere appellantur sed an, an vim aliquando interesset dissentiet. Mei ipsum fierent probatus no, eu mea scripserit neglegentur. Purto dolorem eum ut, ut nullam mnesarchum pri.</p>\r\n\r\n<p>Eam et modo omnes detraxit, at alienum legendos assueverit eum, quo ei legimus salutandi. An his possim oporteat. Quo duis fuisset platonem et, cum cu recteque reprimique interesset. In nihil graeco maiorum mea, no sed deserunt nominati, te cum fuisset aliquando. Tota nominati ex qui, mei an dicunt recusabo invenire.</p>\r\n\r\n<p>His inermis molestiae ne, usu cibo petentium liberavisse in. Omittam mandamus ne nec, agam admodum his an, quem wisi ad sed. Alii mucius temporibus mei id, mei tollit ridens iracundia eu, ei blandit salutandi usu. Impetus assentior est no. In pri veri iriure consetetur. Eu eam mediocrem adipiscing. An cum solet explicari instructior, inimicus persecuti expetendis nec te.</p>\r\n\r\n<p>Mei ipsum scaevola ullamcorper ex, quo oportere dignissim id. Ut eam alia saepe, cu mel tibique adversarium, et choro causae vim. Putant mandamus ut ius, has verear nonumes vivendum te. Habeo nobis tation mea ea, eum an essent impetus debitis. Cum consul sanctus cu, mel at nominavi assentior.</p>\r\n\r\n<p>Ius ea primis vulputate, duo ea munere dolorem. Ne quas augue gloriatur pri, ne qui affert eirmod intellegam, ei quidam inimicus his. Pro eu agam graeco constituam, tale consequat no pri. Putant scribentur eloquentiam ex sea, vel debet deterruisset eu, salutatus dignissim nec ea. Dolorem similique liberavisse ex sit. Fierent nominavi interpretaris vim an.</p>\r\n\r\n<p>Atqui velit tibique mea ex, ut equidem fuisset consequuntur vel, et civibus lobortis similique vis. Dissentiunt definitiones ea vel. At pro laudem moderatius. Nec omittam propriae ea, tollit pericula cum cu.</p>\r\n\r\n<p>Vis labore offendit an, duo quis ridens interpretaris an. Nec ne gloriatur suscipiantur concludaturque, ne sit omnium adipiscing vituperatoribus. Id suas consequat voluptatibus sed. Et mei quod alia commune. Vide nostrud mediocrem eu sed, ipsum idque quidam ut eum.</p>\r\n\r\n<p>Wisi tractatos at his. Quo noster commune ea, id fugit tamquam referrentur eam, quaestio tincidunt ex ius. Vix ex veri mandamus consequat, cu dicta dolor tamquam vel. Sit lucilius omittantur no.</p>\r\n\r\n<p>An pro inani eruditi convenire. Duo posse putent officiis ei. Putant efficiendi ne eam, cu sed justo harum evertitur. Ex vel commune expetenda, qui in zril virtute tractatos. Has meis habeo virtute ut. Soleat graeco et mei, ad admodum forensibus constituam est. Duo id pericula repudiare, te his facete corpora nominati.</p>\r\n\r\n<p>Usu perpetua neglegentur ut, dolor solet has no. In vero lorem vis, in brute aliquid eos. In causae tacimates qui. Timeam definiebas duo an. Vel eruditi suscipit delicata et, eu mea lorem tollit commune. Eu sensibus adversarium vis.</p>\r\n\r\n<p>Est timeam blandit scaevola te, tantas concludaturque ut eam, his no affert ornatus fastidii. Eam at diceret deserunt, sea omnis populo eirmod cu. Ex alienum euripidis vim. Est ut novum admodum electram, laudem utamur ei pri, in quod omnium voluptatum usu. Solet nostrum incorrupte mea ut, dico salutatus an vix.</p>\r\n',1447650952,1447218000,1452574800,'deactivate'),(17,'Find missing emails more easily in Gmail','<p>A thorough search of Gmail typically requires three steps:</p>\r\n\r\n<p>1. Keyword search of your inbox by entering search terms into search box at top of page.</p>\r\n\r\n<p>2. If not found, then opening Spam folder by finding it in left-hand panel (or entering in:spam in search box) and performing same keyword search again.</p>\r\n\r\n<p>3. If not found, then opening Trash folder by finding it in left-hand panel (or entering in:trash in search box) and performing same keyword search again.</p>\r\n\r\n<p><img alt=\"gmail-search.jpg\" src=\"http://cnet1.cbsistatic.com/hub/i/r/2015/11/12/93208dc4-5154-4ded-b4eb-3860b23fb31a/resize/620x/83984320bbbbe957d3f955588b0efa53/gmail-search.jpg\" style=\"height:auto; width:770px\" />Enlarge Image</p>\r\n\r\n<p>Screenshot by Matt Elliott/CNET</p>\r\n\r\n<p><br />\r\nNow, steps two and three are but a click away if your search comes up empty in step one. If a message in either Spam or Trash matches your search query, you&#39;ll see a line at the bottom of your search results:</p>\r\n\r\n<p><strong>Some messages in Trash or Spam match your search. View messages.</strong></p>\r\n\r\n<p>If you click the &quot;View messages&quot; link, you&#39;ll see messages that you may have accidentally trashed or got caught by Gmail&#39;s spam filter. The messages listed are tagged to show you whether they are in the Spam or the Trash folder.</p>\r\n\r\n<p>Also included in this update is help for overly general searches. If your search term is too general to return useful results, you&#39;ll see this message at the bottom of the results:</p>\r\n\r\n<p><strong>Improve result with with search options such as sender, date, size and more.</strong></p>\r\n\r\n<p>Clicking the &quot;search options&quot; link will open a panel that will help you refine your search terms to narrow the results.</p>\r\n',1447651739,1447650000,1452661200,'deactivate'),(29,'superanniversary super super','<p>You are invited to help us celebrate 20 years of automotive service to the residents of Monroe County. We are happy to be the number one dealer in both sales and service for the State of Kansas. It&#39;s something we have worked hard to accomplish. Please drop in at 1600 Main Street during the month of June to enjoy summer refreshments and see the latest models. While you are there, remember to register for our big July giveaway.</p>',1447816252,1447689600,1454169600,'deactivate'),(33,'Announcement !!!','<p>this is the announcementsthis is the announcementsthis is the announcementsthis is the announcementsthis is the announcementsthis is the announcementsthis is the announcementsthis is the announcementsthis is the announcementsthis is the announcementsthis is the announcementsthis is the announcementsthis is the announcementsthis is the announcements</p>\r\n',1451349660,1451278800,1451538000,'deactivate'),(41,'Quick 3','content here',1453947519,1453870800,1453957200,'deactivate'),(60,'fsdfsd','<p>fsdfsdf</p>',1456117200,1455984000,1455984000,'deactivate'),(63,'ffsafas edited','<p>safasfas</p>',1475683200,1475683200,1475769600,'deactivate'),(64,'announcement sample','<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a,</p>',1477639379,1472918400,1478707200,'deactivate'),(65,'example1','example2',1478102400,1478102400,1478188800,'deactivate'),(66,'lkjlk','lkjk',1478102400,1478016000,1478016000,'deactivate'),(67,'sample','sdsfdfsdfgsdgsdgsd',1508428800,1508342400,1508342400,'deactivate'),(68,'adsb   dgfgf','hghghg',1508774400,1508688000,1508688000,'deactivate'),(70,'November 30,2017','<pre style=\"background: rgb(238, 238, 238) none repeat scroll 0% 0%; border: 1px solid rgb(204, 204, 204); padding: 5px 10px;\">\r\nBonifacio Day</pre>',1511280000,1511280000,1511971200,'deactivate'),(71,'gdgdgs','getete',1512489600,1512489600,1512576000,'deactivate'),(78,'tttwtwet','twttwtew',1512489600,1512403200,1512403200,'deactivate'),(80,'Test','<p>Test101</p>',1521993600,1521993600,1522080000,'deactivate'),(87,'batabo','askdjfa',0,0,0,'deactivate'),(88,'sample title','<p>sample body</p>',1524625085,1524585600,1524758400,'deactivate'),(89,'fgdfg','dfdf',1524672000,1524672000,1524672000,'deactivate'),(90,'wkjdfkljk`','kjsdfjkasdkja',1524712329,1524844800,1524758400,'deactivate'),(91,'wkjdfkljk`','kjsdfjkasdkja',1524712419,1524844800,1524758400,'deactivate'),(92,'cvfvsdv','svsvcxvxc',1525190400,1525190400,1525276800,'deactivate'),(93,'test','dfsfdsdfsdfsdf',1525190400,1525190400,1525276800,'deactivate'),(94,'DFGHDFGDFG','DFGDFGDFGDFGFD',1525157763,1525190400,1525190400,'deactivate'),(95,'DFGGD','GDFGDFG',1525190400,1525190400,1525276800,'deactivate'),(96,'kashdfkjah','lkasjdfkljalsdk',1525167331,1526400000,1525795200,'deactivate'),(97,'kashdfkjah','lkasjdfkljalsdk',1525167661,1526400000,1525795200,'deactivate'),(98,'kashdfkjah','<p>hekjkljk</p>',1525167738,1526313600,1527091200,'deactivate'),(99,'Loooooooooooleng','test',1525232756,1525276800,1525363200,'deactivate'),(100,'ghfhgh','fghfgh',1525232791,1525276800,1525363200,'deactivate'),(101,'test title','test body',1525363200,1525363200,1525449600,'deactivate'),(102,'SMUC Now open!','Bukas na ang SM at ang daming tao!',1525325964,1525363200,1525536000,'deactivate'),(103,'SMUC Now open!','kakabukas lng ng SMUC daming tao grabe!',1525622400,1525622400,1526572800,'deactivate'),(104,'qwertyuiop','asdfghjkl',1525326790,1526227200,1527782400,'deactivate'),(105,'qwertyuiop','asdfghjkl',1526227200,1526227200,1527782400,'deactivate'),(106,'isa pa','isa pa',1525327025,1528041600,1527177600,'deactivate'),(107,'Make Announcement','<p>Make Announcement</p>',1525415752,1526227200,1526572800,'deactivate'),(108,'new','new content',1528214400,1528214400,1529078400,'deactivate'),(109,'AkoGwapo','Parang si Coco',1527782400,1527782400,1530374400,'deactivate'),(110,'another testing','another body',1526832000,1526832000,1527782400,'deactivate'),(111,'another testing','another body',1526832000,1526832000,1527782400,'deactivate'),(112,'ksjahdfkjah','kasdjfaksdnckan',1525363200,1525968000,1526659200,'deactivate'),(113,'sample1','content1',1525363200,1525363200,1527264000,'deactivate'),(114,'sample2','content2',1525363200,1526659200,1526486400,'deactivate'),(115,'Test02','Prod Test',1525622400,1525622400,1525622400,'deactivate'),(116,'fhdefhdfh','dfhdfhdfh',1525622400,1525708800,1525795200,'deactivate'),(117,'yuiytrewtrutretyttre','qwertyuiop',1525708800,1525795200,1525968000,'deactivate'),(118,'dasdsad','asdsadasd',1525708800,1525795200,1525881600,'deactivate'),(119,'dfsdfsd','sdfsdfs',1525708800,1525708800,1525795200,'deactivate'),(120,'sample3','alskdjfalkjs',1525708800,1525708800,1525708800,'deactivate'),(121,'sampl 4','content 4',1525708800,1525708800,1525708800,'deactivate'),(122,'hfdfhd','dfgdfg',1525708800,1525708800,1525708800,'deactivate'),(123,'dfgdfg','dfgdfg',1525708800,1525708800,1525708800,'deactivate'),(124,'title sample 1','content sample 2 ',1525795200,1525881600,1525968000,'deactivate'),(125,'sample 3as','contenasd',1525795200,1525795200,1525795200,'deactivate'),(126,'Test`12345','rteastds',1525795200,1525795200,1525795200,'deactivate'),(127,'trryrter','sfgddaasSs',1525795200,1525795200,1525795200,'deactivate'),(128,'testertetrtertert','<p>dfdsgdfgdfgdfg</p>',1537924640,1537891200,1537977600,'deactivate'),(129,'helen og troy','drftgrtfgdfgdfg',1537891200,1537891200,1537977600,'deactivate'),(130,'qweqweqweqweq','sdfsdf123123',1537891200,1537891200,1537977600,'deactivate'),(131,'philphil','<p>philphil</p>',1538442279,1538409600,1538496000,'deactivate'),(133,'announcement','<p>test</p>',1539235293,1539187200,1539273600,'deactivate');
/*!40000 ALTER TABLE `tblannouncements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblblog`
--

DROP TABLE IF EXISTS `tblblog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblblog` (
  `blogID` int(11) NOT NULL AUTO_INCREMENT,
  `blogTitle` varchar(500) NOT NULL,
  `blogSlug` varchar(500) DEFAULT NULL,
  `blogAuthor` varchar(30) DEFAULT NULL,
  `blogContent` text NOT NULL,
  `blogStatus` varchar(100) DEFAULT NULL,
  `blogDate` int(11) NOT NULL,
  `blogPublishDate` int(11) NOT NULL,
  `blogFeatureImage` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`blogID`)
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblblog`
--

LOCK TABLES `tblblog` WRITE;
/*!40000 ALTER TABLE `tblblog` DISABLE KEYS */;
INSERT INTO `tblblog` VALUES (86,'new blog','new blog',NULL,'<p>new</p>','publish',1477639489,1477670400,'/img/programs/post/screenshot-1.jpg'),(90,'kashkjdfkjh','kashkjdfkjh','kasjdflk','<p>lakdfj</p>','publish',1484211304,1484150400,''),(97,'CrystalMagic','','Magic','Healthful minerals','publish',1484533381,1483200000,''),(99,'My blog','','Jeremay Pedigloria','jifddfdsdfgmksdfvdjkvn mvnuhgurfjcmcfhuhdnmcfdscfisjcfndfdyfdfbhffyhbxcddufhduhfdydfydf','publish',1508477401,1508428800,''),(100,'jhhhhhhhhhhhhhhhhhhhhhhhhhh','','','768888','publish',1511403638,1511403638,''),(101,'yuiyuuyuuu','','uuuyyuu','uyuyyyuuyuy','publish',1511403666,1511403666,''),(102,'thtrtyy','','tytyr','yryrtyryrty','publish',1511404390,1511404390,'/img/programs/post/jane.jpg'),(103,'blog na bago','blog-na-bago','zelly','blog&nbsp;blog&nbsp;blog&nbsp;blog&nbsp;blog&nbsp;blog&nbsp;blog blog blog blog blogblog blog blogblog blog blog blog blog blogvblog blog blogblog&nbsp;blog&nbsp;blog&nbsp;blog&nbsp;blog&nbsp;blog&nbsp;blog&nbsp;blog&nbsp;blog&nbsp;blog&nbsp;blog&nbsp;blogblog&nbsp;blogblog&nbsp;blog&nbsp;blog&nbsp;blog&nbsp;blog&nbsp;blog&nbsp;blog&nbsp;blog&nbsp;blog&nbsp;blogblog&nbsp;blogblog&nbsp;blog&nbsp;blog&nbsp;blog&nbsp;blog&nbsp;blog vblog&nbsp;blog&nbsp;blog&nbsp;blog&nbsp;blog&nbsp;blog&nbsp;blog&nbsp;blog&nbsp;blog&nbsp;blog&nbsp;blogblog&nbsp;blog&nbsp;blog','publish',1522807124,1522807124,''),(104,'dsds','dsds','dsdsds','sdsddsd','publish',1522807242,1522807242,''),(107,'LORRAINE','lorraine','RHAINE','DFSFSDFSDF','publish',1524210386,1524210386,'http://abksite.gotitgenius.com/'),(110,'sample blog','sample-blog','sample author','sample content','publish',1525853370,1525853370,'/img/programs/post/SM Urdaneta.jpg');
/*!40000 ALTER TABLE `tblblog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblcontact`
--

DROP TABLE IF EXISTS `tblcontact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblcontact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location` varchar(25) NOT NULL,
  `home` varchar(50) NOT NULL,
  `number` varchar(100) NOT NULL,
  `email` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblcontact`
--

LOCK TABLES `tblcontact` WRITE;
/*!40000 ALTER TABLE `tblcontact` DISABLE KEYS */;
INSERT INTO `tblcontact` VALUES (1,'Philippines','1123, Somewhere City, Philippines','+239-3823-1234 (SMART), 09431234567 (SUN),0987642342423 ,      54353453453(qweqe),','everyone@company.com'),(2,'United States of America','1123, Some Area. Los Angeles, CA. ','+239-3823-3434','someone@company.com ');
/*!40000 ALTER TABLE `tblcontact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbldonations`
--

DROP TABLE IF EXISTS `tbldonations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbldonations` (
  `txn_id` varchar(255) NOT NULL,
  `fname` varchar(64) DEFAULT NULL,
  `lname` varchar(64) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `payment_amount` double NOT NULL,
  `payer_email` varchar(255) NOT NULL,
  `donation_date` int(11) NOT NULL,
  PRIMARY KEY (`txn_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbldonations`
--

LOCK TABLES `tbldonations` WRITE;
/*!40000 ALTER TABLE `tbldonations` DISABLE KEYS */;
INSERT INTO `tbldonations` VALUES ('44D799237N8112152','','','',1,'jplacsinto-buyer@gmail.com',1425199897),('9X43484146366334M','jane','doe',NULL,3,'jplacsinto-test2@gmail.com',1425202565);
/*!40000 ALTER TABLE `tbldonations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblinquiries`
--

DROP TABLE IF EXISTS `tblinquiries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblinquiries` (
  `inqID` int(11) NOT NULL AUTO_INCREMENT,
  `inqSender` varchar(255) NOT NULL,
  `inqSubject` varchar(255) NOT NULL,
  `inqEmail` varchar(255) NOT NULL,
  `inqMessage` varchar(255) NOT NULL,
  `inqDate` int(11) NOT NULL,
  `inqStatus` int(11) NOT NULL,
  `inqLatestReplyDate` int(11) DEFAULT NULL,
  PRIMARY KEY (`inqID`)
) ENGINE=InnoDB AUTO_INCREMENT=129 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblinquiries`
--

LOCK TABLES `tblinquiries` WRITE;
/*!40000 ALTER TABLE `tblinquiries` DISABLE KEYS */;
INSERT INTO `tblinquiries` VALUES (57,'Patrick Mole','This is a sample reply ','pats@mailinator.com','This is a sample reply  This is a sample reply This is a sample reply This is a sample reply This is a sample reply This is a sample reply This is a sample reply This is a sample reply This is a sampl',1456192837,1,1456192860),(58,'sdasda','ds','asd@mailinator.com','dsa',1456195951,1,1456206740),(59,'asad','aaas','fsd@mailinator.com','asaf',1456206831,1,1456207253),(61,'jjjjjjjjj','jjjjjjjjjj','fsd@mailinator.com','jjjjjjjjjjjjjj',1456206989,1,1456207245),(62,'Leo Ordonez','Test Subject','third.ordonez@gmail.com','This is a test message.',1456207020,1,NULL),(64,'3:18 pm','3:18 pm','me@mailinator.com','3:18 pm',1456211931,1,1456211957),(65,'sdasf','4:20','fsgs@mailinator.com','asdfgh',1456215637,1,1456215712),(74,'thisismy fullname','thisismy subject','testmail@mailinator.com','message',1457322521,1,NULL),(94,'Patrick Mole','my subject','patrickmole09@gmail.com','my message',1457340661,0,NULL),(98,'qwerty','123456','qwer@mailinator.com','234567sdfghjDFGH',1457414659,1,NULL),(99,'janine hazel','super refresh','jhazel@mailinator.com','ahahhahahahahahaah\r\n\r\n\r\nahhahah\r\nahahah\r\nahahahah',1457414754,1,NULL),(100,'test ting','1234567','jha@mailinator.com','qwergt\r\nasdfgh\r\nasdfg\r\n12345\r\n~!@#$\r\nQWERT',1457414860,1,NULL),(103,'pangalan ko to','suhbject ko to','emailko@mailinator.com','message ko to',1457417367,0,NULL),(108,'ako to','This is my subject','emailko@mailinator.com','message this',1457418671,1,NULL),(113,'batman','batman','batman@mailinator.com','batman',1457425164,0,NULL),(114,'superman','superman','superman@mailinator.com','superman',1457425616,0,NULL),(116,'jeremay','edededed','jeremaypedigloria019@gmail.com','aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',1511341682,0,NULL),(117,'Lorraine Manzano','Request','manzano@mailinator.com','Can I request some pizza HAHAHA',1512548854,0,NULL),(119,'Rahine','Rahinfe','haha@mailinator.com','jgndfjgndfjgnkdjfg',1521448916,0,NULL),(120,'Kezel Ann Eugenio','Sample only','zel@mailinator.com','Testing purposes only.',1521449043,1,NULL),(123,'Poging Pare','qwe','qwe@mailinator.com','qwe',1524212487,0,1539136669),(124,'sadasdasdasdasdas','dasdas','harris@mailinator.com','afds',1525415530,0,NULL),(126,'Ricbon Ibe','test subject','bonbon@mailinator.com','test message',1526019469,0,NULL),(127,'Lorraine','gsdgsd','MANZANOLORRAINE15@GMAIL.COM','fhdfhdfhdfhdfh',1526622376,0,NULL),(128,'test','tertert','manzanolorraine15@gmail.com','ertdffdg',1539235528,0,NULL);
/*!40000 ALTER TABLE `tblinquiries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblinquiryreplies`
--

DROP TABLE IF EXISTS `tblinquiryreplies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblinquiryreplies` (
  `inqReplyID` int(11) NOT NULL AUTO_INCREMENT,
  `inqID` int(11) NOT NULL,
  `inqReplySender` int(11) NOT NULL,
  `inqReplyMessage` text NOT NULL,
  `inqReplyDate` int(11) NOT NULL,
  PRIMARY KEY (`inqReplyID`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblinquiryreplies`
--

LOCK TABLES `tblinquiryreplies` WRITE;
/*!40000 ALTER TABLE `tblinquiryreplies` DISABLE KEYS */;
INSERT INTO `tblinquiryreplies` VALUES (1,3,1,'hahahaahahahahahahahaha',1417021911),(2,3,1,'grrrsdlf, asfa, sf,la ;f,af, asdf\r\nasdfa\r\nsf asdfl,dsg;l,g sfg,sg \r\nfgsdg sdfg dsfgdsgdsg sdgs gsdfgsdfggf',1417022023),(3,3,1,'grrrsdlf, asfa, sf,la ;f,af, asdf\r\nasdfa\r\nsf asdfl,dsg;l,g sfg,sg \r\nfgsdg sdfg dsfgdsgdsg sdgs gsdfgsdfggf',1417022071),(4,3,1,'akamdlkamalsmaslmlkmdslkam sflmas lfasmfl afmal fmslfmaslfmaslfmaslfmasmflkmsldfmm asfmaslfm asfmas fkmasl mkasdflmaslfkm admfaslfmas lfmasdlfkmasfm aslfkmaslkm faslfm alfmklmlmlsmlsmdfldmsfl smdflms flasmfklsm slkdmfslmf aslkmfaslfm asldfmaslkfmas klfmaslfm asfkmaslfkmmflmflasm adfmas asdf asfsafa sdfasfasfasdfasdf asdfasf asfas fasdfsdfsdfsdfsfd dsgdfdfg',1417022548),(5,1,1,'hello there!',1420996701),(6,1,1,'stfgs gs ',1423325837),(7,2,1,'Vivamus fringilla bibendum dolor in scelerisque. Quisque nec mi vitae nibh maximus lobortis. Integer blandit enim erat. Praesent suscipit, lorem id blandit faucibus, mauris ligula pharetra nunc, vel varius purus orci id elit. Nulla eu tellus rutrum, tempor mauris ut, ullamcorper dolor. Mauris imperdiet tellus id eleifend venenatis. Mauris fermentum lacus at ipsum tempor ultrices. Cras malesuada cursus semper. Curabitur iaculis cursus diam, at bibendum justo. Nam mattis, quam non consequat mattis, leo orci molestie nunc, a condimentum enim elit vel eros. Pellentesque sed luctus odio, id hendrerit urna. Proin viverra nisl sit amet mi aliquam pulvinar. Cras non blandit leo. Nullam sem diam, volutpat non fringilla at, ornare sit amet nulla. Etiam vehicula tortor orci, quis venenatis arcu rutrum eget.\r\n\r\nVivamus nec purus nisl. Proin maximus vestibulum sollicitudin. Sed nulla lectus, mattis at aliquam eu, ultrices at nisl. Quisque id aliquam lectus, at imperdiet erat. Nullam at vestibulum dolor. Ut in gravida ex. Donec ultricies consectetur urna, nec posuere nibh aliquet sagittis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vestibulum luctus neque enim, sed dictum metus pellentesque nec.\r\n\r\nDuis sagittis interdum urna, in interdum sem pharetra sed. Curabitur ac nulla interdum, feugiat nisi ullamcorper, lobortis enim. Morbi tellus eros, dictum vitae convallis ac, accumsan at massa. Nam et mi fermentum tortor molestie vestibulum. Nunc dolor mi, pharetra in ornare a, ullamcorper nec ligula. Ut ut molestie est. Suspendisse dolor nisl, vulputate suscipit feugiat eleifend, sagittis ac arcu. Vivamus dignissim maximus nisl a ultricies.',1425191226),(8,8,1,'really?',1452664930),(9,4,1,'weh?',1452667247),(10,10,1,'Hi Rebecca, \r\nThank you for your interest in TradeMark Media! My name is Gina Hollis, and I\'ll be helping you decide if we\'re a good fit for your project. I\'ll contact you shortly to learn more and answer any questions you might have. \r\n\r\nIn the meantime, feel free to email or call me directly for more information about our services. I look forward to talking with you soon. \r\n\r\nBest, Eninaj Geek\r\n',1452668639),(11,11,1,'hello hello hello hello hello hello \r\nhello hello hello hello \r\nhello hello hello \r\nhello hello \r\nhello ',1453077708),(12,12,1,'ssdfafaadsf',1453084685),(13,13,1,'john una',1453084823),(14,20,1,'\"\'!~!@#$%^&*(',1455688280),(15,49,1,'sasa',1455755304),(16,49,1,'sasa',1455755311),(17,49,1,'dada1',1455755353),(18,49,1,'fdddddhfdhfdh',1455756046),(19,49,1,'dfgsdfsdf',1455756050),(20,49,1,'sdhs',1455756055),(21,49,1,'wetwe',1455756060),(22,49,1,'wetwet',1455756065),(23,49,1,'wet',1455756070),(24,49,1,'ea',1455756136),(25,49,1,'sadd',1455756267),(26,49,1,'dsfdsf',1455756382),(27,29,1,'reply ngayon',1455762831),(28,55,1,'hi this',1456190582),(29,56,1,'yow',1456191056),(30,56,1,'df,gdkldjfgdfg',1456191575),(31,44,1,'sdfsdfsfsdfs',1456191615),(32,56,1,'send',1456191784),(33,24,1,'gdgdgdgfdg',1456192714),(34,57,1,'ok',1456192860),(35,58,1,'dgdgddgf',1456206740),(36,59,1,'sdfdsfsdfsf',1456206893),(37,61,1,'on fifty eigth pm feb twenty three reply',1456207122),(38,61,1,'pacheck ng time',1456207245),(39,59,1,'2:00 pm',1456207253),(40,64,1,'3:19pm',1456211957),(41,65,1,'4:21 pm',1456215712),(42,70,1,'okay',1457319131),(43,72,1,'content',1457321332),(44,86,1,'jyjy',1457335911),(45,91,1,'this',1457340354),(46,109,1,'DF',1457419387),(47,123,1,'asdasdasd',1525065926),(48,123,1,'asdasdasd',1525065943),(49,123,1,'asdasdasd',1525065954),(50,123,1,'ako ang may sala',1525065978),(51,123,1,'jfgjfgjfgjf',1525067999),(52,123,1,'jfgjfgjfgjf',1525068003),(53,123,1,'jfgjfgjfgjfdhghfghfgh',1525068007),(54,123,1,'jfgjfgjfgjfdhghfghfgh',1525068011),(55,123,1,'jfgjfgjfgjfdhghfghfgh',1525068015),(56,123,1,'jfgjfgjfgjfdhghfghfgh',1525068018),(57,123,1,'jfgjfgjfgjfdhghfghfgh',1525068022),(58,123,1,'jfgjfgjfgjfdhghfghfgh',1525068027),(59,123,1,'jfgjfgjfgjfdhghfghfgh',1525068063),(60,123,1,'xzxczxczxcz',1525068078),(61,107,1,'Hello',1525068109),(62,107,283,'fgfhjklgfdsasdfghjgfdsdfghjkhgfdsdghj',1525425381),(63,107,1,'dsfgfgdfgd',1525769225),(64,107,1,'Hello',1525769258),(65,107,1,'asdasdasd',1525845774),(66,107,1,'fgdfgdfgdfg',1525856431),(67,107,1,'hello',1525856448),(68,107,283,'reply reply',1525930796),(69,107,283,'sadasdsad',1525930848),(70,107,283,'sadasdsad',1525930852),(71,123,1,'test test test etst est est est',1539136669);
/*!40000 ALTER TABLE `tblinquiryreplies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblnewsletteremails`
--

DROP TABLE IF EXISTS `tblnewsletteremails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblnewsletteremails` (
  `nleID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `dateAdded` int(11) NOT NULL,
  PRIMARY KEY (`nleID`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblnewsletteremails`
--

LOCK TABLES `tblnewsletteremails` WRITE;
/*!40000 ALTER TABLE `tblnewsletteremails` DISABLE KEYS */;
INSERT INTO `tblnewsletteremails` VALUES (5,'jplacsinto@gmail.com','jplacsinto@gmail.com',1420989474),(11,'sdsdaf','jj@mailinator.com',1450409674),(15,'a','bbb@mailinator.com',1454998458),(16,'Lorraine Manzano','manzano@mailinator.com',1539136759),(17,'Lorraine Manzano','manzanolorraine15@gmail.com',1539224520);
/*!40000 ALTER TABLE `tblnewsletteremails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblother`
--

DROP TABLE IF EXISTS `tblother`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblother` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(20) NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblother`
--

LOCK TABLES `tblother` WRITE;
/*!40000 ALTER TABLE `tblother` DISABLE KEYS */;
INSERT INTO `tblother` VALUES (1,'About ABK','<p><img alt=\"\" src=\"http://www.claruscommerce.com/wp-content/uploads/2014/06/loyalty-small.png\" style=\"height:100px; width:100px\" /></p>\r\n\r\n<p>test Geeksnest Inc. is a growing Web Company that combines internet marketing and advertising solutions partnered with dedicated, motivated and professional team. At Geeksnest Inc. we deliver an exceptional customer satisfaction matched with utmost professionalism, accountability, and transparency in our services. We are experienced and motivated to work with a major client on various systems and projects. We are committed to provide you on uplifting the best potential of your company. We also provide products and services information to international clients and provide comprehensive IT solutions to an exceptional customer satisfaction.</p>\r\n\r\n<p>Geeksnest Inc. is a growing Web Company that combines internet marketing and advertising solutions partnered with dedicated, motivated and professional team. At Geeksnest Inc. we deliver an exceptional customer satisfaction matched with utmost professionalism, accountability, and transparency in our services. We are experienced and motivated to work with a major client on various systems and projects. We are committed to provide you on uplifting the best potential of your company. We also provide products and services information to international clients and provide comprehensive IT solutions to an exceptional customer satisfaction.</p>\r\n\r\n<p>Geeksnest Inc. is a growing Web Company that combines internet marketing and advertising solutions partnered with dedicated, motivated and professional team. At Geeksnest Inc. we deliver an exceptional customer satisfaction matched with utmost professionalism, accountability, and transparency in our services. We are experienced and motivated to work with a major client on various systems and projects. We are committed to provide you on uplifting the best potential of your company. We also provide products and services information to international clients and provide comprehensive IT solutions to an exceptional customer satisfaction.</p>\r\n'),(3,'Main Tagline','The freedom to be a &quot;Changemaker&quot;<br />\r\nfor the common good.<br/>\r\n'),(4,'Donate','<p>jLorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus euismod iaculis orci, eget varius augue tempus et. Quisque fermentum facilisis mi eu finibus. Quisque dapibus accumsan dignissim. Pellentesque sit amet porttitor massa, sit amet lacinia sapien. Vestibulum non eleifend sem. Pellentesque malesuada id justo ac ornare. Sed sit amet imperdiet leo.</p>\r\n\r\n<p>Nunc auctor, ante non mattis vestibulum, augue leo pharetra nisl, non interdum urna mi ac arcu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Pellentesque condimentum a sapien a auctor. Pellentesque euismod odio non dui pretium, id cursus neque vehicula. Nunc pulvinar sapien ut libero blandit gravida. Cras tempus eu augue a placerat. Cras vel euismod odio. Pellentesque eget lacinia dolor, id varius sem. Fusce accumsan faucibus accumsan. Cras vel nulla mollis libero varius mattis. Vestibulum laoreet sodales tellus, ac tincidunt dolor cursus sit amet. Quisque eget nunc dignissim, dignissim lorem at, porttitor ligula. Nullam nec odio suscipit, ultrices eros id, suscipit lacus. Ut id magna congue, blandit magna ut, maximus orci.</p>\r\n'),(5,'Donate Other Info','<h3>I WANT TO SUPPORT ANG BAYAN KO PROGRAMS<br />\r\nSample Fund Name</h3>\r\n\r\n<p>Your gift to the Sample Fund Name will keep ANG BAYAN KO strong well into the future, providing a continuous stream of income that will fund programs in perpetuity.</p>\r\n');
/*!40000 ALTER TABLE `tblother` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblpages`
--

DROP TABLE IF EXISTS `tblpages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblpages` (
  `pageID` int(11) NOT NULL AUTO_INCREMENT,
  `specialPage` int(11) NOT NULL DEFAULT '0',
  `pageParent` int(11) NOT NULL,
  `pageTitle` varchar(255) NOT NULL,
  `pageSlug` varchar(500) DEFAULT NULL,
  `pageKeywords` text,
  `pageContent` text,
  `pageType` varchar(255) NOT NULL,
  `pageOrder` int(11) NOT NULL,
  `pageActive` int(11) DEFAULT '1',
  `pageBanner` varchar(255) DEFAULT NULL,
  `pageLastUpdated` int(11) NOT NULL,
  PRIMARY KEY (`pageID`)
) ENGINE=InnoDB AUTO_INCREMENT=136 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblpages`
--

LOCK TABLES `tblpages` WRITE;
/*!40000 ALTER TABLE `tblpages` DISABLE KEYS */;
INSERT INTO `tblpages` VALUES (6,1,0,'Retiring to Make a Difference','retiring-to-make-a-difference','Retiring to Make a Difference','<p>Retiring to Make a Difference</p>\r\n','pages',0,1,'',1415532210),(7,1,0,'Businesses for Social Change','businesses-for-social-change','Businesses for Social Change','<p>Businesses for Social Change</p>\r\n','pages',0,1,'',1415532601),(8,0,0,'The Board of Trusteesxyzzz','the-board-of-trusteesxyzzz','The Board of Trustees','<p>Lorem ipsum dolor sit amet, choro assueverit nec no, ad eum constituto dissentias. Posse inimicus dissentias quo ne, dico soleat no per. Pri principes scribentur ex, usu no idque dictas tibique. Nam justo delicata id. Ex est nibh laoreet tractatos.</p>\r\n\r\n<p>Vero ubique in eos, assum mandamus vituperata pro ut, has liber consulatu ea. Modus graecis an eos, mel cu ornatus alienum. Vel ea diceret ornatus fastidii, eu nostrud phaedrum accusata pri, ne duo putant diceret splendide. Te stet harum ubique has, eum ne vidisse diceret fastidii, has nobis utroque id. Aliquam instructior per ei. Mel quot oratio vituperatoribus no, ei homero consequuntur mea.</p>\r\n\r\n<p>Affert soluta his at, aliquid labores vim ea. Eos et erat accusam, diam periculis cu sed, ea vix tota salutatus accommodare. Consul iuvaret et eos. Sea solum harum contentiones no, decore mnesarchum scriptorem nam eu, mel ex verterem incorrupte. Vis ut harum labitur adversarium. Nonumy constituam est ut, eos dicta lobortis honestatis ea.</p>\r\n\r\n<p>Ut scaevola imperdiet rationibus eos, eam putant audire fierent ei. Te ullum voluptatibus pro. Augue accusam pro eu, habeo voluptatum inciderint nec an. Esse clita platonem id mei, sed adhuc minim no. Nemore essent placerat vis id, ceteros delicata ne mei.</p>\r\n\r\n<p>Ius no nobis ridens, tota mollis an vim. Sonet putant mediocritatem vis ea. Eu appareat delicatissimi pri, has ad graece tamquam, ea vim facer harum paulo. Vix recteque aliquando abhorreant ne, ei causae doctus omittam mea. Mea ad sale labitur vituperata, ea sed etiam neglegentur.</p>\r\n','pages',2,1,'/img/pages/hacker.png',1476263234),(9,0,0,'Friends Abroad-Abroads','friends-abroadabroad','Friends Abroad','<p>Lorem ipsum dolor sit amet, ei quo accusam offendit consectetuer, ne graeco urbanitas signiferumque mel, nam ad modus fuisset perfecto. Sanctus nostrum assueverit id est, pro ei deserunt qualisque dissentiet, vide eruditi gubergren ex mea. Eam velit prompta at, lorem praesent dignissim eu nam. Ut ius persius explicari similique, ius in viderer fabulas dissentiet. His magna viris debitis no, mel ad purto rebum accumsan. Cu his vide dolor mediocritatem, ius admodum salutatus definitionem ei, qui ut tale eleifend.</p>\r\n\r\n<p>Posse fabellas rationibus et mea, vel delectus liberavisse at. Primis iudicabit pro ei. Ne duo facilisi singulis honestatis, scaevola maiestatis ei sea. Vis graece assentior in, sed ex senserit salutandi eloquentiam.</p>\r\n\r\n<p>Ad mel accusamus torquatos elaboraret, unum malorum per eu, usu eu consectetuer conclusionemque. Eam ad recteque tractatos. Pro molestie periculis id, an posse aliquip vivendum pri. Ut his graeci persius. Magna iuvaret voluptatibus ad has.</p>\r\n\r\n<p>Vel congue vocibus quaestio eu. Te per veniam ridens voluptua. Summo impedit scaevola ne eum. Soleat causae aliquando sit id, periculis expetendis ex qui, et regione graecis his. Pro nobis omnes interesset te. Ei fastidii posidonium nam.</p>\r\n\r\n<p>Iuvaret labores repudiare at nec, vim eu eirmod vocent voluptua. Natum nemore pertinacia an sed. Novum menandri pro ex. Has alienum antiopam id, eum et vocibus feugait, torquatos prodesset mei eu. Per ad tota eligendi repudiare. Ex vim animal saperet, everti indoctum an usu.</p>\r\n\r\n<p>Cu alia iusto aliquip mei, vel error veniam maiestatis ea. Mei ut tacimates disputationi, at cibo falli mnesarchum eos. Invenire necessitatibus eu vis, nam id cetero sanctus mnesarchum. Et nam lorem audiam cotidieque, ius ad graeco theophrastus.</p>\r\n\r\n<p>Summo veritus id qui. Et est suavitate tincidunt dissentias, mutat dolorem propriae vel et. Id duo homero periculis iracundia, ut usu justo deseruisse. An nam veritus graecis forensibus. Qui erat autem democritum eu, modo alia ne sea, ut mel vocent oportere.</p>\r\n\r\n<p>Aperiri corpora torquatos id vix, at dicta summo intellegebat sed. At integre ornatus per. Dicat fierent has ne, ut usu amet aliquam. Vix dicat tempor aliquid cu, eruditi necessitatibus sed in. Nam case vocent ea, etiam utinam conclusionemque eu has. Nam animal interpretaris at, pri in saepe deserunt, diceret gubergren pri ex. Eum admodum perfecto te, ut vel civibus periculis repudiare, nonumy sapientem at mei.</p>\r\n\r\n<p>Nam vero dissentias ut, mea tempor gubergren ad. Vis quando mediocrem intellegat at. Mel te tation soleat, at autem meliore est, dolorem noluisse eu has. Vim magna suavitate et.</p>\r\n\r\n<p>Est fabulas admodum at. In vis quot eligendi adipiscing, eum te eius viderer, consequat dissentiunt eum te. Possit senserit efficiendi et vis, cum ei quem aliquid legimus. Eius perpetua no qui, eos percipit erroribus eu, in audire dolorem sed. Sit vidit velit verear et. Usu quod ornatus te.</p>\r\n','pages',11,0,'',1476322971),(10,0,0,'Be a Partner','be-a-partner','Be a Partner','<p>Be aa partner</p>\r\n','pages',1,0,'/img/pages/volunteering-in-the-philippines.1200.jpg',1453952971),(11,0,0,'Volunteering','volunteering','Volunteering','','pages',5,0,'/img/newsletter/1MB PRINCIPLE (1).jpg',1453969166),(12,0,0,'Total Donations','total-donations','Total Donations','<p>This includes the total donations of Ang Bayan Ko Foundation</p>\r\n','pages',6,0,'',1453969103),(50,1,0,'Support your Chosen Program','support-your-chosen-program','Support your Chosen Program','<p><img alt=\"\" src=\"http://images4.fanpop.com/image/photos/16000000/Beautiful-Cat-cats-16095933-1280-800.jpg\" style=\"height:800px; width:1280px\" />j Lorem ipsum dolor sit amet, choro assueverit nec no, ad eum constituto dissentias. Posse inimicus dissentias quo ne, dico soleat no per. Pri principes scribentur ex, usu no idque dictas tibique. Nam justo delicata id. Ex est nibh laoreet tractatos.</p>\r\n\r\n<p>Vero ubique in eos, assum mandamus vituperata pro ut, has liber consulatu ea. Modus graecis an eos, mel cu ornatus alienum. Vel ea diceret ornatus fastidii, eu nostrud phaedrum accusata pri, ne duo putant diceret splendide. Te stet harum ubique has, eum ne vidisse diceret fastidii, has nobis utroque id. Aliquam instructior per ei. Mel quot oratio vituperatoribus no, ei homero consequuntur mea.</p>\r\n\r\n<p>Affert soluta his at, aliquid labores vim ea. Eos et erat accusam, diam periculis cu sed, ea vix tota salutatus accommodare. Consul iuvaret et eos. Sea solum harum contentiones no, decore mnesarchum scriptorem nam eu, mel ex verterem incorrupte. Vis ut harum labitur adversarium. Nonumy constituam est ut, eos dicta lobortis honestatis ea.</p>\r\n\r\n<p>Ut scaevola imperdiet rationibus eos, eam putant audire fierent ei. Te ullum voluptatibus pro. Augue accusam pro eu, habeo voluptatum inciderint nec an. Esse clita platonem id mei, sed adhuc minim no. Nemore essent placerat vis id, ceteros delicata ne mei.</p>\r\n\r\n<p>Ius no nobis ridens, tota mollis an vim. Sonet putant mediocritatem vis ea. Eu appareat delicatissimi pri, has ad graece tamquam, ea vim facer harum paulo. Vix recteque aliquando abhorreant ne, ei causae doctus omittam mea. Mea ad sale labitur vituperata, ea sed etiam neglegentur.</p>\r\n','pages',0,1,'',1454638852),(55,0,1,'Page No 1 latest','page-no-1-latest','11111111111 latest','<p><img alt=\"\" src=\"http://www.thinkstockphotos.com/CMS/StaticContent/Hero/TS_AnonHP_462882495_01.jpg\" style=\"height:430px; width:497px\" />latestjjjjjjjjjjjjjjdsfadsfasdfasdfdsfadsfasdfasdfdsfadsfasdfasdfdsfadsfasdfasdfdsfadsfasdfasdfdsfadsfasdfasdf</p>\r\n\r\n<p>dsfadsfasdfasdfvdsfadsfasdfasdfdsfadsfasdfasdf</p>\r\n\r\n<p>dsfadsfasdfasdfv</p>','program',1,1,NULL,1457415485),(58,0,1,'Page No 2','page-no-2','Page One','<p><img alt=\"\" src=\"http://all4desktop.com/data_images/original/4237684-images.jpg\" />2Page OnePage OnePage OnePage OnePage OnePage OnePage OnePage OnePage OnePage One</p>\r\n\r\n<p>Page OnePage OnePage OnePage One.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Page OnePage One</p>','program',2,1,NULL,1457315087),(59,0,1,'Page No 4','page-no-4','Page Two','<p>Page TwoPage TwoPage TwoPage TwoPage Two</p>\r\n','program',4,1,NULL,1452570764),(60,0,1,'test page','test-page','test keyword','<p><strong>Always be thankful</strong></p>\r\n\r\n<p><em>Always be thankful</em></p>\r\n\r\n<p><span style=\"background-color:Yellow\">Always be thankful</span></p>','program',5,1,NULL,1524030665),(61,0,1,'Page No 3 latest','page-no-3-latest','2222222222222 latest','<p>latest fiesta Page 1fiesta Page 1fiesta Page 1fiesta Page 1fiesta Page 1</p>','program',3,1,NULL,1457415508),(62,0,2,'Personal Care Page','personal-care-page','Personal Care Page','<p>Personal Care PagePersonal Care PagevPersonal Care Page</p>\r\n\r\n<p>Personal Care PagevPersonal Care Page</p>\r\n\r\n<p>Personal Care PagePersonal Care PagePersonal Care Pagev</p>\r\n\r\n<p>Personal Care PagePersonal Care Page</p>\r\n','program',1,1,NULL,1447730810),(63,0,2,'Personal Care Page for women','personal-care-page-for-women','Personal Care Page for women','<p>Personal Care Page for womenPersonal Care Page for womenPersonal Care Page for womenPersonal Care Page for womenPersonal Care Page for womenPersonal Care Page for womenPersonal Care Page for women</p>\r\n\r\n<p>Personal Care Page for womenPersonal Care Page for womenPersonal Care Page for womenPersonal Care Page for women</p>','program',0,1,NULL,1511243703),(64,0,3,'sample page','sample-page',NULL,NULL,'program',0,NULL,NULL,1447830552),(67,0,0,'About My Communitys(IES)New new','about-my-communitysiesnew-new','communityAbout My Community','<p>About My Community&nbsp;About My Community&nbsp;About My Community&nbsp;About My Community&nbsp;About My Community&nbsp;About My Community&nbsp;About My CommunityAbout My CommunityAbout My CommunityAbout My CommunityAbout My CommunityAbout My CommunityAbout My CommunityAbout My CommunityAbout My CommunityAbout My CommunityAbout My CommunityAbout My CommunityAbout My CommunityAbout My CommunityAbout My CommunityAbout My CommunityAbout My CommunityAbout My CommunityAbout My CommunityAbout My CommunityAbout My CommunityAbout My CommunityAbout My Community panibago</p>\r\n','pages',8,1,'/img/pages/paulbalochechristmasworship.jpg',1522636336),(84,0,4,'Page 1','page-1',NULL,NULL,'program',0,NULL,NULL,1455591350),(85,0,5,'Page 1','page-1',NULL,NULL,'program',0,NULL,NULL,1455591492),(88,0,0,'sdsfsdf','sdsfsdf','','','pages',0,1,'',1455845676),(91,0,0,'               ','','          ','<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\r\n','pages',0,0,'',1456106543),(92,0,0,'gfdg','gfdg','gd','<p>gdgd</p>\r\n','pages',0,1,'',1456133551),(93,0,1,'November 9 CK TEST','november-9-ck-test','disaster new','<blockquote>\r\n<h2 style=\"font-style:italic\"><em><strong><a href=\"https://www.google.com.ph/search?q=inspirational+iphone+wallpaper&amp;espv=2&amp;biw=1024&amp;bih=662&amp;tbm=isch&amp;tbo=u&amp;source=univ&amp;sa=X&amp;sqi=2&amp;ved=0ahUKEwjky-WV_prQAhWJi5QKHYYqDG4QsAQIGA#imgrc=c9Hir6HoURJghM%3A\">https://www.google.com.ph/search?q=inspirational+iphone+wallpaper&amp;espv=2&amp;biw=1024&amp;bih=662&amp;tbm=isch&amp;tbo=u&amp;source=univ&amp;sa=X&amp;sqi=2&amp;ved=0ahUKEwjky-WV_prQAhWJi5QKHYYqDG4QsAQIGA#imgrc=c9Hir6HoURJghM%3A</a>Lorem Ipsum&nbsp;is simply </strong></em><strong>dummy</strong><em><strong> text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five</strong> </em><strong>centuries, </strong>but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</h2>\r\n</blockquote>\r\n\r\n<p><img alt=\"\" src=\"data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhUTEhMWFhUXGBoaGRcXGBgeFxsaGB0aGh8YHhgdHSggGhomHRoXITEhJSkrLi4uFx8zODMsNygtLisBCgoKDg0OGhAQGzIlICUuLS0tKzYrKy0vLTIvLS0tLSstLy4tLS0tLS0tNS0tLSstLS0tLS0tLS0tLS0tLS0tLf/AABEIAOEA4QMBIgACEQEDEQH/xAAbAAABBQEBAAAAAAAAAAAAAAADAAECBAYFB//EAEEQAAIBAgQCBwUGBAYCAgMAAAECEQADBBIhMUFRBQYiYXGBkRMyobHwB0JSwdHhFCNikhUzcoLC8aLiFoNDU3P/xAAZAQEAAwEBAAAAAAAAAAAAAAAAAQIDBAX/xAApEQACAgECBgICAgMAAAAAAAAAAQIRAxIhBBMiMUFRYZFx8DKBFEKh/9oADAMBAAIRAxEAPwAdxzxqIirSrOkGObf9Saq3AeAk8lqTyh1FKaa2bgVWAGVo1kc415Qd5205ipLauGezqACdROsfmQDyJ8azeWC8my4fI/8AUkbk70QLOgAFA9m69ogRO4IPL4aqfB151bttxGxq0ZKStMpPHKDqSFaJAEjwoB97lz41bLDTbhH/AHSyakwPhVihUmmkijXbZ90RzoBnY8KAffSr7xHEeRqjaaNpmpNcM5ieVAGUxIifr/umtqCRO/IbUN7093OKLaIEE0IGZfrlz/Kma2QP2/OmRiAddu7jScn6+tKAa3bmnyjXkJioA8hTGhJJI5UidPz+vrSmR48KmyabHX62oQPbtTrTO/KfPeogxR0WRvoTrprQAZNTQaa0UAZtYiNBVVryAkBh5n8qCg+f9qYrx2FCz67afXGiLxgnegHmmLT+1P46+nypi3KgGgU1P7M0qANa2g71C1ayy2uWNSedTUiNDv3a+eu1RdmYbwsDSONCxy06SRC6NqjSYg7x8mGh5dk/do9jpq2IlpIkSVPaG0kd4JJHe/FhFk4dMpusoMiFECW4Ejx9xfEtss1LDYZNiqz7zEKsangCIjNAUcewdmavNzyxOT2Pa4eMlBFU9MWCGUscrcCDIOvHzIJG4L/0gV8N0tbUEEkxOXQ+R+u+uobKBS3s15KMqnlGpGsSpniSkx26rYfo1SDOXU8uO+g5VtwmnfSmYcY47KRTTpW2DuSPA+tE/wAZtAaTMcjH7eVWGwoBgqnd2RsfChmwp0CgcYgfpyrsOC8fp/ZSbpK3wmpf4pbjjPhVhcOp2Se+KRwgico3jYTQXj9P7Ky9JoOfpR16YtccxPcIA+vCiDDLAOUeMftRWwygRC89h6UF4/T+yinStsH9j9HSiJ0vb47eBq3bW2BGQH/aPnFCJXbKs88o/SgvH6f2Q/xizJ5eB38KHc6WtzIJHLSjvbU6lR6D9Kc2Ej3R5CT50F4/T+yn/iacz6U/+Kp9CrP8KvJfGBHrSTBgzCj0FBeP0/sAOl07/So/4onM+hqw2FAPuj0FRNhfwj0FBeP0/sivStvmf7dPSp/43b5k+VTSwpjsjv7I+jT3MKonsiPAaUF4/T+wL9NW5kSNNNKgnS6KwZYJ2Kssow/CwO4+IoxtINlHoKiBbLhGZLY3ZiswO5QCWbkBUOq3LY9OpaU7/J3cDd6PxLolvPZuuNFUHLIEkQQU4HgKsYrqtfXVHW4OR7DeW6n1FRw3WDCWFC4bDu5A98qFJncl27WvhVfE9acU47At2R3DO/q0D4Vyrm309vk7cnIrr7/BVxVu5bhbiMk6aiAT3MOyfImgK8UjcZzmuu1w/wBZn0XZfIUkEmNq6ldbnmz0309hoHP4UqWXw9aepKEbDZmMdk8Io4GhVtBGuvdVexilESm3Hj41YRg0ZWA7j8oPzoWJ27CuJAclY1zPw2jXh8Kj/DIJ34SMzbbCdeA0HdUbN8rO6tEbR8Z/KiWXzDkeNV0r0X5k/ZDD2RJMseMEkgTPf/Ude80W3oTpOsQO7c+FBaQJHn3edOuKyjIwbnv6eVSkl2Ktt9yxatFzrt+XhVfGoFbskzGp5f8AdRuXGYdkkSZj9D61q+r3U9roFzEuchgqg0YjeSfujuGvfUkxg5OkZOzfykeMz3/UVZ/hw+gkAGSeZ8ajjMfLk2ba2rckKAozRMSzkZifOK1PVLDW8ZbupiLSlrbAC4qhXgg7lY1EfEUJjDU6TMhevBewOG8bH1oDme7TUGur050M2FvlAcykShI5zAPCRB9BXJKtqxnTidp/OhVpp0x1aRHxpo4bjmOFEF5gAYET+ER8qdhGsb90eI8KEEVuaQBM6bSZPIVorHU/Fm3mhFO4Qt2/DaJ7ia4OCdva28iw+dcpE75hHxivWem+m7WGQtcYZo0QHtMeQHLv2obYoRkm5HklxSDBBB3gjj+W1KyhnSfH9xQrt9nZmMEsxb+4yR4a0lIBkHj50MQg5Ex493y405HOPr41G4wkkak8DSS6IYwPr/qhApjv8KgzVO7cBHDyobLB1FAINEEftUlJnTehz3UROJ+h30BGKQNOx4aUwoCROtPmNQAqSUA2bupU2XwpUAK2SJ1pi5zTx/SnIMzOu/iadliMw1+MfIULFq9iC4BzCR3Rvw8KZb/I95A+uesUYYQSfwRtxml/CqFlgMx0nWJoAK3/AHufAfdP799Qy5zp41MqoHOYqS2OKyxI9PrXSgJYRyGXbICMx4ZTEyfXSvagOVeN2MIzqVXRYAadg3KTpJ5TW16vdPPbQJeVntoI9shD5Y2W4FkiB94jx50OjBKtmVOsfUdmY3MMRqSTaYwJOpytwnkfXhXX6Fx+Fw9lUZlsOADcS4YfNABOur7aESIiK0OHvq6hkYMpEhlIII5gjeqnS/RNrEWyl1ZHA/eU8weBqDfQk7iecdZ+lxirpuIwCKMqyYYwZzRGk8jwjai9C9AXsaAxb2doHQxOYjeBPa4jkO/WqNnq238cMI5+9JYCJtxmzDxGncTXp/SLixhrhQBRbtNlA2GVTHlUmEMepuUjy3pc4e1cNuwpuBGyszs3aI3AVCoAnSda1mL6oJdsLcw7OjsisFY5gZAOUk6g8JrJ9B9GYUsGxGLQKDJQC5mbuLMojymtl0x13sJbK4c53ghdCEXTckxMchQiCjTcjN9TehbWLZ/atcBSDCkAa6bxPDhFdHrl1Yw+Hse1tBlbMoMsWzTpx48dOVXfsww2W1effM4WeeVZ/wCdS+027/KtW9NXLa8kWPXtChZRisV0cvqh1QS/bF6+WyNORFMSBpJO+8wByrsdI9QbDCbLvbbhJzL5g6+hql1e60LbwRWJu2RAXbMpOjd8A6xy75rmXetWPuTkZQBHuIIAPGWmB3k0F41FbFLEdBPh7y/xVtjZzDM1uSCO48JPAwan1rweHS5bOHBUFAXXXsk6gEEyrRuPChXenL8/zcS7n8CNCf7isBh/SN+Yqx1N6M/icVmuaosu/JmnQHxJk+BoZbPpj5LPQnU97i+1vt7K3E8MxUD3tdFEayfSuUt6w15VWyWtZgsu7+0ZSYzSCAp1kCK9F67XcuDuwQubKkmYAZgDsCdpHnWO6t4TCW7i3b2KRimq21DgSNiSVBMcooaSxpNRR0OsvUxLdprtgmEGZkbXQbkHfQawZrFraJ2rcdaOuFp7TWbEtnGVnggBTuBOpJEjzrI4dABIJG3Ijv0oZ5tOrpAW7JOsSB8+VSu2QJ1j1qd66NllfE698xxOlBUmYn1FDIGfCmA41K7aIJFSJGmk6cO7jQgHI76VGyDvpUBQaaKL5IynUcOYnkd6CaWtCx2bNtlhTy2mSIjQnzoNnEAntCSOJO0chEVXw93KDGae7blMelEs20YHMQDvuI9fyNAHuOA0x8PlVm0XLIlswbjhdpiSBPxqnZIEDMCAdweHI+U1YGJyXLbDdGV4kR2SDHwoF3LfWJ/5zIpIt2ZRR3jRmMbsSDJ8K0P2cdDXrRu3bqlA4VVVtCYJOYjh3TzNZzrOMl5rlvtWr03EYag5t/MNII8K7eD+0Ei2qtZNy5EEhsoJHPQ6+FDeDiptyA9a8U+AxgfDNlW4udrf3C0kE5e/TUa71surnTSYuyLijKQYdfwsOE8RxBryXprpN8RdN27AJEAAaKo2A9Tr31r+qmLXA4N797Q3Wm2n3mAEAxwBJOvKOdC0MnW/R1Lt1P8AF15+wyf7iS8eOX51pcdhhctvbOzqynwYEfnXkXTvWFrrJdt2sl9WDFlgagjXNPaECMrDhudq2OD6/WlS3/FKbbuoYZYdSDpOhJXwO3M1TWrpm0Wt9/kwOP6PuWHNu6hVhx+63eDxHGiN0ey2zcdcqzCkggseSjiI1J2HpO76Q+0Cwo/lI9w8CRlXznX4Vg+k+lHxFw3LzS2wA90DkBwFXOScYLs7PS+oFjLgrZ4sXb/yIHwArNfaXfnEW1n3Lc+bsR/xFH6o9b7Vq0ti+GXLOVgJBBM6xqCJ5VQ6Y6Vw13pC1fJLWQFDyp0y5o03KyVO3OhrKUXjST9HMwPRN4gMti5cJErAIQDcEtpJO8AjvPCr7dWcfdADWsqjYFkCj/aDA8hQ+mukUTEG5gb12T2naTlzEkxlI1UCNxFdzonr4RC4m2Sfx2/mUPzB8qFIqF02G6v9Scgf+JKsHXLkWSBrObMQO0OEczzoX2fWxaxGKsEyykAHmEZgT8V9av8ASXXmwiE2wzvwBUqJPMnh4TWAwPSl21e/iFMuWJadmzasD3GheUoRao9X6zdHHEYa5aHvESv+pSGA8yI868fu2ijZLiFX2ysIM+FekYTr7hmH8wPbbiMuYT3Fd/MCqvSnX1MpGHQs22Z9AO/Lu3woTl0S3sxF7BOiguMpbVVYQ5X8UbgToJ34bUwYqOXGI8vKmxGKa6zPcbMzGSTuf0HcNNKjbeNAARy135ihyuvAUWpA0kzz2pKSoJA7QO++g76Jhn0jUnlr67UmuAgyAPn4baa0IKSvJM9/rzo62wO0N/wn9ajpJggzry8oqFtiec8qAuZV5N6ftSqr/En8XxNKgKLNvUQamzbcfGokd3pQkkbpMSe7yp3PLQ8uHjQ4p2NAXhi17PZOnKBrHyoeJvSRlWOY7/qKrAePOiT8OP150BewHSj2wbYhrZMm24lZ5jYqe8EUfDWLTksFdI10uKfQFJA8Sa5r6ywM8/PyqNu8RI4HffWhNmi6Jv4FGm9aad1ZjnQeKgD5EVxeuWJW/fz2WLMY7cOAAOBloPgF86HjLTlSidmQO0I2O4nl51w0hGyZmdjwkgDx1qjjbs3hlag40vo6BLDVmSBBMqRI5e95VY6awhZ1dBbRXUMEUkldIytpoeMRv4TVFrA0kSxIHcBuYHgCJ761vUHApexRW4iugtsxVlBEyoG/j8KrLHc1O+1/2VjLbSvJm7eHvC17QgNLhUknUgEtuNQDk/uNerda8FbtYC4FtW82VQIVV7RIEgxpvNc3rYqtjcHhkChQVbKFGilxMctEO1XftGuj2CISQpfM0b5VGw7yzKPOeFaGiSipHlrXLg0yLPMP/wCtOrXG+4s7+/8A+tendXugsLfwSu2HRC4bUElhBIBznWYAPKs11Q6utixmuMRaUxp7xP4RyEb+PpJk8b2+TNLcuDVrcEbduP8Aj30+Gu3C2qjX+qfyrT9G2UvY4WbNlPYKxBBXNKDQuXMmTGmo4UPrx0Vaw95Vs6BlLMkk5YO+usETp3UIcNrONcRo21nUfQ/OghtgRw/eh28UlwkKTpodOP1NTZCPL86GbVDKalJBpMkDX07u+poDpxFCBkAJMmKIlvWJhT37jl41O1bDL66Dh+21SCSs5gxjvkAcPnQAnmSQII+EUMXmiPo981dVVAY5pEDvPhVC5JMzPfQDKedTWBuNDy30plXMYGkfUVYKqoAHvHeeA2igK8LyNKie0XkP/L9KVAUJpqapldAZ3+HChJGpD0qIpTQF27lywjTpttpv3elVBT2juImfqZpgKAkhjXnTFTvw50hUiRA3n96AsOzEa8o8vKqrLrOUDbYcqSvPGn40BWGGJuq5PZA246z9eVei/Zfh+3iLnIIoP9xPyFYVo3A0+te6ut0F1gxODbSw7I/BlhWj7yt59+9DTG+pN+DVJ/N6bPK0nyQfncqt9qWK7dm2Pws5HdMD5H0rOp1vZMa+LSwwLiGQkEHRR72ke6Dtzqr1k6wti7ouNYZCECwCCCASZnT8RqDSUlpa+T1Bj7Do080w3/lk/U0uoqKMDZy8mnxzNNYH/wCd3WwzYe5hy4KFM4OUxEAkagkVU6udb7+EBVbZdCZyMNjzBB04cxQvzFa/Beu4zGYe42Hz3EgkCAdeTCBqD3VzcYtwFzcJzD3yWlu+ePiD4cK6fSn2j3rilEX2JO8BmeO5uHkPOuDZcXbNwqzl197snKLfFifX6kik8kYK2ZODk6iVMKVtpIHackkaEk7wBwjTXxqGHxTAk33XUdlQRp40rtyyJYNPEnMZPpHpFdHoHq0MXL9ldiASxJHfwHqamUlHuTDHKbdIVu4piATp4g8dIozNrCmOYn64VcxfRF3DkqwCkARlIII2mdxxEQNqoA6wZ1Ik/tG1WMGqdMsrhG+6Bpy9fEUzWmzRx5kwfCpsjAMxbKdAMuk+XnQW9oxmOEnmdtfOpIGu4jKdJnaYA/eq1szx+vGneCY2g7x+VMGAHu68z+lAFsqCV1jXhwo3tlEQsnXWY+hVazdOpExxooy5dRqePCgA5jy+NKi/w4/F8KVAUCKY0Q77fGoRQkRammiECNN5pW1BmeU0A1p428PI0xHrUVoinQzG2k70A5uEiP8Auur1U6G/isQLbSLags5HECBAPMkgetchDBmtB1M6VGGxGd/8tlKsQCcswZPcI9JoWhWpWN0xh8SuJezat3EAci2lsMFyzCnTQyNSx75Nc7Hm7buMmIQFhuH38Q41jwMV7OuOtFM4uJkic2YZY8Zisp0d7HHdIPeADW8OiIsjRnJY5+8DWPI8qG8sS8My+B6s3r4z27dxAdR7TKFj/UYJEf0+ddLpbp8Yq2lhrllXzyXHtFtaSAqtlJAg6sYHzrZdbsT7PB32G+QqPF+z+deNMdIoVmuXsvJ3OnOhr+HX+ZahNO2sMvqB2fOuHXsnVi57bBWS4zZrYVp1mOyZ5zFeadbOiBhcSyL7jDMncDPZ8iCPCKEZMelWjjU4otu2WIRVLFtlUEsfAbmr+N6Eu2VDX8lvN7qs0uR/pUGB40MlFs5jYXOCOMft8+FRwVxbdlypbMxiFKgFOKkbkzNdPFdC4m2BNm4VYAgoM6kbgysjjxqh0d0HfS6bJQktDBPvCddQYy8N+6qTgpqmXi5RsMuEuey9qbfYJAPGJGgM7AzoRoedRwdx7P8AlO6A8AzQPATofCtVhur3SNuw1tQoQgzaLKSQeA4A+BrKWgWK8MxABO2pj0qUvZab0vptFnCuTMElyZOZj2o7+JouJUtBaVI4ER6cT5fDeutiOp+LUZyiNEkhGk9+kCduFcdYbtrrG420G2/hVjKUWu5E3x+GQZPGRrw8/lVc3iNjBgQe7kKLdfSI7xI4GNANqr6+8OH1tQqNm5camh1k60fCYcHccNNeNQaxqQDJ35CPr86AGqEabc6QuEbGB8KV1Y1nehqDpQFnOvIep/Wnqv8AXGnoASjstpJ0ju3n5fGhxUwRyn5UrF8o2ZQJ79qEkDTjTvpiZp3tkb0Az7/pT5KWTSalMAjhQEdQf+jUiCNiPI6UmbbSahQDhRyr0v7MMPlw9x/xXPgqqPmTXnNkrMHTeTJ28K9a6jWMmCtf1Zm/uYkfCKG/DrqKX2k3wMIFmM9xR6S3/EV50tosoQL2hr9Gtp9p1/tYdB/W3plAPxNYnBW3d1RNWY5RPNtKEZt5nrnVOyUwdhTvkB9dfzrJfalbm5hgoJYhwANzqkD1Nb/D2giqo2UADwAisliLi3+l0TcYe0WP+o/pmT0qDomulR/B0+qvV5cLbBMG8w7b/wDEclHxrzTrJ0icRiLlzcE5U/0DQR47+LV6z1gxBt4W+43W25HjBj4xXkXV/C+0v2k/qBPkRA8zA86kyzKqgj2bA2MltE/Cir/aAK84zm90vlOoF+R/9Q/9K9MdwqknYAn0rzL7O7RuYxrp3COx8XIH5tUGmXvFHpGNvZLbv+FWb0BNeM9G2c921bjV7iDvEkT+dep9c8RkwV48SoX+8hfzrA9S8PmxVskyV27iQdPHKLh8qkpm3mkerkxrXieIyiXBLTJ0GgnYE+levdO4j2eGvPxW20eMGPjFeP4e7AKknbaCR6UI4l9kVie+p2bU/XDnRWZPu78dNJ7u6hHeBOmnl9TQ5QxJOgOgnmDG/gKg90zm0+uP1zo1pCAdJO8xt5fW1V7sncATy499APdukiCBI41AjhSjSZkzRC+2lABzd9PRcg7qVAV2NN7M6mizLGeP1yo+U5Y+vrWhIO0nAkajTup7uE23Zp8ooq2ew0eHy0qLYhregOvHQGPA0BTZCCQRryqFFILNrMk6nxp7mHIMDXjQEYnlpwpn+NIjjFPlmY1oQPh8LcuMEtqWc6AAfUDvr3Ho7DeytW7Y+4ir/aAK8StX3AjOygfhYjfXhTpjLgIPtGPcWaPnQ2xZFA0v2i35xYB91UUHzJY/Aiun1D6BOb+JdSFE+yBGpke/ESBBIHOZ5VmcH04LbZ/4ew7aHM4uM0/7nOtXsV17xbyF9nb71UlvViR8KEqUNWpm86ydPJhbckg3D7icSeZ5KOJrznqr0t7LGi7db38yux/r1zHuzAeVcfEYh3Yu7MzHcsST8abhFCJ5nKSfo9l6w4Rr2Fu27cFnQhddCdwJ76846vlMLirS3WXNn/mEEFU0IVMw0PaIZjsIXka4qYq4FyC5cCfhDtljwmKEqSYGnjwoJ5baaR7b0opaxdC6k23AjmVMVgvszvqt64p0LoMs8cpJI8YPwNZezjbqDKt64q8luMF9Aa1XUToC1fD3rwz5XyqpJiYDFjB13EeBoXWTXNUaXrxgLt7DZLIBYOrEEgSomdSQN4PlWd6gWB/EsoIYWkJZh7puOQNDxUKGAPGWOxofXf2S3Alq6+Yf5lvO5QSNNCYB7h3V1fs1w8Wrt3TtsFETsg/Vj6ULXeU6HX6/lwbj8RRf/IE/AGvL7iEHUcAf3rY/aT0qGKYdDOU5njgYIC+MEnzFZXD3MoJ47d435/ChlndyHSJ1A59x8eXGgO8mIAHHX8+HgKkDmIkQPM0+JsAGVM93KhiMt6Dp6/rz/eo4hiYnYabfColBEzpt51LKzR3QCeU8aADNSgjz58qlet5TBHh+vzobNw1igFJpUpHI0qAt4fBqwmfrl41MLlUkGR+QP16UreA7JJaBGw48iaqW8QQABznx7qAti4f24A8NJoV3DZjMxO87z4UlYEDK0MOY4eO3rT5jMMRIHMenxoAn8HxDTpx58qhZEnX4UfDuZiCVI9POitbRZ2zHm21AUsUQdvu8I+OlUorr3mdFIVRGst9cdap4To+485bbtpwUmfOKEpNlSk1dy11YxLCfZhT/AFMo+RJqvjOgMTbEtaYjmpDD0GoHjUWi3LlV0co1JZq1gcC945UyT/U6r8CZPkKv4jqxikE+zDD+hgT6bnyFLRChJq0jixTnxpdx4cPypVJUdtP2qIqRpN4/P9KAQ+vCux0L04+Fk2jmzRmVh2NOPOeEiuRbApAE6ChKbTtHU6S6St37hutaZWbVgtwQToJAKEjaujY6y4hkGHwqLaVQYgy5HHtNxMztOtZlYmI3NdLC9GYlHDrbgjY5l9CMwNYZsqgu6T8Wb4oyk7X913Kt1CrQQQ25zTm149/jUfZkidY7+6tfcw63kHtEg8jEqe41lek8C1psrag6q3P99q5+E46OZ6HtL97GnE8JLEtS3X73GtBdMx2OoI0ijOQZluAyxx5SfHWqTXtCSJPMzOlDw14MoJ34xXde9HGXjZnQnePD/up3boGg5co+I8aHYeSF2U93H686TCZPdoI0Jnv+ZqQPhWB0IEaiQPnUbmGGpXYevieVCJI4ny/SrCXdlUCTz02oAPsT+H4GlVv2w+op6EAlvqVCsdI4fW1TGGR9FMQOA495Otc4HSr+HIUztodD4fKhIzWlRJInvj6+Ncy/0goML2p4DWr/AEk3tEZZgHb5/lWVdCpg6Gh0YMcZ9zqNiLhQkASoDZDObIROcawV5xtvtMbToboKzirCXUutyYEAlWG68PLuIrE2MQXQZs2YMAjrvmPIcTPvAbzOh1bQdSem0sX2V+xavKG2OVHWQY/oJDCdvdHA1nKTOvk4/RsOj+rSWjmW9dzcwVAjllggjuM121mIJk84iqmG6VsXP8u9af8A0upPoDVxqybbLxhGPYQpLSNI1BYy3SXQVlr5tuMovS1t1gEONXtng0jtidff1ECh2LOMwR0nEWBuB7wHMA6jwEjwrQ9L4L2tsqujiGtt+G4uqnwnQ9xI40TBYkXLaXAIzAGDuDxU94OnlVtTRR403a2ZycZ0Ph8aovISCw99dz3MDuRtzrnL1GGv887GOwBr39o6VpreECOzpoH1deBbg/c3A89OVWRTU0V5UXu1uZLovq3hrtshg63UOW4A+quN4kRlOjAxqCKo9I9Tbqn+SRcXkYVh6mD8K0PTc2Li4tfdEJfA4250ueKEz/pLV2qKchLBB+Dz7C9UMS3vBbY/qYE+iz86v9GdV7F1Jz3VuKcrqSso43UjLqNiDxBBG9bIVxOkB7HGWbo0W9Nm5yLe9bY9+jLP9VS5siPDwXgzPSXVVlJRbyMd8sPmidyqBj5in6Lw2MtnKGRrY/H7T0GYBx56VvmQTMDkT3cvCq9/D21BYr8T+tcfFRnKDW3t2dGKMItVfo5Qri9bLoFkaSxYZRpOm514R8xVjpTpdLCv990UNkntQSFDE8BLD9K4C43DXSbmKvusyVCqD2eC5d0YHTUENEg6mPO4DhZvIsktkv8Ap0cXk6HGO7Zx7+KaP8o7gQWEknQAATM0OxcuKCpVUI01BJ9QQP8Aqn6VzO6ewF0W11V7oCyZ3B2PqTRWwQ9mpds1xpmPdABgac69vLl0VLwcPCcCuITgv59/ivIHEdJR7hEciJ+MioBb105j2R6d+3GrGHwqqAY1q0taxi63ZxSyxtuKJPTBh31FudKtDnFNKiT3j4/pSoCCoZI2jeaNauajXhvHChsTxO/KmDkRGka6bzQBmxK7BZHfVPEWw4IYDu7vCib/ADp3SKEptboq2rVxrgGjFuyCSEgfgnQW03nLqdRpNem9XOg1w6lyQ91wAzgdkAbIg4IPjFedmj4LG3bJm1cZe4HTzXY1SUL7HVDi2v5I9KxfRVi7/m2bbf6kUn1iqH/xewP8s3bP/wDK7cX/AMZI+FVOg+tauQl+Ebg/3D4/hPw8K0WIxKIuZ3VF/EzAD1OlYuLXc7ITjNWjjP0PiEBNvH3ABr/NS2405mFNUeh8d0jetC6hwzoSwXOtxGZVMZuySBMbUDp/rB/FK2EwIN1n7L3ADkRTv2u8ceUxJrWYDCratJaXZFCjyETSixwsV0xjbKl7uDtsoiTbvjiQB2Ss7kUPC9L37RcNgMQEYlgEyOVZjLbNqCe14k11OkT7S9asj3VIvXPBT/LXzftf/Wa6tQDgL1ptj37GLT/Vh3/4zTp1uwZ//KVB/FbuD4lat9N40qq2kP8ANvHIgG4H3rncqiTPOBxroooChRoAAAO4UByP/kuBYEHE2SCIILjUHhBqp0X1gwtqbLYm0VT/AC39opBTgpM++u2u4APEx28dbTI7MqmFY6gHYE1zegOiLS4awHtWywtJmJRSZyiZJHjQD3eteCG+KteTT8prndJdbMDdttbFx3nY27blgw1VhIAkEAjwrRpgbQ2tWx4Io/KjoI20qQZC31xui2pbBXi+ilmHs7bMTAgtzMad8d9WfadKXdreHw6/1E3HHp2fhUunn9ti8NhhqFb29zwt+6D/ALivrXZ6X6STD2mu3D2VG3Engo7zSgYPrL0JcFy37XENfvODIChBkBCqvZP3rjKo8SeFFxuHw+De0LdpXuW11edDcIEs34gBqFkDtHlVC/jLjP7Zm/mt2iPwyCFUcgisY4liW4Ca1y4WidcqhR3BdhWkYezlz59PTHuNiLzXGL3GLMdyfrQd1Dy0QCmitKOFSa8kYp1FTRalkqSo1vvNRc0+WnyUBL257v7RSqGU0qAiwHOkoHGamwA2MnnUDQDsp04TSBGsiacE8dfGnkRqNe6gIRSilT0BGKZrYIAIkDYHUCeU/WlEWnUd1CU67HU6K6xXrChFCFBsuUCP7Y+NdW/1yJACILbHd3llA5hVgk91ZYjWoxVXFM1jnnHyehdC4zDKpy4hXdjmd3IDs3gYgAaAAQAKBe6fe65tYK0bjD3rtwFbK/CXPcPWsIVqVuRsSPAxVeWjZcW/KPRuiujBZLPcuG5eeM9xoGg2VV+6g10FdMV5K1vnrRbLEe6xHgSPlUcsn/M+D0Xpl8yiwPevdmOIT77eSyB3kVPpDpS1Yj2h1b3UUEse4KK869s8++39x/WpYbFOhLI0N+KAW8iZI8qcvcPi9qo9NtMWUEqVkTlMSO4xImuP031lt2G9kgN6+dFtJqZ/qP3R8axzdK4j/wDfc/uNAwmIa02e3lV9e0LaZtd9cs05ZZcXH0broDo57ee/iCPb3YLx7qKNrYPIakniazfW7pRb7hEOZE1n7pbaRz00nxjfXmYrH3rv+ZcZhyJ7P9o0oAGlWjCtzLLxLkqQwGlQooFQirnKNSNSC0xoCaGmzUpphQDrT0lqRoCE0qUUqCwa05pqVAPSpqVAKlSpUA4qXClSoB34eFQpUqAepLSpUAqklKlQCf8AM0l2NPSoCDU5/KlSoBlqR2p6VASTb65UI0qVAI0jSpUA9RpUqAkKTUqVAKlSpUB//9k=\" style=\"float:right; height:300px; width:300px\" /></p>','program',0,1,NULL,1525669493),(94,0,0,'ssdfdsf','ssdfdsf','','','pages',0,1,'',1456195628),(95,0,0,'ssdfdsfdfss','ssdfdsfdfss','','','pages',0,1,'',1456195747),(96,0,0,'asdasd','asdasd','','','pages',0,1,'',1456195788),(97,0,0,'test lagay','test-lagay','','','pages',0,1,'',1456195831),(98,0,0,'new pages2','new-pages2','','','pages',0,1,'',1521515333),(99,0,0,'a','a','','<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>','pages',0,1,'',1456283411),(100,0,1,'update','update','','<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; asdfghjkl&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; qwert</p>','program',1,0,NULL,1457415450),(102,0,0,'November21','november21','','<div style=\"background:#eee; border:1px solid #ccc; padding:5px 10px\">&quot;There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain...&quot;</div>\r\n','pages',0,1,'',1511230496),(103,0,0,'test nov9','test-nov9','','<p><del>Deleted Text There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain</del></p>\r\n\r\n<p><ins>Inserted Text There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain</ins></p>\r\n\r\n<p><cite>Cited Work There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain</cite></p>\r\n\r\n<p><q>Inline Quotation There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain</q></p>','pages',0,1,'',1478682765),(104,0,0,'sample','sample','sample','<p>sample</p>','pages',0,1,'',1489636539),(105,0,0,'sdsfsdf','sdsfsdf','','<p>fdsfg</p>','pages',0,1,'',1510885825),(107,0,1,'Disaster Risk Management','disaster-risk-management-',NULL,NULL,'program',0,NULL,NULL,1511166349),(108,0,0,'example jv','example-jv','','<p>adfdfsdfsdfsdfsdfsdfsdf</p>','pages',4,1,'',1511169572),(109,0,0,'ererer','ererer','ererre','<p>ererrere</p>','pages',0,1,'/img/pages/2 mb glass (1).jpg',1511245278),(110,0,0,'Volunteering','volunteering','','<p>dsds</p>','pages',0,1,'',1511248034),(112,0,0,'asdfgjv','asdfgjv','','<h2 style=\"font-style:italic;\">&quot;There is no one who loves pain itself, who seeks after and wants to have it, simply because it is pain...&quot;</h2>','pages',0,1,'',1511750763),(113,0,0,'deleted Text','deleted-text','','<p>There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain...</p>','pages',0,1,'',1511751617),(114,0,0,'asdfghjkl','asdfghjkl','qwertyy','<h3 style=\"color:#aaa;font-style:italic;\">asdfghkll;&#39;&#39;</h3>\r\n\r\n<h3 style=\"color:#aaa;font-style:italic;\">&nbsp;</h3>\r\n\r\n<p>&nbsp;</p>','pages',0,1,'',1512535824),(115,0,0,'nature','nature','disaster','<p>ggg</p>','pages',0,1,'',1521515376),(116,0,0,'Pageforyou','pageforyou','sdsdsd','<h2 style=\"font-style:italic;\">Everything happens for a reason</h2>','pages',0,1,'',1521536063),(117,0,0,'Reminder','reminder','','<p>Just be yourself always.</p>','pages',0,1,'',1521536882),(118,0,0,'bagong pahina','bagong-pahina','bago','<p><q>&quot;There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain...&quot;</q></p>','pages',0,1,'',1521686903),(119,0,0,'nadarang','nadarang','','<p>fghdsafhfhdfhdfhdf</p>','pages',0,1,'https://i.pinimg.com/originals/37/78/31/3778318a27f89bc07a8df438c1f5b2bd.jpg',1522056424),(120,0,0,'jujuj','jujuj','ggjjgjgjj','<div style=\"background:#eee;border:1px solid #ccc;padding:5px 10px;\">There is no one who loves pain itself, who seeks after and wants to have it, simply because it is pain...&quot;</div>','pages',0,1,'',1522216853),(121,0,0,'example','example','sample','<div style=\"background:#eee;border:1px solid #ccc;padding:5px 10px;\">&quot;There is no one who loves pain itself, who seeks after and wants to have it, simply because it is pain...&quot;</div>','pages',0,1,'',1522635770),(122,0,0,'bago','bago','bago','<p><del>&quot;There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain...&quot;</del></p>','pages',0,1,'',1522636665),(123,0,0,'aboutpage','aboutpage','about','<div style=\"background:#eee;border:1px solid #ccc;padding:5px 10px;\">&quot;There is no one who loves pain itself, who seeks after and wants to have it, simply because it is pain...&quot;</div>','pages',0,1,'',1522834055),(124,0,0,'new page','new-page','new','<p><del>&quot;There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain...&quot; </del></p>','pages',0,1,'',1522891063),(125,0,16,'jhgfdgfhg','jhgfdgfhg','','<p>jghfhjkjl</p>','program',0,0,NULL,1524110119),(126,0,1,'renren','','','<p><strong>gsdgsdgsd</strong></p>\r\n\r\n<p><strong><em>gfghfghgf</em></strong></p>\r\n\r\n<p><span style=\"background-color:Yellow\"><strong><em>fgjfgjfgjfg</em></strong></span></p>','program',0,1,NULL,1524119711),(127,0,1,'lorraine','',NULL,NULL,'program',0,NULL,NULL,1525684348),(128,0,1,'fhsfbx','',NULL,NULL,'program',0,NULL,NULL,1526458050),(129,0,1,'fjfjfgjfgjfgjfgj','','','<p>holoooooo</p>','program',0,1,NULL,1526620773),(130,0,1,'hooqtest','',NULL,NULL,'program',0,1,NULL,1526620299),(134,0,16,'loleng','',NULL,NULL,'program',0,1,NULL,1540951465),(135,0,16,'dfgsgfgdfgfdgrtertertertertertertergdfgdfgbcvbcvbcv','',NULL,NULL,'program',0,1,NULL,1540951884);
/*!40000 ALTER TABLE `tblpages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblpagesimg`
--

DROP TABLE IF EXISTS `tblpagesimg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblpagesimg` (
  `imgID` int(11) NOT NULL AUTO_INCREMENT,
  `imgpath` varchar(255) NOT NULL,
  `fileName` varchar(255) NOT NULL,
  PRIMARY KEY (`imgID`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblpagesimg`
--

LOCK TABLES `tblpagesimg` WRITE;
/*!40000 ALTER TABLE `tblpagesimg` DISABLE KEYS */;
INSERT INTO `tblpagesimg` VALUES (61,'img/newsletter/','1MB PRINCIPLE (1).jpg'),(62,'img/activities/','img-1-425x400.png'),(63,'img/activities/','cat2 (1).jpg'),(64,'img/activities/','img-2-425x200.png'),(71,'img/activities/','Vortex Healing ATV Rental-01.jpg'),(72,'img/activities/','atv.png'),(73,'img/newsletter/','675.jpeg'),(74,'img/pages/','undefined.png'),(75,'img/newsletter/','nature-hd-background-10.jpg'),(76,'img/pages/','love-62v.jpg'),(77,'img/activities/','Untitled.png'),(78,'img/activities/','594beefa37079_hdnaturewallpaper(19).jpg.dcbaf89ba22c64debda2e06f63a5b56c.jpg'),(79,'img/activities/','nature-hd-background-10.jpg'),(80,'img/activities/','17195562_1782052405153988_426121395_o.jpg'),(81,'img/activities/','christmas-02 (1).jpg'),(82,'img/activities/','jane (1).jpg'),(83,'img/activities/','78515ad6dc37857ffc0a015a68e29694--garden-wallpaper-nature-wallpaper.jpg'),(85,'img/activities/','Chrysanthemum.jpg'),(86,'img/pages/','cat-1285634_960_720 (1).png'),(87,'img/pages/','fuchs-1310826_960_720.jpg'),(88,'img/activities/','config.png'),(89,'img/activities/','front_end.png'),(91,'img/activities/','front_end.png'),(92,'img/pages/','413474560.jpg'),(93,'img/pages/','cat1.png'),(98,'img/activities/','cat-istock-8162989-800x500.jpg'),(99,'img/activities/','cat1.png'),(100,'img/activities/','Cats-Sticking-Out-Tongues-001.jpg'),(101,'img/activities/','413474560.jpg');
/*!40000 ALTER TABLE `tblpagesimg` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblpartnerevents`
--

DROP TABLE IF EXISTS `tblpartnerevents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblpartnerevents` (
  `eventID` int(11) NOT NULL AUTO_INCREMENT,
  `partnerID` int(11) NOT NULL,
  `eventTitle` varchar(255) DEFAULT NULL,
  `eventDate` int(11) NOT NULL,
  `eventVenue` varchar(255) NOT NULL,
  `eventCover` varchar(255) DEFAULT NULL,
  `eventDetails` text,
  `dateCreated` int(11) DEFAULT NULL,
  PRIMARY KEY (`eventID`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblpartnerevents`
--

LOCK TABLES `tblpartnerevents` WRITE;
/*!40000 ALTER TABLE `tblpartnerevents` DISABLE KEYS */;
INSERT INTO `tblpartnerevents` VALUES (1,1,'sample event title',1414195200,'sample venue',NULL,'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ac mauris vel massa fermentum fringilla vitae nec enim. Donec eleifend scelerisque dolor eu pellentesque. Curabitur sit amet nisl ac enim consectetur aliquet vitae et felis. Pellentesque eu purus elit. Praesent convallis dolor sed libero fringilla, sit amet tincidunt mi pharetra. Ut diam augue, accumsan et vestibulum ac, elementum ut ligula. Nulla sed cursus ante, vel vulputate velit.</p>\r\n\r\n<p>Vestibulum dictum mattis augue. Maecenas iaculis leo et dapibus vestibulum. Cras est diam, luctus at sapien sit amet, consequat vulputate mauris. Curabitur ut neque hendrerit, tincidunt massa vitae, tincidunt leo. Phasellus dapibus interdum ornare. Donec feugiat volutpat facilisis. Nullam convallis velit id pellentesque vehicula. Aliquam lacinia ante eget urna commodo volutpat. Phasellus auctor tristique aliquet. Sed sed elit neque.</p>\r\n\r\n<p>Fusce maximus lorem ut mauris tincidunt bibendum. Maecenas auctor augue a aliquet accumsan. Vivamus purus magna, efficitur ut sollicitudin eu, vulputate ac risus. Nam id placerat elit. Curabitur porttitor scelerisque nisl. Proin rutrum cursus lectus vitae imperdiet. Duis vulputate, libero quis iaculis commodo, sem libero aliquam dui, quis imperdiet nisi metus sed sem. Pellentesque lacinia dui at metus congue maximus. Pellentesque viverra imperdiet eros. Curabitur pharetra commodo fringilla. Maecenas dapibus ante velit, ac tempor lectus venenatis et. Integer cursus ultricies ornare. Integer ultricies facilisis efficitur. Praesent non lobortis risus. Nam tristique luctus ullamcorper.</p>\r\n\r\n<p>Etiam lacinia ut ligula non blandit. Nunc eu augue tempus, aliquam sapien nec, dignissim metus. Duis vel odio pharetra, pulvinar quam non, feugiat nunc. Vestibulum finibus imperdiet quam, sed tempor dui lobortis eget. Aenean imperdiet sed ligula eget bibendum. Maecenas lorem justo, tincidunt eget elementum in, viverra sit amet mi. Nulla eros erat, sollicitudin vel aliquet non, iaculis ut urna.</p>\r\n\r\n<p>Curabitur dignissim libero est. Sed nec tristique sem, ac hendrerit ante. Mauris eu libero varius, faucibus eros ut, porttitor quam. Mauris pellentesque aliquet sapien et molestie. Aenean pharetra, lectus cursus cursus tempus, leo mauris tristique risus, vel malesuada arcu lectus vel ipsum. Donec convallis lectus vitae lectus sollicitudin fermentum. Duis eu quam cursus, sodales sem vitae, commodo ligula.</p>\r\n',1413476362),(2,2,'hi this is a sample event',1413849600,'london',NULL,'<p>sdfs fdsf sdfsf sdfds fdsf dsfdsf dsf</p>\r\n\r\n<p>sd fsdf sdf dsf sdfds fdsfds fdsf sdfsdfsfs</p>\r\n\r\n<p>dsfdsf sfdsf dsfsdf sfsdfsdfsdfsdf sfs fsdf</p>\r\n',1413686440),(3,1,'Lorem Ipsum',1413676800,'Lorem Ipsum',NULL,'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quis tristique eros, sed interdum quam. Cras tristique orci sed lorem auctor facilisis eget id velit. Suspendisse placerat diam ut lacinia fringilla. Sed sodales urna vel felis condimentum, fermentum vehicula sapien luctus. Mauris eget arcu hendrerit, accumsan odio nec, hendrerit ante. Morbi vitae nisl vulputate, tincidunt nisi vitae, varius sapien. Sed et ligula at odio hendrerit aliquet. Fusce lobortis eu metus ac varius. Maecenas convallis vitae ligula sodales semper.</p>\r\n\r\n<p>Morbi pharetra quam in tortor cursus mollis. Sed id magna nisl. Interdum et malesuada fames ac ante ipsum primis in faucibus. Mauris vitae nisl id augue luctus auctor. Nam sed felis a sem laoreet rutrum. Nunc feugiat orci nibh, vitae molestie magna auctor at. Morbi quis ornare purus. Donec ac nisi sit amet urna commodo vestibulum nec quis lorem. Sed et nulla in nulla posuere tincidunt.</p>\r\n\r\n<p>Quisque varius viverra sapien, sit amet bibendum lorem tempus non. Donec quis bibendum nibh, sit amet fringilla augue. Suspendisse congue elit sit amet laoreet vestibulum. Nulla tortor nibh, rhoncus non fringilla eget, blandit sit amet massa. Pellentesque nibh diam, semper non imperdiet in, accumsan id massa. Aliquam augue lacus, auctor in molestie sit amet, sollicitudin quis purus. Duis vitae scelerisque sem. Donec pulvinar vehicula semper.</p>\r\n\r\n<p>Fusce lobortis nibh eu purus cursus, eu eleifend magna varius. Praesent volutpat tortor ipsum, id facilisis turpis maximus sed. Vestibulum efficitur pharetra nulla, in ultrices neque pulvinar nec. Phasellus tempus lorem vitae magna eleifend, et hendrerit felis lobortis. Curabitur commodo turpis non hendrerit dapibus. Sed laoreet, lacus nec mattis tristique, ipsum libero lobortis nibh, et varius mauris leo ut sem. Donec sollicitudin tincidunt sem, eget varius nisl porttitor vitae. Ut pretium velit id ipsum cursus sollicitudin. Vestibulum volutpat ante tincidunt arcu venenatis tempus. Mauris justo leo, bibendum ac condimentum eget, laoreet cursus sapien.</p>\r\n\r\n<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed at commodo mauris, ac maximus velit. Interdum et malesuada fames ac ante ipsum primis in faucibus. Cras hendrerit turpis id quam cursus laoreet id at risus. In molestie, lacus vitae convallis scelerisque, dui leo vehicula ante, sed dictum neque eros at magna. Nullam varius felis quis erat viverra aliquet. Pellentesque eleifend leo eu varius viverra. Phasellus tristique lorem facilisis porta aliquam. Quisque a dolor tortor. Nam pharetra, arcu lobortis porttitor egestas, nisl dolor vulputate metus, vitae fringilla dui erat at velit. Nam a nibh libero. Vivamus iaculis nulla quis pretium feugiat. Maecenas lobortis auctor lectus. Donec laoreet commodo ante sit amet cursus.</p>\r\n',1413686663),(5,5,'hi im a sample event!',1413936000,'just somewhere',NULL,'<p><img alt=\"\" src=\"/img/partnerspictures/5/Sample album/img103.jpg\" style=\"height:375px; width:600px\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi auctor dictum enim, eget volutpat arcu interdum eu. Praesent aliquam tortor ac ipsum facilisis, ac sollicitudin risus tristique. Nam dictum enim a ipsum ultricies, sit amet finibus nulla blandit. Integer at ipsum dictum, pulvinar nunc venenatis, lacinia risus. Aenean vehicula leo eu efficitur dapibus. Fusce faucibus mi scelerisque eros ornare interdum ac eu justo. Donec vitae tincidunt justo. Aliquam egestas tincidunt ultrices. Suspendisse lectus urna, imperdiet in fringilla efficitur, mollis ut urna.</p>\r\n\r\n<p>Nullam cursus mauris sodales, dapibus elit hendrerit, eleifend leo. Fusce mollis est purus, eu venenatis enim dictum ac. Nam in venenatis turpis. Aenean ultrices mattis dignissim. Maecenas ornare sem a pulvinar aliquet. Nullam nec massa auctor, aliquet elit at, efficitur massa. Sed eget leo porta, condimentum nunc eget, egestas felis. Pellentesque vitae nunc vitae augue tempor aliquet eu sed purus. Nullam felis nisi, egestas quis metus ut, ornare faucibus urna. Aenean vitae augue vitae nisl fermentum elementum. Nulla quis dolor eget turpis tincidunt congue finibus ut justo. Ut volutpat laoreet aliquam. Aliquam elementum facilisis tortor, sit amet faucibus ex. Nulla vel lobortis massa. Maecenas a pharetra dui. Sed interdum tortor magna.</p>\r\n\r\n<p>In bibendum consectetur sapien, non porttitor justo auctor sit amet. In hac habitasse platea dictumst. Maecenas aliquet vitae elit sit amet bibendum. Aenean iaculis sem neque, sit amet finibus magna vestibulum sit amet. Ut lobortis dui mi, at imperdiet quam luctus vel. Proin suscipit a ante in volutpat. Praesent tempus auctor ornare. Nunc a massa tortor. Nam tempus quam et enim egestas facilisis. Vestibulum mattis tincidunt nunc. Phasellus tincidunt elit id nunc tempus, quis convallis metus rutrum. Quisque ornare pellentesque dolor eu ornare. Aliquam mollis nulla ut ornare rutrum.</p>\r\n\r\n<p>Aenean urna metus, suscipit ut velit vel, rhoncus tincidunt ligula. Morbi ut turpis feugiat, suscipit lacus in, cursus sapien. Nunc nec metus pharetra turpis ornare auctor sit amet vitae diam. Phasellus luctus interdum odio, ac rhoncus elit laoreet quis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nulla a interdum augue, eu iaculis nulla. In vestibulum maximus mollis. Praesent malesuada libero eros, non pellentesque mi cursus in. Duis faucibus rhoncus dui, venenatis sodales orci aliquam non. Aliquam erat volutpat. Phasellus justo felis, iaculis non velit in, accumsan consectetur nibh. Proin ut dignissim purus. Suspendisse nec est quis lacus faucibus consectetur. Aenean vel convallis nisi, molestie accumsan leo.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Vestibulum tincidunt finibus porta. Morbi nec sodales felis. Praesent felis ligula, ultricies id rhoncus non, egestas luctus ante. Fusce a felis scelerisque, aliquet nibh in, consectetur urna. Nullam ultrices sed dolor at pretium. Phasellus mattis lectus porttitor aliquet congue. Suspendisse interdum lectus at risus fringilla facilisis quis a tellus. Nunc bibendum commodo efficitur. Maecenas eu pharetra ligula. Donec luctus massa est, sed pulvinar nunc semper quis. Duis in libero aliquam, dapibus orci sed, facilisis augue.</p>\r\n',1413726268),(6,7,'sample events',1447390800,'fafasfasf',NULL,'<p>asfasfasfsaf</p>\r\n',1447300311),(7,7,'sdfdfs',1451019600,'anywhere',NULL,'<p>vfdgdvvf</p>\r\n',1449819844),(9,14,'event 1',1453525200,'UCU',NULL,'<p>sdsafafa</p>\r\n',1452644928),(12,14,'j event',1456376400,'bcgfgxfx',NULL,'<p>gcgdghdfx</p>\r\n',1454316947),(13,7,'kyben events',1456290000,'hfhgf',NULL,'<p>dfggf</p>\r\n',1454316995),(14,29,'csadasd1',1455166800,'sad1',NULL,'<p>asdsad1</p>\r\n',1455089215),(15,61,'another1',1522771200,'new',NULL,'<p>sasasasasasasas</p>',1522829489),(16,31,'dgdsfgfdg',1523462400,'fgdfgfdg',NULL,'<p>fgdgdfgdfgfd</p>',1523524139),(17,14,'fdfhfhd',1523548800,'dfhdfdf',NULL,'<p>dfhfdhdfhfdh</p>',1523524715),(18,30,'neweventagain',1523548800,'dsds',NULL,'<p>dsdsdsdsdsdsdsd</p>',1523581484),(19,30,'new event',1523548800,'fdffdfd',NULL,'<p>fdfddfdf</p>',1523606065),(20,52,'another event',1523548800,'dssds',NULL,'<p>sdsdsdsd</p>',1523606597),(21,52,'new event',1523635200,'dfdfd',NULL,'<p>fdfdfdfdfdfdfdfdf</p>',1523606774),(22,51,'fgg',1523548800,'fgfg',NULL,'<p>fgfgf</p>',1523615161),(24,101,'asd',1524672000,'asdasdas',NULL,'<p>asdasdasd</p>',1524731394),(25,102,'test101',1524758400,'urdanetta ceuetry',NULL,'<p>vfgdfgdfgdfgfdgdfgfgdfg</p>',1524801269);
/*!40000 ALTER TABLE `tblpartnerevents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblpartners`
--

DROP TABLE IF EXISTS `tblpartners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblpartners` (
  `partnerID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `partnerName` varchar(255) NOT NULL,
  `partnerInfo` text,
  PRIMARY KEY (`partnerID`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblpartners`
--

LOCK TABLES `tblpartners` WRITE;
/*!40000 ALTER TABLE `tblpartners` DISABLE KEYS */;
INSERT INTO `tblpartners` VALUES (5,25,'Bulbasaur is a grass type pokemon','<p><img alt=\"\" src=\"http://fc00.deviantart.net/fs48/i/2011/321/3/a/bulbasaur__anatomical_study_by_joshd1000-d25vroa.jpg\" style=\"border-style:solid; border-width:1px; float:left; height:435px; margin:10px; width:500px\" />Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi auctor dictum enim, eget volutpat arcu interdum eu. Praesent aliquam tortor ac ipsum facilisis, ac sollicitudin risus tristique. Nam dictum enim a ipsum ultricies, sit amet finibus nulla blandit. Integer at ipsum dictum, pulvinar nunc venenatis, lacinia risus. Aenean vehicula leo eu efficitur dapibus. Fusce faucibus mi scelerisque eros ornare interdum ac eu justo. Donec vitae tincidunt justo. Aliquam egestas tincidunt ultrices. Suspendisse lectus urna, imperdiet in fringilla efficitur, mollis ut urna.</p>\r\n\r\n<p>Nullam cursus mauris sodales, dapibus elit hendrerit, eleifend leo. Fusce mollis est purus, eu venenatis enim dictum ac. Nam in venenatis turpis. Aenean ultrices mattis dignissim. Maecenas ornare sem a pulvinar aliquet. Nullam nec massa auctor, aliquet elit at, efficitur massa. Sed eget leo porta, condimentum nunc eget, egestas felis. Pellentesque vitae nunc vitae augue tempor aliquet eu sed purus. Nullam felis nisi, egestas quis metus ut, ornare faucibus urna. Aenean vitae augue vitae nisl fermentum elementum. Nulla quis dolor eget turpis tincidunt congue finibus ut justo. Ut volutpat laoreet aliquam. Aliquam elementum facilisis tortor, sit amet faucibus ex. Nulla vel lobortis massa. Maecenas a pharetra dui. Sed interdum tortor magna.</p>\r\n\r\n<p>In bibendum consectetur sapien, non porttitor justo auctor sit amet. In hac habitasse platea dictumst. Maecenas aliquet vitae elit sit amet bibendum. Aenean iaculis sem neque, sit amet finibus magna vestibulum sit amet. Ut lobortis dui mi, at imperdiet quam luctus vel. Proin suscipit a ante in volutpat. Praesent tempus auctor ornare. Nunc a massa tortor. Nam tempus quam et enim egestas facilisis. Vestibulum mattis tincidunt nunc. Phasellus tincidunt elit id nunc tempus, quis convallis metus rutrum. Quisque ornare pellentesque dolor eu ornare. Aliquam mollis nulla ut ornare rutrum.</p>\r\n\r\n<p>Aenean urna metus, suscipit ut velit vel, rhoncus tincidunt ligula. Morbi ut turpis feugiat, suscipit lacus in, cursus sapien. Nunc nec metus pharetra turpis ornare auctor sit amet vitae diam. Phasellus luctus interdum odio, ac rhoncus elit laoreet quis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nulla a interdum augue, eu iaculis nulla. In vestibulum maximus mollis. Praesent malesuada libero eros, non pellentesque mi cursus in. Duis faucibus rhoncus dui, venenatis sodales orci aliquam non. Aliquam erat volutpat. Phasellus justo felis, iaculis non velit in, accumsan consectetur nibh. Proin ut dignissim purus. Suspendisse nec est quis lacus faucibus consectetur. Aenean vel convallis nisi, molestie accumsan leo.</p>\r\n\r\n<p>Vestibulum tincidunt finibus porta. Morbi nec sodales felis. Praesent felis ligula, ultricies id rhoncus non, egestas luctus ante. Fusce a felis scelerisque, aliquet nibh in, consectetur urna. Nullam ultrices sed dolor at pretium. Phasellus mattis lectus porttitor aliquet congue. Suspendisse interdum lectus at risus fringilla facilisis quis a tellus. Nunc bibendum commodo efficitur. Maecenas eu pharetra ligula. Donec luctus massa est, sed pulvinar nunc semper quis. Duis in libero aliquam, dapibus orci sed, facilisis augue.</p>\r\n'),(9,38,'sasa',''),(10,44,'dsfs','<p>dsfsd</p>\r\n'),(11,45,'dsfs','<p>sdada</p>\r\n'),(12,50,'sgs','<p>sdfs</p>\r\n'),(13,55,'vxcgbx','<p>xbxb</p>\r\n'),(19,57,'leo partner','<p>partner</p>\r\n'),(20,59,'patrick mole','<p>pats</p>\r\n'),(51,143,'abkpartner',''),(52,144,'sample partner',''),(53,145,'partnership forever',''),(55,154,'sasd','<p>gdsfdfsdfsd</p>\r\n'),(56,155,'asdf',''),(57,156,'qwerty',''),(58,159,'dsd','<p>dsad</p>\r\n'),(59,166,'may',''),(60,172,'ddff','<p>fdf</p>\r\n'),(61,181,'fsdfsfas',''),(62,182,'partner name',''),(63,186,'dasd','<p>sadssf</p>\r\n'),(64,189,'dfsdf','<p>dfsfs</p>\r\n'),(65,191,'asdasd','<p>dasdas</p>\r\n'),(66,192,'asdasd','<p>asd</p>\r\n'),(67,197,'qwertyy','<p>hjer3rrq3wqwrqw</p>\r\n'),(68,198,'gbvcbcb','<p>efafafsaf</p>\r\n'),(69,203,'sdfgkjl','<p>asdgfkjljhgwqweryiopikjg</p>\r\n'),(70,205,'Mineski','<p>i love to play games</p>\r\n'),(71,236,'hfgfghfghfgh','<p>ghfghfghfghgf</p>\r\n'),(72,237,'bnvfbnj',''),(73,248,'sdsds','<p>dsds</p>\r\n'),(74,249,'jhhjhjhjh','<p>hghhggh</p>\r\n'),(75,250,'fdfdfd','<p>fdfdfdf</p>\r\n'),(76,251,'dsdsdsdsds','<p>dsdsdsd</p>\r\n'),(77,252,'dfdf','<p>dfdfdf</p>\r\n'),(78,253,'sdsds','<p>sdsdsd</p>\r\n'),(79,254,'sdsds','<p>sdsds</p>\r\n'),(80,256,'sasa',''),(81,259,'dssdsds','<p>sdsds</p>\r\n'),(83,261,'dsdsds',''),(84,264,'sdsdsdsdsd','<p>dsdsdsdsd</p>\r\n'),(85,265,'dsdsd','<p>dsds</p>\r\n'),(86,270,'dsdsds','<p>dsdsdsdsds</p>\r\n'),(89,273,'dsd','<p>sddssdsd</p>\r\n'),(90,274,'dsdssd','<p>dsdsdsd</p>\r\n'),(91,275,'fdfdf','<p>fdfdfdf</p>\r\n'),(92,278,'fdfdf','<p>fdfdfdfdfd</p>\r\n'),(93,279,'dsdsd','<p>dsdsdssds</p>\r\n'),(96,293,'rtyuygfh','<p>dfggjhrtytfghsysgfh</p>\r\n'),(97,294,'qwertyuiop','<p>qwertyuiop[oiuytrewertyuioiuytrewqertyuiytredfggd</p>\r\n'),(98,295,'dhgfhfdgh','<p>safdgsdg</p>\r\n'),(99,296,'This is my partner','<div style=\"background:#eee;border:1px solid #ccc;padding:5px 10px;\"><strong>eto partner ko</strong></div>\r\n'),(100,297,'This is my partner 2','<p>qwertyuiop</p>\r\n'),(101,300,'Arika','<p>Arika1108123323</p>\r\n'),(102,301,'mypartner66','<p>mypartner66</p>\r\n'),(103,303,'user111','<p>user111</p>\r\n');
/*!40000 ALTER TABLE `tblpartners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblpartnersalbums`
--

DROP TABLE IF EXISTS `tblpartnersalbums`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblpartnersalbums` (
  `albumID` int(11) NOT NULL AUTO_INCREMENT,
  `partnerID` int(11) NOT NULL,
  `albumName` varchar(255) NOT NULL,
  `dateCreated` int(11) NOT NULL,
  `coverID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`albumID`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblpartnersalbums`
--

LOCK TABLES `tblpartnersalbums` WRITE;
/*!40000 ALTER TABLE `tblpartnersalbums` DISABLE KEYS */;
INSERT INTO `tblpartnersalbums` VALUES (10,5,'bulbasaur',1413726872,0),(11,5,'Sample album',1413731327,0),(12,5,'sample again',1413732861,41),(14,7,'kaslaawan',1447300167,0),(17,7,'mahal',1453876956,0),(18,19,'leo album',1453886313,0),(19,29,'df',1455088524,0),(20,61,'sample',1511424542,0),(24,61,'album 3',1511426936,0),(25,61,'album album',1512556063,0),(26,14,'Lorraine',1521531631,70),(27,61,'mm',1522215137,0),(29,61,'new',1522227807,0),(30,61,'neww',1522228752,0),(32,61,'panibago',1522633476,0),(34,61,'again',1522633853,0),(35,61,'another1',1522830857,0),(36,14,'Loeeaknlsdgfsdgdg',1523525113,0),(37,30,'kezel',1523581906,0),(39,52,'new',1523607104,0),(40,52,'old',1523607230,0),(43,102,'sdfsghjkgfds',1524646069,0),(44,102,'asdfghjkhjgf',1524646106,0);
/*!40000 ALTER TABLE `tblpartnersalbums` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblpartnerspictures`
--

DROP TABLE IF EXISTS `tblpartnerspictures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblpartnerspictures` (
  `pictureID` int(11) NOT NULL AUTO_INCREMENT,
  `partnerID` int(11) NOT NULL,
  `albumID` int(11) NOT NULL,
  `pictureFilename` varchar(255) NOT NULL,
  `pictureCaption` varchar(255) DEFAULT NULL,
  `pictureSize` int(11) NOT NULL,
  `dateUploaded` int(11) NOT NULL,
  PRIMARY KEY (`pictureID`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblpartnerspictures`
--

LOCK TABLES `tblpartnerspictures` WRITE;
/*!40000 ALTER TABLE `tblpartnerspictures` DISABLE KEYS */;
INSERT INTO `tblpartnerspictures` VALUES (17,5,10,'Bulbasaur_by_misterunlucky.jpg','Bulbasaur_by_misterunlucky.jpg',49232,1413726966),(19,5,10,'realistic_bulbasaur_by_lanceofdragon-d470czo.png','realistic_bulbasaur_by_lanceofdragon-d470czo.png',1024921,1413726998),(28,5,12,'images (1).jpg','images (1).jpg',10573,1413736953),(29,5,11,'images.jpg','images.jpg',13972,1413736954),(30,5,11,'disaster (2).jpg','disaster (2).jpg',75555,1413736954),(31,5,11,'img103.jpg','img103.jpg',443873,1413736954),(32,5,11,'img100 (1).jpg','img100 (1).jpg',212659,1413736954),(33,5,11,'img104.jpg','img104.jpg',294707,1413736955),(34,5,11,'philippines420-420x0 (6).jpg','philippines420-420x0 (6).jpg',73367,1413736973),(35,5,11,'Japan tsunami deestruction (2).jpg','Japan tsunami deestruction (2).jpg',307692,1413736975),(36,5,11,'img102 (1).jpg','img102 (1).jpg',781317,1413736985),(37,5,11,'img101.jpg','img101.jpg',597891,1413736986),(38,5,11,'img105 (1).jpg','img105 (1).jpg',416971,1413736996),(39,5,11,'img105 (1)1.jpg','img105 (1)1.jpg',416971,1413737040),(40,5,11,'img105.jpg','img105.jpg',416971,1413737135),(41,5,12,'images (1).jpg','images (1).jpg',10573,1413738741),(42,5,12,'images (1).jpg','images (1).jpg',10573,1413738789),(43,5,10,'img100.jpg','img100.jpg',212659,1415536390),(45,7,14,'gam7h30izfr1436435277705.jpg','gam7h30izfr1436435277705.jpg',158054,1447300213),(46,7,14,'jw2nwlsdcxr1436435250667.jpg','jw2nwlsdcxr1436435250667.jpg',157488,1447300214),(47,7,14,'now6ario1or14405512203920.jpg','now6ario1or14405512203920.jpg',91152,1447300214),(48,7,14,'3esway8pvi1436435242744.jpg','3esway8pvi1436435242744.jpg',150781,1447300216),(49,7,14,'joxd23mcxr1436435256858.jpg','joxd23mcxr1436435256858.jpg',186642,1447300216),(50,7,14,'pt6v0vhd7vi1436435265945.jpg','pt6v0vhd7vi1436435265945.jpg',285641,1447300217),(51,7,14,'0dceb0dlsor1436435223223.jpg','0dceb0dlsor1436435223223.jpg',241202,1447300218),(54,14,16,'Design_Trends.jpg','Design_Trends.jpg',86024,1452645025),(56,7,14,'LythrumSalicaria-flowers-1mb (1).jpg','LythrumSalicaria-flowers-1mb (1).jpg',669097,1453871629),(57,7,17,'Black-HD-Anonymous-Logo-Wallpaper-Theme.jpeg','Black-HD-Anonymous-Logo-Wallpaper-Theme.jpeg',60777,1453876980),(58,7,17,'aiqucsoloegzbavteeju (1).jpg','aiqucsoloegzbavteeju (1).jpg',732762,1453877012),(59,19,18,'1mb (6).png','1mb (6).png',1028624,1453886467),(60,7,14,'2mb bcjhggfd.jpg','2mb bcjhggfd.jpg',509724,1453943833),(61,7,14,'3 mb Elise_by_the_Se3 ma (1).jpg','3 mb Elise_by_the_Se3 ma (1).jpg',572898,1453961856),(62,7,14,'Snake_River_(5mb).jpg','Snake_River_(5mb).jpg',521247,1453961884),(64,29,19,'photo-original jan 26 1.jpg','photo-original jan 26 1.jpg',157987,1455088817),(65,29,19,'photo-original jan 27 2.jpg','photo-original jan 27 2.jpg',243641,1455088849),(66,29,19,'photo-original jan 27 1.png','photo-original jan 27 1.png',818684,1455088880),(67,61,20,'105012 (1).jpg','105012 (1).jpg',209861,1511425909),(68,61,20,'5456172-nature-background (1).jpg','5456172-nature-background (1).jpg',163644,1511425969),(69,61,20,'fuchs-1310826_960_720.jpg','fuchs-1310826_960_720.jpg',233641,1511425978),(71,61,20,'cat1.png','cat1.png',915851,1522215071),(72,61,20,'cat11.png','cat11.png',915851,1522215104),(73,61,28,'Cats-Sticking-Out-Tongues-001.jpg','Cats-Sticking-Out-Tongues-001.jpg',89673,1522215502),(74,61,20,'cat12.png','cat12.png',915851,1522227884),(75,61,20,'413474560.jpg','413474560.jpg',30658,1522228586),(79,14,26,'373690.png','373690.png',39799,1523525627),(80,14,36,'373690.png','373690.png',39799,1523526272),(81,52,40,'cat1.png','cat1.png',915851,1523607260);
/*!40000 ALTER TABLE `tblpartnerspictures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblporgramactivities`
--

DROP TABLE IF EXISTS `tblporgramactivities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblporgramactivities` (
  `activityID` int(11) NOT NULL AUTO_INCREMENT,
  `programID` int(11) NOT NULL,
  `activity` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`activityID`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblporgramactivities`
--

LOCK TABLES `tblporgramactivities` WRITE;
/*!40000 ALTER TABLE `tblporgramactivities` DISABLE KEYS */;
INSERT INTO `tblporgramactivities` VALUES (6,1,'NDRRMC Program 1',1),(7,1,'Earthquake Drill',1),(8,6,'vghfghfh',0),(9,1,'Fire Drill',1),(10,2,'Personal Care Activity! Let\'s take a bath!!!',1),(11,4,'Economic Livelihood Home / Programs',1),(12,1,'Red Cross Basic Life Support Training',1),(13,1,'ssdsdsd',0),(14,1,'Red Cross Basic Life Suppocxvxc',0),(15,4,'Sustainable Livelihood Program',1),(16,5,'Universal Kindergarten',1),(17,5,' Church-Based Early Childhood',1),(18,6,'Philippine Environment Partnership Program (PEPP)',1),(19,1,'    ',0),(20,1,'               ',0),(21,1,'            ',0),(22,1,'fg',1),(23,14,'eeeeeeeeeeeeeeeee',1),(24,14,'sdt',0),(25,16,'bjghfjkl',1),(26,1,'hjgjghjghj',1);
/*!40000 ALTER TABLE `tblporgramactivities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblpost`
--

DROP TABLE IF EXISTS `tblpost`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblpost` (
  `postID` int(11) NOT NULL AUTO_INCREMENT,
  `postTitle` varchar(500) NOT NULL,
  `postSlug` varchar(500) NOT NULL,
  `postContent` text NOT NULL,
  `postStatus` varchar(100) NOT NULL,
  `postDate` int(11) NOT NULL,
  `postPublishDate` int(11) NOT NULL,
  `postKeyword` varchar(500) DEFAULT NULL,
  `postFeatureImage` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`postID`)
) ENGINE=InnoDB AUTO_INCREMENT=123 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblpost`
--

LOCK TABLES `tblpost` WRITE;
/*!40000 ALTER TABLE `tblpost` DISABLE KEYS */;
INSERT INTO `tblpost` VALUES (90,'sample post','sample-post','sample body','publish',1524626623,1524626623,'sample keyword','/img/programs/post/IMG_175945 (2).jpg'),(91,'sample post 2','sample-post-2','sample body 2','publish',1524626685,1524626685,'sample keyword 2','/img/programs/post/oct (2).jpg'),(92,'sample post 3','sample-post-3','sample body 3','publish',1524626726,1524626726,'','/img/programs/post/fuchs-1310826_960_720.jpg'),(93,'sample post 4','sample-post-4','<p>sample body 4</p>','publish',1524626823,1524585600,'','/img/programs/post/413474560.jpg'),(94,'sample post 5','sample-post-5','sample body&nbsp;5','publish',1524626871,1524626871,'','/img/programs/post/IMG_8893 (5).JPG'),(95,'sample post 6','sample-post-6','sample body 6','publish',1524626910,1524626910,'','/img/programs/post/436th Agew na Pangasinan.jpg'),(102,'Lorraine','lorraine','<p>igu</p>','publish',1524628420,1524585600,'','http://abksite.gotitgenius.com/img/programs/post/436th%20Agew%20na%20Pangasinan.jpg');
/*!40000 ALTER TABLE `tblpost` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblpostcat`
--

DROP TABLE IF EXISTS `tblpostcat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblpostcat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relatedID` int(11) NOT NULL,
  `relatedtype` varchar(255) NOT NULL,
  `postID` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=658 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblpostcat`
--

LOCK TABLES `tblpostcat` WRITE;
/*!40000 ALTER TABLE `tblpostcat` DISABLE KEYS */;
INSERT INTO `tblpostcat` VALUES (6,1,'pages',8),(13,1,'pages',9),(14,3,'pages',9),(15,4,'pages',9),(20,1,'pages',1),(26,1,'pages',5),(31,1,'pages',10),(35,1,'pages',11),(40,1,'pages',14),(42,1,'pages',15),(56,1,'pages',16),(58,1,'pages',18),(60,1,'pages',19),(62,1,'pages',20),(64,1,'pages',21),(66,1,'pages',22),(68,1,'pages',23),(70,1,'pages',24),(72,1,'pages',25),(74,1,'pages',26),(78,1,'pages',28),(80,1,'pages',29),(82,1,'pages',30),(84,1,'pages',31),(86,1,'pages',32),(88,1,'pages',33),(90,1,'pages',34),(100,1,'pages',36),(101,3,'pages',36),(102,4,'pages',36),(106,1,'pages',37),(107,1,'pages',27),(108,1,'programs',38),(109,1,'pages',39),(110,1,'programs',40),(111,1,'pages',40),(114,1,'programs',7),(115,1,'pages',7),(116,1,'programs',41),(117,1,'pages',42),(118,1,'programs',43),(119,1,'pages',43),(122,1,'programs',46),(123,1,'pages',46),(125,1,'programs',52),(127,3,'programs',54),(128,1,'programs',55),(129,1,'pages',55),(135,1,'programs',51),(144,1,'programs',57),(145,1,'pages',57),(146,1,'programs',56),(147,1,'pages',56),(148,1,'programs',45),(149,1,'pages',45),(150,1,'programs',58),(151,1,'pages',58),(152,1,'programs',59),(153,3,'programs',59),(154,14,'programs',59),(155,1,'pages',59),(156,4,'pages',59),(157,1,'programs',60),(158,1,'pages',60),(159,1,'programs',61),(160,1,'pages',61),(161,1,'programs',62),(162,1,'pages',62),(163,1,'programs',63),(164,1,'pages',63),(165,1,'programs',64),(166,1,'pages',64),(169,1,'programs',66),(170,3,'pages',66),(176,1,'programs',67),(177,3,'programs',67),(178,3,'pages',67),(181,1,'programs',69),(182,1,'pages',69),(185,1,'programs',70),(186,4,'programs',70),(187,6,'programs',70),(188,1,'pages',70),(189,3,'pages',70),(190,4,'pages',70),(191,1,'programs',71),(192,1,'pages',71),(197,1,'programs',72),(198,1,'pages',72),(199,1,'programs',73),(200,1,'pages',73),(209,1,'programs',74),(210,1,'pages',74),(220,18,'programs',78),(221,1,'pages',78),(222,1,'programs',79),(223,2,'programs',79),(224,3,'programs',79),(225,4,'programs',79),(226,5,'programs',79),(227,15,'programs',79),(228,16,'programs',79),(229,17,'programs',79),(230,18,'programs',79),(231,19,'programs',79),(232,1,'pages',79),(233,3,'pages',79),(234,4,'pages',79),(235,1,'programs',80),(236,2,'programs',80),(237,3,'programs',80),(238,4,'programs',80),(239,5,'programs',80),(240,15,'programs',80),(241,16,'programs',80),(242,17,'programs',80),(243,18,'programs',80),(244,19,'programs',80),(245,1,'pages',80),(246,3,'pages',80),(247,4,'pages',80),(251,1,'programs',77),(252,4,'programs',77),(253,5,'programs',77),(254,16,'programs',77),(255,18,'programs',77),(256,1,'pages',77),(257,3,'pages',77),(272,1,'programs',75),(273,2,'programs',75),(274,3,'programs',75),(275,4,'programs',75),(276,5,'programs',75),(277,15,'programs',75),(278,16,'programs',75),(279,17,'programs',75),(280,18,'programs',75),(281,19,'programs',75),(282,20,'programs',75),(283,1,'pages',75),(284,3,'pages',75),(285,4,'pages',75),(286,1,'programs',76),(287,1,'pages',76),(288,1,'programs',81),(289,2,'programs',81),(290,3,'programs',81),(291,4,'programs',81),(292,5,'programs',81),(293,15,'programs',81),(294,16,'programs',81),(295,17,'programs',81),(296,18,'programs',81),(297,19,'programs',81),(298,20,'programs',81),(299,21,'programs',81),(300,1,'pages',81),(301,3,'pages',81),(302,4,'pages',81),(307,21,'programs',83),(308,1,'pages',83),(309,3,'pages',83),(315,1,'programs',84),(316,2,'programs',84),(317,3,'programs',84),(318,1,'pages',84),(319,3,'pages',84),(320,1,'programs',82),(321,2,'programs',82),(322,3,'programs',82),(323,4,'programs',82),(324,5,'programs',82),(325,1,'pages',82),(326,3,'pages',82),(337,1,'programs',86),(338,2,'programs',86),(339,3,'programs',86),(340,4,'programs',86),(341,1,'pages',86),(342,3,'pages',86),(343,4,'pages',86),(347,1,'programs',68),(348,1,'pages',68),(349,1,'programs',65),(350,1,'pages',65),(356,1,'programs',87),(357,2,'programs',87),(358,3,'programs',87),(359,1,'pages',87),(360,3,'pages',87),(372,1,'programs',88),(373,4,'programs',88),(374,19,'programs',88),(375,20,'programs',88),(376,21,'programs',88),(377,3,'pages',88),(378,4,'pages',88),(379,1,'programs',85),(380,2,'programs',85),(381,3,'programs',85),(382,4,'programs',85),(383,1,'pages',85),(384,1,'programs',90),(385,3,'programs',90),(386,5,'programs',90),(387,1,'pages',90),(388,3,'pages',90),(389,1,'programs',91),(390,2,'programs',91),(391,3,'programs',91),(392,4,'programs',91),(393,5,'programs',91),(394,15,'programs',91),(395,16,'programs',91),(396,17,'programs',91),(397,18,'programs',91),(398,21,'programs',91),(399,1,'pages',91),(400,3,'pages',91),(401,4,'pages',91),(402,21,'programs',92),(403,4,'pages',92),(405,17,'programs',94),(406,21,'programs',95),(407,21,'programs',96),(408,4,'pages',96),(409,18,'programs',97),(410,4,'pages',98),(411,3,'pages',99),(412,1,'pages',100),(413,1,'pages',101),(414,3,'pages',101),(415,4,'pages',101),(424,1,'programs',104),(425,2,'programs',104),(426,3,'programs',104),(427,1,'pages',104),(428,3,'pages',104),(429,21,'programs',106),(430,1,'pages',106),(431,3,'pages',106),(432,4,'pages',106),(433,1,'programs',105),(434,2,'programs',105),(435,3,'programs',105),(436,4,'programs',105),(437,5,'programs',105),(438,15,'programs',105),(439,16,'programs',105),(440,17,'programs',105),(441,18,'programs',105),(442,21,'programs',105),(443,18,'programs',93),(463,1,'programs',107),(464,4,'programs',107),(465,15,'programs',107),(466,17,'programs',107),(467,18,'programs',107),(468,1,'pages',107),(469,4,'pages',107),(475,21,'programs',109),(476,21,'programs',110),(477,1,'pages',110),(478,3,'pages',110),(479,4,'pages',110),(484,1,'pages',111),(485,3,'pages',111),(494,1,'programs',112),(495,3,'programs',112),(496,4,'programs',112),(497,21,'programs',112),(498,1,'pages',112),(499,3,'pages',112),(500,4,'pages',112),(501,6,'pages',112),(510,21,'programs',113),(511,6,'pages',113),(512,7,'pages',113),(519,1,'programs',114),(520,2,'programs',114),(521,4,'programs',114),(522,21,'programs',114),(523,6,'pages',114),(524,7,'pages',114),(529,21,'programs',115),(530,5,'pages',115),(531,6,'pages',115),(532,7,'pages',115),(552,5,'programs',116),(553,21,'programs',116),(554,5,'pages',116),(555,6,'pages',116),(556,7,'pages',116),(573,21,'programs',117),(574,6,'pages',117),(575,7,'pages',117),(576,50,'pages',117),(577,21,'programs',118),(578,6,'pages',118),(579,7,'pages',118),(580,50,'pages',118),(589,2,'programs',119),(590,21,'programs',119),(591,6,'pages',119),(592,7,'pages',119),(593,50,'pages',119),(594,2,'programs',120),(595,15,'programs',120),(596,16,'programs',120),(597,17,'programs',120),(598,18,'programs',120),(599,21,'programs',120),(600,6,'pages',120),(601,7,'pages',120),(602,50,'pages',120),(611,1,'programs',121),(612,3,'programs',121),(613,4,'programs',121),(614,5,'programs',121),(615,21,'programs',121),(616,6,'pages',121),(617,7,'pages',121),(618,50,'pages',121),(641,1,'programs',122),(642,2,'programs',122),(643,3,'programs',122),(644,4,'programs',122),(645,5,'programs',122),(646,6,'pages',122),(647,7,'pages',122),(648,50,'pages',122),(649,1,'programs',108),(650,3,'programs',108),(651,4,'programs',108),(652,21,'programs',108),(653,1,'programs',102),(654,2,'programs',102),(655,3,'programs',102),(656,4,'programs',102),(657,21,'programs',102);
/*!40000 ALTER TABLE `tblpostcat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblprograms`
--

DROP TABLE IF EXISTS `tblprograms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblprograms` (
  `programID` int(11) NOT NULL AUTO_INCREMENT,
  `programName` varchar(500) NOT NULL,
  `programTooltip` text NOT NULL,
  `programTagline` varchar(255) NOT NULL,
  `programPage` varchar(255) NOT NULL,
  `programLastUpdated` int(11) NOT NULL,
  `programBanner` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`programID`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblprograms`
--

LOCK TABLES `tblprograms` WRITE;
/*!40000 ALTER TABLE `tblprograms` DISABLE KEYS */;
INSERT INTO `tblprograms` VALUES (1,'Disaster Risk Managements.','Are you ready when disaster strikes?','Are you ready when disaster strike?','disaster-risk-managements',1525768023,'/img/programs/1/dmnewsimage.png'),(2,'Create Your Personal Cause','Become the hero you would like to be','Become the hero you would like to be','create-your-personal-cause',1402857672,NULL),(3,'Healthcare For Everyone','Medical missions in your town','Medical missions in your town','healthcare-for-everyone',1402857715,NULL),(4,'Economic Livelihood','Financing for your great idea','Financing for your great idea','economic-livelihood',1525669925,NULL),(5,'Early Childhood Education','The most important education?','The most important education?','early-childhood-education',1402857846,NULL),(16,'testing 23','test tooltip 23','test tag line 23','testing-23',1524039229,NULL),(22,'Lorraine2222','cute ','Manzano','lorraine2222',1525415618,NULL),(26,'TESTERS10101','DFGDFG','GFGDFG','testers10101',1539235005,NULL),(27,'test','f sdfs fsdf sd fsd','sdfsdfs fsdf sd','test',1541469941,NULL);
/*!40000 ALTER TABLE `tblprograms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblprogramsimg`
--

DROP TABLE IF EXISTS `tblprogramsimg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblprogramsimg` (
  `imgname` text NOT NULL,
  `programID` int(11) NOT NULL,
  `imgpath` varchar(500) NOT NULL,
  `imgid` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`imgid`)
) ENGINE=InnoDB AUTO_INCREMENT=163 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblprogramsimg`
--

LOCK TABLES `tblprogramsimg` WRITE;
/*!40000 ALTER TABLE `tblprogramsimg` DISABLE KEYS */;
INSERT INTO `tblprogramsimg` VALUES ('88da11c474fbe989dab9311b9de3ed57img1 (2).png',2,'public/programs/',5),('b79943f1e99c84c2be91fe6d92722c93img3 (2).png',2,'public/programs/',6),('54750d671a3fb003a722aca30a3323abimg4 (2).png',2,'public/programs/',7),('04b12f5dc0ff7ab4d7467ed5f4e01e1aimg2 (2).png',2,'public/programs/',8),('efe79f99158214d23e685acc70e16b19img1 (2).png',3,'public/programs/',9),('9cee864746ef33030538380e9d201e60img3 (2).png',3,'public/programs/',10),('63a14d162eae944371c04746b695ee1bimg2 (2).png',3,'public/programs/',11),('983bb2dfc04e665e0cdf96bc6294fd9bimg4 (2).png',3,'public/programs/',12),('09d48d0dbe4b55ecbf35dc0269f1dc76img2 (2).png',5,'public/programs/',17),('8d508e971de591f619ae64cabc1ac2b6img3 (2).png',5,'public/programs/',18),('74db44b4ac5b78dd0599e1b46bd1c7c4img1 (2).png',5,'public/programs/',19),('7524ff19cd050fbe93102837da678126img4 (2).png',5,'public/programs/',20),('7d5aff17fc9b03345387f08881922bc9IMG_8893 (5).JPG',16,'public/programs/',92),('3cf4f2c222d573565d98c2321675dc87IMG_8894 (5).JPG',16,'public/programs/',93),('a9bd16a30c3e2275aad82635b20f4f76IMG_8892.JPG',16,'public/programs/',94),('c18ac3c98904752d0ff146ef7bc42dab1 (4).png',22,'public/programs/',139),('be2c4bfa4431f661f582554036db4d0aimg1 (2).png',4,'public/programs/',145),('09439398e07fb9ceee8f9b8eadcbf08dimg4 (2).png',4,'public/programs/',146),('b56ee3be3a2b6ae21b1094f196d47b53img2 (2).png',4,'public/programs/',147),('afb9d8e5adf10f5a3b5c2d626e58f04eimg3 (2).png',4,'public/programs/',148),('3a4485df4189f5bc20e9bc1a7faa1adeindex (3).jpg',4,'public/programs/',149),('3d995d752c4bdedd14de82c2835d6875img1 (2).png',1,'public/programs/',150),('dced031b7a774e0f60a57ac82b76b218img2 (2).png',1,'public/programs/',151),('879e0cf5fdb13542f6a9546eaab367bdimg3 (2).png',1,'public/programs/',152),('b36178cf5c9536ad3cb514b7a22f0a91img4 (4).png',1,'public/programs/',153),('085166e511b1585499dbebb5299a4645indedx (1).jpg',1,'public/programs/',154),('b36db5f626fbe9fff7c4d5ca272fe742images.jpg',26,'public/programs/',161),('f442bdc95240cd4f1c88eb8bd8688855beautiful nature.jpg',27,'public/programs/',162);
/*!40000 ALTER TABLE `tblprogramsimg` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblroles`
--

DROP TABLE IF EXISTS `tblroles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblroles` (
  `roleCode` varchar(200) NOT NULL,
  `roleDescription` varchar(255) NOT NULL,
  `rolePage` varchar(255) NOT NULL,
  PRIMARY KEY (`roleCode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblroles`
--

LOCK TABLES `tblroles` WRITE;
/*!40000 ALTER TABLE `tblroles` DISABLE KEYS */;
INSERT INTO `tblroles` VALUES ('annrole','Announcements','announcements,createannocements,viewannouncement,editannouncement'),('conrole','Contacts','inquiries'),('donrole','Manage Donations','donations,abkrescreateactivity,abkrescreateevents'),('enmrole','E-newsletter Management','newsletter,createnewsletter,ajaxmoveuploadnewsletter,deleteRecentNewsletterUpload'),('extrole','Extras',''),('pagerole','Pages','pages,createpage,editpage,ajaxmoveuploadpages,deleteRecentPagesPicUpload'),('partrole','ABK Partner User','ajaxUserView,partners,createpartner,partnersinfo,createpartnerevent,partnerAjaxUploadPicture,partnerAjaxEditPicture,partnersPictures,createAlbumAjax,editAlbumAjax,editEvent,editPartner,ajaxmoveuploadpartnerpictures,deleteRecentPicUpload,ajaxGetAllPictures'),('pmrole','Program Management','programs,programpages,ajaxmoveuploadprogram,ajaxdeleteupload,ajaxviewpage,suggestedprograms'),('postrole','News Post','createpost,ajaxmoveuploadpost,ajaxviewdigitalassets,allnewspost,editnews'),('rescuerole','ABK Rescue','banner,aboutus,donation,blog,volunteer,settings,events'),('townrole','Towns','towns,pintown,townsgallery,townAlbum,townevents,viewtown,townpartners'),('umrole','User Management','createuser,users,edituser,ajaxUserView'),('vmrole','Volunteer Management','volunteers');
/*!40000 ALTER TABLE `tblroles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblslides`
--

DROP TABLE IF EXISTS `tblslides`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblslides` (
  `slideID` int(11) NOT NULL AUTO_INCREMENT,
  `slideImgUrl` text NOT NULL,
  `slideVideoUrl` text,
  `slideVideoAlignment` enum('left','center','right') NOT NULL,
  PRIMARY KEY (`slideID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblslides`
--

LOCK TABLES `tblslides` WRITE;
/*!40000 ALTER TABLE `tblslides` DISABLE KEYS */;
INSERT INTO `tblslides` VALUES (5,'http://abk.gotitgenius.com/img/photos/banner1.png','https://www.youtube.com/embed/JkVnJ2lhYKU','right'),(6,'http://abk.gotitgenius.com/img/photos/banner2.png','https://www.youtube.com/embed/pNh-_SXdUgA','left'),(7,'http://abk.gotitgenius.com/img/photos/banner3.png','https://www.youtube.com/embed/JkVnJ2lhYKU','center'),(8,'http://abk.gotitgenius.com/img/photos/banner4.png','https://www.youtube.com/embed/1_UrLGx4OUk','left');
/*!40000 ALTER TABLE `tblslides` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbltownalbums`
--

DROP TABLE IF EXISTS `tbltownalbums`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbltownalbums` (
  `albumID` int(11) NOT NULL AUTO_INCREMENT,
  `townID` int(11) NOT NULL,
  `albumName` varchar(255) NOT NULL,
  `dateCreated` int(11) NOT NULL,
  `coverID` int(11) DEFAULT '0',
  PRIMARY KEY (`albumID`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbltownalbums`
--

LOCK TABLES `tbltownalbums` WRITE;
/*!40000 ALTER TABLE `tbltownalbums` DISABLE KEYS */;
INSERT INTO `tbltownalbums` VALUES (1,2,'sample',1420128942,NULL),(10,21,'jha',1456974260,0),(11,28,'jajaja',1456988657,0),(12,20,'sample test',1457415634,0),(13,20,'album 2',1457415848,0),(14,51,'            ',1524208297,0),(15,50,'bvcchgj',1524212004,0),(16,50,'                           ',1524648314,0),(17,50,'aaa',1524648408,0);
/*!40000 ALTER TABLE `tbltownalbums` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbltownevents`
--

DROP TABLE IF EXISTS `tbltownevents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbltownevents` (
  `eventID` int(11) NOT NULL AUTO_INCREMENT,
  `townID` int(11) NOT NULL,
  `eventTitle` varchar(255) NOT NULL,
  `eventDate` int(11) NOT NULL,
  `eventVenue` varchar(255) NOT NULL,
  `eventDetails` text NOT NULL,
  `dateCreated` int(11) NOT NULL,
  PRIMARY KEY (`eventID`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbltownevents`
--

LOCK TABLES `tbltownevents` WRITE;
/*!40000 ALTER TABLE `tbltownevents` DISABLE KEYS */;
INSERT INTO `tbltownevents` VALUES (3,0,'sample hahaha',1418400000,'sample venue','<p><img alt=\"\" src=\"/img/towns/hahahaha/tumblr_mf3zh52Kv21ruztjzo3_r3_500.gif\" style=\"height:225px; width:500px\" /></p>\r\n\r\n<p>dsf asd gdfsgsdg sdfgds gdfsgds gsdg sdfgds</p>\r\n\r\n<p>gds gkjasdfkab sbfasbfabskbaf ajf asdf as</p>\r\n\r\n<p>fas fadsf;kam ldfmglk gkmdsfl mdfslmg slgmdsg dsfgsdg dfsgdsjgn&nbsp;</p>\r\n\r\n<p>dsf asd gdfsgsdg sdfgds gdfsgds gsdg sdfgds</p>\r\n\r\n<p>gds gkjasdfkab sbfasbfabskbaf ajf asdf as</p>\r\n\r\n<p>fas fadsf;kam ldfmglk gkmdsfl mdfslmg slgmdsg dsfgsdg dfsgdsjgn&nbsp;</p>\r\n\r\n<p>dsf asd gdfsgsdg sdfgds gdfsgds gsdg sdfgds</p>\r\n\r\n<p>gds gkjasdfkab sbfasbfabskbaf ajf asdf as</p>\r\n\r\n<p>fas fadsf;kam ldfmglk gkmdsfl mdfslmg slgmdsg dsfgsdg dfsgdsjgn&nbsp;</p>\r\n\r\n<p>dsf asd gdfsgsdg sdfgds gdfsgds gsdg sdfgds</p>\r\n\r\n<p>gds gkjasdfkab sbfasbfabskbaf ajf asdf as</p>\r\n\r\n<p>fas fadsf;kam ldfmglk gkmdsfl mdfslmg slgmdsg dsfgsdg dfsgdsjgn&nbsp;<img alt=\"\" src=\"/img/towns/hahahaha/disaster (3).jpg\" style=\"height:479px; width:800px\" /></p>\r\n',1417799988),(22,50,'124567',1524585600,'124567','<p>124567</p>',1524647285),(23,50,'FGDFGDG',1525017600,'FGDFGDFGD','<p>FGDFGDFGDFGD</p>',1525080280),(24,50,'Lorrianeee',1525017600,'Manzano','<p>loraine manzano</p>',1525081903);
/*!40000 ALTER TABLE `tbltownevents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbltownpartners`
--

DROP TABLE IF EXISTS `tbltownpartners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbltownpartners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `townID` int(11) NOT NULL,
  `partnerID` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbltownpartners`
--

LOCK TABLES `tbltownpartners` WRITE;
/*!40000 ALTER TABLE `tbltownpartners` DISABLE KEYS */;
INSERT INTO `tbltownpartners` VALUES (3,2,4),(20,10,5),(22,2,5),(23,14,15),(24,42,99),(26,50,51),(27,50,101),(28,50,65),(29,50,56),(30,50,72),(31,50,63),(32,50,103);
/*!40000 ALTER TABLE `tbltownpartners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbltownpictures`
--

DROP TABLE IF EXISTS `tbltownpictures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbltownpictures` (
  `pictureID` int(11) NOT NULL AUTO_INCREMENT,
  `townID` int(11) NOT NULL,
  `albumID` int(11) NOT NULL,
  `pictureFilename` varchar(255) NOT NULL,
  `pictureCaption` varchar(255) NOT NULL,
  `pictureSize` varchar(255) NOT NULL,
  `dateUploaded` int(11) NOT NULL,
  PRIMARY KEY (`pictureID`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbltownpictures`
--

LOCK TABLES `tbltownpictures` WRITE;
/*!40000 ALTER TABLE `tbltownpictures` DISABLE KEYS */;
INSERT INTO `tbltownpictures` VALUES (3,2,1,'moped3 (1).jpg','moped3 (1).jpg','278506',1420961281),(4,2,1,'moped4 (2).jpg','moped4 (2).jpg','575445',1420961294),(5,2,1,'moped2 (1).jpg','moped2 (1).jpg','504965',1420961296),(6,2,1,'moped1 (15).jpg','moped1 (15).jpg','381875',1420961298),(7,3,2,'volunteering-in-the-philippines.1200.jpg','volunteering-in-the-philippines.1200.jpg','260129',1452735220),(8,10,5,'volunteering-in-the-philippines.1200.jpg','volunteering-in-the-philippines.1200.jpg','260129',1452846125),(9,13,6,'abklogo.png','abklogo.png','31427',1453711457),(10,13,6,'Community Care Pod Image.png','Community Care Pod Image.png','768595',1453711813),(11,13,6,'Fotolia_6821872_XXL.jpg','Fotolia_6821872_XXL.jpg','186436',1453711858),(12,2,1,'abklogo (2).png','abklogo (2).png','',1453872594),(13,2,1,'abklogo (3).png','abklogo (3).png','',1453872831),(14,14,7,'abklogo2 (1).png','abklogo2 (1).png','51472',1453875070),(15,14,7,'LythrumSalicaria-flowers-1mb (3).jpg','LythrumSalicaria-flowers-1mb (3).jpg','669097',1453875088),(16,10,5,'1mb (6).png','1mb (6).png','1028624',1453942985),(17,10,5,'2 mb.jpg','2 mb.jpg','300316',1453943341),(18,10,5,'Snake_River_(5mb).jpg','Snake_River_(5mb).jpg','521247',1453943387),(20,15,8,'1MB PRINCIPLE.jpg','1MB PRINCIPLE.jpg','122952',1453943979),(21,15,8,'2 mb glass (1).jpg','2 mb glass (1).jpg','524850',1453943998),(22,10,5,'1MB PRINCIPLE (1).jpg','1MB PRINCIPLE (1).jpg','122952',1453961998),(23,25,9,'eco.jpg','eco.jpg','5459',1457317327),(24,25,9,'eco-friendly1.jpg','eco-friendly1.jpg','49237',1457317328),(25,25,9,'~!@','~!@','',1457317361),(26,20,12,'abklogo (8).png','abklogo (8).png','31427',1457415678),(27,20,12,'eco.jpg','eco.jpg','5459',1457415679),(28,20,12,'eco-friendly1.jpg','eco-friendly1.jpg','49237',1457415680),(29,20,12,'aaaa.jpg','aaaa.jpg','47769',1457415681),(30,20,12,'adfsdff.jpg','adfsdff.jpg','51143',1457415682),(31,20,13,'abklogo (8).png','abklogo (8).png','31427',1457415905),(32,20,13,'~!@','~!@','',1457415906);
/*!40000 ALTER TABLE `tbltownpictures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbltowns`
--

DROP TABLE IF EXISTS `tbltowns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbltowns` (
  `townID` int(11) NOT NULL AUTO_INCREMENT,
  `townName` varchar(255) NOT NULL,
  `townLat` varchar(255) NOT NULL,
  `townLong` varchar(255) NOT NULL,
  `townInfo` text NOT NULL,
  `dateAdded` int(11) NOT NULL,
  PRIMARY KEY (`townID`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbltowns`
--

LOCK TABLES `tbltowns` WRITE;
/*!40000 ALTER TABLE `tbltowns` DISABLE KEYS */;
INSERT INTO `tbltowns` VALUES (20,'Naga','13.604879782135493','123.20068359375','<p>Naga City</p>\r\n',1455677703),(21,'Puerto Galera','13.507956609687405','120.9649658203125','<p><s>Puerto Galera</s></p>\r\n\r\n<p>&nbsp;</p>',1456901087),(35,'Tagaytay','14.057059116554786','120.9429931640625','<p>Thus</p>\r\n\r\n<p>is</p>\r\n\r\n<p>Tagaytay</p>\r\n\r\n<p>&nbsp;</p>',1456973965),(36,'Tayug','16.010175966953078','120.74386596679688','<p>tayug our kailyan!!!</p>',1477291376),(37,'tayug2','16.011495987391143','120.74798583984375','<p>tapsilogan</p>',1477294297),(38,'borongan city','11.623990188446712','125.36169430240989','<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a,</p>',1477650702),(39,'dalicno benguet','16.333318573391765','120.69305419921875','<p>hasdadff</p>',1508813666),(40,'sadsasdas','14.535732627008766','121.0308837890625','<p>dsadsa</p>',1510911485),(41,'batangas','13.7313809749427','121.0748291015625','<p>asshakdjakdafankn</p>',1511255265),(42,'baguio city','16.409739933377747','120.5859375','<p>city of pines</p>',1511333700),(43,'baler','15.713690944809595','121.57470703125','<p>a beautiful place where there are many tourist spots</p>',1511493244),(44,'cabanatuan city','15.480396076122638','120.97231455664064','<p>asjkdasd</p>',1511759790),(47,'Tarllac City','15.518820633745003','120.59284964510368','<p>sdsdsd</p>',1521690469),(49,'urd','15.976756620149574','120.57131358007814','<p>me sm dito</p>',1524193340),(50,'ang bayan ko','14.594461297930993','120.99428721289064','<p>DONEDFSDFSDF</p>',1525081159),(51,'tugigs','17.61078284901649','121.72784218216475','<p>tugig&nbsp;2</p>',1524206094),(52,'FGDFG','15.07767286248872','120.39553233007814','<p>DFDFGDFG</p>',1525080823),(53,'test town','16.946712573588208','121.12062998632814','<p>prod test</p>',1539235409);
/*!40000 ALTER TABLE `tbltowns` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbluserroles`
--

DROP TABLE IF EXISTS `tbluserroles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbluserroles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `userRoles` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1835 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbluserroles`
--

LOCK TABLES `tbluserroles` WRITE;
/*!40000 ALTER TABLE `tbluserroles` DISABLE KEYS */;
INSERT INTO `tbluserroles` VALUES (40,13,'partrole'),(41,13,'conrole'),(42,13,'extrole'),(43,15,'umrole'),(44,15,'annrole'),(45,15,'pmrole'),(46,15,'pagerole'),(47,16,'umrole'),(48,16,'annrole'),(49,16,'townrole'),(53,17,'annrole'),(54,17,'pagerole'),(55,18,'umrole'),(56,18,'partrole'),(57,18,'pagerole'),(58,19,'umrole'),(59,19,'annrole'),(62,21,'partrole'),(63,22,'partrole'),(64,23,'partrole'),(65,24,'partrole'),(66,25,'partrole'),(67,26,'partrole'),(68,27,'partrole'),(111,31,'partrole'),(112,30,'umrole'),(113,30,'annrole'),(114,30,'pmrole'),(115,30,'partrole'),(116,30,'townrole'),(117,30,'conrole'),(118,30,'pagerole'),(119,30,'extrole'),(120,30,'postrole'),(124,34,'umrole'),(125,34,'partrole'),(126,34,'conrole'),(127,35,'annrole'),(128,36,'umrole'),(129,36,'partrole'),(130,36,'conrole'),(134,38,'partrole'),(147,37,'umrole'),(148,32,'umrole'),(149,39,'annrole'),(150,39,'townrole'),(151,39,'pagerole'),(152,39,'extrole'),(153,39,'postrole'),(158,40,'umrole'),(159,40,'annrole'),(160,40,'pmrole'),(161,40,'partrole'),(162,40,'townrole'),(163,40,'conrole'),(164,40,'pagerole'),(165,40,'extrole'),(166,40,'postrole'),(167,41,'donrole'),(168,41,'enmrole'),(169,41,'vmrole'),(170,41,'umrole'),(171,41,'annrole'),(172,41,'pmrole'),(173,41,'partrole'),(174,41,'townrole'),(175,41,'conrole'),(176,41,'pagerole'),(177,41,'extrole'),(178,41,'postrole'),(179,42,'donrole'),(180,42,'vmrole'),(181,42,'umrole'),(182,42,'conrole'),(189,44,'partrole'),(190,45,'partrole'),(192,46,'partrole'),(193,47,'partrole'),(194,48,'partrole'),(195,49,'partrole'),(196,50,'partrole'),(197,51,'partrole'),(198,52,'partrole'),(199,53,'partrole'),(200,54,'partrole'),(201,55,'partrole'),(202,56,'partrole'),(207,57,'partrole'),(213,58,'enmrole'),(214,58,'annrole'),(215,58,'pmrole'),(216,58,'postrole'),(217,59,'partrole'),(218,60,'donrole'),(219,60,'annrole'),(220,60,'conrole'),(221,61,'partrole'),(222,62,'partrole'),(223,65,'partrole'),(224,66,'partrole'),(225,67,'partrole'),(226,68,'partrole'),(227,69,'partrole'),(228,70,'partrole'),(229,71,'partrole'),(230,72,'partrole'),(231,73,'partrole'),(232,74,'partrole'),(258,77,'partrole'),(261,79,'partrole'),(262,80,'partrole'),(263,81,'vmrole'),(264,100,'vmrole'),(265,101,'partrole'),(266,102,'partrole'),(267,103,'annrole'),(268,104,'annrole'),(269,105,'partrole'),(270,106,'partrole'),(271,107,'partrole'),(272,108,'partrole'),(273,109,'partrole'),(274,110,'partrole'),(275,111,'partrole'),(276,112,'townrole'),(277,112,'pagerole'),(278,112,'postrole'),(279,113,'townrole'),(282,28,'enmrole'),(283,28,'partrole'),(284,114,'annrole'),(285,115,'umrole'),(286,115,'partrole'),(287,116,'partrole'),(288,117,'townrole'),(289,118,'donrole'),(290,118,'enmrole'),(291,118,'vmrole'),(292,118,'umrole'),(293,118,'annrole'),(294,118,'pmrole'),(295,118,'partrole'),(296,118,'townrole'),(297,118,'conrole'),(298,118,'pagerole'),(299,118,'extrole'),(300,118,'postrole'),(301,119,'pmrole'),(302,120,'annrole'),(303,121,'annrole'),(304,122,'partrole'),(305,123,'pmrole'),(306,124,'vmrole'),(307,124,'partrole'),(308,124,'postrole'),(309,125,'partrole'),(316,126,'donrole'),(317,126,'enmrole'),(318,126,'vmrole'),(319,126,'umrole'),(320,126,'annrole'),(321,126,'pmrole'),(322,126,'partrole'),(323,126,'townrole'),(324,126,'conrole'),(325,126,'pagerole'),(326,126,'extrole'),(327,126,'postrole'),(328,127,'donrole'),(329,127,'enmrole'),(330,127,'vmrole'),(331,127,'umrole'),(332,127,'annrole'),(333,127,'pmrole'),(334,127,'partrole'),(335,127,'townrole'),(336,127,'conrole'),(337,127,'pagerole'),(338,127,'extrole'),(339,127,'postrole'),(340,128,'postrole'),(343,43,'umrole'),(344,43,'partrole'),(345,89,'vmrole'),(357,129,'partrole'),(358,20,'annrole'),(359,20,'partrole'),(360,130,'annrole'),(361,131,'partrole'),(362,132,'donrole'),(363,132,'enmrole'),(364,132,'vmrole'),(365,132,'umrole'),(366,132,'annrole'),(367,132,'pmrole'),(368,132,'partrole'),(369,132,'townrole'),(370,132,'conrole'),(371,132,'pagerole'),(372,132,'extrole'),(373,132,'postrole'),(374,133,'donrole'),(375,133,'enmrole'),(376,133,'vmrole'),(377,133,'umrole'),(378,133,'annrole'),(379,133,'pmrole'),(380,133,'partrole'),(381,133,'townrole'),(382,133,'conrole'),(383,133,'pagerole'),(384,133,'extrole'),(385,133,'postrole'),(386,134,'annrole'),(387,135,'annrole'),(389,136,'donrole'),(390,136,'enmrole'),(391,136,'vmrole'),(392,136,'umrole'),(393,136,'annrole'),(394,136,'pmrole'),(395,136,'partrole'),(396,136,'townrole'),(397,136,'conrole'),(398,136,'pagerole'),(399,136,'extrole'),(400,136,'postrole'),(401,137,'partrole'),(402,138,'annrole'),(403,139,'annrole'),(404,140,'annrole'),(405,141,'annrole'),(406,142,'conrole'),(407,143,'partrole'),(408,144,'partrole'),(409,145,'partrole'),(454,148,'partrole'),(455,149,'annrole'),(456,150,'donrole'),(457,150,'enmrole'),(458,150,'vmrole'),(459,150,'umrole'),(460,150,'annrole'),(461,150,'pmrole'),(462,150,'partrole'),(463,150,'townrole'),(464,150,'conrole'),(465,150,'pagerole'),(466,150,'extrole'),(467,150,'postrole'),(468,151,'donrole'),(469,151,'enmrole'),(470,151,'vmrole'),(471,151,'umrole'),(472,151,'annrole'),(473,151,'pmrole'),(474,151,'partrole'),(475,151,'townrole'),(476,151,'conrole'),(477,151,'pagerole'),(478,151,'extrole'),(479,151,'postrole'),(480,152,'donrole'),(481,152,'enmrole'),(482,152,'vmrole'),(483,152,'umrole'),(484,152,'annrole'),(485,152,'pmrole'),(486,152,'partrole'),(487,152,'townrole'),(488,152,'conrole'),(489,152,'pagerole'),(490,152,'extrole'),(491,152,'postrole'),(492,88,'conrole'),(493,88,'postrole'),(494,153,'annrole'),(525,154,'partrole'),(526,155,'partrole'),(527,156,'partrole'),(528,157,'pagerole'),(529,158,'pmrole'),(530,159,'partrole'),(531,160,'donrole'),(532,160,'enmrole'),(533,160,'vmrole'),(534,160,'umrole'),(535,160,'annrole'),(536,160,'pmrole'),(537,160,'partrole'),(538,160,'townrole'),(539,160,'conrole'),(540,160,'pagerole'),(541,160,'extrole'),(542,160,'postrole'),(543,161,'donrole'),(544,161,'enmrole'),(545,161,'vmrole'),(546,161,'umrole'),(547,161,'annrole'),(548,161,'pmrole'),(549,161,'partrole'),(550,161,'townrole'),(551,161,'conrole'),(552,161,'pagerole'),(553,161,'extrole'),(554,161,'postrole'),(555,162,'donrole'),(556,162,'enmrole'),(557,162,'vmrole'),(558,162,'umrole'),(559,162,'annrole'),(560,162,'pmrole'),(561,162,'partrole'),(562,162,'townrole'),(563,162,'conrole'),(564,162,'pagerole'),(565,162,'extrole'),(566,162,'postrole'),(567,163,'donrole'),(568,163,'enmrole'),(569,163,'vmrole'),(570,163,'umrole'),(571,163,'annrole'),(572,163,'pmrole'),(573,163,'partrole'),(574,163,'townrole'),(575,163,'conrole'),(576,163,'pagerole'),(577,163,'extrole'),(578,163,'postrole'),(603,164,'donrole'),(604,164,'enmrole'),(605,164,'vmrole'),(606,164,'umrole'),(607,164,'annrole'),(608,164,'pmrole'),(609,164,'partrole'),(610,164,'townrole'),(611,164,'conrole'),(612,164,'pagerole'),(613,164,'extrole'),(614,164,'postrole'),(615,165,'donrole'),(616,165,'enmrole'),(617,165,'vmrole'),(618,165,'umrole'),(619,165,'annrole'),(620,165,'pmrole'),(621,165,'partrole'),(622,165,'townrole'),(623,165,'conrole'),(624,165,'pagerole'),(625,165,'extrole'),(626,165,'postrole'),(627,166,'partrole'),(628,167,'donrole'),(629,167,'enmrole'),(630,167,'vmrole'),(631,167,'umrole'),(632,167,'annrole'),(633,167,'pmrole'),(634,167,'partrole'),(635,167,'townrole'),(636,167,'conrole'),(637,167,'pagerole'),(638,167,'extrole'),(639,167,'postrole'),(640,168,'donrole'),(641,168,'enmrole'),(642,168,'vmrole'),(643,168,'umrole'),(644,168,'annrole'),(645,168,'pmrole'),(646,168,'partrole'),(647,168,'townrole'),(648,168,'conrole'),(649,168,'pagerole'),(650,168,'extrole'),(651,168,'postrole'),(652,169,'annrole'),(662,170,'donrole'),(663,170,'enmrole'),(664,170,'vmrole'),(665,170,'umrole'),(666,170,'annrole'),(667,170,'pmrole'),(668,170,'partrole'),(669,170,'townrole'),(670,170,'conrole'),(671,170,'pagerole'),(672,170,'extrole'),(673,170,'postrole'),(686,171,'donrole'),(687,171,'enmrole'),(688,171,'vmrole'),(689,171,'umrole'),(690,171,'annrole'),(691,171,'pmrole'),(692,171,'partrole'),(693,171,'townrole'),(694,171,'conrole'),(695,171,'pagerole'),(696,171,'extrole'),(697,171,'postrole'),(698,172,'partrole'),(699,173,'annrole'),(700,174,'annrole'),(701,175,'donrole'),(702,175,'enmrole'),(703,175,'vmrole'),(704,175,'umrole'),(705,175,'annrole'),(706,175,'pmrole'),(707,175,'partrole'),(708,175,'townrole'),(709,175,'conrole'),(710,175,'pagerole'),(711,175,'extrole'),(712,175,'postrole'),(713,176,'donrole'),(714,176,'enmrole'),(715,176,'vmrole'),(716,176,'umrole'),(717,176,'annrole'),(718,176,'pmrole'),(719,176,'partrole'),(720,176,'townrole'),(721,176,'conrole'),(722,176,'pagerole'),(723,176,'extrole'),(724,176,'postrole'),(725,177,'donrole'),(726,177,'enmrole'),(727,177,'vmrole'),(728,177,'umrole'),(729,177,'annrole'),(730,177,'pmrole'),(731,177,'partrole'),(732,177,'townrole'),(733,177,'conrole'),(734,177,'pagerole'),(735,177,'extrole'),(736,177,'postrole'),(737,178,'donrole'),(738,178,'enmrole'),(739,178,'vmrole'),(740,178,'umrole'),(741,178,'annrole'),(742,178,'pmrole'),(743,178,'partrole'),(744,178,'townrole'),(745,178,'conrole'),(746,178,'pagerole'),(747,178,'extrole'),(748,178,'postrole'),(749,179,'annrole'),(750,179,'pmrole'),(751,179,'partrole'),(752,179,'townrole'),(753,179,'conrole'),(754,179,'pagerole'),(755,179,'extrole'),(756,179,'postrole'),(757,180,'donrole'),(758,180,'enmrole'),(759,180,'vmrole'),(760,180,'umrole'),(761,180,'annrole'),(762,180,'pmrole'),(763,180,'partrole'),(764,180,'townrole'),(765,180,'conrole'),(766,180,'pagerole'),(767,180,'extrole'),(768,180,'postrole'),(769,181,'partrole'),(770,182,'partrole'),(771,183,'partrole'),(772,184,'donrole'),(773,184,'enmrole'),(774,184,'vmrole'),(775,184,'umrole'),(776,184,'annrole'),(777,184,'pmrole'),(778,184,'partrole'),(779,184,'townrole'),(780,184,'conrole'),(781,184,'pagerole'),(782,184,'extrole'),(783,184,'postrole'),(784,185,'enmrole'),(785,185,'vmrole'),(786,185,'umrole'),(787,185,'annrole'),(788,186,'partrole'),(789,187,'partrole'),(790,188,'partrole'),(791,189,'partrole'),(792,190,'donrole'),(793,190,'enmrole'),(794,190,'vmrole'),(795,190,'umrole'),(796,190,'annrole'),(797,190,'pmrole'),(798,190,'partrole'),(799,190,'townrole'),(800,190,'conrole'),(801,190,'pagerole'),(802,190,'extrole'),(803,190,'postrole'),(814,146,'vmrole'),(815,146,'umrole'),(816,146,'pmrole'),(817,146,'partrole'),(818,146,'townrole'),(819,146,'pagerole'),(820,146,'extrole'),(821,146,'postrole'),(822,191,'partrole'),(823,192,'partrole'),(824,193,'donrole'),(825,193,'enmrole'),(826,193,'vmrole'),(827,193,'umrole'),(828,193,'annrole'),(829,193,'pmrole'),(830,193,'partrole'),(831,193,'townrole'),(832,193,'conrole'),(833,193,'pagerole'),(834,193,'extrole'),(835,193,'postrole'),(836,194,'donrole'),(837,194,'enmrole'),(838,194,'vmrole'),(839,194,'umrole'),(840,194,'annrole'),(841,194,'pmrole'),(842,194,'partrole'),(843,194,'townrole'),(844,194,'conrole'),(845,194,'pagerole'),(846,194,'extrole'),(847,194,'postrole'),(848,195,'donrole'),(849,195,'enmrole'),(850,195,'vmrole'),(851,195,'umrole'),(852,195,'annrole'),(853,195,'pmrole'),(854,195,'partrole'),(855,195,'townrole'),(856,195,'conrole'),(857,195,'pagerole'),(858,195,'extrole'),(859,195,'postrole'),(860,196,'donrole'),(861,196,'enmrole'),(862,196,'vmrole'),(863,196,'umrole'),(864,196,'annrole'),(865,196,'pmrole'),(866,196,'partrole'),(867,196,'townrole'),(868,196,'conrole'),(869,196,'pagerole'),(870,196,'extrole'),(871,196,'postrole'),(872,197,'partrole'),(873,198,'partrole'),(874,199,'donrole'),(875,199,'enmrole'),(876,199,'vmrole'),(877,199,'umrole'),(878,199,'annrole'),(879,199,'pmrole'),(880,199,'partrole'),(881,199,'townrole'),(882,199,'conrole'),(883,199,'pagerole'),(884,199,'extrole'),(885,199,'postrole'),(886,200,'donrole'),(887,200,'enmrole'),(888,200,'vmrole'),(889,200,'umrole'),(890,200,'annrole'),(891,200,'pmrole'),(892,200,'partrole'),(893,200,'townrole'),(894,200,'conrole'),(895,200,'pagerole'),(896,200,'extrole'),(897,200,'postrole'),(898,201,'donrole'),(899,201,'enmrole'),(900,201,'vmrole'),(901,201,'umrole'),(902,201,'annrole'),(903,201,'pmrole'),(904,201,'partrole'),(905,201,'townrole'),(906,201,'conrole'),(907,201,'pagerole'),(908,201,'extrole'),(909,201,'postrole'),(910,202,'donrole'),(911,202,'enmrole'),(912,202,'vmrole'),(913,202,'umrole'),(914,202,'annrole'),(915,202,'pmrole'),(916,202,'partrole'),(917,202,'townrole'),(918,202,'conrole'),(919,202,'pagerole'),(920,202,'extrole'),(921,202,'postrole'),(922,203,'partrole'),(923,204,'donrole'),(924,204,'enmrole'),(925,204,'vmrole'),(926,204,'umrole'),(927,204,'annrole'),(928,204,'pmrole'),(929,204,'partrole'),(930,204,'townrole'),(931,204,'conrole'),(932,204,'pagerole'),(933,204,'extrole'),(934,204,'postrole'),(935,205,'partrole'),(936,206,'donrole'),(937,206,'enmrole'),(938,206,'vmrole'),(939,206,'umrole'),(940,206,'annrole'),(941,206,'pmrole'),(942,206,'partrole'),(943,206,'townrole'),(944,206,'conrole'),(945,206,'pagerole'),(946,206,'extrole'),(947,206,'postrole'),(948,207,'donrole'),(949,207,'enmrole'),(950,207,'vmrole'),(951,207,'umrole'),(952,207,'annrole'),(953,207,'pmrole'),(954,207,'partrole'),(955,207,'townrole'),(956,207,'conrole'),(957,207,'pagerole'),(958,207,'extrole'),(959,207,'postrole'),(960,208,'donrole'),(961,208,'enmrole'),(962,208,'vmrole'),(963,208,'umrole'),(964,208,'annrole'),(965,208,'pmrole'),(966,208,'partrole'),(967,208,'townrole'),(968,208,'conrole'),(969,208,'pagerole'),(970,208,'extrole'),(971,208,'postrole'),(972,209,'donrole'),(973,209,'enmrole'),(974,209,'vmrole'),(975,209,'umrole'),(976,209,'annrole'),(977,209,'pmrole'),(978,209,'partrole'),(979,209,'townrole'),(980,209,'conrole'),(981,209,'pagerole'),(982,209,'extrole'),(983,209,'postrole'),(984,210,'donrole'),(985,210,'enmrole'),(986,210,'vmrole'),(987,210,'umrole'),(988,210,'annrole'),(989,210,'pmrole'),(990,210,'partrole'),(991,210,'townrole'),(992,210,'conrole'),(993,210,'pagerole'),(994,210,'extrole'),(995,210,'postrole'),(996,211,'donrole'),(997,211,'enmrole'),(998,211,'vmrole'),(999,211,'umrole'),(1000,211,'annrole'),(1001,211,'pmrole'),(1002,211,'partrole'),(1003,211,'townrole'),(1004,211,'conrole'),(1005,211,'pagerole'),(1006,211,'extrole'),(1007,211,'postrole'),(1008,212,'donrole'),(1009,212,'enmrole'),(1010,212,'vmrole'),(1011,212,'umrole'),(1012,212,'annrole'),(1013,212,'pmrole'),(1014,212,'partrole'),(1015,212,'townrole'),(1016,212,'conrole'),(1017,212,'pagerole'),(1018,212,'extrole'),(1019,212,'postrole'),(1020,213,'donrole'),(1021,213,'enmrole'),(1022,213,'vmrole'),(1023,213,'umrole'),(1024,213,'annrole'),(1025,213,'pmrole'),(1026,213,'partrole'),(1027,213,'townrole'),(1028,213,'conrole'),(1029,213,'pagerole'),(1030,213,'extrole'),(1031,213,'postrole'),(1032,214,'donrole'),(1033,214,'enmrole'),(1034,214,'vmrole'),(1035,214,'umrole'),(1036,214,'annrole'),(1037,214,'pmrole'),(1038,214,'partrole'),(1039,214,'townrole'),(1040,214,'conrole'),(1041,214,'pagerole'),(1042,214,'extrole'),(1043,214,'postrole'),(1044,215,'donrole'),(1045,215,'enmrole'),(1046,215,'vmrole'),(1047,215,'umrole'),(1048,215,'annrole'),(1049,215,'pmrole'),(1050,215,'partrole'),(1051,215,'townrole'),(1052,215,'conrole'),(1053,215,'pagerole'),(1054,215,'extrole'),(1055,215,'postrole'),(1056,216,'donrole'),(1057,216,'enmrole'),(1058,216,'vmrole'),(1059,216,'umrole'),(1060,216,'annrole'),(1061,216,'pmrole'),(1062,216,'partrole'),(1063,216,'townrole'),(1064,216,'conrole'),(1065,216,'pagerole'),(1066,216,'extrole'),(1067,216,'postrole'),(1068,217,'donrole'),(1069,217,'enmrole'),(1070,217,'vmrole'),(1071,217,'umrole'),(1072,217,'annrole'),(1073,217,'pmrole'),(1074,217,'partrole'),(1075,217,'townrole'),(1076,217,'conrole'),(1077,217,'pagerole'),(1078,217,'extrole'),(1079,217,'postrole'),(1080,218,'donrole'),(1081,218,'enmrole'),(1082,218,'vmrole'),(1083,218,'umrole'),(1084,218,'annrole'),(1085,218,'pmrole'),(1086,218,'partrole'),(1087,218,'townrole'),(1088,218,'conrole'),(1089,218,'pagerole'),(1090,218,'extrole'),(1091,218,'postrole'),(1092,219,'donrole'),(1093,219,'enmrole'),(1094,219,'vmrole'),(1095,219,'umrole'),(1096,219,'annrole'),(1097,219,'pmrole'),(1098,219,'partrole'),(1099,219,'townrole'),(1100,219,'conrole'),(1101,219,'pagerole'),(1102,219,'extrole'),(1103,219,'postrole'),(1104,220,'donrole'),(1105,220,'enmrole'),(1106,220,'vmrole'),(1107,220,'umrole'),(1108,220,'annrole'),(1109,220,'pmrole'),(1110,220,'partrole'),(1111,220,'townrole'),(1112,220,'conrole'),(1113,220,'pagerole'),(1114,220,'extrole'),(1115,220,'postrole'),(1128,222,'donrole'),(1129,222,'enmrole'),(1130,222,'vmrole'),(1131,222,'umrole'),(1132,222,'annrole'),(1133,222,'pmrole'),(1134,222,'partrole'),(1135,222,'townrole'),(1136,222,'conrole'),(1137,222,'pagerole'),(1138,222,'extrole'),(1139,222,'postrole'),(1140,223,'donrole'),(1141,223,'enmrole'),(1142,223,'vmrole'),(1143,223,'umrole'),(1144,223,'annrole'),(1145,223,'pmrole'),(1146,223,'partrole'),(1147,223,'townrole'),(1148,223,'conrole'),(1149,223,'pagerole'),(1150,223,'extrole'),(1151,223,'postrole'),(1152,224,'donrole'),(1153,224,'enmrole'),(1154,224,'vmrole'),(1155,224,'umrole'),(1156,224,'annrole'),(1157,224,'pmrole'),(1158,224,'partrole'),(1159,224,'townrole'),(1160,224,'conrole'),(1161,224,'pagerole'),(1162,224,'extrole'),(1163,224,'postrole'),(1164,225,'donrole'),(1165,225,'enmrole'),(1166,225,'vmrole'),(1167,225,'umrole'),(1168,225,'annrole'),(1169,225,'pmrole'),(1170,225,'partrole'),(1171,225,'townrole'),(1172,225,'conrole'),(1173,225,'pagerole'),(1174,225,'extrole'),(1175,225,'postrole'),(1176,226,'donrole'),(1177,226,'enmrole'),(1178,226,'vmrole'),(1179,226,'umrole'),(1180,226,'annrole'),(1181,226,'pmrole'),(1182,226,'partrole'),(1183,226,'townrole'),(1184,226,'conrole'),(1185,226,'pagerole'),(1186,226,'extrole'),(1187,226,'postrole'),(1188,227,'donrole'),(1189,227,'enmrole'),(1190,227,'vmrole'),(1191,227,'umrole'),(1192,227,'annrole'),(1193,227,'pmrole'),(1194,227,'partrole'),(1195,227,'townrole'),(1196,227,'conrole'),(1197,227,'pagerole'),(1198,227,'extrole'),(1199,227,'postrole'),(1200,228,'donrole'),(1201,228,'enmrole'),(1202,228,'vmrole'),(1203,228,'umrole'),(1204,228,'annrole'),(1205,228,'pmrole'),(1206,228,'partrole'),(1207,228,'townrole'),(1208,228,'conrole'),(1209,228,'pagerole'),(1210,228,'extrole'),(1211,228,'postrole'),(1212,229,'donrole'),(1213,229,'enmrole'),(1214,229,'vmrole'),(1215,229,'umrole'),(1216,229,'annrole'),(1217,229,'pmrole'),(1218,229,'partrole'),(1219,229,'townrole'),(1220,229,'conrole'),(1221,229,'pagerole'),(1222,229,'extrole'),(1223,229,'postrole'),(1224,230,'donrole'),(1225,230,'enmrole'),(1226,230,'vmrole'),(1227,230,'umrole'),(1228,230,'annrole'),(1229,230,'pmrole'),(1230,230,'partrole'),(1231,230,'townrole'),(1232,230,'conrole'),(1233,230,'pagerole'),(1234,230,'extrole'),(1235,230,'postrole'),(1236,231,'donrole'),(1237,231,'enmrole'),(1238,231,'vmrole'),(1239,231,'umrole'),(1240,231,'annrole'),(1241,231,'pmrole'),(1242,231,'partrole'),(1243,231,'townrole'),(1244,231,'conrole'),(1245,231,'pagerole'),(1246,231,'extrole'),(1247,231,'postrole'),(1272,233,'donrole'),(1273,233,'enmrole'),(1274,233,'vmrole'),(1275,233,'umrole'),(1276,233,'annrole'),(1277,233,'pmrole'),(1278,233,'partrole'),(1279,233,'townrole'),(1280,233,'conrole'),(1281,233,'pagerole'),(1282,233,'extrole'),(1283,233,'postrole'),(1284,221,'donrole'),(1285,221,'enmrole'),(1286,221,'vmrole'),(1287,221,'umrole'),(1288,221,'annrole'),(1289,221,'pmrole'),(1290,221,'partrole'),(1291,221,'townrole'),(1292,221,'conrole'),(1293,221,'pagerole'),(1294,221,'extrole'),(1295,221,'postrole'),(1308,234,'donrole'),(1309,234,'enmrole'),(1310,234,'vmrole'),(1311,234,'umrole'),(1312,234,'annrole'),(1313,234,'pmrole'),(1314,234,'partrole'),(1315,234,'townrole'),(1316,234,'conrole'),(1317,234,'pagerole'),(1318,234,'extrole'),(1319,234,'postrole'),(1320,235,'donrole'),(1321,235,'enmrole'),(1322,235,'vmrole'),(1323,235,'umrole'),(1324,235,'annrole'),(1325,235,'pmrole'),(1326,235,'partrole'),(1327,235,'townrole'),(1328,235,'conrole'),(1329,235,'pagerole'),(1330,235,'extrole'),(1331,235,'postrole'),(1332,236,'partrole'),(1333,237,'partrole'),(1346,239,'donrole'),(1347,239,'enmrole'),(1348,239,'vmrole'),(1349,239,'umrole'),(1350,239,'annrole'),(1351,239,'pmrole'),(1352,239,'partrole'),(1353,239,'townrole'),(1354,239,'conrole'),(1355,239,'pagerole'),(1356,239,'extrole'),(1357,239,'postrole'),(1370,240,'donrole'),(1371,240,'enmrole'),(1372,240,'vmrole'),(1373,240,'umrole'),(1374,240,'annrole'),(1375,240,'pmrole'),(1376,240,'partrole'),(1377,240,'townrole'),(1378,240,'conrole'),(1379,240,'pagerole'),(1380,240,'extrole'),(1381,240,'postrole'),(1382,241,'donrole'),(1383,241,'enmrole'),(1384,241,'vmrole'),(1385,241,'umrole'),(1386,241,'annrole'),(1387,241,'pmrole'),(1388,241,'partrole'),(1389,241,'townrole'),(1390,241,'conrole'),(1391,241,'pagerole'),(1392,241,'extrole'),(1393,241,'postrole'),(1406,242,'donrole'),(1407,242,'enmrole'),(1408,242,'vmrole'),(1409,242,'umrole'),(1410,242,'annrole'),(1411,242,'pmrole'),(1412,242,'partrole'),(1413,242,'townrole'),(1414,242,'conrole'),(1415,242,'pagerole'),(1416,242,'extrole'),(1417,242,'postrole'),(1430,232,'donrole'),(1431,232,'enmrole'),(1432,232,'vmrole'),(1433,232,'umrole'),(1434,232,'annrole'),(1435,232,'pmrole'),(1436,232,'partrole'),(1437,232,'townrole'),(1438,232,'conrole'),(1439,232,'pagerole'),(1440,232,'extrole'),(1441,232,'postrole'),(1442,238,'donrole'),(1443,238,'enmrole'),(1444,238,'vmrole'),(1445,238,'umrole'),(1446,238,'annrole'),(1447,238,'pmrole'),(1448,238,'partrole'),(1449,238,'townrole'),(1450,238,'conrole'),(1451,238,'pagerole'),(1452,238,'extrole'),(1453,238,'postrole'),(1454,244,'donrole'),(1455,244,'enmrole'),(1456,244,'vmrole'),(1457,244,'umrole'),(1458,244,'annrole'),(1459,244,'pmrole'),(1460,244,'partrole'),(1461,244,'townrole'),(1462,244,'conrole'),(1463,244,'pagerole'),(1464,244,'extrole'),(1465,244,'postrole'),(1466,245,'donrole'),(1467,245,'enmrole'),(1468,245,'vmrole'),(1469,245,'umrole'),(1470,245,'annrole'),(1471,245,'pmrole'),(1472,245,'partrole'),(1473,245,'townrole'),(1474,245,'conrole'),(1475,245,'pagerole'),(1476,245,'extrole'),(1477,245,'postrole'),(1478,246,'vmrole'),(1479,247,'donrole'),(1480,247,'enmrole'),(1481,247,'vmrole'),(1482,247,'umrole'),(1483,247,'annrole'),(1484,247,'pmrole'),(1485,247,'partrole'),(1486,247,'townrole'),(1487,247,'conrole'),(1488,247,'pagerole'),(1489,247,'extrole'),(1490,247,'postrole'),(1491,248,'partrole'),(1492,249,'partrole'),(1493,250,'partrole'),(1494,251,'partrole'),(1495,252,'partrole'),(1496,253,'partrole'),(1497,254,'partrole'),(1498,255,'partrole'),(1499,256,'partrole'),(1500,257,'partrole'),(1501,258,'partrole'),(1502,259,'partrole'),(1503,260,'partrole'),(1504,261,'partrole'),(1505,262,'partrole'),(1506,263,'partrole'),(1507,264,'partrole'),(1508,265,'partrole'),(1509,266,'donrole'),(1510,266,'enmrole'),(1511,266,'vmrole'),(1512,266,'umrole'),(1513,266,'annrole'),(1514,266,'pmrole'),(1515,266,'partrole'),(1516,266,'townrole'),(1517,266,'conrole'),(1518,266,'pagerole'),(1519,266,'extrole'),(1520,266,'postrole'),(1521,267,'donrole'),(1522,267,'enmrole'),(1523,267,'annrole'),(1524,267,'partrole'),(1525,267,'conrole'),(1526,267,'pagerole'),(1527,267,'extrole'),(1528,268,'extrole'),(1529,269,'partrole'),(1530,269,'pagerole'),(1531,269,'extrole'),(1532,270,'partrole'),(1533,271,'partrole'),(1534,272,'partrole'),(1535,273,'partrole'),(1536,274,'partrole'),(1537,275,'partrole'),(1538,276,'pagerole'),(1539,277,'donrole'),(1540,277,'pmrole'),(1541,277,'partrole'),(1542,277,'pagerole'),(1543,277,'extrole'),(1544,278,'partrole'),(1545,279,'partrole'),(1546,280,'partrole'),(1547,281,'partrole'),(1548,282,'annrole'),(1561,284,'donrole'),(1562,284,'enmrole'),(1563,284,'vmrole'),(1564,284,'umrole'),(1565,284,'annrole'),(1566,284,'pmrole'),(1567,284,'partrole'),(1568,284,'townrole'),(1569,284,'conrole'),(1570,284,'pagerole'),(1571,284,'extrole'),(1572,284,'postrole'),(1577,285,'townrole'),(1578,285,'postrole'),(1579,286,'annrole'),(1580,286,'conrole'),(1581,286,'postrole'),(1582,287,'donrole'),(1583,287,'enmrole'),(1584,287,'vmrole'),(1585,287,'umrole'),(1586,287,'annrole'),(1587,287,'pmrole'),(1588,287,'partrole'),(1589,287,'townrole'),(1590,287,'conrole'),(1591,287,'pagerole'),(1592,287,'extrole'),(1593,287,'postrole'),(1594,288,'annrole'),(1604,289,'donrole'),(1605,289,'enmrole'),(1606,289,'vmrole'),(1607,289,'umrole'),(1608,289,'annrole'),(1609,289,'pmrole'),(1610,289,'partrole'),(1611,289,'townrole'),(1612,289,'conrole'),(1613,289,'pagerole'),(1614,289,'extrole'),(1615,289,'postrole'),(1618,291,'annrole'),(1619,292,'annrole'),(1620,29,'umrole'),(1621,29,'annrole'),(1622,29,'pmrole'),(1623,29,'partrole'),(1624,29,'townrole'),(1625,29,'conrole'),(1626,29,'pagerole'),(1627,29,'extrole'),(1628,29,'postrole'),(1629,293,'partrole'),(1630,294,'partrole'),(1631,295,'partrole'),(1632,296,'partrole'),(1633,297,'partrole'),(1634,298,'donrole'),(1635,298,'enmrole'),(1636,298,'vmrole'),(1637,298,'umrole'),(1638,298,'annrole'),(1639,298,'pmrole'),(1640,298,'partrole'),(1641,298,'townrole'),(1642,298,'conrole'),(1643,298,'pagerole'),(1644,298,'extrole'),(1645,298,'postrole'),(1647,300,'partrole'),(1651,299,'annrole'),(1652,299,'partrole'),(1653,299,'pagerole'),(1689,283,'donrole'),(1690,283,'enmrole'),(1691,283,'vmrole'),(1692,283,'umrole'),(1693,283,'annrole'),(1694,283,'pmrole'),(1695,283,'partrole'),(1696,283,'townrole'),(1697,283,'conrole'),(1698,283,'pagerole'),(1699,283,'extrole'),(1700,283,'postrole'),(1701,283,'rescuerole'),(1702,301,'partrole'),(1722,303,'partrole'),(1736,302,'donrole'),(1737,302,'enmrole'),(1738,302,'vmrole'),(1739,302,'umrole'),(1740,302,'annrole'),(1741,302,'pmrole'),(1742,302,'partrole'),(1743,302,'townrole'),(1744,302,'conrole'),(1745,302,'pagerole'),(1746,302,'extrole'),(1747,302,'postrole'),(1748,302,'rescuerole'),(1749,304,'donrole'),(1750,304,'enmrole'),(1751,304,'vmrole'),(1752,304,'umrole'),(1753,304,'annrole'),(1754,304,'pmrole'),(1755,304,'partrole'),(1756,304,'townrole'),(1757,304,'conrole'),(1758,304,'pagerole'),(1759,304,'extrole'),(1760,304,'postrole'),(1761,304,'rescuerole'),(1762,305,'donrole'),(1763,305,'enmrole'),(1764,305,'vmrole'),(1765,305,'umrole'),(1766,305,'annrole'),(1767,305,'pmrole'),(1768,305,'partrole'),(1769,305,'townrole'),(1770,305,'conrole'),(1771,305,'pagerole'),(1772,305,'extrole'),(1773,305,'postrole'),(1774,305,'rescuerole'),(1775,306,'donrole'),(1776,306,'enmrole'),(1777,306,'vmrole'),(1778,306,'umrole'),(1779,306,'annrole'),(1780,306,'pmrole'),(1781,306,'partrole'),(1782,306,'townrole'),(1783,306,'conrole'),(1784,306,'pagerole'),(1785,306,'extrole'),(1786,306,'postrole'),(1787,306,'rescuerole'),(1788,307,'donrole'),(1789,307,'enmrole'),(1790,307,'annrole'),(1791,307,'pmrole'),(1792,307,'partrole'),(1793,307,'conrole'),(1794,307,'pagerole'),(1795,307,'extrole'),(1796,307,'postrole'),(1797,307,'rescuerole'),(1798,308,'donrole'),(1799,308,'enmrole'),(1800,308,'vmrole'),(1801,308,'umrole'),(1802,308,'annrole'),(1803,308,'pmrole'),(1804,308,'partrole'),(1805,308,'townrole'),(1806,308,'conrole'),(1807,308,'pagerole'),(1808,308,'extrole'),(1809,308,'postrole'),(1810,308,'rescuerole'),(1811,309,'donrole'),(1812,309,'enmrole'),(1813,309,'vmrole'),(1814,309,'annrole'),(1815,309,'partrole'),(1816,309,'conrole'),(1817,309,'extrole'),(1822,290,'donrole'),(1823,290,'enmrole'),(1824,290,'vmrole'),(1825,290,'umrole'),(1826,290,'annrole'),(1827,290,'pmrole'),(1828,290,'partrole'),(1829,290,'townrole'),(1830,290,'conrole'),(1831,290,'pagerole'),(1832,290,'extrole'),(1833,290,'postrole'),(1834,290,'rescuerole');
/*!40000 ALTER TABLE `tbluserroles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblusers`
--

DROP TABLE IF EXISTS `tblusers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblusers` (
  `userID` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(255) NOT NULL,
  `userEmail` varchar(255) NOT NULL,
  `userPassword` varchar(255) NOT NULL,
  `userLastname` varchar(255) NOT NULL,
  `userFirstname` varchar(255) NOT NULL,
  `userMiddlename` varchar(255) DEFAULT NULL,
  `userAddress` text,
  `userCompany` varchar(255) DEFAULT NULL,
  `userPosition` varchar(255) DEFAULT NULL,
  `userContact` varchar(255) NOT NULL,
  `userStatus` varchar(255) NOT NULL,
  `userLevel` int(11) NOT NULL,
  `userForgotToken` varchar(255) DEFAULT NULL,
  `dateCreated` int(11) NOT NULL,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB AUTO_INCREMENT=310 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblusers`
--

LOCK TABLES `tblusers` WRITE;
/*!40000 ALTER TABLE `tblusers` DISABLE KEYS */;
INSERT INTO `tblusers` VALUES (1,'abkadmin','janinelabadia023@gmail.com','7c222fb2927d828af22f592134e8932480637c0d','Dela Cruz','John','Xy','Somewhere over the Rainbow','GeeksNest','Administrator','(545) 464-5657','active',1,NULL,1457428090),(29,'abkf2015','abkf2015@gmail.com','7c222fb2927d828af22f592134e8932480637c0d','foundation','angbayanko','fgdfgdfg','tajkhkjhkjhkjh','Geeksnest.com','46463','13212321321','deactivate',0,NULL,1524119164),(43,'janineadmin','jha@mailinator.com','30e1fe53111f7e583c382596a32885fd27283970','Labadia','Eninaj','Ramos','San Jose','Geeksnest.com','Co-admin','(978) 574-6535','deactivate',0,NULL,1476413631),(82,'heavenleih1','heaven.mojica1@mailinator.com','606563a6fa524c4122b2fd5538cb57ff1aff1fc0','mojica','heavenleih','','','','','(111) 111-1111','active',0,NULL,1455589411),(88,'Heaven','sdsa1sadd222@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','dasd','asdf','12313132131','','','','(122) 222-2222','deactivate',0,NULL,1508477707),(89,'heaven12','sdsa1ssadd222@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','hd','super','dh','madasdasdaduadfig','asjdsasdfsd','','(122) 222-2222','deactivate',0,NULL,1476413668),(90,'heaven123','sdsad1ssadd222@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','dasd','sad','','','','','(122) 222-2222','active',0,NULL,1511159624),(91,'heaven1234','sdsadd1ssadd222@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','dasd','sa','','','','','(122) 222-2222','active',0,NULL,1455590884),(92,'heaven12345','sdsaddd1ssadd222@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','dasd','sa','','','','','(122) 222-2222','active',0,NULL,1455590890),(95,'heaven12345678','876sdsaddd1ssadd222@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','dasd','sa','','','','','(122) 222-2222','active',0,NULL,1508478035),(97,'heaven1234567890','09876sdsaddd1ssadd222@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','dasd','sa','','','','','(122) 222-2222','active',0,NULL,1455590914),(98,'heaven12345678901','109876sdsaddd1ssadd222@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','dasd','sa','','','','','(122) 222-2222','active',0,NULL,1455590920),(99,'1heaven12345678901','1109876sdsaddd1ssadd222@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','dasd','sa','','','','','(122) 222-2222','active',0,NULL,1455590929),(100,'1he1aven12345678901','11091876sdsaddd1ssadd222@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','dasd','sa','','','','','(122) 222-2222','active',0,NULL,1455590941),(104,'eninajfdgdf','ewwet@mailinator.com','30e1fe53111f7e583c382596a32885fd27283970','twetw','twew','twetw','','','','(312) 421-4255','active',0,NULL,1455672350),(114,'janinedffsfsd','fsd@mailinator.com','30e1fe53111f7e583c382596a32885fd27283970','gfdgd','gfdgd','gdfd','gfd','gfdgd','24214234','(231) 421-4234','active',0,NULL,1455761533),(115,'partneruser','pu@mailinator.com','30e1fe53111f7e583c382596a32885fd27283970','gss','gdgd','fsds','','','','(214) 124-1515','active',0,NULL,1455761657),(118,'jabkdmin','abk@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','gdd','fdgdg','','','','','(141) 343-2523','active',0,NULL,1455765328),(119,'puroSPACES','hghfh@mailinator.com','4fb7423b553dc5b243042a4dd3f9d1d5737dff80','              ','             ','              ','','','','(123) 456-7890','active',0,NULL,1455872275),(120,'fdgdfhdfhd','v@mailinator.com','4fb7423b553dc5b243042a4dd3f9d1d5737dff80','              ','                ','            ','           ','                 ','                  ','(132) 456-7878','active',0,NULL,1456105428),(121,'sadsaasada','aaa@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','wertyu','qwerty','ertyui','rtyui','rtyui','qwertyu','(123) 456-7890','active',0,NULL,1456132717),(123,'testSpace','ts@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','a           b','a            b','','','','','(234) 567-8909','active',0,NULL,1456278814),(124,'ayosnato','isa@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','asfffdfad','asddasf','','','','','(123) 456-7890','active',0,NULL,1456305163),(126,'adminabk','adminabk@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','Ko','Ang Bayan','','','','','(789) 787-9789','active',0,NULL,1475741988),(127,'angbayanko','angbayanko@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','Ko','Ang Bayan','','','','','(789) 787-9789','active',0,NULL,1475742050),(128,'fffe43434','gmail@example.com','4de69ee6b12b7fc91070873b71ba6e2929b90619','fefe','fefe','fefe','fefef','efefef','1231231232131','(123) 123-1321','active',0,NULL,1475745754),(130,'test1jason','test1@yahoo.com','a642a77abd7d4f51bf9226ceaf891fcbb5b299b8','xd','js','j','j','j','j','(111) 111-1111','active',0,NULL,1477037460),(132,'ilchiadmin','ilchi@mailinator.com','4e0753c953f68b1962d7a90f077fba02b8841b80','sfaf','ilch','','','','','(124) 124-1414','active',0,NULL,1477039945),(133,'ilchiadmin2','jacinto.escano@geekesnest.com','8db6b3cffb394b456ae26a2595a471569545b5ec','sfaf','ilch','','','','','(124) 124-1414','active',0,NULL,1477040207),(134,'kokoko','masscomers@yahoo.com','00baaca2795af5775618af0c596fc4ae8306704d','kokoko','kokoko','','','','','(124) 124-1251','active',0,NULL,1477040415),(136,'kakakaka','kukuku@mailinator.com','fdae81713cbf3e0d24391b6a9cc372b09fb90116','kukuku','kukuku','','','','','(314) 124-1241','active',0,NULL,1477041902),(138,'testung123','asddxsxsx@yahoo.com','88ea39439e74fa27c09a4fc0bc8ebe6d00978392','samplelastname','samplename','','','','','(123) 123-1231','active',0,NULL,1477045746),(139,'sampeluser','asdasd@yahoo.com','cbfdac6008f9cab4083784cbd1874f76618d2a97','ksjdfcjksfc','kasdaiusdg','','','','','(321) 321-4654','active',0,NULL,1477270894),(140,'sampeluser123','asdasd123123@yahoo.com','cbfdac6008f9cab4083784cbd1874f76618d2a97','ksjdfcjksfc','kasdaiusdg','','','','','(321) 321-4654','active',0,NULL,1477271291),(141,'sample','jason123@yahoo.com','88ea39439e74fa27c09a4fc0bc8ebe6d00978392','samplesss','samplesss','','','','','(324) 324-2342','active',0,NULL,1477271753),(142,'sampleuser','superemailjason@yahoo.com','cbfdac6008f9cab4083784cbd1874f76618d2a97','nas','jason','','','','','(213) 216-5464','active',0,NULL,1477273618),(143,'partnerssample','superemailasd123@yahoo.com','88ea39439e74fa27c09a4fc0bc8ebe6d00978392','lnamesample','fnamesample','','','','','(213) 123-4245','deactivate',2,NULL,1477273663),(144,'jasooonn','nastertw@yahoo.com','88ea39439e74fa27c09a4fc0bc8ebe6d00978392','sample12321','sample123','','','','','(123) 213-1231','deactivate',2,NULL,1477277580),(145,'superpartner','sad23@yahoo.com','f0f8e902ca7a41c634c5c8247d4b94f2c9b351fb','and freinds','barney','','','','','(123) 123-1232','deactivate',2,NULL,1477277830),(146,'/?{}_->()','acer@mailinator.com','e1c0ef01b786990d346004f4548cd32d6ba3710a','santos','mark','san','3156 E Laurelhurst Dr Ne Seattle, WA 98105','dell','manager','(123) 456-7789','active',0,NULL,1511748804),(147,'??>','carl@mailinator.com','174c407b85c56d21ff556d49190d2492e246dd46','esperon','carlman','zac','927 N Old Manor Rd Wichita, KS 67208','apple','secretary','(796) 432-1468','active',0,NULL,1477624298),(149,'bingbong','ronald@gmail.com','7d8f4b4b4613dc7e15333e6449692ad4af502d1d','soria','ronald','calantoc','santa maria city','geeksnest','tester','(099) 550-5284','deactivate',0,NULL,1478225264),(150,'masterjb','jb@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','Doria III','JB','Ortiz','Urdaneta City','Geeksnest','Software QA Tester','(012) 345-6789','active',0,NULL,1478657509),(151,'master_jb','masterjb@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','Doria III','JB','Ortiz','Urdaneta City','Geeksnest','Software QA Tester','(012) 345-6789','active',0,NULL,1478657653),(152,'jeive12345678','jeive@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','Lafeguera','Jeive','','','','','(223) 232-2322','active',0,NULL,1508477104),(153,'rrgrg','rgrgr','f865b53623b121fd34ee5426c792e5c33af8c227','Sssssssssssss','sssss','','','','','(669) 999-9999','active',0,NULL,1508479157),(154,'jeive12345','jeive@mailinator.coml','7c222fb2927d828af22f592134e8932480637c0d','Lafeguera','Jeive','','','','','(232) 322-3222','deactivate',2,NULL,1508481125),(155,'sample ojt','sample@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','ojt','samplw','a','','','','(445) 454-5454','deactivate',2,NULL,1508824914),(156,'cd','fdf','7c222fb2927d828af22f592134e8932480637c0d','manen','sample','','','','','(121) 212-1212','active',2,NULL,1508825341),(157,'sampleuser12','samplesample@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','sample','sample','','','','','(091) 978-9263','active',0,NULL,1510712222),(158,'sampleojt','jla@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','asf','dasd','sd','das','','','(432) 432-4234','active',0,NULL,1510883932),(159,'sds','sdsd','7c222fb2927d828af22f592134e8932480637c0d','asasd','asds','a','sdsad','','','(333) 333-3333','deactivate',2,NULL,1510911573),(160,'dfdf','ererfdf','7c222fb2927d828af22f592134e8932480637c0d','uiop','qwerty','asdf','afdf','','','(242) 242-5545','active',0,NULL,1510913057),(161,'ewe','wew','7c222fb2927d828af22f592134e8932480637c0d','werwer','rer','werwe','rewrwe','rwer','','(233) 434-3434','active',0,NULL,1511147296),(162,'dasdda','jeivela@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','dsad','asdas','','','','','(343) 243-2434','active',0,NULL,1511147342),(163,'asdfgh','jlaf@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','hhyj','dsffg','','','','','(444) 342-3334','active',0,NULL,1511154566),(165,'jaja','jaja@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','jiji','jaja','jeje','','','','(000) 000-0000','active',0,NULL,1511165988),(166,'angelo','angelo@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','gelo','gelo','','','','','(333) 336-3664','deactivate',2,NULL,1511167195),(167,'jajaja','jajaja@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','hihi','jajaja','hehe','','','','12323222222222223','active',0,NULL,1511234354),(168,'grthrg','effetetr','7c222fb2927d828af22f592134e8932480637c0d','fdfdf','fdfdfd','','','','','(344) 355-3553','active',0,NULL,1511234445),(169,'admin','gelmaraquino444@yahoo.com','f7c3bc1d808e04732adf679965ccc34ca7ae3441','FINISHED','FINISHED','','FINISHED','FINISHED','FINISHED','(888) 888-8888','active',0,NULL,1511240584),(170,'rtrtt','rtrtrt','7c222fb2927d828af22f592134e8932480637c0d','dksdfndskfff','may','','','','','(132) 323-2323','active',0,NULL,1511246044),(171,'jeremaypedigloria','jejeje@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','_p','jmay','','','','','(232) 323-2323','active',0,NULL,1511251657),(172,'ambagel','ambagel@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','fdfsd','\'ds','','','','','(323) 232-4344','deactivate',2,NULL,1511317685),(173,'gelmar_04','gelmaraquino4444@yahoo.com','7c222fb2927d828af22f592134e8932480637c0d','aquino','tyson','','san manuel pangasinan','SG','','(999) 999-9999','active',0,NULL,1511330164),(174,'!@#$%^&*()','gelmaraquino4555@yahoo.com','f7c3bc1d808e04732adf679965ccc34ca7ae3441','aquino','ramleg','','','','','(999) 999-9999','active',0,NULL,1511330698),(175,'jeremay_p','jma','7c222fb2927d828af22f592134e8932480637c0d','pedigloria','jeremay','','','','','(575) 676-7676','active',0,NULL,1511403485),(176,'$$_45','jwjrk$#4','7c222fb2927d828af22f592134e8932480637c0d','we  we','w we','','','','','(123) 232-2322','active',0,NULL,1511403860),(177,'fwef@$$_23','qwertyuiopasdf#@$%gh_22','7c222fb2927d828af22f592134e8932480637c0d','fjsdkfjsdfsd','sdfsfsfsdjk','','','','','(224) 242-4233','active',0,NULL,1511404236),(178,'awanngata','awanngata@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','ADDA','awan','','','','','(232) 232-3232','active',0,NULL,1511418206),(179,'pwedenadaw','pwedenadaw@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','nadaw','pwede','','','','','(121) 223-2121','active',0,NULL,1511418535),(180,'kid235__*()&','lhoreng@mailinator.com','f7c3bc1d808e04732adf679965ccc34ca7ae3441','Petmalu','lodi','Werpa','Social Media','Facevbook','President','(324) 165-4968','active',0,NULL,1511419013),(181,'afadfadfew4@@$#$23','hasjda@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','asdaf\"\"\"\"\"\"ASFaf21','\"\"\"\"\"\"ASFaf21','','','','','(231) 141-4424','deactivate',2,NULL,1511424333),(182,'partner1','partner1@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','apelyedo','partner','','','','','(234) 424-2343','active',2,NULL,1511429595),(183,'partner2','partner2@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','apelyedo','partner 2','','','','','(212) 434-3423','deactivate',2,NULL,1511429703),(184,'jeremay_19','jeremaype@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','pedigloria','jmay','','','','','(565) 656-5656','active',0,NULL,1511493309),(185,'haha','haha@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','asdasd','dasd','','','','','(232) 443-4343','active',0,NULL,1511501643),(186,'lkjhg','lkjhg@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','sadfsdfsdfsd','asasdfasdf','','','','','(121) 232-4234','deactivate',2,NULL,1511512962),(187,'ytyuy','lkjhgzfdf@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','sadfsdfsdfsd','asasdfasdf','','','','','(121) 232-4234','deactivate',2,NULL,1511513003),(188,'dasd_d','dasd_d@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','ass','daf','','','','','(111) 111-1111','deactivate',2,NULL,1511513643),(189,'\"@$%&^','\"@$%&^@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','dsdsd','dasds','','','','','(232) 232-3232','deactivate',2,NULL,1511514753),(190,'rrgrg$%^&*8_','abkadmin@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','admin','admin','','','','','(343) 433-3434','active',0,NULL,1511748417),(191,'!@$@#$','hehe@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','asdas','adasd','','','','','(333) 333-3333','deactivate',2,NULL,1511749390),(192,'amasda','amasda','bd239609f8b578c774401d88f14fcb7658b44ba8','asdasdasd','adasdfdas','','','','','(454) 545-4545','deactivate',2,NULL,1511749546),(193,'@#@$#%$$%^','@#@$#%$$%^@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','adadsf','adadsf','','','','','(234) 547-8999','active',0,NULL,1511765224),(194,'dtfhmkdth','rth','7c222fb2927d828af22f592134e8932480637c0d','sfgndfsh','sdfgn','','','','xdfgn','(235) 647-0867','active',0,NULL,1512525389),(195,'aefhsb','asdfsdgz@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','xcv nxf','cxz nx','x nx','xbn d','xfb n','2346236','(235) 673-2356','active',0,NULL,1512525495),(196,'asdfghjkl','asdfghjkl@mailinator','7c222fb2927d828af22f592134e8932480637c0d','uiop','qwerty','','','','','(123) 323-3323','active',0,'7cf48f90b806681f06c04bf02724679eff824d59',1512529324),(197,'zxzcvx','asdfghjklsdfs@mailinator.com','bd239609f8b578c774401d88f14fcb7658b44ba8','asdfgbn','asdfgh','','','','','(243) 412-3456','active',2,NULL,1512536342),(198,'vcvcvc','etetteetet@maiinator.com','f58cf5e7e10f195e21b553096d092c763ed18b0e','gsdgdsg','dbfbf','','','','','(354) 275-6765','deactivate',2,NULL,1512536615),(199,'jibi','jibi@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','Lafeguera','Jibi','C','','','','(934) 347-3747','active',0,NULL,1512537683),(200,'rdgdgklkgjfdrs','asdfghjkl@mailinator.com','5fa339bbbb1eeaced3b52e54f44576aaf0d77d96','reffrwqr','wqdsfds','','','','','(123) 456-7890','active',0,NULL,1512538968),(201,'Lorraine','lorriane@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','Manzano','Lorraine','Alnas','Cabaruan Urdaneta City','MEDISOURCE','President','(091) 047-6424','active',0,NULL,1512542480),(203,'dsafa','fasfs@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','afasfasf','a\'sfsf','','','','','(234) 567-8909','deactivate',2,NULL,1512544498),(204,'mikenathaniel','jkmontero25@gmail.com','77a9d57f01e8f86e69f0df748b144bfce3318b03','tangalin','mike','galera','macayo, alcala, pangasinan','','','(975) 202-8937','active',0,NULL,1512612911),(205,'TNC','tani@gmail.com','77a9d57f01e8f86e69f0df748b144bfce3318b03','tani','mang','','','','','(985) 762-3758','active',2,NULL,1512624473),(206,'Mageline','hehehek@yahoo.com','7c222fb2927d828af22f592134e8932480637c0d','Valdez','Mageline','','','','','(091) 588-9807','active',0,NULL,1516166401),(207,'bevsss','bev@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','Limmmmmmm','Beverly Je-ann','','','','','(091) 596-1460','active',0,NULL,1516166897),(208,'bevu','bevu@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','sim','bevuu bevuu','','','','','(010) 203-0405','active',0,NULL,1516166993),(209,'jijiji','jijiji@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','Hadid','jiji jean','','','','','(090) 909-0909','active',0,NULL,1516167114),(210,'geazy','geazy@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','HADID','G-EAZY','','','','','(090) 909-0909','active',0,NULL,1516167241),(211,'bowiners','bowin@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','Matias','Bowin fe','','','','','(030) 303-0303','active',0,NULL,1516167327),(212,'Princess','princess@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','Santos','Prinmcess Dy','','','','','(020) 202-0202','active',0,NULL,1516167418),(213,'Xierra','xierra@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','Leal XD','Xierra','','','','','(010) 101-0101','active',0,NULL,1516167519),(214,'Bianca','bianca@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','koooooooo','Bianca','','','','','(060) 606-0606','active',0,NULL,1516167604),(215,'Colleen','colleen@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','Casicas','Lilacuh','','','','','(050) 505-0505','active',0,NULL,1516167680),(216,'Danieee','danieee@mailinitor.com','7c222fb2927d828af22f592134e8932480637c0d','Talavera','Danie mae','','','','','(010) 101-0101','active',0,NULL,1516167809),(217,'chiners','chiners@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','Tabiayih','Chiners','','','','','(080) 880-8008','active',0,NULL,1516167970),(218,'Joseph','joseph@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','Bruno','Joseph','','','','','(070) 707-0707','active',0,NULL,1516168043),(219,'Kristian','kristian@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','Selga','Kristian','','','','','(030) 303-0303','active',0,NULL,1516168110),(220,'Dwaynersss','dwayne@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','cucucucu','Dwayne','','','','','(020) 220-2020','active',0,NULL,1516168239),(221,'gepoyers','gepoy@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','huhuhuhuh','Gepoyers','','','','','(090) 900-9999','active',0,NULL,1521449391),(222,'kellyers','kellyers@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','Domingo','kelly mae','','','','','(020) 202-0202','active',0,NULL,1516168527),(223,'Ireners','irene@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','hewhew','Irene','','','','','(080) 808-0808','active',0,NULL,1516168614),(224,'markyyy','marky@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','MOYYY','Markyy','','','','','(808) 080-8080','active',0,NULL,1516168679),(225,'gelayyy','gelay@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','FIRME','JENNY MAE','','','','','(123) 456-7888','active',0,NULL,1516168761),(226,'Helenyy','Helen@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','dydydy','Helen CU','','','','','(020) 202-0202','active',0,NULL,1516168877),(227,'kikiko','kiki@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','december','kikoki','','','','','(753) 951-1235','active',0,NULL,1516168986),(228,'Lylycuh','lylyly@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','Bautista','Jeannn','','','','','(789) 456-1230','active',0,NULL,1516169134),(230,'josefa','josefa@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','kdjdisdd9cfeuwfiw','kjnvjdsbviuaujc','','','','','(321) 654-9870','active',0,NULL,1516169394),(231,'Remyyy','remy@mailinator.com','f7c3bc1d808e04732adf679965ccc34ca7ae3441','jimenez','remy anne','','','','','(789) 456-1230','active',0,NULL,1516169482),(232,'kzelann','zelly@mailinator.com','b05ca374f9a639fea3cb5c5eff54633585c02303','Eugenio','Kezel Annn','Ballesteros','hhhhh','Medisource','dfdfdf','(232) 323-2121','active',0,NULL,1521709284),(234,'tiffany','tiff@mailinator.com','b05ca374f9a639fea3cb5c5eff54633585c02303','Itchon','Tiff','Montero','fhdjfdjf','Medisource','fdfdfdf','(232) 323-2323','active',0,NULL,1521449848),(235,'annieann','zel@mailinator.com','b05ca374f9a639fea3cb5c5eff54633585c02303','Eugenio','Annie','Ballesteros','dfdfd','Medisource','ddssd','(121) 223-2323','deactivate',0,NULL,1521516723),(236,'vncvn','cvncvncv','7c222fb2927d828af22f592134e8932480637c0d','cvncvn','vncvn','cvncvn','cvncvn','cvncvn','hdghd','(546) 512-1321','deactivate',2,NULL,1521517292),(237,'gjfg g','fgfgfg','7c222fb2927d828af22f592134e8932480637c0d','fgjfgj','cvncvncv','dgfgj','vdcv','xcbc','ghfcvbnbv','(574) 747-6556','active',2,NULL,1521517512),(238,'kzlann','annie@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','Eugenio','Kezel Annn','Ballesteros','vhghg','Medisource','dfdfdf','(232) 323-2323','active',0,NULL,1521770558),(239,'aaaaaaaaaaaaaa','aaaaaaaaaaaaaaaaaaa1@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','aa','aa','','aa','aaa','','(212) 121-3131','active',0,NULL,1521527377),(240,'%%$%^&&***((&#$%^&*(*!@#$%^&*(((()','ann@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','Eugenio','Ann','Ballesteros','ghgfghjhgdffghk','Medisource','ffhghkhgfghgf','(245) 765-4232','active',0,NULL,1521533360),(241,'$%$%!@#$%^&*()','anna@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','Eugenio','Anna','Ballesteros','asddfghjkkllkjhgfdfg','Medisource','ghjjjhfghjk','(132) 345-6789','active',0,NULL,1521533906),(243,'gionello','gio@mailinator.com','d66e90461d247712cd105fa8042a088441823d23','Fonbuena','Gionello','','','','','(123) 432-4323','active',0,NULL,1521535417),(244,'$#$$#^%^','jin@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','Eugenio','Kezel Anne','','','','','(121) 121-2121','active',0,NULL,1521770982),(245,'dsdsjdsdkjs','zelann@mailinator.com','b05ca374f9a639fea3cb5c5eff54633585c02303','Eugenio','Zel Annie','Ballesteros','dsdsd','sdsd','dffdfdf','(873) 823-2832','active',0,NULL,1521776516),(247,'@#@#@%^%**','lekzi@mailiator.com','48ecb192ac0de681d8c54d6641eadef5f18cd322','ballest','sasha','eugenio','fggjhdffhhgfhghgfghg','fgfhgdggdsdghgfdsfdfgfg','ddgffhfhfgffggffgfg','(576) 754-6576','active',0,NULL,1522031719),(248,'*(*((*','anning@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','Eugenio','Anna','Ballesteros','dsdsd','Medisource','sdsdssd','(212) 121-2121','active',2,NULL,1522207740),(249,'##%$%$','zellyy@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','Eugenio','zellyyy','Ballesteros','ghghghhg','ghghg','hjhhjhjh','(788) 787-8787','active',2,NULL,1522208937),(250,'^&^&^&','roldem@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','Eugenio','roldem','Ballesteros','ffdfdf','dfd','dfddfdf','(223) 232-3232','active',2,NULL,1522214321),(251,'%&Hh','zing@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','Eugenio','zing','Ballesteros','dsds','Medisource','dsdsdsdsdsd','(121) 212-1212','active',2,NULL,1522219866),(252,'kezela','hh@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','Fonbuena','gio','Figuracion','dfdfdfd','Medisource','fdfdfdfdfdfd','(223) 232-3232','deactivate',2,NULL,1522222523),(253,'rol','ll@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','Eugenio','demy','Ballesteros','sdsds','dsdsd','sdsdsd','(223) 232-3232','deactivate',2,NULL,1522222586),(254,'anothe','another','7c222fb2927d828af22f592134e8932480637c0d','Eugenio','roldemzel','Ballesteros','hdsdsd','dsds sdsd sdsd sds sdsds','sddsds','(121) 212-1212','deactivate',2,NULL,1522222859),(256,'desa','dess@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','rombaoa','deshang','Ballesteros','hghggh','fgfgf','hghghgh','(787) 878-7877','active',2,NULL,1522225763),(257,'desss','desss@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','rombaoa','dess','Ballest','ffdfdf','fdfdf','fdfdfd','(211) 212-2121','deactivate',2,NULL,1522226381),(259,'geli','gelica','7c222fb2927d828af22f592134e8932480637c0d','antonito','gelica','sdsdsd','sdsdsds','dsdsd','dsdsdsdsdsd','(211) 212-1212','deactivate',2,NULL,1522631026),(261,'mariel','elle@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','barnachea','elle','ico','dsdsdsd','sdsdsds','dsdsdsds','(232) 323-1212','deactivate',2,NULL,1522631452),(262,'roldem','roldemzz@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','Eugenio','roldemz','Ballesteros','dsdsd','dsdsd','dsdsd','(234) 323-1212','active',2,NULL,1522631832),(263,'annie','lala@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','Eugenio','ellaa','Ballesteros','sdsds','dsds sdsd sdsd sds sdsds','asasa','(121) 212-1212','active',2,NULL,1522632166),(264,'desh','desiree@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','\'rombaoa','\'dess','\'ballesteros','hdsdsd','sdsd','ssdsd','(232) 321-2212','active',2,NULL,1522632752),(265,'^^%^%','lend@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','Eugenio','lena','Ballesteros','dsdsdsds','dsdsd','dsdsdsdsdsdsd','(232) 312-1212','active',2,NULL,1522647503),(267,'kjuendfd','edfmdfhdjhu@hajhjd.com','7c222fb2927d828af22f592134e8932480637c0d','asdwfwfdefew','dagsdge','dwfdsgweg','fefefefe','efefefefef','afafadfa','(2415) 235-4363','active',0,NULL,1522741287),(268,'dsdsdfgfgf','eugenio@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','eugenio','Anna','ddsd','dsdsdsd','sdsdsd','sasas','(5455) 554-4554','active',0,NULL,1522744139),(269,'dsdsdsdsdsd','another@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','sdsd','dsd','dsds','dsds','dsds','fdfdff','(3232) 323-2323','active',0,NULL,1522749451),(270,'sdsdsd','another11@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','balgoa','laurence','sdsds','sdsds','dsds','dsdsdsd','(2121) 212-1221','active',2,NULL,1522823421),(273,'sasaasasasasasassasasasasasasasasasasasasasa','new@mailinator.com','fe19715cc88195b5de7c40894be609183bac9a7d','sample','another','sample','sample','sample','dsdsdsd','(1121) 223-2212','active',2,NULL,1522824861),(274,'dsdsdsdgio','fonbuena','fe19715cc88195b5de7c40894be609183bac9a7d','Fonbuena','gio','Figuracion','sdsdsdsd','dsdsd','dsdsd','(1221) 212-1212','active',2,NULL,1522825124),(275,'deshang','deshh@mailinator','fe19715cc88195b5de7c40894be609183bac9a7d','rombaoa','deshang','Ballest','fddfdfdfdf','fdfdfdf','dfdfdf','(3232) 323-2323','active',2,NULL,1522911040),(276,'kzelannie','annalin@mailinator.com','fe19715cc88195b5de7c40894be609183bac9a7d','Eugenio','kezel ann','Ballesteros','dsdssdsdsdsd','dsdsdsdsds','dsdsdsdsdsd','09787878787','active',0,NULL,1523582696),(277,'zoeyyy','zoey@mailinator.com','fe19715cc88195b5de7c40894be609183bac9a7d','fdfddfd','Zoey','fdfdf','fddfdf','dfdfdf','sdsdsdsdsfdfffdfdf','09766767677','active',0,NULL,1523582782),(278,'kellyannn','annnie@mailinator.com','fe19715cc88195b5de7c40894be609183bac9a7d','fdfdf','kelly','fdfdf','dfdf','fdfdf','fdfd','09876543234','active',2,NULL,1523601488),(279,'dsdsdsdsd','dsdssds@mailinator.com','fe19715cc88195b5de7c40894be609183bac9a7d','dsds','dsdsd','s','dsds','dsd','dsdsd','09087878787','active',2,NULL,1523601556),(283,'harrisbmariano','harrisbmariano@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','Mariano','Harris','','','','','12341234123','active',0,NULL,1524644783),(285,'qwerty123','qwerty123@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','Mariano','Harris','','','','','09998765432','active',0,NULL,1524021652),(286,'poopopo','poopopo@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','poopo','poopo','','','','','09998765432','active',0,NULL,1524030211),(287,'lhoreng','lhoreng123@mailinator.com','f7c3bc1d808e04732adf679965ccc34ca7ae3441','Manzano','Lorraine','','Cabaruan','Medisource','Software Application Tester','09123465978','active',0,NULL,1524108025),(290,'johnny','Jhonny@yahoo.com','a642a77abd7d4f51bf9226ceaf891fcbb5b299b8','Cenese11','karlo','N','lelemaan','medisource','User','09123456789','active',0,NULL,1537926367),(293,'mypartner','mypartner@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','Partner','My','123123','qqq','www','eee','09998765432','active',2,NULL,1524191771),(294,'qwertyuiop','qwerty1233@mailinator.com','b0399d2029f64d445bd131ffaa399a42d2f8e7dc','qwertyuiop','qwertyuiop','','','','','43232546646','active',2,NULL,1524125137),(295,'wweewq','ertewqwert@mailinator.com','b0399d2029f64d445bd131ffaa399a42d2f8e7dc','trtfghdhh','ewrturtytr','','','','','09998765432','active',2,NULL,1524125187),(296,'angpartnerko','angpartnerko@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','Mariano','Harris','','','','','09998765432','active',2,NULL,1524193478),(297,'angpartnerko2','angpartnerko2@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','Magno','Ryan','','','','','09998765432','active',2,NULL,1524193715),(299,'fghgfgdfgdfgfg','dfgdfgdfg@mailinator.com','01b307acba4f54f55aafc33bb06bbbf6ca803e9a','dfgfdg','fgdfgfdg','dfgdfg','dfgdfgdfgfd','dfgdfg','fgdf','3442 534 5434','active',0,NULL,1524208567),(300,'Arika1108','arielbermudez0901@gmail.com','64c80a541ae032b7b3acbd555f2043950454bceb','Bermudez','Ariel','Ramos','Anis','Medisource','Intern','09105129446','deactivate',2,NULL,1524208050),(301,'mypartner66','mypartner66@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','mypartner66','mypartner66','mypartner66','mypartner66','mypartner66','mypartner66','11223344556','deactivate',2,NULL,1524645133),(302,'username123','fdgdf@akjsgdas.com','a642a77abd7d4f51bf9226ceaf891fcbb5b299b8','sdgfhgfjg               rtewrwerw                    sfsfsd','sdgfhgfjg               rtewrwerw                    sfsfsd','','','','','36463523412','active',0,NULL,1525674832),(303,'user111','user111@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','user111','user111','','','','','09998765645','active',2,NULL,1525420754),(304,'sampleuser123','sampleuser@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','sampleuser','sampleuser','sampleuser','sampleuser','sampleuser','sampleuser','09998765432','active',0,NULL,1525770327),(305,'sampleuser1234','sampleuse@yahoo.com','a642a77abd7d4f51bf9226ceaf891fcbb5b299b8','qqqqqqqq','qqqqqqqq','qqqqqqqq','qqqqqqqq','qqqqqqqq','sampleuser123','09123456789','active',0,NULL,1525846405),(306,'sampleuser1235','sampleuser123@gmail.com','a642a77abd7d4f51bf9226ceaf891fcbb5b299b8','sampleuser123','sampleuser123','sampleuser123','sampleuser123skldjf','sampleuser123','sampleuser123','09123456789','active',0,NULL,1525846517),(307,'harrisbmariano123','maznano@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','dfsdf','dfsdfsd','dfsdfsd','fsdfsdf','sdfsdfsdf','dfgdfgdfgdfgdf','54234234234','active',0,NULL,1525943478),(309,'lhoreng123','lhoreng@mailinator.com','7c222fb2927d828af22f592134e8932480637c0d','Manznao','Lorraine','','','','Tester','09140454545','active',0,NULL,1537863461);
/*!40000 ALTER TABLE `tblusers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblvolunteeractivities`
--

DROP TABLE IF EXISTS `tblvolunteeractivities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblvolunteeractivities` (
  `vActivityID` int(11) NOT NULL AUTO_INCREMENT,
  `volunteerID` int(11) NOT NULL,
  `activityID` int(11) NOT NULL,
  `custom` varchar(255) DEFAULT NULL,
  `customDesc` varchar(255) DEFAULT NULL,
  `dateAdded` int(11) DEFAULT NULL,
  PRIMARY KEY (`vActivityID`)
) ENGINE=InnoDB AUTO_INCREMENT=125 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblvolunteeractivities`
--

LOCK TABLES `tblvolunteeractivities` WRITE;
/*!40000 ALTER TABLE `tblvolunteeractivities` DISABLE KEYS */;
INSERT INTO `tblvolunteeractivities` VALUES (48,32,7,'','',1455502342),(49,33,10,'','',1455502728),(50,34,10,'','',1455503011),(51,35,6,'','',1455503249),(52,36,6,'','',1455503436),(53,37,0,'rgdfg','dgfdgd',1455503679),(54,38,0,'vfdgdf','dfgfd',1455504795),(55,39,6,'','',1455504996),(56,40,7,'','',1455505196),(57,41,10,'','',1455505447),(60,44,17,'','',1455591718),(61,45,17,'','',1455594432),(62,46,6,'','',1455594918),(63,46,7,'','',1455594918),(64,46,9,'','',1455594918),(65,46,12,'','',1455594918),(66,46,10,'','',1455594918),(67,46,11,'','',1455594918),(68,46,15,'','',1455594918),(69,46,16,'','',1455594918),(70,46,17,'','',1455594918),(71,46,18,'','',1455594918),(72,47,6,'','',1455614570),(73,47,18,'','',1455614570),(74,48,11,'','',1455674781),(75,48,15,'','',1455674781),(76,49,16,'','',1455674838),(77,49,18,'','',1455674838),(78,50,0,'janine suggest','lorem ipsum',1455695029),(79,51,6,'','',1455757442),(80,51,7,'','',1455757442),(81,51,9,'','',1455757442),(82,51,12,'','',1455757442),(83,51,10,'','',1455757442),(84,51,11,'','',1455757442),(85,51,15,'','',1455757442),(86,51,16,'','',1455757442),(87,51,17,'','',1455757442),(88,51,18,'','',1455757442),(89,52,7,'','',1455757553),(90,52,10,'','',1455757553),(91,53,7,'','',1455757647),(92,53,10,'','',1455757647),(93,53,15,'','',1455757647),(97,57,16,'','',1455872934),(100,60,0,'suggest 18','dfsdg',1456283685),(101,61,0,'my suggest','my suggestion',1456974134),(102,61,18,'','',1526020445),(103,61,6,'','',1526020445),(104,61,10,'','',1526020445),(105,61,11,'','',1526020445),(106,61,16,'','',1526020445),(107,61,25,'','',1526020445),(108,62,18,'','',1526022004),(109,62,6,'','',1526022004),(110,62,10,'','',1526022004),(111,62,11,'','',1526022004),(112,62,16,'','',1526022004),(113,62,25,'','',1526022004),(114,63,18,'','',1539235878),(115,63,9,'','',1539235878),(116,63,12,'','',1539235878),(117,63,22,'','',1539235878),(118,63,25,'','',1539235878),(119,64,18,'','',1539236108),(120,64,9,'','',1539236108),(121,64,10,'','',1539236108),(122,64,15,'','',1539236108),(123,64,17,'','',1539236108),(124,64,25,'','',1539236108);
/*!40000 ALTER TABLE `tblvolunteeractivities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblvolunteers`
--

DROP TABLE IF EXISTS `tblvolunteers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblvolunteers` (
  `volunteerID` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(5) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `mname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `extname` varchar(255) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `activationKey` varchar(255) DEFAULT NULL,
  `dateAdded` int(11) NOT NULL,
  `forgotToken` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`volunteerID`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblvolunteers`
--

LOCK TABLES `tblvolunteers` WRITE;
/*!40000 ALTER TABLE `tblvolunteers` DISABLE KEYS */;
INSERT INTO `tblvolunteers` VALUES (33,'Mrs.','sadsa','safsd','fsdfsd','fsdfs','dfdggfd','30e1fe53111f7e583c382596a32885fd27283970','dfsgfd','rf@mailinator.com','',0,'a6f9e4156e76094c143637d344307f1e',1455502728,NULL),(52,'Ms.','df','df','ffffg','','dsfdsgsdg','7c4a8d09ca3762af61e59520943dc26494f8941b','fffffffg','ashn@ailinator.com','(111) 111-1111',0,'ff002161bb22722fdfe7d6e4c32e153a',1455757553,NULL),(61,'Mr.','Harris','Bayucan','Mariano','JR','harrisbmariano','7c222fb2927d828af22f592134e8932480637c0d','Binalonan','harris@mailinator.com','(098) 987-6775',0,'21d57ce448ec01ecf3e908bad5389807',1526020445,NULL),(62,'Mr.','Harris','Bayucan','Mariano','JR','harrisbmariano2','7c222fb2927d828af22f592134e8932480637c0d','Binalonan','harris@mailinator.com','(075) 568-1126',0,'7cad7ce3583b15cad6df56737124bab2',1526022004,NULL),(63,'Mr.','Raymark','Chan','Bornales','','kramyl','b46f08eba0b982031ee731c07eab63c7edf379b8','Jenkins Street','kramyl333@gmail.com','(926) 657-8978',0,'a3fcd8d0e3da8df810afd235a8249ecf',1539235878,NULL),(64,'Ms.','phil','qwe','qweq','weqwe','qweqwe','f4542db9ba30f7958ae42c113dd87ad21fb2eddb','qweqwe','aqua.walker26@gmail.com','(123) 123-1231',0,'c1e86d855601e3aceb71d6ee349a0226',1539236108,NULL);
/*!40000 ALTER TABLE `tblvolunteers` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-03 13:20:11
