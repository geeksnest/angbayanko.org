/*Sparklines */

/* Sparkline plugin Section starts */















$("#todayspark1").sparkline([8,11,9,12,10,9,13,11,12,11,10,12,11,], {
    type: 'bar',
    height: '30',
    barWidth: 5,
    barColor: '#f04040'});

$("#todayspark2").sparkline([8,11,9,12,10,9,13,11,12,11,10,12,11,], {
    type: 'bar',
    height: '30',
    barWidth: 5,
    barColor: '#1eafed'});

$("#todayspark3").sparkline([8,11,9,12,10,9,13,11,12,11,10,12,11,], {
    type: 'bar',
    height: '30',
    barWidth: 5,
    barColor: '#fb7d45'});

$("#todayspark4").sparkline([8,11,9,12,10,9,13,11,12,11,10,12,11,], {
    type: 'bar',
    height: '30',
    barWidth: 5,
    barColor: '#0ec216'});

$("#todayspark5").sparkline([3,6,3,9,9,5,4,6,5,6,4,9,7,9,7,4,6,8,9,5,6,7,8,6,7,2,4,6,3,7,8,9,7,5,8,9,5,7,8,8,5 ], {
    type: 'line',
    width: '250',
    height: '30',
    lineColor: '#0ca5e7',
    fillColor: '#e5f3f9'});


/* Sparkline plugin section ends */

